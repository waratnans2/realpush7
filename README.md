[TOC]
# Project RealPush7
แยกขาดกับ StockRadar เป็นเสมอ Library หลังบ้านเท่านั้น แล้ว StockRadar มีการหยิบ lib นี้ไปใช้
### concept

> Maintainable
> 
> Stable
> 
> Very fast

## การใช้งาน project

1. เปิดด้วย `RealPush7.xcworkspace` แทน xcodeproj
2. แก้ Dependency ด้วย [cocoapods](http://cocoapods.org/) ที่ Podfile, อย่าไปยุ่งกับ .lock


## Fast
### Fast Number
API ใน NSNumber+formatter

1. Format , `price`, `volume`
2. Validate `price`, `volume` ใน TextField
ตัวอย่าง API

```
#!objective-c
-(NSString*) priceFormatStyle;
-(NSString*) changeFormatStyle;
+(BOOL) validateVolumeTextField: (UITextField*) textField
          charactersChangeRange: (NSRange) range
              replacementString: (NSString*) string ;
+(BOOL) validatePriceTextField: (UITextField*) textField
         charactersChangeRange: (NSRange) range
             replacementString: (NSString*) string ;

```
## Config
### config file

ไฟล์ชื่อ `Config.h/.m` เนื่องจากถ้าใส่ config plist จะสามารถ access ได้ง่ายเมื่อใช้ Tools ข้างนอก เพื่อความปลอดภัยจึงได้ใส่ config ไฟล์ไว้ใน sourcecode เลย เพราะเมื่อ compile เสร็จแล้วก็จะถูกซ่อนไปกับ project เลยวิธีการแก้ไขก็ไม่ได้ยากมากเกินไป และวิธีใช้ยิ่งง่ายเข้าไปใหญ่

**ตัวอย่าง config**

```
#!objective-c
+(NSString *)zmicoProductionHost {
    return @"iospush.ktzmico.com";
}
+(NSString *)zmicoTestHost {
    return @"58.97.9.108";
}
+(Broker)brokerType {
    return BrokerKimeng;
}

```

**ตัวอย่างเมื่อใช้งาน**

```
#!objective-C

//getter of stockUser
-(StockUser *)stockUser {
    if (_stockUser) {
        return _stockUser;
    }
    switch ([Config brokerType]) {
        case BrokerKimeng:
            _stockUser = [[KEStockUser alloc] init];
            break;
        case BrokerZmico:
            _stockUser = [[ZMStockUser alloc] init];
            break;
        default:
            break;
    }
    return _stockUser;
}

```
## Categories
ตัวอย่าง categories 

- NSData+utf8String

```
#!objective-c
-(void) kimengLoginDone:(NSData *)data {
    NSString* xmlString = [data utf8String];
```
- NSNumber+formatter

```
#!objective-c
[@10 priceFormatStyle]; //10.00
[@200000 volumeFormatStyle]; //200,000
...

//allow only price format or volume format
-(BOOL)textField:(UITextField *)textField 
       shouldChangeCharactersInRange:(NSRange)range 
                   replacementString:(NSString *)string {
                   
    if (textField == self.priceTextField) {
        return [NSNumber validatePriceTextField:textField 
                          charactersChangeRange:range 
                              replacementString:string];
    } 
    
```



## Warning ใน project
1. if (NSUInteger)object  & 0x1 , ของ JSONKit เป็น Warning ของ ObjC คิดว่า JSONKit คงเข้าใจว่าทำอะไรอยู่แต่ Compiler ไม่เข้าใจ
2. JSONKit, จะมีการใช้ `object->isa` เยอะ, แต่ว่า Xcode แนะนำเป็น `object_getClass()` เพราะว่า `object_getClass()` น่าจะปลอดภัยกว่า แต่ว่าจริงๆ แล้วช้ากว่าหลายเท่าเลยทีเดียว ทีม JSONKit จึงเลือกที่จะใช้ `object->isa` มากกว่าแต่ใน `RealPush7` จะแก้เป็น `object_getClass()` เพื่อตัด warning เหล่านี้ออกเพื่อที่จะไม่ให้มันรกหูรกตาเพราะว่าเราไม่ได้ต้องการความเร็วสูงเวอร์ๆ ขนาดนั้น ,เราต้องการ code ที่ไม่ต้องมี warning เยอะ เพราะว่าเวลามี Warning เพิ่มขึ้นมาอีก 1 เราจะได้ตามไปจัดการได้ง่ายกว่า
3. `performSelector` จะมี warning เพราะถ้าเกิดเราใช้อย่างไม่ระวังอาจจะเกิดปัญหาได้ ปิดได้ด้วยใส่ `#pragma clang diagnostic ignored "-Warc-performSelector-leaks"` เข้าไปที่ file `.m`

## อ้างอิง
* [cocoapods.org](http://cocoapods.org)  
* ตัวแก้ไข Readme.md -> [Mou](http://mouapp.com)

## Change note

### 28/04/2014 
* เพิ่ม marketConfiguration ใน Config.h
* เปลี่ยน initial ของ ZmicoStockModel
* เปลี่ยน load Configuration (ใน `AppDelegate`)
* เพิ่ม View ในการแสดง Market Detail

### 29/04/2014
* เพิ่ม `libxml2` เข้ามาใน framework
* เพิ่มไฟล์ `ZMICO_SERVICE` เข้ามาใน project
* เพิ่ม `/usr/include/libxml2` ใน header search path
* เพิ่ม `-lxml2` ใน  Other Linker Flags property (Project-> Build Settings)
* เพิ่ม `-I/usr/include/libxml2` ใน  Other C Flags
* ใช้ WSDL2ObjC ที่สร้างขึ้นเอง
* 
### 07/05/2014
* เพิ่ม Pull Market จาก Zmico
