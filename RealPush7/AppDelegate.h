//
//  AppDelegate.h
//  RealPush7
//
//  Created by Sutean Rutjanalard on 5/2/2557 BE.
//  Copyright (c) 2557 Sutean Rutjanalard. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StockUser.h"
#import "ITStockModel.h"
#import "ITUITheme.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) StockUser* stockUser;
@property (nonatomic, strong) ITStockModel *stockModel;
@property (nonatomic, strong) ITUITheme *theme;


@end
