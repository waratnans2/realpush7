//
//  AppDelegate.m
//  RealPush7
//
//  Created by Sutean Rutjanalard on 5/2/2557 BE.
//  Copyright (c) 2557 Sutean Rutjanalard. All rights reserved.
//

#import "AppDelegate.h"
#import "Config.h"
#import "KEStockUser.h"
#import "ZMStockUser.h"
#import "ZMStockModel.h"
#import "KimengTheme.h"
#import "ZMUITheme.h"
@implementation AppDelegate


-(StockUser *)stockUser {
    if (_stockUser) {
        return _stockUser;
    }
    switch ([Config brokerType]) {
        case BrokerKimeng:
            _stockUser = [[KEStockUser alloc] init];
            break;
        case BrokerZmico:
            _stockUser = [[ZMStockUser alloc] init];
            break;
        default:
            break;
    }
    return _stockUser;
}
-(ITStockModel *)stockModel {
    if (_stockModel) {
        return _stockModel;
    }
    switch ([Config brokerType]) {
        case BrokerKimeng:
            _stockModel = nil;
            break;
        case BrokerZmico:
            _stockModel = [[ZMStockModel alloc] initWithHost:self.stockUser.streammingHost
                                                        port:self.stockUser.streammingPort
                                      marketConfigurationURL:[Config marketConfiguration]];
            break;
        default:
            break;
    }
    return _stockModel;
}
-(ITUITheme *)theme {
    if (_theme) {
        return _theme;
    }
    switch ([Config brokerType]) {
        case BrokerKimeng:
            self.theme = [[KimengTheme alloc] init];
            break;
            
        default:
            self.theme = [[ZMUITheme alloc] init];
            break;
    }
    return _theme;
}
-(void)loadConfigDone:(StockUser*) user {
    NSLog(@"Load config done %@", user);
}
-(void) loadConfigFail:(NSString*) message {
    NSLog(@"Load config fail =%@", message);
}
#pragma mark - application life cycle
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    [self.stockUser loadConfigurationDone:^(StockUser* stockUser) { NSLog(@"Load config done.");}
                                     fail:^(NSString* error) {
                                         NSLog(@"Load config fail.");
                                     } ];
    [self.stockModel startConnectWithUsername:self.stockUser.username password:self.stockUser.password];
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
