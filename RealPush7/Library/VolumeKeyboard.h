//
//  VolumeKeyboard.h
//  RealPush7
//
//  Created by Sutean Rutjanalard on 7/4/2557 BE.
//  Copyright (c) 2557 Sutean Rutjanalard. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PriceKeyboard.h"


@interface VolumeKeyboard : UIView<UIInputViewAudioFeedback>


@property (strong, nonatomic) id<UITextInput> textView;
@property (nonatomic, weak) UITextField *textField;
@property (nonatomic, strong) IBOutletCollection(UIButton) NSArray* numberButtons;
@property (nonatomic, weak) id<KeyboardDelegate> delegate;
@property (nonatomic, strong) NSString* doneButtonTitle;

- (instancetype)initWithDelegate:(id<KeyboardDelegate>) delegate;

@end
