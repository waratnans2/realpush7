//
//  VolumeKeyboard.m
//  RealPush7
//
//  Created by Sutean Rutjanalard on 7/4/2557 BE.
//  Copyright (c) 2557 Sutean Rutjanalard. All rights reserved.
//

#import "PriceKeyboard.h"

@interface PriceKeyboard()
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;

@end


@implementation PriceKeyboard

- (instancetype)initWithDelegate:(id<KeyboardDelegate>) delegate
{
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    CGRect frame;
    
    if(UIDeviceOrientationIsLandscape(orientation))
        frame = CGRectMake(0, 0, 480, 162);
    else
        frame = CGRectMake(0, 0, 320, 216);
    
    self = [super initWithFrame:frame];
    if (self) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PriceKeyboard" owner:self options:nil];
        [[nib objectAtIndex:0] setFrame:frame];
        self = [nib objectAtIndex:0];
        [self setupHighlightKeyboard];
        [self.deleteButton addGestureRecognizer:[[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(deleteAll:)]];
        self.delegate = delegate;

    }
    return self;
}
-(void)setDoneButtonTitle:(NSString *)doneButtonTitle {
    [self.doneButton setTitle:doneButtonTitle forState:UIControlStateNormal];
    _doneButtonTitle = doneButtonTitle;
}

+ (UIImage *) imageFromColor:(UIColor *)color {
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}


-(void) setupHighlightKeyboard {
    for (UIButton* button in self.numberButtons) {
        [button setBackgroundImage:[PriceKeyboard imageFromColor:[UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:1]] forState:UIControlStateHighlighted];
    }
}
-(void)setTextView:(id<UITextInput>)textView {
    [(UITextField *)textView setInputView:self];
    _textView = textView;
}
- (void)setTextField:(UITextField *)textField {
    _textField = textField;
    _textField.inputView = self;
    self.textView = _textField;
}
- (NSRange) selectedRangeInTextView:(UITextField*)textView
{
    UITextPosition* beginning = textView.beginningOfDocument;
    
    UITextRange* selectedRange = textView.selectedTextRange;
    UITextPosition* selectionStart = selectedRange.start;
    UITextPosition* selectionEnd = selectedRange.end;
    
    const NSInteger location = [textView offsetFromPosition:beginning toPosition:selectionStart];
    const NSInteger length = [textView offsetFromPosition:selectionStart toPosition:selectionEnd];
    
    return NSMakeRange(location, length);
}
-(void) insertText:(NSString*) string{
    if([self.textField.delegate textField:self.textField
            shouldChangeCharactersInRange:[self selectedRangeInTextView:self.textField]
                        replacementString:string] ) {
        [self.textView insertText:string];
        
    }
}

-(void) insertDot {
    NSRange dot = [self.textField.text rangeOfString:@"."];
    if (dot.location == NSNotFound && _textField.text.length == 0) {
        [self.textView insertText:@"0."];
    } else if (dot.location == NSNotFound) {
        [self insertText:@"."];
    }
}
- (IBAction)numberPressed:(UIButton *)button {
    [[UIDevice currentDevice] playInputClick];
    NSString *keyText = button.titleLabel.text;
    if ([@"." isEqualToString:keyText]) {
        [self insertDot];
    } else {
        [self insertText:keyText];
    }

}

- (IBAction)hideKeyboard:(id)sender {
    //    NSLog(@"hide keyboard pressed");
    [[UIDevice currentDevice] playInputClick];
    [self.delegate keyboard:self hideFromTextField:self.textField];
}
- (BOOL)enableInputClicksWhenVisible {
    return YES;
}
- (IBAction)donePressed:(UIButton *)sender {
    //    NSLog(@"Done pressed");
    [[UIDevice currentDevice] playInputClick];
    [self.delegate keyboard:self donePressedFromeTextField:self.textField];
}
- (IBAction)backPressed:(UIButton *)sender {
    [[UIDevice currentDevice] playInputClick];
    [self.delegate keyboard:self backPressedFromeTextField:self.textField];

}
- (IBAction)deletePressed:(UIButton *)sender {
    [[UIDevice currentDevice] playInputClick];
    if ([@"0." isEqualToString:self.textField.text]) {
        self.textField.text = @"";
        
        return;
    } else {
        if ([self.textField.delegate textField:self.textField
                 shouldChangeCharactersInRange:[self selectedRangeInTextView:self.textField]
                             replacementString:@""]) {
            [self.textView deleteBackward];
        }
    }

}
-(void) deleteAll:(UIButton*) button {
    [[UIDevice currentDevice] playInputClick];
    self.textField.text = @"";
}
@end
