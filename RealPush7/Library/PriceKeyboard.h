//
//  VolumeKeyboard.h
//  RealPush7
//
//  Created by Sutean Rutjanalard on 7/4/2557 BE.
//  Copyright (c) 2557 Sutean Rutjanalard. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol KeyboardDelegate <NSObject>

-(void) keyboard:(id) keyboard hideFromTextField:(UITextField*) textField;
-(void) keyboard:(id) keyboard donePressedFromeTextField:(UITextField*) textField;
-(void) keyboard:(id) keyboard backPressedFromeTextField:(UITextField*) textField;

@end

@interface PriceKeyboard: UIView<UIInputViewAudioFeedback>
@property (strong, nonatomic) id<UITextInput> textView;
@property (nonatomic, weak) UITextField *textField;
@property (nonatomic, strong) IBOutletCollection(UIButton) NSArray* numberButtons;
@property (nonatomic, strong) NSString* doneButtonTitle;

@property (nonatomic, weak) id<KeyboardDelegate> delegate;
- (instancetype)initWithDelegate:(id<KeyboardDelegate>) delegate;
@end
