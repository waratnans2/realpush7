//
//  VolumeKeyboard.m
//  RealPush7
//
//  Created by Sutean Rutjanalard on 7/4/2557 BE.
//  Copyright (c) 2557 Sutean Rutjanalard. All rights reserved.
//

#import "VolumeKeyboard.h"

@interface VolumeKeyboard()
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;

@end

@implementation VolumeKeyboard

- (instancetype)initWithDelegate:(id<KeyboardDelegate>) delegate
{
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    CGRect frame;
    
    if(UIDeviceOrientationIsLandscape(orientation))
        frame = CGRectMake(0, 0, 480, 162);
    else
        frame = CGRectMake(0, 0, 320, 216);
    
    self = [super initWithFrame:frame];
    if (self) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"VolumeKeyboard" owner:self options:nil];
        [[nib objectAtIndex:0] setFrame:frame];
        self = [nib objectAtIndex:0];
        [self setupHighlightKeyboard];
        self.delegate = delegate;
        [self.deleteButton addGestureRecognizer:[[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(deleteAll:)]];
    }
    return self;
}
- (BOOL)enableInputClicksWhenVisible {
    return YES;
}


-(void)setDoneButtonTitle:(NSString *)doneButtonTitle {
    [self.doneButton setTitle:doneButtonTitle forState:UIControlStateNormal];
    _doneButtonTitle = doneButtonTitle;
}

+ (UIImage *) imageFromColor:(UIColor *)color {
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}


-(void) setupHighlightKeyboard {
    for (UIButton* button in self.numberButtons) {
        [button setBackgroundImage:[VolumeKeyboard imageFromColor:[UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:1]] forState:UIControlStateHighlighted];
    }
}
-(void)setTextView:(id<UITextInput>)textView {
    [(UITextField *)textView setInputView:self];
    _textView = textView;
}
- (void)setTextField:(UITextField *)textField {
    _textField = textField;
    _textField.inputView = self;
    self.textView = _textField;
}

- (NSRange) selectedRangeInTextView:(UITextField*)textView
{
    UITextPosition* beginning = textView.beginningOfDocument;
    
    UITextRange* selectedRange = textView.selectedTextRange;
    UITextPosition* selectionStart = selectedRange.start;
    UITextPosition* selectionEnd = selectedRange.end;
    
    const NSInteger location = [textView offsetFromPosition:beginning toPosition:selectionStart];
    const NSInteger length = [textView offsetFromPosition:selectionStart toPosition:selectionEnd];
    
    return NSMakeRange(location, length);
}

- (IBAction)numberPressed:(UIButton *)button {
    [[UIDevice currentDevice] playInputClick];
    NSString *keyText = button.titleLabel.text;
    if([self.textField.delegate textField:self.textField
            shouldChangeCharactersInRange:[self selectedRangeInTextView:self.textField]
                        replacementString:keyText] ) {
        [self.textView insertText:keyText];

    }
}

- (IBAction)hideKeyboard:(id)sender {
//    NSLog(@"hide keyboard pressed");
    [[UIDevice currentDevice] playInputClick];

    [self.delegate keyboard:self hideFromTextField:self.textField];
}
- (IBAction)donePressed:(UIButton *)sender {
//    NSLog(@"Done pressed");
    [[UIDevice currentDevice] playInputClick];

    [self.delegate keyboard:self donePressedFromeTextField:self.textField];
}
- (IBAction)backPressed:(UIButton *)sender {
    [[UIDevice currentDevice] playInputClick];

    [self.delegate keyboard:self backPressedFromeTextField:self.textField];
}
- (IBAction)deletePressed:(UIButton *)sender {
    [[UIDevice currentDevice] playInputClick];
    
    if ([self.textField.delegate textField:self.textField
             shouldChangeCharactersInRange:[self selectedRangeInTextView:self.textField]
                         replacementString:@""]) {
        [self.textView deleteBackward];
    }
}


-(void) deleteAll:(UIButton*) button {
    [[UIDevice currentDevice] playInputClick];
    self.textField.text = @"";
}




@end
