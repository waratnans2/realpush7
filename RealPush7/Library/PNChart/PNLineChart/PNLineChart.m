//
//  PNLineChart.m
//  PNChartDemo
//
//  Created by kevin on 11/7/13.
//  Copyright (c) 2013年 kevinzhow. All rights reserved.
//

#import "PNLineChart.h"
#import "PNColor.h"
#import "PNChartLabel.h"
#import "PNLineChartData.h"
#import "PNLineChartDataItem.h"

#define xLblHeight 20
#define xLblWidth 30

//------------------------------------------------------------------------------------------------
// private interface declaration
//------------------------------------------------------------------------------------------------
@interface PNLineChart ()
{
    NSMutableArray * pointXforDrawLine;
    NSMutableArray * pointYforDrawLine;
    NSMutableArray * yAxisValues;
    UIView * touchLine;
    int chartStartTime;
}

@property (nonatomic) NSMutableArray *chartLineArray; // Array[CAShapeLayer]

@property (nonatomic) NSMutableArray *chartPath; //Array of line path, one for each line.

- (void)setDefaultValues;

@end


//------------------------------------------------------------------------------------------------
// public interface implementation
//------------------------------------------------------------------------------------------------
@implementation PNLineChart

#pragma mark initialization

- (id)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];

    if (self) {
        [self setDefaultValues];
    }

    return self;
}


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];

    if (self) {
        [self setDefaultValues];
    }

    return self;
}


#pragma mark instance methods

- (void)setYLabels:(NSArray *)yLabels
{
    
    CGFloat yStep = (_yValueMax - _yValueMin) / _yLabelNum;
    CGFloat yStepHeight = _chartCavanHeight / _yLabelNum;
    
    yAxisValues = [[NSMutableArray alloc]init];
    pointYforDrawLine = [[NSMutableArray alloc]init];
    
    NSInteger index = 0;
    NSInteger num = _yLabelNum + 1;

    while (num > 0) {
        
        NSString *yLabelFormat = self.yLabelFormat ? self.yLabelFormat : @"%.02f";
        
        float yAxis = _yValueMin +  (yStep * index);
        NSString * val = [NSString stringWithFormat:yLabelFormat,yAxis];
        [yAxisValues addObject:val];

        index += 1;
        num -= 1;
    }
    if(_compareMode){
        for (int i = 1; i<yAxisValues.count; i++) {
            
            float val = [yAxisValues[i] floatValue];
            NSLog(@"----------------y value %f",val);
            if(val>=0){
                
                if(fabs([yAxisValues[i-1] floatValue]) < fabs([yAxisValues[i] floatValue])){
                    yAxisValues[i-1] = @"0.00";
                }else{
                    yAxisValues[i] = @"0.00";
                }
                
                
                break;
            }
            
        }
    }
    
    for (int i = 0; i<yAxisValues.count; i++) {
        float pointY = (_chartCavanHeight - i * yStepHeight) +10 ;
        PNChartLabel *label = [[PNChartLabel alloc] initWithFrame:CGRectMake(0.0, pointY , _chartMargin, _yLabelHeight)];
        pointY +=   _yLabelHeight/2;
        
        [pointYforDrawLine addObject:[NSNumber numberWithFloat:pointY]];
        
        [label setTextAlignment:NSTextAlignmentCenter];
        
        if(_compareMode){
            label.text = [NSString stringWithFormat:@"%@%%",yAxisValues[i]];
        }else{
            label.text = yAxisValues[i];
        }
        
        label.font = _labelFont;
        label.textColor = _labelTextColor;
        [self addSubview:label];
    }
    
    
    
}

#pragma mark chartLinePointCreate
-(float)findPointForYValue:(CGFloat)yValue
{
    
    int secter = 0;
    int count = [yAxisValues count];
    if(yValue>= [yAxisValues[count-1] floatValue]){
        secter = count-1;
    }else{
        for (NSUInteger i=0; i<count; i++) {
            float compareValue = [yAxisValues[i] floatValue];
            if(yValue <= compareValue){
                secter = i;
                break;
            }
        }
    }
    
    float minYforSecter = 0.0;
    float maxYforSecter = 0.0;
    
    if((secter - 1) >= 0){
        maxYforSecter = [yAxisValues[secter] floatValue];
        minYforSecter = [yAxisValues[secter-1] floatValue];
    }else{
        maxYforSecter = [yAxisValues[secter] floatValue];
    }
    
    float divine = 0;
    if((maxYforSecter - minYforSecter) == 0){
        divine = 1;
    }else{
        divine = (maxYforSecter - minYforSecter);
    }
    
    float innerGrade = (yValue - minYforSecter) / divine;

    CGFloat yStepHeight = _chartCavanHeight / _yLabelNum;
    
    float pointY = 10 + (_chartCavanHeight - ((secter-1) * yStepHeight ) - innerGrade * yStepHeight ) + _yLabelHeight/2 ;
    
    NSLog(@"secter %d point Y %f for %f",secter,pointY ,yValue);
//    if(secter!=0)
//    NSLog(@"\nyValue:%f\nsecter:%d\nmax:%f\nmin:%f\ninner:%f\npoint:%f\n\n",yValue,secter,[yAxisValues[secter] floatValue],[yAxisValues[secter-1] floatValue],innerGrade,pointY);

//    CGPoint point = CGPointMake(4+_chartMargin +  (i * _xLabelWidth),
//                        10 + _chartCavanHeight - (innerGrade * _chartCavanHeight) + (_yLabelHeight / 2));
//   
//    
    
    return pointY;
}

- (void)setXLabels:(NSArray *)xLabels
{
    _xLabels = xLabels;
    NSString *labelText;
    
    pointXforDrawLine = [[NSMutableArray alloc]init];
    _pointXArray = [[NSMutableArray alloc]init];
    
    if (_showLabel) {
        
        int labelAddCount = 0;
        _xLabelWidth = _chartCavanWidth / [xLabels count];
        
        for (int index = 0; index < xLabels.count; index++) {
            labelAddCount += 1;
            
            float pointX = 4 + _chartMargin +  (index * _xLabelWidth);// - (xLblWidth / 2);

            [_pointXArray addObject:[NSNumber numberWithFloat:pointX]];
 
            if (labelAddCount == _xLabelSkip) {
                
                double pointY =  2 * _chartMargin + _chartCavanHeight - xLblHeight;
                PNChartLabel *label = [[PNChartLabel alloc] initWithFrame:CGRectMake(pointX- (xLblWidth / 2),pointY, xLblWidth, xLblHeight)];
                labelText = xLabels[index];
                label.font = _labelFont;
                [label setTextAlignment:NSTextAlignmentCenter];
                label.textColor = _labelTextColor;
                labelAddCount = 0;

                if(_showLabelTimeMode){
                    int time = [xLabels[index] intValue];
                    
                    if((time/100) >= chartStartTime && (time/100)!=14){
                        NSLog(@"%d divine %d",time,time/100);
                        chartStartTime = time/100 + 1;
                        [pointXforDrawLine addObject:[NSNumber numberWithFloat:pointX]];

                        NSRange hrRange = {0,2};
                        label.text = [labelText substringWithRange:hrRange];
                        [self addSubview:label];
                    }else{
                        label = nil;
                    }
                    
//                    NSRange minRange = {2,2};
//                    labelText = [NSString stringWithFormat:@"%@:%@",[xLabels[index] substringWithRange:hrRange],[xLabels[index] substringWithRange:minRange]];
                
                }else{
                    
                    [pointXforDrawLine addObject:[NSNumber numberWithFloat:pointX]];
                    label.text = labelText;
                    [self addSubview:label];
                }
            
            }
            
        }
//        _xLabelWidth = _chartCavanWidth / [xLabels count];
//        for (int index = 0; index < xLabels.count; index++) {
//            
//            float pointX = 4 + _chartMargin +  (index * _xLabelWidth);// - (xLblWidth / 2);
//            
//                [_pointXArray addObject:[NSNumber numberWithFloat:pointX]];
//            
//                int time = [xLabels[index] intValue];
//            
//                if(time%100==0){
//                    
//                labelText = [xLabels[index] substringToIndex:2];
//
//                float pointY =  2 * _chartMargin + _chartCavanHeight - xLblHeight;
//                
//            
//                PNChartLabel *label = [[PNChartLabel alloc] initWithFrame:CGRectMake(pointX- (xLblWidth / 2),pointY, xLblWidth, xLblHeight)];
//                label.font = _labelFont;
//                [label setTextAlignment:NSTextAlignmentCenter];
//                label.text = labelText;
//                
//                //                label.backgroundColor = [UIColor grayColor];
//            
//
//            
//            
//                    [pointXforDrawLine addObject:[NSNumber numberWithFloat:pointX]];
//                    [self addSubview:label];
//                }
//        }
        

    }
    else {
        _xLabelWidth = (self.frame.size.width) / [xLabels count];
    }
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self touchPoint:touches withEvent:event];
    [self touchKeyPoint:touches withEvent:event];
}


- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self touchPoint:touches withEvent:event];
    [self touchKeyPoint:touches withEvent:event];
}


- (void)touchPoint:(NSSet *)touches withEvent:(UIEvent *)event
{
    //Get the point user touched
    UITouch *touch = [touches anyObject];
    CGPoint touchPoint = [touch locationInView:self];
    

    for (int p = _pathPoints.count - 1; p >= 0; p--) {
        NSArray *linePointsArray = _pathPoints[p];
        int count = ([linePointsArray count] - 1);
        for (int i = 0; i < count; i += 1) {
            CGPoint p1 = [linePointsArray[i] CGPointValue];
            CGPoint p2 = [linePointsArray[i + 1] CGPointValue];

            // Closest distance from point to line
            float distance = fabsf(((p2.x - p1.x) * (touchPoint.y - p1.y)) - ((p1.x - touchPoint.x) * (p1.y - p2.y)));
            distance /= hypot(p2.x - p1.x, p1.y - p2.y);

            if (distance <= 5.0) {
                // Conform to delegate parameters, figure out what bezier path this CGPoint belongs to.
                for (UIBezierPath *path in _chartPath) {
                    BOOL pointContainsPath = CGPathContainsPoint(path.CGPath, NULL, p1, NO);

                    if (pointContainsPath) {


                        [_delegate userClickedOnLinePoint:touchPoint lineIndex:[_chartPath indexOfObject:path]];
                        return;
                    }
                }
            }
        }
    }
}


- (void)touchKeyPoint:(NSSet *)touches withEvent:(UIEvent *)event
{
    //Get the point user touched
    UITouch *touch = [touches anyObject];
    CGPoint touchPoint = [touch locationInView:self];

    for (int p = _pathPoints.count - 1; p >= 0; p--) {
        NSArray *linePointsArray = _pathPoints[p];
        int count = ([linePointsArray count] - 1);

        for (int i = 0; i < count; i += 1) {
            CGPoint p1 = [linePointsArray[i] CGPointValue];
            CGPoint p2 = [linePointsArray[i + 1] CGPointValue];

            float distanceToP1 = fabsf(hypot(touchPoint.x - p1.x, touchPoint.y - p1.y));
            float distanceToP2 = hypot(touchPoint.x - p2.x, touchPoint.y - p2.y);

            float distance = MIN(distanceToP1, distanceToP2);

            if (distance <= 10.0) {

                int pIndex = (distance == distanceToP2 ? i + 1 : i);
                
                if(_showLineWhenTouch)
                [self showTouchLine:[linePointsArray[pIndex] CGPointValue].x];
                [_delegate userClickedOnLineKeyPoint:touchPoint
                                           lineIndex:p
                                       andPointIndex:pIndex];
                return;
            }
        }
    }
}
-(void)showTouchLine:(CGFloat)pointx
{
    
    if(!touchLine){
        touchLine = [[UIView alloc]initWithFrame:CGRectMake(pointx, 0, 1, _chartCavanHeight)];
        touchLine.backgroundColor = PNFreshGreen;
        [self addSubview:touchLine];
    }
    
    touchLine.frame = CGRectMake(pointx-0.5, 0, 1, self.frame.size.height-_chartMargin/2);
    
}

- (void)strokeChart
{
    _chartPath = [[NSMutableArray alloc] init];

    //Draw each line
    for (NSUInteger lineIndex = 0; lineIndex < self.chartData.count; lineIndex++) {
        PNLineChartData *chartData = self.chartData[lineIndex];
        CAShapeLayer *chartLine = (CAShapeLayer *)self.chartLineArray[lineIndex];
        CGFloat yValue;
        CGFloat innerGrade;
        CGPoint point;

        UIGraphicsBeginImageContext(self.frame.size);
        UIBezierPath *progressline = [UIBezierPath bezierPath];
        [_chartPath addObject:progressline];

        if (!_showLabel) {
            _chartCavanHeight = self.frame.size.height - 2 * _yLabelHeight;
            _chartCavanWidth = self.frame.size.width;
            _chartMargin = 0.0;
            _xLabelWidth = (_chartCavanWidth / ([_xLabels count] - 1));
        }

        NSMutableArray *linePointsArray = [[NSMutableArray alloc] init];
        [progressline setLineWidth:3.0];
        [progressline setLineCapStyle:kCGLineCapRound];
        CGFloat dashes[] = {6, 2};
        [progressline setLineDash:dashes count:1 phase:1];
        [progressline setLineJoinStyle:kCGLineJoinRound];

        for (NSUInteger i = 0; i < chartData.itemCount; i++) {
            yValue = chartData.getData(i).y;

//            float divin = 0;
//            if((_yValueMax - _yValueMin) == 0){
//                divin = 1;
//            }else{
//                divin = (_yValueMax - _yValueMin);
//            }
//            
//            innerGrade = (yValue - _yValueMin) / divin;
  
            point = CGPointMake(4+_chartMargin +  (i * _xLabelWidth),
                                [self findPointForYValue:yValue]);

            if (i != 0) {
                [progressline addLineToPoint:point];
            }

            [progressline moveToPoint:point];
            [linePointsArray addObject:[NSValue valueWithCGPoint:point]];
        }

        [_pathPoints addObject:[linePointsArray copy]];

        // setup the color of the chart line
        if (chartData.color) {
            chartLine.strokeColor = [chartData.color CGColor];
        }
        else {
            chartLine.strokeColor = [PNGreen CGColor];
        }

        [progressline stroke];

        chartLine.path = progressline.CGPath;

        CABasicAnimation *pathAnimation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
        pathAnimation.duration = 1.0;
        pathAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        pathAnimation.fromValue = @0.0f;
        pathAnimation.toValue   = @1.0f;
        [chartLine addAnimation:pathAnimation forKey:@"strokeEndAnimation"];

        chartLine.strokeEnd = 1.0;
        
        UIGraphicsEndImageContext();
    }
}

-(CGFloat)priorValue
{
    
//    float innerGrade = (_priorValue - _yValueMin) / (_yValueMax - _yValueMin);
    
    return [self findPointForYValue:_priorValue];
   
}
- (void)setChartData:(NSArray *)data
{
    if (data != _chartData) {
        NSMutableArray *yLabelsArray = [NSMutableArray arrayWithCapacity:data.count];
        CGFloat yMax = 0.0f;
        CGFloat yMin = MAXFLOAT;
        CGFloat yValue;

        // remove all shape layers before adding new ones
        for (CALayer *layer in self.chartLineArray) {
            [layer removeFromSuperlayer];
        }
        
        self.chartLineArray = [NSMutableArray arrayWithCapacity:data.count];

        for (PNLineChartData *chartData in data) {
            // create as many chart line layers as there are data-lines
            CAShapeLayer *chartLine = [CAShapeLayer layer];
            chartLine.lineCap   = kCALineCapRound;
            chartLine.lineJoin  = kCALineJoinBevel;
            
//            chartLine.lineDashPattern = @[@2,@3];
//            chartLine.fillColor = [[UIColor redColor] CGColor];
            
            chartLine.lineWidth = _lineWidth;
            chartLine.strokeEnd = 0.0;
            
            [self.layer addSublayer:chartLine];
            [self.chartLineArray addObject:chartLine];

            for (NSUInteger i = 0; i < chartData.itemCount; i++) {
                yValue = chartData.getData(i).y;
                [yLabelsArray addObject:[NSString stringWithFormat:@"%2f", yValue]];
                yMax = fmaxf(yMax, yValue);
                yMin = fminf(yMin, yValue);
            }
        }

        // Min value for Y label
//        if (yMax < 5) {
//            yMax = 5.0f;
//        }
//
//        if (yMin < 0) {
//            yMin = 0.0f;
//        }

//        _yValueMin = yMin;
//        _yValueMax = yMax;
        
        if(_yValueMax == _yValueMin){
//            _yValueMin --;
            _yValueMax += 0.5;
        }
        
        _chartData = data;

        if (_showLabel) {
            [self setYLabels:yLabelsArray];
        }

        [self setNeedsDisplay];
    }
}

#define IOS7_OR_LATER [[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0

- (void)drawRect:(CGRect)rect
{
    if (self.isShowCoordinateAxis) {
        
        CGContextRef ctx = UIGraphicsGetCurrentContext();
        UIGraphicsPushContext(ctx);
        CGContextSetLineWidth(ctx, self.axisWidth);
        CGContextSetStrokeColorWithColor(ctx, [self.axisColor CGColor]);

        // draw coordinate axis
        CGContextMoveToPoint(ctx, _chartMargin  , 0);
        CGContextAddLineToPoint(ctx, _chartMargin , 2 * _chartMargin + _chartCavanHeight - xLblHeight);
        CGContextAddLineToPoint(ctx, CGRectGetWidth(rect) - _chartMargin/2  ,2 * _chartMargin + _chartCavanHeight - xLblHeight);
        CGContextStrokePath(ctx);
        
        CGFloat dashes[] = { 6, 2 };

        for (NSNumber * point in pointXforDrawLine) {
            
            float pointX = [point floatValue];
            
            CGContextMoveToPoint(ctx, pointX , 0);
            CGContextAddLineToPoint(ctx, pointX , 2 * _chartMargin + _chartCavanHeight - xLblHeight);
            CGContextSetLineDash(ctx, 0.0, dashes, 2);
            CGContextStrokePath(ctx);
        }
        
        for (NSNumber * point in pointYforDrawLine) {
            
            float pointY = [point floatValue];
            
            CGContextMoveToPoint(ctx, _chartMargin , pointY);
            CGContextAddLineToPoint(ctx, _chartMargin*1.5 + _chartCavanWidth , pointY);
            CGContextSetLineDash(ctx, 0.0, dashes, 2);
            CGContextStrokePath(ctx);
        }
        //draw prior
        if(_priorValue!=0 || _compareMode){
            CGContextSetStrokeColorWithColor(ctx,[PNYellow CGColor]);
            CGContextMoveToPoint(ctx, _chartMargin +1 , self.priorValue);
            CGContextAddLineToPoint(ctx, 1.5 * _chartMargin + _chartCavanWidth , self.priorValue);
            CGContextSetLineDash(ctx, 0.0, dashes, 2);
            CGContextSetLineWidth(ctx, self.axisWidth+0.5);
            CGContextStrokePath(ctx);
            
            if(_compareMode){
                
                PNChartLabel *label = [[PNChartLabel alloc] initWithFrame:CGRectMake(0.0, self.priorValue - _yLabelHeight/2 , _chartMargin, _yLabelHeight)];
                [label setTextAlignment:NSTextAlignmentCenter];
                label.text = @"0.00%";
                label.font = _labelFont;
                label.textColor = PNYellow;
                [self addSubview:label];
                
            }

        }
/*
        // draw y axis arrow
        CGContextMoveToPoint(ctx, _chartMargin + 6, 8);
        CGContextAddLineToPoint(ctx, _chartMargin + 10, 0);
        CGContextAddLineToPoint(ctx, _chartMargin + 14, 8);
        CGContextStrokePath(ctx);

        // draw x axis arrow
        CGContextMoveToPoint(ctx, CGRectGetWidth(rect) - _chartMargin - 8, _chartMargin + _chartCavanHeight - 4);
        CGContextAddLineToPoint(ctx, CGRectGetWidth(rect) - _chartMargin, _chartMargin + _chartCavanHeight);
        CGContextAddLineToPoint(ctx, CGRectGetWidth(rect) - _chartMargin - 8, _chartMargin + _chartCavanHeight + 4);
        CGContextStrokePath(ctx);
*/
        
        UIFont *font = [UIFont systemFontOfSize:11];
        // draw y unit
        if ([self.yUnit length]) {
            CGFloat height = [PNLineChart heightOfString:self.yUnit withWidth:30.f font:font];
            CGRect drawRect = CGRectMake(_chartMargin + 10 + 5, 0, 30.f, height);
            [self drawTextInContext:ctx text:self.yUnit inRect:drawRect font:font];
        }

        // draw x unit
        if ([self.xUnit length]) {
            CGFloat height = [PNLineChart heightOfString:self.xUnit withWidth:30.f font:font];
            CGRect drawRect = CGRectMake(CGRectGetWidth(rect) - _chartMargin + 5, _chartMargin + _chartCavanHeight - height/2, 25.f, height);
            [self drawTextInContext:ctx text:self.xUnit inRect:drawRect font:font];
        }
    
    }
    
    [super drawRect:rect];
}

#pragma mark private methods

- (void)setDefaultValues
{
    // Initialization code
    self.backgroundColor = [UIColor clearColor];
    self.clipsToBounds   = YES;
    self.chartLineArray  = [NSMutableArray new];
    _showLabel           = YES;
    _pathPoints = [[NSMutableArray alloc] init];
    self.userInteractionEnabled = YES;

    _yLabelNum = 5.0;
    _yLabelHeight = [[[[PNChartLabel alloc] init] font] pointSize];
    _labelFont = [UIFont systemFontOfSize:8.0f];
    _chartMargin = 40;
    _labelTextColor      = [UIColor grayColor];
    _chartCavanWidth = self.frame.size.width - _chartMargin *2;
    _chartCavanHeight = self.frame.size.height - _chartMargin *2;
    
    // Coordinate Axis Default Values
    _showCoordinateAxis = YES;
    _axisColor = [UIColor colorWithRed:0.4f green:0.4f blue:0.4f alpha:1.f];
    _axisWidth = 1.f;
    
    //label skip
    _xLabelSkip = 1;
    _lineWidth = 1.0;
    chartStartTime = 10;
    
}

#pragma mark - tools

+ (float)heightOfString:(NSString *)text withWidth:(float)width font:(UIFont *)font
{
    NSInteger ch;
    //设置字体
    CGSize size = CGSizeMake(width, MAXFLOAT);
    if ([text respondsToSelector:@selector(boundingRectWithSize:options:attributes:context:)])
    {
        NSDictionary * tdic = [NSDictionary dictionaryWithObjectsAndKeys:font, NSFontAttributeName,nil];
        size =[text boundingRectWithSize:size
                                 options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading
                              attributes:tdic
                                 context:nil].size;
    }
    else
    {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
        size = [text sizeWithFont:font constrainedToSize:size lineBreakMode:NSLineBreakByCharWrapping]; //ios7以上已经摒弃的这个方法
#pragma clang diagnostic pop
    }
    ch = size.height;
    
    return ch;
}

- (void)drawTextInContext:(CGContextRef )ctx text:(NSString *)text inRect:(CGRect)rect font:(UIFont *)font
{
    if (IOS7_OR_LATER) {
        NSMutableParagraphStyle *priceParagraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
        priceParagraphStyle.lineBreakMode = NSLineBreakByTruncatingTail;
        priceParagraphStyle.alignment = NSTextAlignmentLeft;
        
        [text drawInRect:rect
          withAttributes:@{NSParagraphStyleAttributeName:priceParagraphStyle, NSFontAttributeName:font}];
    } else {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
        [text drawInRect:rect
                            withFont:font
                       lineBreakMode:NSLineBreakByTruncatingTail
                           alignment:NSTextAlignmentLeft];
#pragma clang diagnostic pop
    }
}


@end
