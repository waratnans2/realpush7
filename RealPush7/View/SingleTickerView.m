//
//  SingleTickerView.m
//  RealPush7
//
//  Created by Sutean Rutjanalard on 7/4/2557 BE.
//  Copyright (c) 2557 Sutean Rutjanalard. All rights reserved.
//

#import "SingleTickerView.h"
#import "NSNumber+formatter.h"
#import "NSString+formatter.h"
#import "SingleStockTableViewCell.h"
#import "ITUITheme.h"
#import "UILabel+blink.h"
@interface SingleTickerView () <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) int currentIndex;
@property (nonatomic) int previousIndex;


@end
static int MAX_TICKER = 9;
@implementation SingleTickerView
-(void)setTheme:(ITUITheme *)theme {
    if (theme != _theme) {
        _theme = theme;
    }
}
//[ [time, side, price, volume,change],[time, side, price, volume,change],...  ]
-(void) setTickersArray:(NSArray*) tickersArray prior:(NSNumber*) prior {
    [self resetAllTickerCell];
    for (NSArray* ticker in tickersArray) {
        NSString* time   = ticker[0];
        NSString* side   = ticker[1];
        NSNumber* price  = ticker[2];
        NSNumber* volume = ticker[3];
        NSNumber* change = ticker[4];
        [self addTickerTime:time side:side price:price volume:volume change:change prior:prior];
    }
}

-(void) highlightCurrentCell:(SingleStockTableViewCell*) cell {
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.previousIndex inSection:0];
    SingleStockTableViewCell* previousCell = (SingleStockTableViewCell*) [self.tableView cellForRowAtIndexPath:indexPath];
    previousCell.highlighted = NO;

    cell.highlighted = YES;
    self.previousIndex = self.currentIndex;
    self.currentIndex = (self.currentIndex+1) % MAX_TICKER;
}
-(void) addTickerTime:(NSString*) time
                 side:(NSString*) side
                price:(NSNumber*) price
               volume:(NSNumber*) volume
               change:(NSNumber*) change
                prior:(NSNumber *)prior
{
    NSIndexPath* indexPath = [NSIndexPath indexPathForRow:self.currentIndex inSection:0];
    SingleStockTableViewCell* cell  = (SingleStockTableViewCell*)[self.tableView cellForRowAtIndexPath:indexPath];
    cell.time.text = [time timeFormatStyle];
    cell.side.text = side;
    [cell.price setColorWithPrior:prior currentValue:price withTheme:self.theme blink:NO];
    cell.volume.text = [volume volumeFormatStyle];
    cell.change.text = [change changeFormatStyle];
    cell.change.textColor = cell.price.textColor;
   
    if ([side isEqualToString:@"B"]) {
        cell.volume.textColor =
        cell.side.textColor = self.theme.cyanColor;
    } else {
        cell.volume.textColor =
        cell.side.textColor = self.theme.magentaColor;
    }
    [self highlightCurrentCell:cell];
    
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return [self createCellforRowAtIndexPath:indexPath];
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return MAX_TICKER;
}
-(void)resetAllTickerCell {
    [self.tableView reloadData];
    self.currentIndex = 0;
    self.previousIndex = 0;
}
-(SingleStockTableViewCell*) createCellforRowAtIndexPath:(NSIndexPath*) indexPath {
    static NSString* cellIdentifier = @"SingleStockTableViewCell";
    SingleStockTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil){
        NSLog(@"Cell not found");
    }
    //custom cell configuration
    cell.time.text = @"";
    cell.price.text = @"";
    cell.change.text = @"";
    cell.volume.text = @"";
    cell.side.text = @"";
    cell.time.backgroundColor =
    cell.price.backgroundColor =
    cell.change.backgroundColor =
    cell.side.backgroundColor =
    cell.volume.backgroundColor = self.theme.tableBackgroundColor;
    cell.time.textColor = self.theme.textColor;
    cell.volume.textColor = self.theme.yellowColor;
    return cell;
}


@end
