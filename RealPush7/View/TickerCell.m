//
//  TickerCell.m
//  RealPush
//
//  Created by Sutean Rutjanalard on 7/29/13.
//  Copyright (c) 2013 Sutean Rutjanalard. All rights reserved.
//

#import "TickerCell.h"

@interface TickerCell()
@property (nonatomic, strong) UIColor* baseColor;


@end

@implementation TickerCell
-(UIColor *)baseColor {
    if ( ! _baseColor) {
        _baseColor = self.lastTickerSign.backgroundColor;
    }
    return _baseColor;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)awakeFromNib {
    self.baseColor = self.lastTickerSign.backgroundColor;
    self.lastTickerSign.frame = self.frame;
}
-(void)setHighlightedLastTicker:(BOOL)highlightedLastTicker {
    if (highlightedLastTicker) {
        self.lastTickerSign.backgroundColor = self.baseColor;
    } else {
        self.lastTickerSign.backgroundColor = [UIColor clearColor];
    }
}
@end
