//
//  StockSmallCelliPhone.h
//  RealPush
//
//  Created by Sutean Rutjanalard on 9/10/2556 BE.
//  Copyright (c) 2556 Sutean Rutjanalard. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StockFavoriteCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *stockSymbol;
@property (weak, nonatomic) IBOutlet UILabel *price;
@property (weak, nonatomic) IBOutlet UIButton *change;
@property (weak, nonatomic) IBOutlet UILabel *description;


@end
