//
//  SingleTickerView.h
//  RealPush7
//
//  Created by Sutean Rutjanalard on 7/4/2557 BE.
//  Copyright (c) 2557 Sutean Rutjanalard. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ITUITheme.h"
@interface SingleTickerView : UIViewController

@property (nonatomic, weak) ITUITheme* theme;
-(void) resetAllTickerCell;

//[ [time, side, price, volume,change],[time, side, price, volume,change],...  ]
-(void) setTickersArray:(NSArray*) tickersArray prior:(NSNumber*) prior;

-(void) addTickerTime:(NSString*) time
                 side:(NSString*) side
                price:(NSNumber*) price
               volume:(NSNumber*) volume
               change:(NSNumber*) change
                prior:(NSNumber*) prior;


@end
