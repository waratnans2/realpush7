//
//  SlideViewController.h
//  SlideMenuApp
//
//  Created by Sutean Rutjanalard on 3/9/2556 BE.
//  Copyright (c) 2556 Sutean Rutjanalard. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ITUITheme.h"

@interface SlideViewController : UIViewController
@property (nonatomic, strong) UIView* titleView;
@property (nonatomic, strong) UIButton* leftMenuButton;
@property (nonatomic, strong) UIButton* rightMenuButton;
@property (nonatomic, strong) UIButton* centerButton;

@property (nonatomic, strong) NSString* titleText;
@property (nonatomic, strong) NSString* leftMenuTitle;
@property (nonatomic, strong) NSString* rightMenuTitle;
@property (nonatomic, weak) ITUITheme* theme;


-(void) applyThemeOnCell:(UITableViewCell*) cell ;

-(void) applyTheme;

@end
