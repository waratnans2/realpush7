//
//  SlideViewController.m
//  SlideMenuApp
//
//  Created by Sutean Rutjanalard on 3/9/2556 BE.
//  Copyright (c) 2556 Sutean Rutjanalard. All rights reserved.
//

#import "SlideViewController.h"
#import "AppDelegate.h"

@interface SlideViewController ()

@end

@implementation SlideViewController

-(ITUITheme *)theme {
    if (!_theme) {
        _theme = ((AppDelegate*)[[UIApplication sharedApplication] delegate]).theme;
    }
    return _theme;
}

-(void) applyTheme {
    self.view.backgroundColor = self.theme.defaultBackgroundColor;
}
-(void) applyThemeOnCell:(UITableViewCell*) cell {
    //    cell.backgroundView.backgroundColor = self.theme.tableBackgroundColor;
    
    cell.backgroundColor = self.theme.tableBackgroundColor;
    cell.selectedBackgroundView = [[UIImageView alloc] initWithImage:self.theme.tableBackgroundImage highlightedImage:self.theme.tableSelectedBackgroundImage];
    cell.textLabel.textColor = self.theme.defaultButtonTextColor;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self applyTheme];
	
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
