//
//  ITUITheme.h
//  RealPush
//
//  Created by Sutean Rutjanalard on 26/9/2556 BE.
//  Copyright (c) 2556 Sutean Rutjanalard. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ITUITheme : NSObject
//header
-(UIImage*) titleBackgroundImage;
-(UIImage*) leftMenuImage;
-(UIImage*) rightMenuImage;
-(UIColor*) titleBackgroundColor;
-(UIColor*) titleTextColor;
-(UIImage*) titleIconImage;
-(UIColor*) menuBackgroundColor;
-(UIImage*) leftMenuBackgroundImage;

//table
-(UIImage*) tableBackgroundImage;
-(UIColor*) tableBackgroundColor;
-(UIColor*) tableDefaultTextColor;
-(UIColor*) tableSelectedTextColor;
-(UIImage*) tableSelectedBackgroundImage;
-(UIImage*) tableSectionImage;
-(UIColor*) tableSectionTextColor;
-(UIImage*) tableButtonRedImage;
-(UIImage*) tableButtonYellowImage;
-(UIImage*) tableButtonGreenImage;
-(UIImage*) tableButtonWhiteImage;
-(UIColor*) underLineColor;
//page control
-(UIColor*) pageIndicatorTintColor;
-(UIColor*) currentPageIndicatorTintColor;

//left menu
-(UIImage*) marketIcon;
-(UIImage*) quoteIcon;
-(UIImage*) tickerIcon;
-(UIImage*) watchIcon;

//default color
-(UIColor*) redColor;
-(UIColor*) greenColor;
-(UIColor*) yellowColor;
-(UIColor*) whiteColor;
-(UIColor*) magentaColor;
-(UIColor*) cyanColor;


//button
-(UIColor*) defaultButtonColor;
-(UIColor*) defaultButtonTextColor;
-(UIColor*) selectedButtonBackgroundColor;
-(UIColor*) selectedButtonTextColor;
-(UIColor*) deselectedButtonBackgroundColor;
-(UIColor*) deselectedButtonTextColor;

//generic
-(UIColor*) labelTextColor;
-(UIColor*) textColor;
-(UIColor*) defaultBackgroundColor;
-(UIColor*) lightBackgroundColor;
-(UIColor*) backgroundColorHighlightColor;

//watch
-(UIImage*) imageChangeBoxGreen;
-(UIImage*) imageChangeBoxRed;
-(UIImage*) imageChangeBoxWhite;
-(UIImage*) imageChangeBoxYellow;

//bid/offer graph
-(double) graphWidth;
-(double) offerStartX;

//price+vol chart
-(UIColor*)chartBackgroundColor;
-(UIColor*)lineChartlabelColor;
-(UIColor*)barChartlabelColor;
-(UIColor*)chartAxisColor;

@property (nonatomic, strong) NSArray *leftMenuIcons;
@property (nonatomic, strong) NSArray *leftMenuActiveIcons;
@property (nonatomic, strong) NSArray *leftMenuTitle;



@end
