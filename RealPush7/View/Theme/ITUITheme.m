//
//  ITUITheme.m
//  RealPush
//
//  Created by Sutean Rutjanalard on 26/9/2556 BE.
//  Copyright (c) 2556 Sutean Rutjanalard. All rights reserved.
//

#import "ITUITheme.h"

#define BTN_BG [UIColor colorWithRed:153.0/255.0 green:204.0/255.0 blue:255.0/255.0 alpha:1]
#define BTN_TEXT [UIColor colorWithRed:50.0/255.0 green:79.0/255.0 blue:133.0/255.0 alpha:1]
#define TITLE_COLOR [UIColor colorWithRed:51/255.0 green:51/255.0 blue:51/255.0 alpha:1]

@interface ITUITheme()
@property (nonatomic, strong) UIImage* titleBackgroundImage;
@property (nonatomic, strong) UIImage* leftMenuImage;
@property (nonatomic, strong) UIImage* rightMenuImage;

@property (nonatomic, strong) UIImage* tableBackgroundImage;
@property (nonatomic, strong) UIImage* tableSelectedBackgroundImage;
@property (nonatomic, strong) UIImage* tableSectionImage;

@property (nonatomic, strong) UIColor* tableBackgroundColor;
@property (nonatomic, strong) UIColor* tableDefaultTextColor;
@property (nonatomic, strong) UIColor* tableSelectedTextColor;
@property (nonatomic, strong) UIImage* marketIcon;
@property (nonatomic, strong) UIImage* tickerIcon;
@property (nonatomic, strong) UIImage* watchIcon;
@property (nonatomic, strong) UIImage* quoteIcon;
@property (nonatomic, strong) UIImage* tableButtonRedImage;
@property (nonatomic, strong) UIImage* tableButtonYellowImage;
@property (nonatomic, strong) UIImage* tableButtonGreenImage;
@property (nonatomic, strong) UIImage* tableButtonWhiteImage;

@property (nonatomic, strong) UIImage* imageChangeBoxGreen;
@property (nonatomic, strong) UIImage* imageChangeBoxRed;
@property (nonatomic, strong) UIImage* imageChangeBoxWhite;
@property (nonatomic, strong) UIImage* imageChangeBoxYellow;



@end


@implementation ITUITheme
#pragma mark - left menu
-(NSArray *)leftMenuIcons {
    if ( ! _leftMenuIcons) {
        _leftMenuIcons = [NSArray arrayWithObjects:
                          [UIImage imageNamed:@"market"],
                          [UIImage imageNamed:@"watch"],
                          [UIImage imageNamed:@"quote"],
                          [UIImage imageNamed:@"quote"],
                          [UIImage imageNamed:@"ticker"],
                          nil];
    }
    return _leftMenuIcons;
}
-(NSArray *)leftMenuActiveIcons {
    if ( ! _leftMenuActiveIcons) {
        _leftMenuActiveIcons =[NSArray arrayWithObjects:
                               [UIImage imageNamed:@"market_active"],
                               [UIImage imageNamed:@"watch_active"],
                               [UIImage imageNamed:@"quote_active"],
                               [UIImage imageNamed:@"quote_active"],
                               [UIImage imageNamed:@"ticker_active"],
                               nil];
    }
    return _leftMenuActiveIcons;
}
-(NSArray *)leftMenuTitle {
    if ( ! _leftMenuTitle) {
        _leftMenuTitle = [NSArray arrayWithObjects:@"Market",
                          @"Watch",
                          @"Quote",
                          @"Trade",
                          @"Ticker", nil];
    }
    return _leftMenuTitle;
}
-(UIImage *)leftMenuBackgroundImage {
    return [UIImage imageNamed:@"left_menu_background"];
}
#pragma mark - title
-(UIImage *)titleBackgroundImage {
    if ( ! _titleBackgroundImage) {
        _titleBackgroundImage = [UIImage imageNamed:@"title_background"];
    }
    return _titleBackgroundImage;
}
-(UIImage *)titleIconImage {
    return [UIImage imageNamed:@"title_icon"];
}
-(UIImage *)leftMenuImage {
    if ( ! _leftMenuImage) {
        _leftMenuImage = [UIImage imageNamed:@"left_menu"];
    }
    return _leftMenuImage;
}
-(UIImage*) rightMenuImage {
    if (! _rightMenuImage) {
        _rightMenuImage = [UIImage imageNamed:@"right_menu"];
    }
    return _rightMenuImage;
}
#pragma mark - table
-(UIImage *)tableSelectedBackgroundImage {
    if ( ! _tableSelectedBackgroundImage) {
        _tableSelectedBackgroundImage = [UIImage imageNamed:@"selected_cell"];
    }
    return _tableSelectedBackgroundImage;
}
-(UIImage *)tableBackgroundImage {
    if ( ! _tableBackgroundImage) {
        _tableBackgroundImage = [UIImage imageNamed:@"deselect_cell"];
    }
    return _tableBackgroundImage;
}
-(UIImage *)tableSectionImage {
    if ( ! _tableSectionImage) {
        _tableSectionImage = [UIImage imageNamed:@"table_section"];
    }
    return _tableSectionImage;
}
-(UIImage*) tableButtonRedImage {
    if ( ! _tableButtonRedImage) {
        _tableButtonRedImage = [UIImage imageNamed:@"table_btn_red"];
    }
    return _tableButtonRedImage;
}

-(UIColor*) tableSectionTextColor {
    return [UIColor blackColor];
}
-(UIImage*) tableButtonYellowImage {
    if ( ! _tableButtonYellowImage) {
        _tableButtonYellowImage = [UIImage imageNamed:@"table_btn_yellow"];
    }
    return _tableButtonYellowImage;
    
}
-(UIImage*) tableButtonGreenImage {
    if ( ! _tableButtonGreenImage) {
        _tableButtonGreenImage = [UIImage imageNamed:@"table_btn_green"];
    }
    return _tableButtonGreenImage;
}
-(UIImage *)tableButtonWhiteImage {
    if ( ! _tableButtonWhiteImage) {
        _tableButtonWhiteImage = [UIImage imageNamed:@"table_btn_white"];
    }
    return _tableButtonWhiteImage;
}
-(UIImage*) imageChangeBoxGreen {
    if ( ! _imageChangeBoxGreen) {
        _imageChangeBoxGreen = [UIImage imageNamed:@"change_green_box"];
    }
    return _imageChangeBoxGreen;
}
-(UIImage*) imageChangeBoxRed {
    if ( ! _imageChangeBoxRed) {
        _imageChangeBoxRed = [UIImage imageNamed:@"change_red_box"];
    }
    return _imageChangeBoxRed;
}
-(UIImage*) imageChangeBoxWhite {
    if ( ! _imageChangeBoxWhite) {
        _imageChangeBoxWhite = [UIImage imageNamed:@"change_white_box"];
    }
    return _imageChangeBoxWhite;
}
-(UIImage*) imageChangeBoxYellow {
    if ( ! _imageChangeBoxYellow) {
        _imageChangeBoxYellow = [UIImage imageNamed:@"change_yellow_box"];
    }
    return _imageChangeBoxYellow;
}

-(UIColor*) tableDefaultTextColor { return self.whiteColor; }
-(UIColor *)redColor { return [UIColor redColor]; }
-(UIColor *)greenColor { return [UIColor greenColor];}
-(UIColor *)yellowColor { return [UIColor yellowColor];}
-(UIColor *)whiteColor { return [UIColor whiteColor];}
-(UIColor *)magentaColor { return [UIColor magentaColor]; };
-(UIColor *)cyanColor { return [UIColor cyanColor]; };
-(UIColor *)textColor { return [UIColor blackColor]; };
//light blue
-(UIColor*) defaultButtonColor { return [UIColor colorWithRed:167.0/255.0 green:202.0/255.0 blue:255.0/255.0 alpha:1];  }
-(UIColor *)defaultButtonTextColor { return [UIColor colorWithRed:51/255.0 green:51/255.0 blue:51/255.0 alpha:1]; }
-(UIColor *)titleBackgroundColor { return [UIColor clearColor]; }
-(UIColor *)labelTextColor { return [UIColor whiteColor];}
-(UIColor *)defaultBackgroundColor { return [UIColor blackColor]; }

-(UIColor *)lightBackgroundColor { return [UIColor lightGrayColor]; }
-(UIColor*) underLineColor {return [UIColor lightGrayColor];}
-(UIColor *)backgroundColorHighlightColor {return [UIColor blueColor];}
//light blue
-(UIColor*) menuBackgroundColor { return  [UIColor colorWithRed:167.0/255.0 green:202.0/255.0 blue:255.0/255.0 alpha:1]; }
-(UIColor *)selectedButtonBackgroundColor { return [UIColor colorWithRed:167.0/255.0 green:202.0/255.0 blue:255.0/255.0 alpha:1]; }
-(UIColor *)selectedButtonTextColor { return [UIColor blueColor]; }
-(UIColor*) deselectedButtonBackgroundColor { return [UIColor lightGrayColor];}
-(UIColor*) deselectedButtonTextColor { return [UIColor darkGrayColor];}
-(UIColor *)titleTextColor { return TITLE_COLOR;};
-(UIColor *)pageIndicatorTintColor { return self.defaultButtonColor;};
-(UIColor *)currentPageIndicatorTintColor { return self.whiteColor; }
-(double) graphWidth { return 152.0;} 
-(double) offerStartX { return 7.0;}


-(UIColor*)chartBackgroundColor{return [UIColor darkTextColor];}
-(UIColor*)lineChartlabelColor{return [UIColor whiteColor];}
-(UIColor*)barChartlabelColor{return [UIColor lightGrayColor];}
-(UIColor*)chartAxisColor{return [UIColor colorWithRed:0.4f green:0.4f blue:0.4f alpha:1.f];}

@end
