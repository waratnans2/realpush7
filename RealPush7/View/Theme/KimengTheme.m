//
//  KimengTheme.m
//  RealPush
//
//  Created by Sutean Rutjanalard on 9/1/2557 BE.
//  Copyright (c) 2557 Sutean Rutjanalard. All rights reserved.
//

#import "KimengTheme.h"

@implementation KimengTheme
-(UIColor *)menuBackgroundColor {
    return [UIColor whiteColor];
}
-(UIColor *)defaultButtonTextColor {
    return [UIColor blackColor];
}
-(UIColor *)titleBackgroundColor {
    return [UIColor yellowColor];
}
-(UIColor *)titleTextColor {
    return [UIColor blackColor];
}
-(UIColor *)tableBackgroundColor {
    return [UIColor whiteColor];
}
-(UIColor *)tableDefaultTextColor {
    return [UIColor blackColor];
}
+ (UIImage *)imageWithColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

-(UIImage *)tableSelectedBackgroundImage {
    return [KimengTheme imageWithColor:[UIColor lightGrayColor]];
}
-(UIImage *)tableSectionImage {
    return [KimengTheme imageWithColor:[UIColor blackColor]];
}
-(UIColor*) tableSectionTextColor {
    return [UIColor yellowColor];
}

-(UIColor *)labelTextColor  {
    return [UIColor blackColor];
}
-(UIColor*) defaultBackgroundColor {
    return [UIColor lightGrayColor];
}
-(UIColor *)lightBackgroundColor {
    return [UIColor whiteColor];
}
@end
