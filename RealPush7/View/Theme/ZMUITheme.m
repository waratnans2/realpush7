//
//  ZMUITheme.m
//  RealPush
//
//  Created by Sutean Rutjanalard on 26/9/2556 BE.
//  Copyright (c) 2556 Sutean Rutjanalard. All rights reserved.
//

#import "ZMUITheme.h"

@implementation ZMUITheme

-(UIColor *)redColor {
    return [UIColor colorWithRed:232.0/255 green:63.0/255.0 blue:60.0/255.0 alpha:1];
}
-(UIColor *)greenColor {
    return [UIColor colorWithRed:106.0/255.0 green:191.0/255.0 blue:103.0/255.0 alpha:1];
}
-(UIColor *)yellowColor {
    return [UIColor colorWithRed:241.0/255.0 green:195/255.0 blue:53/255.0 alpha:1];
}
-(UIColor *)cyanColor { return [UIColor colorWithRed:86.0/255.0 green:190.0/255.0 blue:240.0/255.0 alpha:1]; }
-(UIColor *)magentaColor { return [UIColor colorWithRed:232.0/255.0 green:96.0/255.0 blue:165.0/255.0 alpha:1]; }

-(UIColor *)whiteColor {
    return [UIColor whiteColor];
}
-(UIColor *)menuBackgroundColor {
    return [UIColor colorWithRed:53.0/255.0 green:74.0/255.0 blue:94.0/255.0 alpha:1];
}
//sky blue //none selected segment
-(UIColor*) defaultButtonColor {return [UIColor colorWithRed:44.0/255.0 green:62.0/255.0 blue:79.0/255.0 alpha:1];}
-(UIColor*) defaultButtonTextColor { return [UIColor colorWithRed:76.0/255.0 green:175.0/255.0 blue:239.0/255.0 alpha:1]; }
-(UIColor*) selectedButtonTextColor { return self.whiteColor; }
-(UIColor*) selectedButtonBackgroundColor { return [UIColor colorWithRed:76.0/255.0 green:175.0/255.0 blue:239.0/255.0 alpha:1];}

-(UIColor*) titleBackgroundColor { return [UIColor clearColor]; }
-(UIColor*) titleTextColor { return self.whiteColor; }
-(UIColor*) labelTextColor { return  [UIColor colorWithRed:64.0/255.0 green:144.0/255.0 blue:176.0/255.0 alpha:1];}
-(UIColor *)textColor { return self.whiteColor;}
-(UIColor*) defaultBackgroundColor {return [UIColor colorWithRed:38/255.0 green:41/255.0 blue:46/255.0 alpha:1];}
-(UIColor*) backgroundColorHighlightColor {return [UIColor blueColor];}

-(UIColor*) lightBackgroundColor { return [UIColor colorWithRed:53.0/255.0 green:74.0/255.0 blue:94.0/255.0 alpha:1];}
-(UIColor*) underLineColor { return [UIColor colorWithRed:64.0/255.0 green:144.0/255.0 blue:176.0/255.0 alpha:1];}
-(UIColor*) deselectedButtonBackgroundColor { return [UIColor colorWithRed:44.0/255.0 green:62.0/255.0 blue:79.0/255.0 alpha:1];}
-(UIColor*) deselectedButtonTextColor {return [UIColor colorWithRed:62.0/255.0 green:139./255.0 blue:170.0/255.0 alpha:1];}
-(UIColor *)pageIndicatorTintColor { return [UIColor colorWithRed:61.0/255.0 green:86.0/255.0 blue:109.0/255.0 alpha:1]; }
-(UIColor *)currentPageIndicatorTintColor { return [UIColor whiteColor]; }
//table
-(UIColor *)tableBackgroundColor { return self.defaultBackgroundColor; }
@end
