//
//  ListOfStocksView.h
//  RealPush7
//
//  Created by Sutean Rutjanalard on 22/4/2557 BE.
//  Copyright (c) 2557 Sutean Rutjanalard. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListOfStocksView : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end
