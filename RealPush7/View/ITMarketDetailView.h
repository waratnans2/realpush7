//
//  ITMarketDetailView.h
//  RealPush
//  Created by Sutean Rutjanalard on 8/15/13.
//  Copyright (c) 2013 Sutean Rutjanalard. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ITUITheme.h"

@interface ITMarketDetailView : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *status;
@property (weak, nonatomic) IBOutlet UILabel *prior;
@property (weak, nonatomic) IBOutlet UILabel *high;
@property (weak, nonatomic) IBOutlet UILabel *low;
@property (weak, nonatomic) IBOutlet UILabel *change;
@property (weak, nonatomic) IBOutlet UILabel *percentChange;
@property (weak, nonatomic) IBOutlet UILabel *advance;
@property (weak, nonatomic) IBOutlet UILabel *unchange;
@property (weak, nonatomic) IBOutlet UILabel *decline;
@property (weak, nonatomic) IBOutlet UILabel *timestamp;
@property (weak, nonatomic) IBOutlet UILabel *value;
@property (weak, nonatomic) IBOutlet UILabel *volume;
@property (weak, nonatomic) IBOutlet UILabel *sellVolume;
@property (weak, nonatomic) IBOutlet UILabel *buyVolume;



-(void) setTheme:(ITUITheme*) theme;
@end
