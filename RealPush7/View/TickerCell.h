//
//  TickerCell.h
//  RealPush
//
//  Created by Sutean Rutjanalard on 7/29/13.
//  Copyright (c) 2013 Sutean Rutjanalard. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TickerCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *symbolLabel;
@property (strong, nonatomic) IBOutlet UILabel *sideLabel;
@property (strong, nonatomic) IBOutlet UILabel *priceLabel;
@property (strong, nonatomic) IBOutlet UILabel *volumeLabel;
@property (strong, nonatomic) IBOutlet UILabel *changeLabel;
@property (strong, nonatomic) IBOutlet UILabel *percentChangeLabel;
@property (weak, nonatomic) IBOutlet UIView *lastTickerSign;
@property (nonatomic, readwrite) BOOL highlightedLastTicker;

@end
