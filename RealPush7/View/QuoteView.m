//
//  QuoteView.m
//  RealPush
//
//  Created by Sutean Rutjanalard on 20/9/2556 BE.
//  Copyright (c) 2556 Sutean Rutjanalard. All rights reserved.
//

#import "QuoteView.h"

@implementation QuoteView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (IBAction)selectPriceBtn:(UIButton *)sender {
    if ([self.delegate conformsToProtocol:@protocol(QuoteViewDelegate) ]) {
        [self.delegate selectedPrice:sender.titleLabel.text];
    }
}

@end
