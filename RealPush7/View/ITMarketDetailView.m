//
//  ITMarketDetailView.m
//  RealPush
//
//  Created by Sutean Rutjanalard on 8/15/13.
//  Copyright (c) 2013 Sutean Rutjanalard. All rights reserved.
//

#import "ITMarketDetailView.h"

@interface ITMarketDetailView()
@property (nonatomic, weak) IBOutlet UILabel* labelForStatus;

@property (nonatomic, weak) IBOutlet UILabel* labelForPrior;
@property (nonatomic, weak) IBOutlet UILabel* labelForHigh;
@property (nonatomic, weak) IBOutlet UILabel* labelForLow;
@property (nonatomic, weak) IBOutlet UILabel* labelForChange;
@property (nonatomic, weak) IBOutlet UILabel* labelForPercentChange;
@property (nonatomic, weak) IBOutlet UILabel* labelForAdvance;
@property (nonatomic, weak) IBOutlet UILabel* labelForUnchange;
@property (nonatomic, weak) IBOutlet UILabel* labelForDecline;
@property (nonatomic, weak) IBOutlet UILabel* labelForTimestamp;
@property (nonatomic, weak) IBOutlet UILabel* labelForValue;
@property (nonatomic, weak) IBOutlet UILabel* labelForVolume;
@property (nonatomic, weak) IBOutlet UILabel* labelForBuyVolume;
@property (nonatomic, weak) IBOutlet UILabel* labelForSellVolume;
@property (weak, nonatomic) IBOutlet UIView *backgroundView;

@end
@implementation ITMarketDetailView

-(void)setTheme:(ITUITheme *)theme {
    self.view.backgroundColor =
    self.labelForAdvance.backgroundColor =
    self.labelForStatus.backgroundColor =
    self.labelForChange.backgroundColor =
    self.labelForDecline.backgroundColor =
    self.labelForHigh.backgroundColor =
    self.labelForLow.backgroundColor =
    self.labelForPercentChange.backgroundColor =
    self.labelForPrior.backgroundColor =
    self.labelForTimestamp.backgroundColor =
    self.labelForUnchange.backgroundColor =
    self.labelForValue.backgroundColor =
    self.labelForBuyVolume.backgroundColor =
    self.labelForSellVolume.backgroundColor =
    self.labelForVolume.backgroundColor = theme.defaultBackgroundColor;
    
    self.backgroundView.backgroundColor = 
    self.prior.backgroundColor =
    self.high.backgroundColor =
    self.low.backgroundColor =
    self.change.backgroundColor =
    self.percentChange.backgroundColor =
    self.advance.backgroundColor =
    self.unchange.backgroundColor =
    self.decline.backgroundColor =
    self.timestamp.backgroundColor =
    self.volume.backgroundColor =
    self.value.backgroundColor =
    self.status.backgroundColor = theme.lightBackgroundColor;
    
    
    self.labelForStatus.textColor =
    self.labelForAdvance.textColor =
    self.labelForChange.textColor =
    self.labelForDecline.textColor =
    self.labelForHigh.textColor =
    self.labelForLow.textColor =
    self.labelForPercentChange.textColor =
    self.labelForPrior.textColor =
    self.labelForTimestamp.textColor =
    self.labelForUnchange.textColor =
    self.labelForValue.textColor =
    self.labelForVolume.textColor = theme.labelTextColor;
    
    self.advance.textColor = theme.greenColor;
    self.unchange.textColor = theme.yellowColor;
    self.decline.textColor = theme.redColor;
    self.timestamp.textColor =
    self.value.textColor =
    self.volume.textColor =
    self.status.textColor = theme.titleTextColor;
    
    
    
    self.status.text =
    self.advance.text =
    self.unchange.text =
    self.decline.text =
    self.timestamp.text =
    self.value.text =
    self.volume.text =
    self.high.text =
    self.low.text =
    self.prior.text =
    self.percentChange.text =
    self.change.text = @"";
    
    
}
@end
