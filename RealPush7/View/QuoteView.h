//
//  QuoteView.h
//  RealPush
//
//  Created by Sutean Rutjanalard on 20/9/2556 BE.
//  Copyright (c) 2556 Sutean Rutjanalard. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol QuoteViewDelegate <NSObject>

-(void) selectedPrice:(NSString*) priceString;

@end

@interface QuoteView : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *volbid1;
@property (weak, nonatomic) IBOutlet UILabel *volbid2;
@property (weak, nonatomic) IBOutlet UILabel *volbid3;
@property (weak, nonatomic) IBOutlet UILabel *volbid4;
@property (weak, nonatomic) IBOutlet UILabel *volbid5;

@property (weak, nonatomic) IBOutlet UILabel *voloff1;
@property (weak, nonatomic) IBOutlet UILabel *voloff2;
@property (weak, nonatomic) IBOutlet UILabel *voloff3;
@property (weak, nonatomic) IBOutlet UILabel *voloff4;
@property (weak, nonatomic) IBOutlet UILabel *voloff5;

@property (weak, nonatomic) IBOutlet UIButton *bid1;
@property (weak, nonatomic) IBOutlet UIButton *bid2;
@property (weak, nonatomic) IBOutlet UIButton *bid3;
@property (weak, nonatomic) IBOutlet UIButton *bid4;
@property (weak, nonatomic) IBOutlet UIButton *bid5;

@property (weak, nonatomic) IBOutlet UIButton *offer1;
@property (weak, nonatomic) IBOutlet UIButton *offer2;
@property (weak, nonatomic) IBOutlet UIButton *offer3;
@property (weak, nonatomic) IBOutlet UIButton *offer4;
@property (weak, nonatomic) IBOutlet UIButton *offer5;

@property (weak, nonatomic) IBOutlet UILabel *high;
@property (weak, nonatomic) IBOutlet UILabel *low;
@property (weak, nonatomic) IBOutlet UILabel *prior;
@property (weak, nonatomic) IBOutlet UILabel *avg;
@property (weak, nonatomic) IBOutlet UILabel *projected;
@property (weak, nonatomic) IBOutlet UILabel *projectedVol;
@property (weak, nonatomic) IBOutlet UILabel *labelForAvg;
@property (weak, nonatomic) IBOutlet UILabel *labelForHigh;
@property (weak, nonatomic) IBOutlet UILabel *labelForLow;
@property (weak, nonatomic) IBOutlet UILabel *labelForPrior;
@property (weak, nonatomic) IBOutlet UILabel *labelForProjectedPrice;
@property (weak, nonatomic) IBOutlet UILabel *labelForProjectedVolume;

@property (weak, nonatomic) IBOutlet UIView *graphBid1;
@property (weak, nonatomic) IBOutlet UIView *graphBid2;
@property (weak, nonatomic) IBOutlet UIView *graphBid3;
@property (weak, nonatomic) IBOutlet UIView *graphBid4;
@property (weak, nonatomic) IBOutlet UIView *graphBid5;

@property (weak, nonatomic) IBOutlet UIView *graphOffer1;
@property (weak, nonatomic) IBOutlet UIView *graphOffer2;
@property (weak, nonatomic) IBOutlet UIView *graphOffer3;
@property (weak, nonatomic) IBOutlet UIView *graphOffer4;
@property (weak, nonatomic) IBOutlet UIView *graphOffer5;



@property (nonatomic, weak) id<QuoteViewDelegate> delegate;



@end
