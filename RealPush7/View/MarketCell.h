//
//  MarketCell.h
//  RealPush
//
//  Created by Sutean Rutjanalard on 8/15/13.
//  Copyright (c) 2013 Sutean Rutjanalard. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MarketCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIButton *change;
@property (strong, nonatomic) IBOutlet UILabel *index;
@property (strong, nonatomic) IBOutlet UILabel *symbol;

@end
