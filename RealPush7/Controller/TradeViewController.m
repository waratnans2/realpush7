//
//  TradeViewController.m
//  RealPush
//
//  Created by Sutean Rutjanalard on 20/9/2556 BE.
//  Copyright (c) 2556 Sutean Rutjanalard. All rights reserved.
//

#import "TradeViewController.h"
#import "AppDelegate.h"
#import "UILabel+blink.h"
#import "NSNumber+formatter.h"
#import "UILabel+blink.h"
#import "UIButton+blink.h"
#import "QuoteView.h"
#import "ZenKeyboard.h"
#import "UIView+bidOfferGraph.h"
#import <QuartzCore/QuartzCore.h>
#import "SingleTickerView.h"
#import "VolumeKeyboard.h"
#import "PriceKeyboard.h"
#import "PVChartMakerViewController.h"
#import "CompareChartViewController.h"
@interface TradeViewController () <UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate,QuoteViewDelegate, KeyboardDelegate, UITextFieldDelegate>

#pragma mark - trade object
@property (weak, nonatomic) IBOutlet UIButton *buyBtn;
@property (weak, nonatomic) IBOutlet UIButton *sellBtn;
@property (weak, nonatomic) IBOutlet UITextField *symbolTF;
@property (weak, nonatomic) IBOutlet UITextField *priceTF;
@property (weak, nonatomic) IBOutlet UITextField *volumeTF;
@property (weak, nonatomic) IBOutlet UITextField *icebergTF;
@property (weak, nonatomic) IBOutlet UITextField *pinTF;
@property (weak, nonatomic) IBOutlet UIButton *priceTypeBtn;
@property (weak, nonatomic) IBOutlet UIButton *dayBtn;
@property (weak, nonatomic) IBOutlet UIButton *confirmBtn;
@property (weak, nonatomic) IBOutlet UIButton *clearBtn;
@property (weak, nonatomic) IBOutlet UISwitch *nvdrSwitch;
@property (weak, nonatomic) IBOutlet UIButton *hideKeyboard;
@property (weak, nonatomic) IBOutlet UITableView *searchTableView;
@property (weak, nonatomic) IBOutlet UILabel *estimatePrice;

#pragma mark - content object
@property (weak, nonatomic) IBOutlet UILabel *symbolLb;
@property (weak, nonatomic) IBOutlet UILabel *stockFullname;
@property (weak, nonatomic) IBOutlet UILabel *priceLb;
@property (weak, nonatomic) IBOutlet UILabel *changeLb;
@property (weak, nonatomic) IBOutlet UILabel *percentChangeLb;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (nonatomic, strong) QuoteView* quoteView;
@property (nonatomic,strong) SingleTickerView *singleTickerView;
@property (nonatomic, strong) PVChartMakerViewController *chartView;
@property (nonatomic, readwrite) BOOL pageControlBeingUsed;


#pragma mark - account
@property (weak, nonatomic) IBOutlet UILabel *accountLb;
@property (weak, nonatomic) IBOutlet UIButton *onhandBtn;
@property (weak, nonatomic) IBOutlet UILabel *buyPriceLb;
@property (weak, nonatomic) IBOutlet UILabel *cashOrCreditLb;
@property (weak, nonatomic) IBOutlet UIButton *accountSwitchBtn;
@property (weak, nonatomic) IBOutlet UIButton *balanceDetailBtn;
@property (weak, nonatomic) IBOutlet UIView *accountView;

- (IBAction)hideKeyboardAction;
- (IBAction)clearAction;
- (IBAction)confirmAction;
- (IBAction)priceTypeAction:(id)sender;
- (IBAction)dayAction:(id)sender;
- (IBAction)buyAction:(id)sender;
- (IBAction)sellAction:(id)sender;
- (IBAction)finishModalAction:(id)sender;
- (IBAction)cashDetailAction:(id)sender;
- (IBAction)accountSwitchAction:(id)sender;

#pragma mark - Model
@property (nonatomic, strong) NSArray* searchResultArray;
@property (nonatomic, weak) AppDelegate* appDelegate;
@property (nonatomic, weak) ITStockDetail * currentStock;
@property (weak, nonatomic) IBOutlet UIView *tradeStatusBackgroundView;
@property (weak, nonatomic) IBOutlet UIView *tradeStatusForegroundView;
@property (nonatomic, weak) IBOutlet UIImageView *buyBackground;
@property (nonatomic, weak) IBOutlet UIImageView *sellBackground;
@property (nonatomic) BOOL isBuy;




@end

#define PRICE_TAG 1
#define DAY_TAG 2
#define IS_WIDESCREEN ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )

@implementation TradeViewController

-(AppDelegate *)appDelegate {
    if (! _appDelegate) {
        _appDelegate = [[UIApplication sharedApplication] delegate];
    }
    return _appDelegate;
}

-(QuoteView *)quoteView {
    if ( ! _quoteView) {
        _quoteView = [self.storyboard instantiateViewControllerWithIdentifier:@"QuoteView"];
        _quoteView.projectedVol.textColor = self.theme.yellowColor;
    }
    return _quoteView;
}
-(SingleTickerView *)singleTickerView {
    if ( ! _singleTickerView) {
        _singleTickerView = [self.storyboard instantiateViewControllerWithIdentifier:@"SingleTickerView"];
        _singleTickerView.theme = self.theme;
    }
    return _singleTickerView;
}
-(PVChartMakerViewController*)chartView {
    if(!_chartView){
        _chartView = [[PVChartMakerViewController alloc]init];
    }
    return _chartView;
}
static int currentStockObservanceContext;
-(void)setCurrentStock:(ITStockDetail *)currentStock {
    [_currentStock removeObserver:self  forKeyPath:@"hasUpdate" context:&currentStockObservanceContext];
    [_currentStock removeObserver:self forKeyPath:@"lastTickerArrived" context:&currentStockObservanceContext];
    [_currentStock removeObserver:self forKeyPath:@"chartTimeSeries" context:&currentStockObservanceContext];
    _currentStock = currentStock;
    [self.singleTickerView resetAllTickerCell];
    [currentStock addObserver:self forKeyPath:@"hasUpdate" options:NSKeyValueObservingOptionNew context:&currentStockObservanceContext];
    [currentStock addObserver:self forKeyPath:@"lastTickerArrived" options:NSKeyValueObservingOptionNew context:&currentStockObservanceContext];
    [currentStock addObserver:self forKeyPath:@"chartTimeSeries" options:NSKeyValueObservingOptionNew context:&currentStockObservanceContext];
}

- (void)viewDidUnload {
    [self setBuyBtn:nil];
    [self setSellBtn:nil];
    [self setSymbolTF:nil];
    [self setPriceTF:nil];
    [self setVolumeTF:nil];
    [self setIcebergTF:nil];
    [self setPinTF:nil];
    [self setPriceTypeBtn:nil];
    [self setDayBtn:nil];
    [self setConfirmBtn:nil];
    [self setClearBtn:nil];
    [self setNvdrSwitch:nil];
    [self setHideKeyboard:nil];
    [self setSearchTableView:nil];
    [self setEstimatePrice:nil];
    [self setSymbolLb:nil];
    [self setPriceLb:nil];
    [self setChangeLb:nil];
    [self setStockFullname:nil];
    [self setPercentChangeLb:nil];
    [self setAccountLb:nil];
    [self setOnhandBtn:nil];
    [self setBuyPriceLb:nil];
    [self setCashOrCreditLb:nil];
    [self setAccountSwitchBtn:nil];
    [self setBalanceDetailBtn:nil];
    [self setScrollView:nil];
    [self setPageControl:nil];
    [super viewDidUnload];
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void) setupUI {
    self.titleText = @"Quote/Trade";
    self.priceTF.delegate = self;
    self.volumeTF.delegate = self;
    self.icebergTF.delegate = self;
    PriceKeyboard *priceKB = [[PriceKeyboard alloc] initWithDelegate:self];
    priceKB.doneButtonTitle = @"Next";
    priceKB.textField = self.priceTF;
    VolumeKeyboard *volomeKB = [[VolumeKeyboard alloc] initWithDelegate:self];
    volomeKB.textField = self.volumeTF;
    volomeKB.doneButtonTitle = @"Next";
    VolumeKeyboard *icebergKB = [[VolumeKeyboard alloc] initWithDelegate:self];
    icebergKB.textField = self.icebergTF;
    

}
-(void)keyboard:(id)keyboard backPressedFromeTextField:(UITextField *)textField {
    if (textField == self.icebergTF) {
        [self.volumeTF becomeFirstResponder];
    } else if (textField == self.volumeTF) {
        [self.priceTF becomeFirstResponder];
    } else if (textField == self.priceTF) {
        [self.symbolTF becomeFirstResponder];
    }
}
-(void) keyboard:(id)keyboard donePressedFromeTextField:(UITextField *)textField {
    if (textField == self.priceTF) {
        [self.volumeTF becomeFirstResponder];
    } else if (textField == self.volumeTF) {
        [self.icebergTF becomeFirstResponder];
    } else {
        [self.pinTF becomeFirstResponder];
    }
}
-(void)keyboard:(id)keyboard hideFromTextField:(UITextField *)textField {
    [textField resignFirstResponder];
}
-(void)applyTheme {
    [super applyTheme];
    self.quoteView.projectedVol.textColor = self.theme.yellowColor;
    self.quoteView.labelForAvg.textColor =
    self.quoteView.labelForHigh.textColor =
    self.quoteView.labelForLow.textColor =
    self.quoteView.labelForProjectedVolume.textColor =
    self.quoteView.labelForProjectedPrice.textColor  =
    self.quoteView.labelForPrior.textColor = self.theme.labelTextColor;
    self.symbolLb.backgroundColor =
    self.changeLb.backgroundColor =
    self.percentChangeLb.backgroundColor =
    self.quoteView.projected.backgroundColor =
    self.quoteView.projectedVol.backgroundColor = [UIColor clearColor];
    self.quoteView.view.backgroundColor = self.theme.defaultBackgroundColor;
    self.tradeStatusForegroundView.backgroundColor = self.theme.defaultBackgroundColor;
    self.priceTypeBtn.backgroundColor =
    self.dayBtn.backgroundColor =
    self.confirmBtn.backgroundColor =
    self.hideKeyboard.backgroundColor =
    self.clearBtn.backgroundColor = self.theme.defaultButtonColor;
    [self.priceTypeBtn setTitleColor:self.theme.deselectedButtonTextColor forState:UIControlStateNormal];
    [self.dayBtn setTitleColor:self.theme.deselectedButtonTextColor forState:UIControlStateNormal];
    [self.confirmBtn setTitleColor:self.theme.deselectedButtonTextColor forState:UIControlStateNormal];
    [self.clearBtn setTitleColor:self.theme.deselectedButtonTextColor forState:UIControlStateNormal];
    
    self.quoteView.volbid1.textColor =
    self.quoteView.volbid2.textColor =
    self.quoteView.volbid3.textColor =
    self.quoteView.volbid4.textColor =
    self.quoteView.volbid5.textColor =
    self.quoteView.voloff1.textColor =
    self.quoteView.voloff2.textColor =
    self.quoteView.voloff3.textColor =
    self.quoteView.voloff4.textColor =
    self.quoteView.voloff5.textColor = self.theme.yellowColor;
    self.quoteView.bid1.backgroundColor =
    self.quoteView.bid2.backgroundColor =
    self.quoteView.bid3.backgroundColor =
    self.quoteView.bid4.backgroundColor =
    self.quoteView.bid5.backgroundColor =
    self.quoteView.offer1.backgroundColor =
    self.quoteView.offer2.backgroundColor =
    self.quoteView.offer3.backgroundColor =
    self.quoteView.offer4.backgroundColor =
    self.quoteView.offer5.backgroundColor = self.theme.defaultButtonColor;
    self.quoteView.graphBid1.backgroundColor =
    self.quoteView.graphBid2.backgroundColor =
    self.quoteView.graphBid3.backgroundColor =
    self.quoteView.graphBid4.backgroundColor =
    self.quoteView.graphBid5.backgroundColor = self.theme.cyanColor;
    self.quoteView.graphOffer1.backgroundColor =
    self.quoteView.graphOffer2.backgroundColor =
    self.quoteView.graphOffer3.backgroundColor =
    self.quoteView.graphOffer4.backgroundColor =
    self.quoteView.graphOffer5.backgroundColor = self.theme.magentaColor;
    
    self.quoteView.graphBid1.layer.cornerRadius =
    self.quoteView.graphBid2.layer.cornerRadius =
    self.quoteView.graphBid3.layer.cornerRadius =
    self.quoteView.graphBid4.layer.cornerRadius =
    self.quoteView.graphBid5.layer.cornerRadius =
    self.quoteView.graphOffer1.layer.cornerRadius =
    self.quoteView.graphOffer2.layer.cornerRadius =
    self.quoteView.graphOffer3.layer.cornerRadius =
    self.quoteView.graphOffer4.layer.cornerRadius =
    self.quoteView.graphOffer5.layer.cornerRadius = 1;
    
    
    self.searchTableView.backgroundColor = self.theme.tableBackgroundColor;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self setUpPageControlUI];
//    [self updatePort];
    [self setupUI];
}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.searchTableView.hidden = YES;
    if (IS_WIDESCREEN) { //set new location for accountbar
        double high = [ [ UIScreen mainScreen ] bounds ].size.height;
        CGRect frame = self.accountView.frame;
        frame.origin.y = high-frame.size.height-self.titleView.frame.size.height;
        self.accountView.frame = frame;
    }
    
}
-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self clearAction];
    if (self.currentStock.symbol) {
        self.symbolLb.text =
        self.symbolTF.text = self.currentStock.symbol;
        [self listenToNewSymbol:self.currentStock.symbol];
        [self hideKeyboardAction];
        
    }
    
}
-(void)viewWillUnload {
    [super viewWillUnload];
    [self stopObserve];
}
-(void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self stopObserve];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - page control
static int totalPage = 4;
-(CGRect) pageFrameAtIndex:(NSUInteger) index {
    CGRect frame;
    frame.origin.x = self.scrollView.frame.size.width * index;
    frame.origin.y = 0;
    frame.size = self.scrollView.frame.size;
    NSLog(@"%f %d %f %f",frame.size.width*index,0,frame.size.width ,frame.size.height);
    return CGRectMake(frame.size.width*index,0,frame.size.width ,frame.size.height);
}

-(void) setUpPageControlUI {
    for (int i = 0; i < totalPage; i++) {
        
        if (i ==0 ) {
            [self.scrollView addSubview:self.quoteView.view];
        }
        else if (i == 1) {
            self.singleTickerView.view.frame = [self pageFrameAtIndex:i];
            [self.scrollView addSubview:self.singleTickerView.view];
        }
        else if (i == 2){
//            NSString *val = @"Graph ?";
            //garph here
//            UILabel *txt=[[UILabel alloc] initWithFrame:[self pageFrameAtIndex:i]];
//            txt.text = val;
//            txt.textColor = [UIColor blackColor];
//            txt.backgroundColor = [UIColor yellowColor];
//            txt.textAlignment = NSTextAlignmentCenter;
            
            self.chartView.view.frame = [self pageFrameAtIndex:i];
            [self.scrollView addSubview:self.chartView.view];


        } else if (i == 3) {
            
            NSString *val = @"News ?";
            
            
            UILabel *txt=[[UILabel alloc] initWithFrame:[self pageFrameAtIndex:i]];
            txt.text = val;
            txt.textColor = [UIColor blackColor];
            txt.backgroundColor = [UIColor blueColor];
            txt.textAlignment = NSTextAlignmentCenter;
            
            [self.scrollView addSubview:txt];
        }
        
	}
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width * totalPage, self.scrollView.frame.size.height);
    
}

- (void)scrollViewDidScroll:(UIScrollView *)sender {
	if (!self.pageControlBeingUsed) {
		// Switch the indicator when more than 50% of the previous/next page is visible
		CGFloat pageWidth = self.scrollView.frame.size.width;
		int page = floor((self.scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
		self.pageControl.currentPage = page;
	}
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
	self.pageControlBeingUsed = NO;
    if (self.pageControl.hidden) {
        [self showPageControl];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
	self.pageControlBeingUsed = NO;
    
}

-(void) showPageControl {
    self.pageControl.hidden = NO;
    self.pageControl.alpha = 1;
    [self performSelector:@selector(hidePageControl) withObject:nil afterDelay:3];
}

-(void)hidePageControl{
    /// @todo animate when fade in out
    if ( ! self.pageControl.hidden) {
        [UIView animateWithDuration:0.3 animations:^{
            self.pageControl.alpha = 0;
        } completion: ^(BOOL finished) {
            self.pageControl.hidden = YES;
        }];
    }
    
    
}

/// change when touch in page controll
- (IBAction)changePage {
	// Update the scroll view to the appropriate page
	CGRect frame;
	frame.origin.x = self.scrollView.frame.size.width * self.pageControl.currentPage;
	frame.origin.y = 0;
	frame.size = self.scrollView.frame.size;
	[self.scrollView scrollRectToVisible:frame animated:YES];
	
	self.pageControlBeingUsed = YES;
}
#pragma mark - public api
-(void)prepareDefaultStockName:(NSString *)stockName {
    self.currentStock = [self.appDelegate.stockModel.stocks objectForKey:stockName ];
}
#pragma mark - private method
//change symbol that this view will listen to
- (void) listenToNewSymbol:(NSString*) enteredSymbol {
    if (self.currentStock) {
        [self stopObserve];//try to remove previous symbol
    }
    
    self.currentStock = [self.appDelegate.stockModel.stocks objectForKey:enteredSymbol ];
    if (self.currentStock) {
//        [self.appDelegate pullUpdateForSymbol:enteredSymbol];
        [self.appDelegate.stockModel pullStock:enteredSymbol];
        [self.appDelegate.stockModel pullChartForStock:enteredSymbol];

        [self updateFromModel];
//        [self updatePort];
    } else {
        if ([enteredSymbol length] > 0) {
            [self alert:[NSString stringWithFormat:@"Stock %@ not found", enteredSymbol]];
        }
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"hasUpdate"]) {
        [self updateFromModel];
    } else if ([keyPath isEqualToString:@"lastTickerArrived"]) {
        [self updateLastTicker];
    }else if([keyPath isEqualToString:@"chartTimeSeries"]){
        
//        if (self.first == object) {
//            <#statements#>
//        } else if
        
        [self updateChartFromModel];
    }
}
-(void) stopObserve {
    @try {
        self.currentStock = nil;
        //NSLog(@"self.currentStock removeObserver:self forKeyPath:@'hasUpdate'");
    }
    @catch (NSException *exception) {
        NSLog(@"Fail to self.currentStock removeObserver:self forKeyPath:@'hasUpdate'");
        
    }
}

//-(EquityPort*) searchUserStock:(NSString*) stockName {
//    for (EquityPort* port in self.appDelegate.userModel.equityPorts) {
//        if ([port.stockName isEqualToString:stockName]) {
//            return port;
//        }
//    }
//    return nil;
//}
//-(void) updatePort {
//    EquityPort* port = [self searchUserStock:self.currentStock.symbol];
//    [self.accountLb setTextWithKeepingAttributes: self.appDelegate.userModel.investorNumber];
////    [self.cashOrCreditLb setTextWithKeepingAttributes: self.appDelegate.userModel.];
//    if (port) {
//        [self.onhandBtn.titleLabel setMinimumScaleFactor:5];
//        self.onhandBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
//        [self.onhandBtn setTextWithKeepingAttributes: [[NSNumber numberWithLongLong:port.sellable] volumeFormatStyle]];
//        [self.buyPriceLb setTextWithKeepingAttributes:[port.avgPrice portAvgFormat]];
//        
//    } else {
//        [self.onhandBtn setTextWithKeepingAttributes: @"-"];
//        [self.buyPriceLb setTextWithKeepingAttributes:@"-"];
//        
//    }
//}

-(void)updateLastTicker {
    [self.singleTickerView  addTickerTime:self.currentStock.lastTickerTimeStamp
                                     side:self.currentStock.lastTickerSide
                                    price:self.currentStock.price
                                   volume:self.currentStock.volume
                                   change:self.currentStock.change
                                    prior:self.currentStock.prior];
}
-(void)updateFromModel {
    [self.singleTickerView setTickersArray:self.currentStock.tickersArray prior:self.currentStock.prior];
    [self.stockFullname setTextAndBlink: self.currentStock.fullname];
    [self.symbolLb setTextAndBlink:self.currentStock.symbol theme:self.theme];
    [self.changeLb setTextAndBlink:[self.currentStock.change changeFormatStyle] theme:self.theme];
    [self.priceLb setTextAndBlink:[self.currentStock.price priceFormatStyle] theme:self.theme];
    [self.percentChangeLb setTextAndBlink:[NSString stringWithFormat:@"%@%%" , [self.currentStock.percentChange changeFormatStyle]] theme:self.theme];
    [self.priceLb  setColorWithPrior:self.currentStock.prior currentValue:self.currentStock.price withTheme:self.theme blink:YES];
    self.changeLb.textColor =
    self.percentChangeLb.textColor =
    self.symbolLb.textColor = self.priceLb.textColor ;
    
    //update QuoteView
    [self.quoteView.volbid1 setVolumeText:self.currentStock.volbid1
                                oldVolume:self.currentStock.volbid1Old theme:self.theme blink:YES];
     [self.quoteView.volbid2 setVolumeText:self.currentStock.volbid2
                                 oldVolume:self.currentStock.volbid2Old theme:self.theme blink:YES];
     [self.quoteView.volbid3 setVolumeText:self.currentStock.volbid3
                                 oldVolume:self.currentStock.volbid3Old theme:self.theme blink:YES];
     [self.quoteView.volbid4 setVolumeText:self.currentStock.volbid4
                                 oldVolume:self.currentStock.volbid4Old theme:self.theme blink:YES];
     [self.quoteView.volbid5 setVolumeText:self.currentStock.volbid5
                                 oldVolume:self.currentStock.volbid5Old theme:self.theme blink:YES];
    
    [self.quoteView.voloff1 setVolumeText:self.currentStock.voloffer1
                                oldVolume:self.currentStock.voloffer1Old theme:self.theme blink:YES];
    [self.quoteView.voloff2 setVolumeText:self.currentStock.voloffer2
                                oldVolume:self.currentStock.voloffer2Old theme:self.theme blink:YES];
    [self.quoteView.voloff3 setVolumeText:self.currentStock.voloffer3
                                oldVolume:self.currentStock.voloffer3Old theme:self.theme blink:YES];
    [self.quoteView.voloff4 setVolumeText:self.currentStock.voloffer4
                                oldVolume:self.currentStock.voloffer4Old theme:self.theme blink:YES];
    [self.quoteView.voloff5 setVolumeText:self.currentStock.voloffer5
                                oldVolume:self.currentStock.voloffer5Old theme:self.theme blink:YES];
    
    [self.quoteView.avg setColorWithPrior:self.currentStock.prior
                             currentValue:self.currentStock.averagePrice
                                withTheme:self.theme
                                    blink:YES];
    [self.quoteView.projected setColorWithPrior:self.currentStock.prior
                                   currentValue:self.currentStock.projectOpen];
    self.quoteView.projectedVol.text = [self.currentStock.projectVolume volumeFormatStyle];
    self.quoteView.projectedVol.textColor = self.theme.yellowColor;
    self.quoteView.projectedVol.backgroundColor = self.theme.defaultBackgroundColor;
    
    if ([self.currentStock.bid1 hasPrefix:@"A"]) {
        [self.quoteView.bid1 setTitle:self.currentStock.bid1 forState:UIControlStateNormal];
        [self.quoteView.bid1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    } else {
        [self.quoteView.bid1 setColorWithPrior: self.currentStock.prior currentValue:self.currentStock.bid1Number withTheme:self.theme blink:YES];
    }
    [self.quoteView.bid2 setColorWithPrior: self.currentStock.prior currentValue:self.currentStock.bid2 withTheme:self.theme blink:YES];
    [self.quoteView.bid3 setColorWithPrior: self.currentStock.prior currentValue:self.currentStock.bid3 withTheme:self.theme blink:YES];
    [self.quoteView.bid4 setColorWithPrior: self.currentStock.prior currentValue:self.currentStock.bid4 withTheme:self.theme blink:YES];
    [self.quoteView.bid5 setColorWithPrior: self.currentStock.prior currentValue:self.currentStock.bid5 withTheme:self.theme blink:YES];
    if ([self.currentStock.offer1 hasPrefix:@"A"]) {
        [self.quoteView.offer1 setTitle:self.currentStock.offer1 forState:UIControlStateNormal];
        [self.quoteView.offer1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    } else {
        [self.quoteView.offer1 setColorWithPrior: self.currentStock.prior currentValue:self.currentStock.offer1Number withTheme:self.theme blink:YES];
    }
    
    [self.quoteView.offer2 setColorWithPrior: self.currentStock.prior currentValue:self.currentStock.offer2 withTheme:self.theme blink:YES];
    [self.quoteView.offer3 setColorWithPrior: self.currentStock.prior currentValue:self.currentStock.offer3 withTheme:self.theme blink:YES];
    [self.quoteView.offer4 setColorWithPrior: self.currentStock.prior currentValue:self.currentStock.offer4 withTheme:self.theme blink:YES];
    [self.quoteView.offer5 setColorWithPrior: self.currentStock.prior currentValue:self.currentStock.offer5 withTheme:self.theme blink:YES];
    
    [self.quoteView.high setColorWithPrior:self.currentStock.prior currentValue:self.currentStock.high  withTheme:self.theme blink:YES];
    [self.quoteView.low setColorWithPrior:self.currentStock.prior currentValue:self.currentStock.low withTheme:self.theme blink:YES];
    [self.quoteView.prior setColorWithPrior:self.currentStock.prior currentValue:self.currentStock.prior withTheme:self.theme blink:YES];
    if (self.currentStock.averagePrice != nil) {
        [self.quoteView.avg setColorWithPrior:self.currentStock.prior currentValue:self.currentStock.averagePrice withTheme:self.theme blink:YES];
    } else {
        self.quoteView.avg.text = @"";
    }
    if (self.currentStock.projectOpen) {
        [self.quoteView.projected setColorWithPrior:self.currentStock.prior currentValue:self.currentStock.projectOpen withTheme:self.theme blink:YES];
        self.quoteView.projectedVol.text =  [self.currentStock.projectVolume volumeFormatStyle];
    } else {
        [self.quoteView.projected setText:@""];
        [self.quoteView.projectedVol setText:@""];
    }
    
    [self.currentStock updatePercentBidOfferVolume];
    [self.quoteView.graphBid1 setBidPercent:self.currentStock.percentBid1 theme:self.theme];
    [self.quoteView.graphBid2 setBidPercent:self.currentStock.percentBid2 theme:self.theme];
    [self.quoteView.graphBid3 setBidPercent:self.currentStock.percentBid3 theme:self.theme];
    [self.quoteView.graphBid4 setBidPercent:self.currentStock.percentBid4 theme:self.theme];
    [self.quoteView.graphBid5 setBidPercent:self.currentStock.percentBid5 theme:self.theme];
    
    [self.quoteView.graphOffer1 setOfferPercent:self.currentStock.percentOffer1 theme:self.theme];
    [self.quoteView.graphOffer2 setOfferPercent:self.currentStock.percentOffer2 theme:self.theme];
    [self.quoteView.graphOffer3 setOfferPercent:self.currentStock.percentOffer3 theme:self.theme];
    [self.quoteView.graphOffer4 setOfferPercent:self.currentStock.percentOffer4 theme:self.theme];
    [self.quoteView.graphOffer5 setOfferPercent:self.currentStock.percentOffer5 theme:self.theme];
    
}

-(void)updateChartFromModel
{
//    self.chartView.chartTimeSeries = [self.chartView.chartTimeSeries mutableCopy];
    [self.chartView setChartTimeSeries:self.currentStock.chartTimeSeries];
    [self.chartView setPrior:self.currentStock.prior];
    [self.chartView setHigh:self.currentStock.high];
    [self.chartView setLow:self.currentStock.low];
    [self.chartView createGraph];
    
}
#pragma mark - ui helper
-(void)alert:(NSString*) message {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:self.title
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles: nil];
    [alert show];
}
#pragma mark - search symbol

- (IBAction)searchDidBegin:(UITextField *)sender {
    self.searchTableView.hidden = NO;
    [self.searchTableView reloadData];

}
- (IBAction)searchTextDidChanged:(UITextField *)sender {
    self.searchTableView.hidden = NO;
    self.searchResultArray = [self.appDelegate.stockModel filterSymbolByName:sender.text.uppercaseString];
    [self.searchTableView reloadData];
}
- (IBAction)symbolEntered:(UITextField *)sender {
    NSString* enteredSymbol = [sender.text uppercaseString];
    [self listenToNewSymbol:enteredSymbol];
    sender.text = enteredSymbol;
    self.searchTableView.hidden = YES;
    [self hideKeyboardAction];
}
#pragma mark - table view delegate/datasource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([self.symbolTF.text length] == 0) {
        return [self.appDelegate.stockModel.stocksNameArray count];
    }
    return [self.searchResultArray count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *simpleTableIdentifier = @"searchResultCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    cell.backgroundColor = [UIColor whiteColor];
    if ([self.symbolTF.text length] > 0) {
        cell.textLabel.text = [self.searchResultArray objectAtIndex:indexPath.row];
    } else {
        cell.textLabel.text = [self.appDelegate.stockModel.stocksNameArray objectAtIndex:indexPath.row];
    }
    [self applyThemeOnCell:cell];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString* symbolName = nil;
    if ([self.symbolTF.text length] == 0 ) { //not search yet
        symbolName = [self.appDelegate.stockModel.stocksNameArray objectAtIndex:indexPath.row];
    } else { // when searching
        symbolName = [self.searchResultArray objectAtIndex:indexPath.row];
    }
    self.symbolTF.text = symbolName;
    [self listenToNewSymbol:symbolName];
    self.searchTableView.hidden = YES;
    [self hideKeyboardAction];
}

#pragma mark - text field action
-(void) updateEstimatePrice {
    double volume = [NSNumber numberWithString:self.volumeTF.text numberStyle:volumeFormatStyle ].doubleValue;
    double estimatePrice =[self.priceTF.text doubleValue] *  volume;
    self.estimatePrice.text = [NSNumber numberWithDouble:estimatePrice].priceFormatStyle;
}
- (IBAction)priceChange:(UITextField *)sender {
    if ([self.volumeTF.text length] > 0) {
        [self updateEstimatePrice];
    }
}
- (IBAction)volumeChange:(UITextField *)sender {
    if ([self.priceTF.text length] > 0) {
        [self updateEstimatePrice];
    }
}
- (IBAction)volumeBeginEdit:(UITextField *)sender {
    //remove comma from 1,000,000
    if ([sender.text length] > 0) {
        sender.text = [[NSNumber numberWithString:sender.text numberStyle:volumeFormatStyle] stringValue];
    }
}
- (IBAction)volumeEndEditing:(UITextField *)sender {
    if ([sender.text length] > 0) {
        sender.text = [NSNumber numberWithDouble:[sender.text doubleValue]].volumeFormatStyle;
    }
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField == self.priceTF) {
        return [NSNumber validatePriceTextField:textField charactersChangeRange:range replacementString:string];
    } else if (textField == self.volumeTF) {
        return [NSNumber validateVolumeTextField:textField charactersChangeRange:range replacementString:string];
    } else if (textField == self.icebergTF) {
        return [NSNumber validateVolumeTextField:textField charactersChangeRange:range replacementString:string];
    }
    return NO;
}
#pragma mark - Button action
- (IBAction)hideKeyboardAction {
    [self.symbolTF resignFirstResponder];
    [self.priceTF resignFirstResponder];
    [self.volumeTF resignFirstResponder];
    [self.icebergTF resignFirstResponder];
    [self.pinTF resignFirstResponder];
    self.searchTableView.hidden = YES;
}

- (IBAction)clearAction {
    self.symbolTF.text = @"";
    self.priceTF.text = @"";
    self.volumeTF.text = @"";
    self.icebergTF.text = @"";
    self.pinTF.text = @"";
    [self.estimatePrice setTextWithKeepingAttributes:@"-"];
    [self.symbolTF becomeFirstResponder];
}

- (IBAction)confirmAction {
}

- (IBAction)priceTypeAction:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:@"Price Type?"
                                  delegate:self
                                  cancelButtonTitle:@"Cancel"
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:@"ATO",@"ATC",
                                  @"MP", @"MO", @"ML", nil];
    
    actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
    actionSheet.tag = PRICE_TAG;
    [actionSheet showInView:self.view];
}

- (IBAction)dayAction:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:@"Day Type?"
                                  delegate:self
                                  cancelButtonTitle:@"Cancel"
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:@"DAY", @"FOK",
                                  @"IOC", nil];
    
    actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
    actionSheet.tag = DAY_TAG;
    [actionSheet showInView:self.view];
}

- (IBAction)sellableAction:(UIButton *)sender {
    self.volumeTF.text = sender.titleLabel.text;
    [self updateEstimatePrice];
}
- (IBAction)buyAction:(id)sender {
    
    [self.tradeStatusBackgroundView bringSubviewToFront:self.buyBackground];
    
    [self.sellBtn setBackgroundColor: [UIColor darkGrayColor]];
    [self.buyBtn setBackgroundColor:[UIColor colorWithRed:102.0/255.0 green:153.0/255.0 blue:1 alpha:1]];
    //self.buyBtn.backgroundColor = [UIColor clearColor];
    self.buyBtn.selected = YES;
    self.sellBtn.selected = NO;
}


- (IBAction)sellAction:(id)sender {
    
    [self.tradeStatusBackgroundView bringSubviewToFront:self.sellBackground];
    
    self.buyBtn.backgroundColor = [UIColor darkGrayColor];
    self.sellBtn.backgroundColor = [UIColor colorWithRed:233.0/255.0 green:92.0/255.0 blue:163.0/255.0 alpha:1];
    //self.sellBtn.backgroundColor = [UIColor clearColor];
    self.buyBtn.selected = NO;
    self.sellBtn.selected = YES;
}

- (IBAction)finishModalAction:(id)sender {
    /// @todo go back to call modal
}

- (IBAction)cashDetailAction:(id)sender {
    /// @todo load account detail
}

- (IBAction)accountSwitchAction:(id)sender {
    /// load switch account detail maybe actionsheet
}
#pragma mark - action sheet 
- (void)actionSheet:(UIActionSheet *)actionSheet willDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (actionSheet.tag == PRICE_TAG) {
        switch (buttonIndex) {
            case 0:
                self.priceTF.placeholder = @"ATO";
                break;
            case 1:
                self.priceTF.placeholder = @"ATC";
                break;
            case 2:
                self.priceTF.placeholder = @"MP";
                break;
            case 3:
                self.priceTF.placeholder = @"MO";
                break;
            case 4:
                self.priceTF.placeholder = @"ML";
                break;
            default:
                break;
        }
        self.priceTF.text = @"";
    } else if (actionSheet.tag == DAY_TAG) {
        switch (buttonIndex) {
            case 0:
                self.icebergTF.placeholder = @"Iceberg Vol";
                break;
            case 1:
                self.icebergTF.placeholder = @"FOK";
                self.icebergTF.text = @"";
                break;
            case 2:
                self.icebergTF.placeholder = @"IOC";
                self.icebergTF.text = @"";
                break;
            default:
                break;
        }
    }
}

#pragma mark - quote view delegate 
- (void)selectedPrice:(NSString *)priceString {
    if ([priceString isEqualToString:@"0"]) {
        return;
    }
    self.priceTF.text = priceString;
    [self updateEstimatePrice];
}

#pragma mark test chartcompare
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"compareChartSegue"]){
        CompareChartViewController * vc = [segue destinationViewController];
        vc.firstStock = self.currentStock;
    }
}
@end
