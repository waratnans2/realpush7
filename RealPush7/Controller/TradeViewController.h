//
//  TradeViewController.h
//  RealPush
//
//  Created by Sutean Rutjanalard on 20/9/2556 BE.
//  Copyright (c) 2556 Sutean Rutjanalard. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SlideViewController.h"
@interface TradeViewController : SlideViewController<UIScrollViewDelegate>
-(void) prepareDefaultStockName:(NSString*) stockName;

@end
