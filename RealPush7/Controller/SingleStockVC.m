//
//  SingleStockVC.m
//  RealPush
//
//  Created by Sutean Rutjanalard on 6/21/13.
//  Copyright (c) 2013 Sutean Rutjanalard. All rights reserved.
//

#import "SingleStockVC.h"
#import "AppDelegate.h"
#import "ITStockDetail.h"
#import "NSNumber+formatter.h"
#import "UILabel+blink.h"
#import "UIView+bidOfferGraph.h"
#import <QuartzCore/QuartzCore.h>

@interface SingleStockVC () <UISearchBarDelegate,UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UISearchBar *symbol;
@property (weak, nonatomic) IBOutlet UITableView *searchResultTableView;
@property (weak, nonatomic) IBOutlet UILabel *stockFullname;
@property (weak, nonatomic) IBOutlet UILabel *stockName;
@property (weak, nonatomic) IBOutlet UILabel *price;
@property (weak, nonatomic) IBOutlet UILabel *changeAndPercent;
@property (weak, nonatomic) IBOutlet UIView *changeBoxImage;

@property (weak, nonatomic) IBOutlet UILabel *labelForVolBid;
@property (weak, nonatomic) IBOutlet UILabel *labelForBid;
@property (weak, nonatomic) IBOutlet UILabel *labelForOffer;
@property (weak, nonatomic) IBOutlet UILabel *labelForVolOffer;
@property (weak, nonatomic) IBOutlet UIView *separateLine;
@property (weak, nonatomic) IBOutlet UILabel *date;
@property (weak, nonatomic) IBOutlet UILabel *time;

@property (weak, nonatomic) IBOutlet UILabel *bid1;
@property (weak, nonatomic) IBOutlet UILabel *bid2;
@property (weak, nonatomic) IBOutlet UILabel *bid3;
@property (weak, nonatomic) IBOutlet UILabel *bid4;
@property (weak, nonatomic) IBOutlet UILabel *bid5;
@property (weak, nonatomic) IBOutlet UIView *bid1graph;
@property (weak, nonatomic) IBOutlet UIView *bid2graph;
@property (weak, nonatomic) IBOutlet UIView *bid3graph;
@property (weak, nonatomic) IBOutlet UIView *bid4graph;
@property (weak, nonatomic) IBOutlet UIView *bid5graph;

@property (weak, nonatomic) IBOutlet UIView *offer1graph;
@property (weak, nonatomic) IBOutlet UIView *offer2graph;
@property (weak, nonatomic) IBOutlet UIView *offer3graph;
@property (weak, nonatomic) IBOutlet UIView *offer4graph;
@property (weak, nonatomic) IBOutlet UIView *offer5graph;
@property (weak, nonatomic) IBOutlet UIView *detailView;



@property (weak, nonatomic) IBOutlet UILabel *offer1;
@property (weak, nonatomic) IBOutlet UILabel *offer2;
@property (weak, nonatomic) IBOutlet UILabel *offer3;
@property (weak, nonatomic) IBOutlet UILabel *offer4;
@property (weak, nonatomic) IBOutlet UILabel *offer5;

@property (weak, nonatomic) IBOutlet UILabel *volbid1;
@property (weak, nonatomic) IBOutlet UILabel *volbid2;
@property (weak, nonatomic) IBOutlet UILabel *volbid3;
@property (weak, nonatomic) IBOutlet UILabel *volbid4;
@property (weak, nonatomic) IBOutlet UILabel *volbid5;

@property (weak, nonatomic) IBOutlet UILabel *voloffer1;
@property (weak, nonatomic) IBOutlet UILabel *voloffer2;
@property (weak, nonatomic) IBOutlet UILabel *voloffer3;
@property (weak, nonatomic) IBOutlet UILabel *voloffer4;
@property (weak, nonatomic) IBOutlet UILabel *voloffer5;

@property (weak, nonatomic) IBOutlet UIButton *dismissModalButton;


@property (nonatomic, strong) NSArray* searchResultArray;
@property (nonatomic, weak) AppDelegate* appDelegate;
@property (nonatomic, weak) ITStockDetail * currentStock;
@property (nonatomic, readwrite) BOOL isModalView;

-(void)updateFromModel;
@end

@implementation SingleStockVC
-(AppDelegate *)appDelegate {
    if (! _appDelegate) {
        _appDelegate = [[UIApplication sharedApplication] delegate];
    }
    return _appDelegate;
}
static int currentStockObservanceContext;
-(void)setCurrentStock:(ITStockDetail *)currentStock {
    [_currentStock removeObserver:self  forKeyPath:@"hasUpdate" context:&currentStockObservanceContext];
    _currentStock = currentStock;
    [currentStock addObserver:self forKeyPath:@"hasUpdate" options:NSKeyValueObservingOptionNew context:&currentStockObservanceContext];
}
-(void)prepareDefaultStockName:(NSString *)stockName {
    self.currentStock = [self.appDelegate.stockModel.stocks objectForKey:stockName ];
    
}
-(void)prepareDefaultStockName:(NSString *)stockName withModalDelegate:(id<SingleStockVCDelegate>)delegate {
    self.delegate = delegate;
    [self prepareDefaultStockName:stockName];
}
#pragma mark - view life cycle
-(void)applyTheme {
//    [super applyTheme];
    self.date.backgroundColor =
    self.time.backgroundColor =
    self.price.backgroundColor =
    self.stockFullname.backgroundColor =
    self.stockName.backgroundColor = self.theme.defaultBackgroundColor;
    self.stockName.textColor =
    self.stockFullname.textColor =
    self.date.textColor =
    self.time.textColor =
    self.price.textColor =
    self.bid1.textColor =
    self.bid2.textColor =
    self.bid3.textColor =
    self.bid4.textColor =
    self.bid5.textColor =
    self.offer1.textColor =
    self.offer2.textColor =
    self.offer3.textColor =
    self.offer4.textColor =
    self.offer5.textColor = self.theme.textColor;
    
    
    self.detailView.backgroundColor =
    self.labelForBid.backgroundColor =
    self.labelForOffer.backgroundColor =
    self.labelForVolBid.backgroundColor =
    self.labelForVolOffer.backgroundColor =
    self.volbid1.backgroundColor =
    self.volbid2.backgroundColor =
    self.volbid3.backgroundColor =
    self.volbid4.backgroundColor =
    self.volbid5.backgroundColor =
    self.voloffer1.backgroundColor =
    self.voloffer2.backgroundColor =
    self.voloffer3.backgroundColor =
    self.voloffer4.backgroundColor =
    self.voloffer5.backgroundColor =
    self.bid1.backgroundColor =
    self.bid2.backgroundColor =
    self.bid3.backgroundColor =
    self.bid4.backgroundColor =
    self.bid5.backgroundColor =
    self.offer1.backgroundColor =
    self.offer2.backgroundColor =
    self.offer3.backgroundColor =
    self.offer4.backgroundColor =
    self.offer5.backgroundColor = self.theme.lightBackgroundColor;
    
    self.labelForBid.textColor =
    self.labelForOffer.textColor =
    self.labelForVolBid.textColor =
    self.labelForVolOffer.textColor = self.theme.labelTextColor;
    self.volbid1.textColor =
    self.volbid2.textColor =
    self.volbid3.textColor =
    self.volbid4.textColor =
    self.volbid5.textColor =
    self.voloffer1.textColor =
    self.voloffer2.textColor =
    self.voloffer3.textColor =
    self.voloffer4.textColor =
    self.voloffer5.textColor = self.theme.yellowColor;
    self.searchResultTableView.backgroundColor = self.theme.tableBackgroundColor;
    self.separateLine.layer.cornerRadius =1;
    self.bid1graph.layer.cornerRadius =
    self.bid2graph.layer.cornerRadius =
    self.bid3graph.layer.cornerRadius =
    self.bid4graph.layer.cornerRadius =
    self.bid5graph.layer.cornerRadius =
    self.offer1graph.layer.cornerRadius =
    self.offer2graph.layer.cornerRadius =
    self.offer3graph.layer.cornerRadius =
    self.offer4graph.layer.cornerRadius =
    self.offer5graph.layer.cornerRadius = 2;
    self.bid1graph.backgroundColor =
    self.bid2graph.backgroundColor =
    self.bid3graph.backgroundColor =
    self.bid4graph.backgroundColor =
    self.bid5graph.backgroundColor = self.theme.cyanColor;
    self.offer1graph.backgroundColor =
    self.offer2graph.backgroundColor =
    self.offer3graph.backgroundColor =
    self.offer4graph.backgroundColor =
    self.offer5graph.backgroundColor = self.theme.magentaColor;
    self.changeBoxImage.layer.cornerRadius = 5;
}
-(void)viewWillUnload {
    [super viewWillUnload];
    [self stopObserve];
}
-(void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self stopObserve];
}
-(void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = YES;
    //Slide view title
    self.titleText = @"Quote";
    [self hideSearchTool];
}
-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (self.currentStock.symbol) {
        self.symbol.text = self.currentStock.symbol;
        [self listenToNewSymbol:self.currentStock.symbol];
    }
}
#pragma mark - ui helper
-(void)alert:(NSString*) message {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:self.title
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles: nil];
    [alert show];
}
#pragma mark - table view delegate/datasource
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    if (scrollView == self.searchResultTableView) {
        [self.symbol resignFirstResponder];//hide only keyboard
    }
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([self.symbol.text length] == 0) {
        return [self.appDelegate.stockModel.stocksNameArray count];
    }
    return [self.searchResultArray count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *simpleTableIdentifier = @"searchResultCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    if ([self.symbol.text length] > 0) {
        cell.textLabel.text = [self.searchResultArray objectAtIndex:indexPath.row];
    } else {
        cell.textLabel.text = [self.appDelegate.stockModel.stocksNameArray objectAtIndex:indexPath.row];
    }
    [self applyThemeOnCell:cell];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString* symbolName = nil;
    if ([self.symbol.text length] == 0 ) { //not search yet
        symbolName = [self.appDelegate.stockModel.stocksNameArray objectAtIndex:indexPath.row];
    } else { // when searching
        symbolName = [self.searchResultArray objectAtIndex:indexPath.row];
    }
    self.symbol.text = symbolName;
    [self listenToNewSymbol:symbolName];
    [self hideSearchTool];
}

#pragma mark - observer stuff
-(void) stopObserve {
    @try {
        self.currentStock = nil;
        NSLog(@"self.currentStock removeObserver:self forKeyPath:@'hasUpdate'");
    }
    @catch (NSException *exception) {
        NSLog(@"Fail to self.currentStock removeObserver:self forKeyPath:@'hasUpdate'");

    }
}
-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    [self showSearchTool];
    [self.searchResultTableView reloadData];
}
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    self.searchResultArray = [self.appDelegate.stockModel filterSymbolByName:searchText.uppercaseString];
    [self.searchResultTableView reloadData];
}
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    NSString* enteredSymbol = [self.symbol.text uppercaseString];
    [self listenToNewSymbol:enteredSymbol];
    [self hideSearchTool];
}
-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    //leave with not enter
}

- (IBAction)hideSearchView:(id)sender {
    [self hideSearchTool];
}

-(void) hideSearchTool {
    self.searchResultTableView.hidden = YES;
    //self.hideSearchButton.hidden = YES;
    [self.symbol resignFirstResponder];
}
-(void) showSearchTool {
    self.searchResultTableView.hidden = NO;
    //self.hideSearchButton.hidden = NO;
}

//change symbol that this view will listen to
- (void) listenToNewSymbol:(NSString*) enteredSymbol {
    if (self.currentStock) {
        [self stopObserve];//try to remove previous symbol
    }
    self.currentStock = [self.appDelegate.stockModel.stocks objectForKey:enteredSymbol ];
    if (self.currentStock) {
//        [self.appDelegate pullUpdateForSymbol:enteredSymbol];
        [self updateFromModel];
    } else {
        if ([enteredSymbol length] > 0) {
            [self alert:[NSString stringWithFormat:@"Stock %@ not found", enteredSymbol]];
        }
        
    }

}

-(void)updateFromModel {
    self.stockFullname.text = self.currentStock.fullname;
    self.stockName.text = self.currentStock.symbol;
    if ([self.currentStock.bid1 hasPrefix:@"A"]) {
        self.bid1.text = self.currentStock.bid1;
        self.bid1.textColor = self.theme.whiteColor;
    } else {
        if ([self.currentStock.bid1Number isEqualToNumber:self.currentStock.price]) {
            self.bid1.backgroundColor = self.theme.backgroundColorHighlightColor;
            self.offer1.backgroundColor = [UIColor clearColor];
        }
        [self.bid1 setColorWithPrior:self.currentStock.prior currentValue:self.currentStock.bid1Number withTheme:self.theme blink:YES];
    }
    [self.bid2 setColorWithPrior:self.currentStock.prior currentValue:self.currentStock.bid2 withTheme:self.theme blink:YES];
    [self.bid3 setColorWithPrior:self.currentStock.prior currentValue:self.currentStock.bid3 withTheme:self.theme blink:YES];
    [self.bid4 setColorWithPrior:self.currentStock.prior currentValue:self.currentStock.bid4 withTheme:self.theme blink:YES];
    [self.bid5 setColorWithPrior:self.currentStock.prior currentValue:self.currentStock.bid5 withTheme:self.theme blink:YES];
    if ([self.currentStock.offer1 hasPrefix:@"A"]) {
        self.offer1.text = self.currentStock.offer1;
        self.offer1.textColor = self.theme.whiteColor;
    } else {
        if ([self.currentStock.offer1Number isEqualToNumber:self.currentStock.price]) {
            self.offer1.backgroundColor = self.theme.backgroundColorHighlightColor;
            self.bid1.backgroundColor = [UIColor clearColor];
        }
        [self.offer1 setColorWithPrior:self.currentStock.prior currentValue:self.currentStock.offer1Number withTheme:self.theme blink:YES];
    }
    [self.offer2 setColorWithPrior:self.currentStock.prior currentValue:self.currentStock.offer2 withTheme:self.theme blink:YES];
    [self.offer3 setColorWithPrior:self.currentStock.prior currentValue:self.currentStock.offer3 withTheme:self.theme blink:YES];
    [self.offer4 setColorWithPrior:self.currentStock.prior currentValue:self.currentStock.offer4 withTheme:self.theme blink:YES];
    [self.offer5 setColorWithPrior:self.currentStock.prior currentValue:self.currentStock.offer5 withTheme:self.theme blink:YES];
    
    [self.volbid1 setVolumeText:self.currentStock.volbid1 oldVolume:self.currentStock.volbid1Old theme:self.theme blink:YES];
    [self.volbid2 setVolumeText:self.currentStock.volbid2 oldVolume:self.currentStock.volbid2Old theme:self.theme blink:YES];
    [self.volbid3 setVolumeText:self.currentStock.volbid3 oldVolume:self.currentStock.volbid3Old theme:self.theme blink:YES];
    [self.volbid4 setVolumeText:self.currentStock.volbid4 oldVolume:self.currentStock.volbid4Old theme:self.theme blink:YES];
    [self.volbid5 setVolumeText:self.currentStock.volbid5 oldVolume:self.currentStock.volbid5Old theme:self.theme blink:YES];
    
    [self.voloffer1 setVolumeText:self.currentStock.voloffer1 oldVolume:self.currentStock.voloffer1Old theme:self.theme blink:YES];
    [self.voloffer2 setVolumeText:self.currentStock.voloffer2 oldVolume:self.currentStock.voloffer2Old theme:self.theme blink:YES];
    [self.voloffer3 setVolumeText:self.currentStock.voloffer3 oldVolume:self.currentStock.voloffer3Old theme:self.theme blink:YES];
    [self.voloffer4 setVolumeText:self.currentStock.voloffer4 oldVolume:self.currentStock.voloffer4Old theme:self.theme blink:YES];
    [self.voloffer5 setVolumeText:self.currentStock.voloffer5 oldVolume:self.currentStock.voloffer5Old theme:self.theme blink:YES];
    
    [self.price setColorWithPrior:self.currentStock.prior currentValue:self.currentStock.price withTheme:self.theme blink:YES];
    self.stockName.textColor = self.price.textColor;
    if ( ! self.currentStock.change ) {
        self.changeBoxImage.backgroundColor = self.theme.lightBackgroundColor;
    } else if ( self.currentStock.change.doubleValue ==  0) {
        self.changeBoxImage.backgroundColor = self.theme.yellowColor;
    } else if ( self.currentStock.change.doubleValue > 0 ) {
        self.changeBoxImage.backgroundColor = self.theme.greenColor;
    } else {
        self.changeBoxImage.backgroundColor = self.theme.redColor;
    }
    self.changeAndPercent.text = [NSString stringWithFormat:@"%@ ( %@%% )", self.currentStock.change.changeFormatStyle , self.currentStock.percentChange.changeFormatStyle];
    [self.currentStock updatePercentBidOfferVolume];
    [self.bid1graph setBidPercent:self.currentStock.percentBid1 theme:self.theme];
    [self.bid2graph setBidPercent:self.currentStock.percentBid2 theme:self.theme];
    [self.bid3graph setBidPercent:self.currentStock.percentBid3 theme:self.theme];
    [self.bid4graph setBidPercent:self.currentStock.percentBid4 theme:self.theme];
    [self.bid5graph setBidPercent:self.currentStock.percentBid5 theme:self.theme];
    [self.offer1graph setOfferPercent:self.currentStock.percentOffer1 theme:self.theme];
    [self.offer2graph setOfferPercent:self.currentStock.percentOffer2 theme:self.theme];
    [self.offer3graph setOfferPercent:self.currentStock.percentOffer3 theme:self.theme];
    [self.offer4graph setOfferPercent:self.currentStock.percentOffer4 theme:self.theme];
    [self.offer5graph setOfferPercent:self.currentStock.percentOffer5 theme:self.theme];

}
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"hasUpdate"]) {
        [self updateFromModel];
        //NSLog(@"current stock price %@", self.currentStock.price);
    }
}

#pragma mark  - modal
- (IBAction)dismissModal:(id)sender {
    [self.delegate singleStockVCWillDismissModal];
    self.leftMenuButton.hidden = NO;
    self.rightMenuButton.hidden = NO;
    [self.dismissModalButton removeFromSuperview];
}


@end
