//
//  WatchViewController.m
//  RealPush
//
//  Created by Sutean Rutjanalard on 8/8/13.
//  Copyright (c) 2013 Sutean Rutjanalard. All rights reserved.
//

#import "WatchViewController.h"
#import "QuoteCell.h"
#import "AppDelegate.h"
#import "NSNumber+formatter.h"
#import "UILabel+blink.h"
#import "UIButton+blink.h"
#import "UIView+bidOfferGraph.h"
@interface WatchViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *valueBtn;
@property (weak, nonatomic) IBOutlet UIButton *volumeBtn;
@property (weak, nonatomic) IBOutlet UIButton *swingBtn;
@property (weak, nonatomic) IBOutlet UIButton *gainerBtn;
@property (weak, nonatomic) IBOutlet UIButton *loserBtn;
@property (weak, nonatomic) IBOutlet UIButton *mSETButton;
@property (weak, nonatomic) IBOutlet UIButton *mSet50Button;
@property (weak, nonatomic) IBOutlet UIButton *mSet100Button;
@property (weak, nonatomic) IBOutlet UIButton *mSetHDButton;
@property (weak, nonatomic) IBOutlet UIButton *mMaiButton;

//point to global model
@property (nonatomic, weak) ITStockModel* model;
//list that point to stock name
@property (nonatomic, weak) NSArray* mostList;
//list that point to ITStockDetail
@property (nonatomic, strong) NSArray* stockList;




///color for tab
@property (nonatomic, strong) UIColor* baseColor;
@property (nonatomic, strong) UIColor* selectedColor;

@property (nonatomic, weak) AppDelegate *appDelegate;
@property (nonatomic) MostType currentMostType;
@property (nonatomic, strong) NSString* currentMarketName;


- (IBAction)changeTab:(UIButton *)sender;
-(IBAction)changeMarket:(UIButton*)sender;
@end

@implementation WatchViewController
@synthesize model = _model;

-(AppDelegate *)appDelegate {
    if ( ! _appDelegate) {
        _appDelegate = [[UIApplication sharedApplication] delegate];
    }
    return _appDelegate;
}
static int mostListContext;
-(ITStockModel *)model {
    if ( ! _model) {
        _model = self.appDelegate.model;

        [_model addObserver:self forKeyPath:@"mostList"
                    options:NSKeyValueObservingOptionNew
                    context: &mostListContext];
    }
    return _model;
}
-(NSString *)currentMarketName {
    if ( ! _currentMarketName) {
        _currentMarketName = @"SET";
    }
    return _currentMarketName;
}
-(void)setStockList:(NSArray *)stockList {
    if (stockList != _stockList) {
        //remove all observer first
        for (ITStockDetail* stock  in _stockList) {
            [stock removeObserver:self forKeyPath:@"hasUpdate"];
        }
        _stockList = stockList;
        for (ITStockDetail* stock  in stockList) {
            [stock addObserver:self forKeyPath:@"hasUpdate" options:NSKeyValueObservingOptionNew context:nil];
            [self.appDelegate pullUpdateForSymbol:stock.symbol];//full fill prior , ceiling, floor , bid/offer
        }
    }
    
    
}
-(void)setModel:(ITStockModel *)model {
    [_model removeObserver:self forKeyPath:@"mostList" context:&mostListContext];
    _model = model;
    [model addObserver:self forKeyPath:@"mostList" options:NSKeyValueObservingOptionNew context:&mostListContext];
}
-(void)setMostList:(NSArray *)mostList {
    if (mostList != _mostList) {
        _mostList = mostList;
        
        //watch to new stock list
        NSMutableArray* newStockList = [NSMutableArray arrayWithCapacity:self.appDelegate.mostRows];
        for (NSString* stockName in _mostList) {
            ITStockDetail* stock = [self.model stockByName:stockName];
            [newStockList addObject:stock];
        }
        
        self.stockList = newStockList;
        
        
        [self.tableView reloadData];
    }
}

#pragma mark - viwe life
-(void)applyTheme {
    [super applyTheme];
    self.selectedColor = self.theme.selectedButtonBackgroundColor;
    self.baseColor = self.theme.defaultButtonColor;
    [self.valueBtn setTitleColor:self.theme.defaultButtonTextColor forState:UIControlStateNormal];
    [self.volumeBtn setTitleColor:self.theme.defaultButtonTextColor forState:UIControlStateNormal];
    [self.swingBtn setTitleColor:self.theme.defaultButtonTextColor forState:UIControlStateNormal];
    [self.gainerBtn setTitleColor:self.theme.defaultButtonTextColor forState:UIControlStateNormal];
    [self.loserBtn setTitleColor:self.theme.defaultButtonTextColor forState:UIControlStateNormal];
    [self.mSETButton setTitleColor:self.theme.defaultButtonTextColor forState:UIControlStateNormal];
    [self.mSet50Button setTitleColor:self.theme.defaultButtonTextColor forState:UIControlStateNormal];
    [self.mSet100Button setTitleColor:self.theme.defaultButtonTextColor forState:UIControlStateNormal];
    [self.mSetHDButton setTitleColor:self.theme.defaultButtonTextColor forState:UIControlStateNormal];
    [self.mMaiButton setTitleColor:self.theme.defaultButtonTextColor forState:UIControlStateNormal];
    
    self.valueBtn.backgroundColor = self.baseColor;
    self.mSETButton.backgroundColor = self.baseColor;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
	// Do any additional setup after loading the view.
    self.model = self.appDelegate.model;
    //SlideViewController API
    self.titleText = @"Watch";

}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self applyTheme];
    [self changeMarket:self.mSETButton]; // reset all color
    [self changeTab:self.valueBtn]; //set default most

    /// @todo use color from nib
    {
        //        self.selectedColor = self.valueBtn.backgroundColor;
        //        self.baseColor = self.volumeBtn.backgroundColor;
    }
}
-(void) viewWillDisappear:(BOOL)animated {
    // remove observer
    self.model = nil;
    self.stockList = nil;
    [super viewWillDisappear:animated];
}
-(void) viewWillUnload {
    //remove observer
    self.model = nil;
    self.stockList = nil;
    [super viewWillUnload];
}
- (void)viewDidUnload {
    [self setMSETButton:nil];
    [self setMSet50Button:nil];
    [self setMSet100Button:nil];
    [self setMSetHDButton:nil];
    [self setMMaiButton:nil];
    [super viewDidUnload];
}

#pragma mark - observer
-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (object == self.model) {
        if (self.model.mostType == self.currentMostType) {
            NSLog(@"most list CHANGE!!!");
            self.mostList = self.model.mostList;
        }
    } else if ([object isMemberOfClass:[ITStockDetail class]]) {
        ITStockDetail* stockDetail = object;
        
        NSUInteger rowIndex = [self.stockList indexOfObject:stockDetail];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:rowIndex inSection:0];

        QuoteCell* cell = (QuoteCell*)[self.tableView cellForRowAtIndexPath:indexPath];
        NSArray* visibleCells= [self.tableView visibleCells];
        for (QuoteCell *visibleCell in visibleCells) {
            if (visibleCell == cell) {
                [self updateCell:cell withStock:stockDetail withBlink:YES];
            }
        }
        
    }
   
}
-(void) updateCell:(QuoteCell*) cell withStock:(ITStockDetail*) stock withBlink:(BOOL) blink {
    
    cell.symbol.text = stock.symbol;
    cell.stockFullname.text = stock.fullname;
    [cell.price setColorWithPrior:stock.prior currentValue:stock.price withTheme:self.theme blink:blink];
    [cell.prior setColorWithPrior:stock.prior currentValue:stock.prior withTheme:self.theme blink:blink];
    [cell.change setTextWithKeepingAttributes: stock.change.changeFormatStyle];
    [cell.percentChange setTextWithKeepingAttributes:[NSString stringWithFormat:@"( %@%% )", stock.percentChange.changeFormatStyle]];
    cell.symbol.textColor  = cell.price.textColor;
    
    [cell.ceiling setColorWithPrior:stock.prior currentValue:stock.ceiling withTheme:self.theme blink:blink];
    [cell.floor setColorWithPrior:stock.prior currentValue:stock.floor withTheme:self.theme blink:blink];
    [cell.high setColorWithPrior:stock.prior currentValue:stock.high withTheme:self.theme blink:blink];
    [cell.low setColorWithPrior:stock.prior currentValue:stock.low withTheme:self.theme blink:blink];
    [cell.projectedPrice setColorWithPrior:stock.prior currentValue:stock.projectOpen withTheme:self.theme blink:blink];
   
    [cell.volumeBid1 setVolumeText:stock.volbid1 oldVolume:stock.volbid1Old theme:self.theme blink:blink];
    [cell.volumeBid2 setVolumeText:stock.volbid2 oldVolume:stock.volbid2Old theme:self.theme blink:blink];
    [cell.volumeBid3 setVolumeText:stock.volbid3 oldVolume:stock.volbid3Old theme:self.theme blink:blink];
    [cell.volumeBid4 setVolumeText:stock.volbid4 oldVolume:stock.volbid4Old theme:self.theme blink:blink];
    [cell.volumeBid5 setVolumeText:stock.volbid5 oldVolume:stock.volbid5Old theme:self.theme blink:blink];
    [cell.volumeOffer1 setVolumeText:stock.voloffer1 oldVolume:stock.voloffer1Old theme:self.theme blink:blink];
    [cell.volumeOffer2 setVolumeText:stock.voloffer2 oldVolume:stock.voloffer2Old theme:self.theme blink:blink];
    [cell.volumeOffer3 setVolumeText:stock.voloffer3 oldVolume:stock.voloffer3Old theme:self.theme blink:blink];
    [cell.volumeOffer4 setVolumeText:stock.voloffer4 oldVolume:stock.voloffer4Old theme:self.theme blink:blink];
    [cell.volumeOffer5 setVolumeText:stock.voloffer5 oldVolume:stock.voloffer5Old theme:self.theme blink:blink];
    if ([stock.bid1 hasPrefix:@"A"]) {
        [cell.bid1Button setTitle:stock.bid1 forState:UIControlStateNormal];
        [cell.bid1Button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    } else {
        [cell.bid1Button setColorWithPrior:stock.prior currentValue:stock.bid1Number withTheme:self.theme blink:blink];
    }
    [cell.bid2Button setColorWithPrior:stock.prior currentValue:stock.bid2 withTheme:self.theme blink:blink];
    [cell.bid3Button setColorWithPrior:stock.prior currentValue:stock.bid3 withTheme:self.theme blink:blink];
    [cell.bid4Button setColorWithPrior:stock.prior currentValue:stock.bid4 withTheme:self.theme blink:blink];
    [cell.bid5Button setColorWithPrior:stock.prior currentValue:stock.bid5 withTheme:self.theme blink:blink];
    if ([stock.offer1 hasPrefix:@"A"]) {
        [cell.offer1Button setTitle:stock.offer1 forState:UIControlStateNormal];
        [cell.offer1Button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    } else {
        [cell.offer1Button setColorWithPrior:stock.prior currentValue:stock.offer1Number withTheme:self.theme blink:blink];
    }
    [cell.offer2Button setColorWithPrior:stock.prior currentValue:stock.offer2 withTheme:self.theme blink:blink];
    [cell.offer3Button setColorWithPrior:stock.prior currentValue:stock.offer3 withTheme:self.theme blink:blink];
    [cell.offer4Button setColorWithPrior:stock.prior currentValue:stock.offer4 withTheme:self.theme blink:blink];
    [cell.offer5Button setColorWithPrior:stock.prior currentValue:stock.offer5 withTheme:self.theme blink:blink];
    if ( ! stock.change ) {
        cell.changeBackgroundImage.image = self.theme.imageChangeBoxWhite;
    } else if ( stock.change.doubleValue ==  0) {
        cell.changeBackgroundImage.image = self.theme.imageChangeBoxYellow;
    } else if ( stock.change.doubleValue > 0 ) {
        cell.changeBackgroundImage.image = self.theme.imageChangeBoxGreen;
    } else {
        cell.changeBackgroundImage.image = self.theme.imageChangeBoxRed;
    }
    [stock updatePercentBidOfferVolume];
    [cell.graphBid1 setBidPercent:stock.percentBid1 theme:self.theme];
    [cell.graphBid2 setBidPercent:stock.percentBid2 theme:self.theme];
    [cell.graphBid3 setBidPercent:stock.percentBid3 theme:self.theme];
    [cell.graphBid4 setBidPercent:stock.percentBid4 theme:self.theme];
    [cell.graphBid5 setBidPercent:stock.percentBid5 theme:self.theme];
    
    [cell.graphOffer1 setOfferPercent:stock.percentOffer1 theme:self.theme];
    [cell.graphOffer2 setOfferPercent:stock.percentOffer2 theme:self.theme];
    [cell.graphOffer3 setOfferPercent:stock.percentOffer3 theme:self.theme];
    [cell.graphOffer4 setOfferPercent:stock.percentOffer4 theme:self.theme];
    [cell.graphOffer5 setOfferPercent:stock.percentOffer5 theme:self.theme];
    
}

#pragma mark - Table view
-(void) applyThemeOnCell:(QuoteCell *)cell {
    [super applyThemeOnCell:cell];
    cell.volumeBid1.textColor =
    cell.volumeBid2.textColor =
    cell.volumeBid3.textColor =
    cell.volumeBid4.textColor =
    cell.volumeBid5.textColor =
    cell.volumeOffer1.textColor =
    cell.volumeOffer2.textColor =
    cell.volumeOffer3.textColor =
    cell.volumeOffer4.textColor =
    cell.volumeOffer5.textColor = self.theme.yellowColor;
    cell.symbol.backgroundColor =
    cell.stockFullname.backgroundColor =
    cell.price.backgroundColor =
    cell.labelForCeiling.backgroundColor =
    cell.labelForFloor.backgroundColor =
    cell.labelForHigh.backgroundColor =
    cell.labelForLow.backgroundColor =
    cell.labelForPrior.backgroundColor =
    cell.labelForProj.backgroundColor =
    cell.prior.backgroundColor =
    cell.high.backgroundColor =
    cell.low.backgroundColor =
    cell.ceiling.backgroundColor =
    cell.floor.backgroundColor = [UIColor clearColor];
    cell.stockFullname.textColor =
    cell.labelForCeiling.textColor =
    cell.labelForFloor.textColor =
    cell.labelForHigh.textColor =
    cell.labelForLow.textColor =
    cell.labelForPrior.textColor =
    cell.labelForProj.textColor =self.theme.labelTextColor;
    cell.bid1Button.backgroundColor =
    cell.bid2Button.backgroundColor =
    cell.bid3Button.backgroundColor =
    cell.bid4Button.backgroundColor =
    cell.bid5Button.backgroundColor =
    cell.offer1Button.backgroundColor =
    cell.offer2Button.backgroundColor =
    cell.offer3Button.backgroundColor =
    cell.offer4Button.backgroundColor =
    cell.offer5Button.backgroundColor = self.theme.defaultButtonColor;
    cell.projectedPrice.text = @"";
    cell.graphBid1.backgroundColor =
    cell.graphBid2.backgroundColor =
    cell.graphBid3.backgroundColor =
    cell.graphBid4.backgroundColor =
    cell.graphBid5.backgroundColor = self.theme.cyanColor;
    cell.graphOffer1.backgroundColor =
    cell.graphOffer2.backgroundColor =
    cell.graphOffer3.backgroundColor =
    cell.graphOffer4.backgroundColor =
    cell.graphOffer5.backgroundColor = self.theme.magentaColor;
    cell.graphBid1.layer.cornerRadius =
    cell.graphBid2.layer.cornerRadius =
    cell.graphBid3.layer.cornerRadius =
    cell.graphBid4.layer.cornerRadius =
    cell.graphBid5.layer.cornerRadius =
    cell.graphOffer1.layer.cornerRadius =
    cell.graphOffer2.layer.cornerRadius =
    cell.graphOffer3.layer.cornerRadius =
    cell.graphOffer4.layer.cornerRadius =
    cell.graphOffer5.layer.cornerRadius = 1;
    cell.backgroundColor = self.theme.tableBackgroundColor;
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.mostList count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    return  [self createCellforRowAtIndexPath:indexPath];
}

-(QuoteCell*) createCellforRowAtIndexPath:(NSIndexPath*) indexPath {
    static NSString* cellIdentifier = @"quoteCell";
    QuoteCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil){
        NSLog(@"can not create cell");
    }
    //custom cell configuration
    ITStockDetail* stock = [self.model stockByName:[self.mostList objectAtIndex:indexPath.row]];
    [self applyThemeOnCell:cell];
    [self updateCell:cell withStock:stock withBlink:NO];
    return cell;
}
#pragma mark - tab control
-(void) setSelectedTab:(UIButton*) sender {
    self.valueBtn.backgroundColor =
    self.volumeBtn.backgroundColor =
    self.swingBtn.backgroundColor =
    self.gainerBtn.backgroundColor =
    self.loserBtn.backgroundColor = self.baseColor;
    self.valueBtn.selected =
    self.volumeBtn.selected =
    self.swingBtn.selected =
    self.gainerBtn.selected =
    self.loserBtn.selected = NO;
    sender.backgroundColor = self.selectedColor;
    [sender setTitleColor:self.theme.selectedButtonTextColor forState:UIControlStateSelected];
    sender.selected = YES;
}
-(void) setSelectedMarketTab:(UIButton*) sender {
    self.mSETButton.backgroundColor =
    self.mSet50Button.backgroundColor =
    self.mSet100Button.backgroundColor =
    self.mSetHDButton.backgroundColor =
    self.mMaiButton.backgroundColor = self.baseColor;
    self.mSETButton.selected =
    self.mSet50Button.selected =
    self.mSet100Button.selected =
    self.mSetHDButton.selected =
    self.mMaiButton.selected = NO;
    sender.backgroundColor = self.selectedColor;
    [sender setTitleColor:self.theme.selectedButtonTextColor forState:UIControlStateSelected];
    sender.selected = YES;
}


- (IBAction)changeTab:(UIButton *)sender {
    NSLog(@"tab selected = %@", sender.titleLabel.text);
    [self setSelectedTab:sender];
    if (sender == self.valueBtn) {
        self.currentMostType = MostValue;
    } else if (sender == self.volumeBtn) {
        self.currentMostType = MostVolume;
    } else if (sender == self.swingBtn) {
        self.currentMostType = MostSwing;
    } else if (sender == self.gainerBtn) {
        self.currentMostType = MostGainer;
    } else if (sender == self.loserBtn) {
        self.currentMostType = MostLose;
    }
    [self.appDelegate pullForMost:self.currentMostType withMarketName:self.currentMarketName];

}
- (IBAction)changeMarket:(UIButton *)sender {
    NSLog(@"Market selected = %@", sender.titleLabel.text);
    self.currentMarketName = sender.titleLabel.text;
    [self setSelectedMarketTab:sender];
    [self.appDelegate pullForMost:self.currentMostType withMarketName:self.currentMarketName];
}

@end
