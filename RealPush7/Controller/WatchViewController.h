//
//  WatchViewController.h
//  RealPush
//
//  Created by Sutean Rutjanalard on 8/8/13.
//  Copyright (c) 2013 Sutean Rutjanalard. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SlideViewController.h"

@interface WatchViewController : SlideViewController<UITableViewDataSource, UITableViewDelegate>

@end
