//
//  LoginViewController.m
//  RealPush7
//
//  Created by Sutean Rutjanalard on 24/2/2557 BE.
//  Copyright (c) 2557 Sutean Rutjanalard. All rights reserved.
//

#import "LoginViewController.h"
#import <AFNetworking/AFNetworking.h>

@interface LoginViewController ()
@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UILabel *loginStatusLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityView;

@end
static NSString* hostURL = @"https://tradings.maybank-ke.co.th/KEAPI_S2/";
static NSString* productName = @"keitrade";
static NSString* loginPath = @"login.asp";
@implementation LoginViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}
- (IBAction)serverSegmentChanged:(id)sender {
    
}
- (IBAction)loginAction:(id)sender {
    NSString* username = self.usernameTextField.text;
    NSString* password = self.passwordTextField.text;
    NSDictionary* parameters = @{@"user":username,
                                 @"password": password,
                                 @"product": productName};
    AFHTTPRequestOperationManager* manager = [AFHTTPRequestOperationManager manager];
    manager.securityPolicy.allowInvalidCertificates = NO;
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSString* loginURL = [hostURL stringByAppendingString:loginPath];
    [manager POST:loginURL
       parameters:parameters
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              NSString* xmlString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];

              NSLog(@"output = %@", xmlString);
              
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"Error: %@", error);
          }];
}
- (IBAction)reloadAction:(id)sender {
    
}

@end
