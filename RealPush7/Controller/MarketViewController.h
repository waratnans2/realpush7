//
//  MarketViewController.h
//  RealPush
//
//  Created by Sutean Rutjanalard on 8/15/13.
//  Copyright (c) 2013 Sutean Rutjanalard. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SlideViewController.h"

@interface MarketViewController : SlideViewController <UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate>

@end
