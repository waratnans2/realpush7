//
//  PVChartMakerViewController.h
//  RealPush7
//
//  Created by Waratnan Suriyasorn on 5/20/2557 BE.
//  Copyright (c) 2557 Sutean Rutjanalard. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PNChartDelegate.h"
#import "ITUITheme.h"
@interface PVChartMakerViewController : UIViewController//<PNChartDelegate>
//@[ @[@"time", @"price", @"vol"] , @[...] , ... ]
@property (nonatomic, strong) NSArray * chartTimeSeries;
@property (nonatomic, strong) NSNumber* prior;
@property (nonatomic, strong) NSNumber* high;
@property (nonatomic, strong) NSNumber* low;

@property (nonatomic, weak) ITUITheme* theme;

-(void)createGraph;
@end
