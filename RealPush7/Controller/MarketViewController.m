//
//  MarketViewController.m
//  RealPush
//
//  Created by Sutean Rutjanalard on 8/15/13.
//  Copyright (c) 2013 Sutean Rutjanalard. All rights reserved.
//

#import "MarketViewController.h"
#import "ITMarketDetailView.h"
#import "ITStockModel.h"
#import "AppDelegate.h"
#import "MarketCell.h"
#import "ITEquityMarket.h"
#import "NSNumber+formatter.h"
#import "ITUITheme.h"
#import "UILabel+blink.h"
#import "StockFavoriteCell.h"
#import "SingleStockVC.h"
#import "UILabel+blink.h"
#import "ListOfStocksView.h"

@interface MarketViewController ()<SingleStockVCDelegate>
@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) IBOutlet UIPageControl *pageControl;

@property (nonatomic, strong) ITMarketDetailView* marketDetailView;
@property (nonatomic, strong) ListOfStocksView* listOfStocksView;

@property (nonatomic, readwrite) BOOL pageControlBeingUsed;
@property (weak, nonatomic) IBOutlet UITableView *mainTableView;
@property (nonatomic, strong) UITableView* stockListTableView;

@property (nonatomic, weak) ITStockModel* model;
@property (nonatomic, weak) AppDelegate* appDelegate;

@property (nonatomic, strong) NSArray* marketNames;
@property (nonatomic, strong) NSArray* industryNames;
@property (nonatomic, strong) NSArray* sectorNames;


@property (nonatomic, weak) NSString* currentMarketName;
@property (nonatomic, strong) NSString* currentStockName;

@property (nonatomic, weak) SingleStockVC* singleVC;

@end

@implementation MarketViewController
@synthesize marketNames = _marketNames;
static int totalPage = 3;

#pragma mark  - initial stuff
-(AppDelegate *)appDelegate {
    if ( ! _appDelegate) {
        _appDelegate  = [[UIApplication sharedApplication] delegate];
    }
    return _appDelegate;
}

-(ITStockModel *)model {
    if ( ! _model) {
        _model = self.appDelegate.stockModel;
    }
    return _model;
}
-(NSString *)currentMarketName {
    if ( ! _currentMarketName) {
        _currentMarketName = @"SET";
    }
    return _currentMarketName;
}
-(ListOfStocksView *)listOfStocksView {
    if ( ! _listOfStocksView) {
        _listOfStocksView = [self.storyboard instantiateViewControllerWithIdentifier:@"ListOfStocksView"];
    }
    return _listOfStocksView;
}
-(void)setIndustryNames:(NSArray *)industryNames {
    if (_industryNames != industryNames) {
        for (NSString* industryName in _industryNames) {
            ITIndustry* industry = [self.model.industries objectForKey:industryName];
            @try {
                [industry removeObserver:self forKeyPath:@"hasUpdate"];
                NSLog(@"remove %@ observer", industry.name);
            }
            @catch (NSException *exception) {
                NSLog(@"'Cannot remove an observer <MarketViewController> for the key path 'hasUpdate' from <ITIndustry> because it is not registered as an observer.'");
            }
            
        }
        _industryNames = industryNames;
        for (NSString* industryName in _industryNames) {
            ITIndustry* industry = [self.model.industries objectForKey:industryName];
            [industry addObserver:self forKeyPath:@"hasUpdate" options:NSKeyValueObservingOptionNew context:nil];
//            [self.appDelegate pullUpdateForIndexName:industryName];
        }
    }
}
-(void)setSectorNames:(NSArray *)sectorNames {
    if (_sectorNames != sectorNames) {
        for (NSString* sectorName in _sectorNames) {
            ITSector* sector =[self.model.sectors objectForKey:sectorName];
            [sector removeObserver:self forKeyPath:@"hasUpdate"];
        }
        _sectorNames = sectorNames;
        for (NSString* sectorName in _sectorNames) {
            ITSector* sector = [self.model.sectors objectForKey:sectorName];
            [sector addObserver:self forKeyPath:@"hasUpdate" options:NSKeyValueObservingOptionNew context:nil];
        }
    }
}
-(NSArray *)marketNames {
    if ( ! _marketNames) {
        _marketNames = self.model.marketNameArray;
        for (NSString* marketName in _marketNames) {
            ITEquityMarket* market = [self.model marketOrIndustryOrSectorByName:marketName];
            [market addObserver:self forKeyPath:@"hasUpdate" options:NSKeyValueObservingOptionNew context:nil];
//            [self.appDelegate pullUpdateForIndexName:marketName];
        }

    }
    return _marketNames;
}
-(void)setMarketNames:(NSArray *)marketNames {
    if (_marketNames != marketNames) {
        for (NSString* marketName in _marketNames) {
            ITEquityMarket* market = [self.model.markets objectForKey:marketName];
            NSLog(@"remove %@ observer", market.name);
            [market removeObserver:self forKeyPath:@"hasUpdate"];
        }
        _marketNames = marketNames;
        for (NSString* marketName in _marketNames) {
            ITEquityMarket* market = [self.model marketOrIndustryOrSectorByName:marketName];
            [market addObserver:self forKeyPath:@"hasUpdate" options:NSKeyValueObservingOptionNew context:nil];
//            [self.appDelegate pullUpdateForIndexName:marketName];
            
        }

    }
    
}

-(ITMarketDetailView *)marketDetailView {
    if ( ! _marketDetailView) {
        _marketDetailView = [self.storyboard instantiateViewControllerWithIdentifier:@"MarketDetailView"];
        [_marketDetailView setTheme: self.theme];
    }
    return _marketDetailView;
}


-(void) setupModelAndDatasource {
    self.marketNames = self.model.marketNameArray ;
    self.sectorNames = [[self.model.sectors allKeys] sortedArrayUsingSelector:@selector(localizedCompare:)];
;
    self.industryNames = [[self.model.industries allKeys] sortedArrayUsingSelector:@selector(localizedCompare:)];
}
#pragma mark - page control
-(CGRect) pageFrameAtIndex:(NSUInteger) index {
    CGRect frame;
    frame.origin.x = self.scrollView.frame.size.width * index;
    frame.origin.y = 0;
    frame.size = self.scrollView.frame.size;
    return CGRectMake(frame.size.width*index,0,frame.size.width ,frame.size.height);
    
}

-(void) setUpPageControlUI {
    for (int i = 0; i < totalPage; i++) {
        
        if (i ==0 ) {
            [self.scrollView addSubview:self.marketDetailView.view];
        }
        else if (i == 1) {
            self.stockListTableView = self.listOfStocksView.tableView;
            [self.stockListTableView setFrame:[self pageFrameAtIndex:i]];
            self.stockListTableView.delegate = self;
            self.stockListTableView.dataSource =self;
            self.stockListTableView.backgroundColor = self.theme.tableBackgroundColor;
            [self.scrollView addSubview:self.stockListTableView];
            
            
        }
        else if (i == 2){
            
            
            NSString *val = @"How about news? or other thing?";
            
            
            UILabel *txt=[[UILabel alloc] initWithFrame:[self pageFrameAtIndex:i]];
            txt.text = val;
            txt.textColor = [UIColor blackColor];
            txt.backgroundColor = [UIColor purpleColor];
            txt.textAlignment = NSTextAlignmentCenter;
            
            [self.scrollView addSubview:txt];
        }
        
	}
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width * totalPage, self.scrollView.frame.size.height);
    self.pageControl.pageIndicatorTintColor = self.theme.pageIndicatorTintColor;
    
    self.pageControl.currentPageIndicatorTintColor = self.theme.currentPageIndicatorTintColor;

}

- (void)scrollViewDidScroll:(UIScrollView *)sender {
	if (!self.pageControlBeingUsed) {
		// Switch the indicator when more than 50% of the previous/next page is visible
		CGFloat pageWidth = self.scrollView.frame.size.width;
		int page = floor((self.scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
		self.pageControl.currentPage = page;
	}
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    if (scrollView == self.scrollView) {
        self.pageControlBeingUsed = NO;
    }
	
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if (scrollView == self.scrollView) {
        self.pageControlBeingUsed = NO;
    }
}



/// change when touch in page controll
- (IBAction)changePage {
	// Update the scroll view to the appropriate page
	CGRect frame;
	frame.origin.x = self.scrollView.frame.size.width * self.pageControl.currentPage;
	frame.origin.y = 0;
	frame.size = self.scrollView.frame.size;
	[self.scrollView scrollRectToVisible:frame animated:YES];
	
	self.pageControlBeingUsed = YES;
}

#pragma mark - view life
-(void)applyTheme {
    [super applyTheme];
}
-(void) setupViewFrame {
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) { //move up for ios 7
        CGRect frame = self.scrollView.frame;
        frame.origin.y = frame.origin.y -20;
        self.scrollView.frame = frame;
        CGRect tableFrame = self.mainTableView.frame;
        tableFrame.size.height = tableFrame.size.height -20;
        self.mainTableView.frame = tableFrame;
        
        CGRect pageControlFrame = self.pageControl.frame;
        pageControlFrame.origin.y = pageControlFrame.origin.y - 20;
        self.pageControl.frame = pageControlFrame;
    }

    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"Market%@ did load", self);

}
-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    NSLog(@"Market%@ will appear", self);
    // Do any additional setup after loading the view.
    [self setupViewFrame];
    [self setUpPageControlUI];
    [self setupModelAndDatasource];
    self.mainTableView.layer.cornerRadius = 10;
    self.scrollView.layer.cornerRadius = 10;
    self.mainTableView.backgroundColor = [UIColor clearColor];
    self.view.backgroundColor = self.theme.defaultBackgroundColor;
    //Slide View title
    self.titleText = @"Market";
    [self.appDelegate.stockModel pullMarket];

}

-(void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.marketNames = nil;
    self.industryNames = nil;
    self.sectorNames = nil;
    NSLog(@"Market%@ will disappear", self);
}
-(void)viewWillUnload {
    [super viewWillUnload];
    self.marketNames= nil;
    self.industryNames = nil;
    self.sectorNames = nil;
    NSLog(@"Market%@ will unload", self);
        
}
-(void) viewDidAppear:(BOOL)animated   {
    [super viewDidAppear:animated];
    NSLog(@"Market%@ did appear",self);
    [self selectDefaultCell];
}
#pragma mark - table view
-(void) applyThemeOnCell:(UITableViewCell*) cell {
//    cell.backgroundView.backgroundColor = self.theme.tableBackgroundColor;
    
    cell.backgroundColor = self.theme.tableBackgroundColor;
    cell.selectedBackgroundView = [[UIImageView alloc] initWithImage:self.theme.tableBackgroundImage highlightedImage:self.theme.tableSelectedBackgroundImage];
    cell.contentView.backgroundColor = [UIColor clearColor];
    
    if ([cell isMemberOfClass:[MarketCell class]]) {
        MarketCell* marketCell = (MarketCell*)cell;
        marketCell.index.backgroundColor = self.theme.tableBackgroundColor;
        marketCell.change.backgroundColor = self.theme.tableBackgroundColor;
        [marketCell.change setTitleColor:self.theme.whiteColor forState:UIControlStateNormal];
    }
    else if ([cell isMemberOfClass:[StockFavoriteCell class]]) {
        StockFavoriteCell *fav = (StockFavoriteCell*)cell;
        fav.stockSymbol.textColor =
        fav.price.textColor = self.theme.whiteColor;
        [fav.change setTitleColor:self.theme.whiteColor forState:UIControlStateNormal];

    }

}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (tableView == self.mainTableView) {
        return 3; //Set index, Industry, Market
    } else if ( tableView == self.stockListTableView) {
        return 1;
    }
    return 0;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (tableView == self.mainTableView) {
        if (section == 0) {
            return @"Set index";
        } else if (section == 1) {
            return @"Industry";
        } else {
            return @"Sector";
        }
    } else if(tableView == self.stockListTableView){
        if ([self.currentMarketName isEqualToString:@"SET"]) {
            return @"All Stocks are in SET";
        }
        ITEquityMarket* market = [self.model marketOrIndustryOrSectorByName:self.currentMarketName];
        return [NSString stringWithFormat:@"Stocks in %@", market.fullname ];
    }
    return @"";
}
-(UITableViewCell *)tableView:(UITableView *)tableView
        cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == self.mainTableView) {
        return [self createCellforRowAtIndexPath:indexPath];
    } else if (tableView == self.stockListTableView) {
        return [self createStockCellForRowAtIndexPath:indexPath];
    }
    return nil;
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (tableView == self.mainTableView) {
        UIView *headerView = [[UIImageView alloc] initWithImage:self.theme.tableSectionImage];
        UILabel *tempLabel=[[UILabel alloc]initWithFrame:CGRectMake(10,0,320,20)];
        tempLabel.backgroundColor=[UIColor clearColor];
        tempLabel.textColor = self.theme.tableSectionTextColor;
        tempLabel.font = [UIFont boldSystemFontOfSize:17];
        tempLabel.text= [self tableView:self.mainTableView titleForHeaderInSection:section];
        [headerView addSubview:tempLabel];
        return headerView;
    } else {
        UIView *headerView = [[UIImageView alloc] initWithImage:self.theme.tableSectionImage];
        UILabel *tempLabel=[[UILabel alloc]initWithFrame:CGRectMake(10,0,320,20)];
        tempLabel.backgroundColor=[UIColor clearColor];
        tempLabel.textColor = self.theme.titleTextColor;
        tempLabel.font = [UIFont boldSystemFontOfSize:17];
        tempLabel.text= [self tableView:self.stockListTableView titleForHeaderInSection:section];
        [headerView addSubview:tempLabel];
        return headerView;
    }
   
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.mainTableView) {
        if (section == 0) {
            return [self.model.markets count];
        } else if (section == 1) {
            return [self.model.industries count];
        } else {
            return [self.model.sectors count];
        }

    } else if(tableView == self.stockListTableView){
        if ([self.currentMarketName isEqualToString:@"SET"]) {
            return 0;
        }
        ITEquityMarket* market = [self.model marketOrIndustryOrSectorByName:self.currentMarketName];
        NSLog(@"<Market> symbol in market= %d", market.stocks.count);
        
        return market.stocks.count;
    }
    return 0;
}

-(UITableViewCell* )createStockCellForRowAtIndexPath:(NSIndexPath*) indexPath {
    static NSString* cellIdentifier = @"favoriteCell";
    StockFavoriteCell *cell = [self.stockListTableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil){
        
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"StockFavoriteCell" owner:nil options:nil];
        
        for(id currentObject in topLevelObjects)
        {
            if([currentObject isKindOfClass:[StockFavoriteCell class]])
            {
                cell = (StockFavoriteCell *)currentObject;
                break;
            }
        }
    }

    ITEquityMarket* market = [self.model marketOrIndustryOrSectorByName:self.currentMarketName];
    ITStockDetail* stock = [market.stocks objectAtIndex:indexPath.row];
    [self applyThemeOnCell:cell];
    cell.stockSymbol.text = stock.symbol;
    cell.description.text = stock.fullname;
    cell.description.textColor = self.theme.defaultButtonTextColor;
    if (stock.prior.doubleValue > 0) {
        [cell.price  setColorWithPrior:stock.prior
                          currentValue:stock.price withTheme:self.theme blink:NO];
        cell.stockSymbol.textColor  = cell.price.textColor;
//        if ([stock.change doubleValue] > 0) {
//            cell.changeBox.image =self.theme.tableButtonGreenImage;
//        } else if (stock.change.doubleValue < 0) {
//            cell.changeBox.image =self.theme.tableButtonRedImage;
//        } else if (stock.change.doubleValue == 0) {
//            cell.changeBox.image = self.theme.tableButtonYellowImage;
//        }
        [cell.change setTitle:[NSString stringWithFormat:@"%@ ( %@%%)", stock.change.changeFormatStyle, stock.percentChange.changeFormatStyle] forState:UIControlStateNormal];

    } else {
        cell.price.text = @"-";
        [cell.change setTitle:@"-" forState:UIControlStateNormal];
//        cell.changeBox.image = self.theme.tableButtonWhiteImage;
        
    }
    
    
    return cell;
}
-(MarketCell*) createCellforRowAtIndexPath:(NSIndexPath*) indexPath {
    static NSString* cellIdentifier = @"marketCell";
    MarketCell *cell = [self.mainTableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil){
        NSLog(@"cell == nil");
    }
    //custom cell configuration
    [self applyThemeOnCell:cell];

    if (indexPath.section == 0) { // SET INDEX
        NSString* indexName = [self.marketNames objectAtIndex:indexPath.row];
        ITEquityMarket* market = [self.model.markets objectForKey:indexName];
        [self updateCell:cell withMarket:market];

    } else if (indexPath.section == 1) { // Industry
        NSString* industryName = [self.industryNames objectAtIndex:indexPath.row];
        ITIndustry* industry = [self.model.industries objectForKey:industryName];
        [self updateCell:cell withMarket:industry];
    } else  { // Sector
        NSString* sectorName = [self.sectorNames objectAtIndex:indexPath.row];
        ITSector* sector = [self.model.sectors objectForKey:sectorName];
        [self updateCell:cell withMarket:sector];
    }
    return cell;
}
/// @todo update detail view
-(void) updateDetailViewWithCurrentMarketName {
    ITEquityMarket* market = [self.model marketOrIndustryOrSectorByName:self.currentMarketName];
    if (market.prior.doubleValue > 0 ) {
        self.marketDetailView.prior.textColor = self.theme.yellowColor;
    } else {
        self.marketDetailView.prior.textColor = self.theme.whiteColor;
    }
    self.marketDetailView.status.text = self.appDelegate.stockModel.marketStatus;
    self.marketDetailView.prior.text = [market.prior priceFormatStyle];
    
    self.marketDetailView.high.text = market.high.priceFormatStyle;
    [self.marketDetailView.high setColorWithPrior:market.prior currentValue:market.high withTheme:self.theme blink:YES];
    self.marketDetailView.low.text = market.low.priceFormatStyle;
    [self.marketDetailView.low setColorWithPrior:market.prior currentValue:market.low withTheme:self.theme blink:YES];

    self.marketDetailView.change.text = market.change.changeFormatStyle;
    self.marketDetailView.percentChange.text = [market.percentChange changeFormatStyle];
    if ([market.change doubleValue] > 0) {
        self.marketDetailView.change.textColor =
        self.marketDetailView.percentChange.textColor = self.theme.greenColor;
    } else if ([market.change doubleValue] < 0) {
        self.marketDetailView.change.textColor =
        self.marketDetailView.percentChange.textColor = self.theme.redColor;
    } else {
        self.marketDetailView.change.textColor =
        self.marketDetailView.percentChange.textColor = self.theme.yellowColor;
    }
    self.marketDetailView.advance.text = market.advance;
    self.marketDetailView.unchange.text = market.unchange;
    self.marketDetailView.decline.text = market.decline;
    self.marketDetailView.timestamp.text = market.indexTime;
    self.marketDetailView.value.text = [market.totalValue volumeFormatStyle];
    self.marketDetailView.volume.text = [market.totalVolume volumeFormatStyle];
    
    self.marketDetailView.buyVolume.text = [market.buyVolume volumeFormatStyle];
    self.marketDetailView.sellVolume.text = [market.sellVolume volumeFormatStyle];
}
-(void) selectDefaultCell {
    NSIndexPath *indexPath=[NSIndexPath indexPathForRow:0 inSection:0];
    [self.mainTableView selectRowAtIndexPath:indexPath animated:NO
                              scrollPosition:UITableViewScrollPositionBottom];
    self.currentMarketName = [self.marketNames objectAtIndex:0];
    
    MarketCell* cell = (MarketCell*)[self.mainTableView cellForRowAtIndexPath:indexPath];
    cell.change.selected = NO;
    cell.change.highlighted  = NO;
    [self updateDetailViewWithCurrentMarketName];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.mainTableView) {
        if (indexPath.section == 0) {
            self.currentMarketName = [self.marketNames objectAtIndex:indexPath.row];
        } else if (indexPath.section == 1) {
            self.currentMarketName = [self.industryNames objectAtIndex:indexPath.row];
        } else if (indexPath.section == 2) {
            self.currentMarketName = [self.sectorNames objectAtIndex:indexPath.row];
        } else {
            NSLog(@"no market name at indexpath = %@", indexPath);
        }
        MarketCell* cell = (MarketCell*)[tableView cellForRowAtIndexPath:indexPath];
        cell.change.selected = NO;
        cell.change.highlighted  = NO;
        [self updateDetailViewWithCurrentMarketName];
//        ITEquityMarket* market = [self.model marketOrIndustryOrSectorByName:self.currentMarketName];
//        NSLog(@"<Market> %@ stock='%@'", market.name, market.stocks);
        [self.stockListTableView reloadData];
    } else if (self.stockListTableView) {
        StockFavoriteCell* cell = (StockFavoriteCell*)[tableView cellForRowAtIndexPath:indexPath];
        self.currentStockName = cell.stockSymbol.text;
        

        
    }
}
-(void)updateCell:(MarketCell*) cell withMarket:(ITEquityMarket*)market {
    
    cell.symbol.text = market.name;

    //set on nib not work
    cell.symbol.font = [UIFont systemFontOfSize:24];
    cell.symbol.numberOfLines = 1;
    cell.symbol.minimumScaleFactor = 0.5;
    cell.symbol.adjustsFontSizeToFitWidth = YES;
    cell.symbol.textColor = self.theme.tableDefaultTextColor;
    
    [cell.index setColorWithPrior:market.prior currentValue:market.indexValue withTheme:self.theme blink:YES];
    
    cell.index.font = [UIFont systemFontOfSize:20];
    cell.index.numberOfLines = 1;
    cell.index.minimumScaleFactor = 0.5;
    cell.index.adjustsFontSizeToFitWidth = YES;
    
    [cell.change setTitle:[market.change changeFormatStyle] forState:UIControlStateNormal];
    if ([market.change doubleValue] > 0) {
        cell.index.textColor = self.theme.greenColor;
        [cell.change setBackgroundImage:self.theme.tableButtonGreenImage forState:UIControlStateNormal];
    } else if ([market.change doubleValue] < 0) {
        cell.index.textColor = self.theme.redColor;
        [cell.change setBackgroundImage:self.theme.tableButtonRedImage forState:UIControlStateNormal];

    } else if([market.prior doubleValue] == 0) {
        cell.index.textColor = self.theme.whiteColor;
        [cell.change setBackgroundImage:self.theme.tableButtonWhiteImage forState:UIControlStateNormal];

    } else {
        cell.index.textColor =  self.theme.yellowColor;
        [cell.change setBackgroundImage:self.theme.tableButtonYellowImage forState:UIControlStateNormal];
    }

}
-(void) updateAtRowIndex:(int) rowIndex
               inSection:(int) section
              withMarket:(ITEquityMarket*) market {
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:rowIndex inSection:section];
    if ([market.name isEqualToString:self.currentMarketName]) {
        [self updateDetailViewWithCurrentMarketName];
    }
    
    MarketCell* cell = (MarketCell*)[self.mainTableView cellForRowAtIndexPath:indexPath];
    if (cell) {
        [self updateCell:cell withMarket:market];
    }

}
#pragma mark - observer

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([object isKindOfClass:[ITEquityMarket class]]) {
        ITEquityMarket* market = object;
        
        NSUInteger rowIndex = [self.marketNames indexOfObject:market.name];
        if (rowIndex != NSNotFound) {
            [self updateAtRowIndex:(int)rowIndex inSection:0 withMarket:market];
        }
        rowIndex = [self.industryNames indexOfObject:market.name];
        if (rowIndex != NSNotFound) {
            [self updateAtRowIndex:(int)rowIndex inSection:1 withMarket:market];
        }
        rowIndex = [self.sectorNames indexOfObject:market.name];
        if (rowIndex != NSNotFound) {
            [self updateAtRowIndex:(int)rowIndex inSection:2 withMarket:market];
        } else {
            //NSLog(@"No row for index name = %@", market.name);
        }
        
    }
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"market2quote"] ) {
        self.singleVC = segue.destinationViewController;
        [self.singleVC prepareDefaultStockName:self.currentStockName withModalDelegate:self];
    }
    
}
-(void)singleStockVCWillDismissModal {
    [self.singleVC dismissViewControllerAnimated:YES completion:^(){
        
    }];
}





@end
