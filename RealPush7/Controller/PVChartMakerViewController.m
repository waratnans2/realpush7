//
//  PVChartMakerViewController.m
//  RealPush7
//
//  Created by Waratnan Suriyasorn on 5/20/2557 BE.
//  Copyright (c) 2557 Sutean Rutjanalard. All rights reserved.
//

#import "PVChartMakerViewController.h"
#import "PNChart.h"
#import "PNLineChartData.h"
#import "PNLineChartDataItem.h"
#import "AppDelegate.h"
#import "CompareChartViewController.h"
@interface PVChartMakerViewController ()

@property (nonatomic, weak) AppDelegate* appDelegate;
@property (nonatomic, strong) PNLineChart * lineChart;
@property (nonatomic, strong) PNBarChart * barChart;
@end

@implementation PVChartMakerViewController
-(ITUITheme *)theme {
    if (!_theme) {
        _theme = ((AppDelegate*)[[UIApplication sharedApplication] delegate]).theme;
    }
    return _theme;
}

-(PNLineChart*)lineChart
{
    if(!_lineChart){
        _lineChart = [[PNLineChart alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 170.0)];
        _lineChart.yLabelFormat = @"%1.2f";
        _lineChart.backgroundColor = self.theme.chartBackgroundColor;
        _lineChart.showLabelTimeMode = YES;
//        _lineChart.delegate = self;
        _lineChart.lineWidth = 1;
        _lineChart.yLabelNum = 3;
        _lineChart.axisWidth = 1;
        _lineChart.labelTextColor = self.theme.lineChartlabelColor;
        [self.view addSubview:_lineChart];
    }
    return _lineChart;
}

-(PNBarChart*)barChart
{
    if(!_barChart){
        _barChart = [[PNBarChart alloc] initWithFrame:CGRectMake(0, 110, self.view.frame.size.width, 60.0)];
        _barChart.yLabelFormatter = ^(CGFloat yValue){
            CGFloat yValueParsed = yValue;
            NSString * labelText = [NSString stringWithFormat:@"%1.f",yValueParsed];
            return labelText;
        };
        _barChart.backgroundColor = [UIColor clearColor];
        _barChart.barBackgroundColor = [UIColor clearColor];
        _barChart.strokeColor = PNTwitterColor;
        _barChart.yLabelSum = 3;
        _barChart.barRadius = 0.0;
        _barChart.labelFont = [UIFont systemFontOfSize:6.0f];
        _barChart.labelTextColor = self.theme.barChartlabelColor;
//        _barChart.delegate = self;
        [self.view addSubview:_barChart];
    }
    return _barChart;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view setBackgroundColor:[UIColor blackColor]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)createGraph
{
    [_lineChart removeFromSuperview];
    _lineChart = nil;
    [_barChart removeFromSuperview];
    _barChart = nil;
    
    NSMutableArray * times = [[NSMutableArray alloc]init];
    NSMutableArray * priceData = [[NSMutableArray alloc]init];
    NSMutableArray * volData = [[NSMutableArray alloc]init];
    
    for (NSArray * data in self.chartTimeSeries) {
        
        [times addObject:data[0]];
        [priceData addObject:data[1]];
        [volData addObject:data[2]];
    }
    
    [self.lineChart setPriorValue:[self.prior floatValue]];
//    [self.lineChart setXLabelSkip:60];
    
    [self.lineChart setXLabels:times];
    PNLineChartData *data01 = [PNLineChartData new];
    data01.color = PNWeiboColor;
    data01.itemCount = priceData.count;
    data01.getData = ^(NSUInteger index) {
        CGFloat yValue = [priceData[index] floatValue];
        return [PNLineChartDataItem dataItemWithY:yValue];
    };
    
    float yMax = fmaxf([self.high floatValue], [self.prior floatValue]);
    float yMin = fminf([self.low floatValue], [self.prior floatValue]);
    
    [self.lineChart setYValueMax:yMax];
    [self.lineChart setYValueMin:yMin];
    
    if(yMax*100 - yMin*100 <3)
          [self.lineChart setYLabelNum:2];
    self.lineChart.chartData = @[data01];
    
    [self.lineChart strokeChart];
//    [self.barChart setBarWidth:0.8];
    [self.barChart setBarWidth:(int)self.lineChart.chartCavanWidth/(int)(volData.count+1)];
    [self.barChart setXLabels:times];
    [self.barChart setYValues:volData];
    [self.barChart setPointXArray:self.lineChart.pointXArray];
    [self.barChart strokeChart];

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"compareChartSegue"]){
        CompareChartViewController * vc = [segue destinationViewController];
        
    }
}
*/
//#pragma mark chart delegate
//
//-(void)userClickedOnLineKeyPoint:(CGPoint)point lineIndex:(NSInteger)lineIndex andPointIndex:(NSInteger)pointIndex{
//    NSLog(@"Click Key on line %f, %f line index is %d and point index is %d",point.x, point.y,(int)lineIndex, (int)pointIndex);
//    
//}
//
//-(void)userClickedOnLinePoint:(CGPoint)point lineIndex:(NSInteger)lineIndex{
//    NSLog(@"Click on line %f, %f, line index is %d",point.x, point.y, (int)lineIndex);
//}
//
//- (void)userClickedOnBarCharIndex:(NSInteger)barIndex
//{
//    NSLog(@"Click on bar %@", @(barIndex));
//}

@end
