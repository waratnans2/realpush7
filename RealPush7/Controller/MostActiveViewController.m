//
//  MostActiveViewController.m
//  RealPush7
//
//  Created by Sutean Rutjanalard on 12/5/2557 BE.
//  Copyright (c) 2557 Sutean Rutjanalard. All rights reserved.
//

#import "MostActiveViewController.h"
#import "AppDelegate.h"
#import "NSNumber+formatter.h"
@interface MostActiveViewController () <UITableViewDataSource, UITableViewDelegate>
@property (nonatomic,weak) AppDelegate* appDelegate;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segment;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation MostActiveViewController

-(AppDelegate *)appDelegate {
    if ( ! _appDelegate) {
        _appDelegate = [[UIApplication sharedApplication] delegate];
    }
    return _appDelegate;
}
-(void)viewDidLoad {
    [super viewDidLoad];
    [self.appDelegate.stockModel pullMost:MostValue];
    self.segment.selectedSegmentIndex = 0;
}
-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.appDelegate.stockModel addObserver:self
                                  forKeyPath:@"mostList"
                                     options:NSKeyValueObservingOptionNew
                                     context:nil];
}
-(void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.appDelegate.stockModel removeObserver:self
                                     forKeyPath:@"mostList"];
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.appDelegate.stockModel.mostList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString* cellIdentifier = @"MostCell";
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"MostCell"];
    if ( ! cell) {
        NSLog(@"Can not create cell using identifier %@", cellIdentifier);
    }
    ITStockDetail *stockDetail = self.appDelegate.stockModel.mostList[indexPath.row];
    cell.textLabel.text = stockDetail.symbol;
    cell.detailTextLabel.text  = [NSString stringWithFormat:@"price: %@", stockDetail.price.priceFormatStyle];
    return cell;
}
- (IBAction)segmentChanged:(UISegmentedControl *)sender {
    if (sender.selectedSegmentIndex == 0) {
        [self.appDelegate.stockModel pullMost:MostValue];
    } else if (sender.selectedSegmentIndex == 1) {
        [self.appDelegate.stockModel pullMost:MostVolume];
    } else if (sender.selectedSegmentIndex == 2){
        [self.appDelegate.stockModel pullMost:MostSwing];
    } else if (sender.selectedSegmentIndex == 3) {
        [self.appDelegate.stockModel pullMost:MostGainer];
    } else if (sender.selectedSegmentIndex == 4) {
        [self.appDelegate.stockModel pullMost:MostSwing];
    }
}
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"mostList"]) {
        [self.tableView reloadData];
    }
}

@end
