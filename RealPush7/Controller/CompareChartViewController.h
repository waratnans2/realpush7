//
//  CompareChartViewController.h
//  RealPush7
//
//  Created by Waratnan Suriyasorn on 5/22/2557 BE.
//  Copyright (c) 2557 Sutean Rutjanalard. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface CompareChartViewController : UIViewController 

@property (nonatomic, weak) ITStockDetail * firstStock;
@property (nonatomic, weak) ITStockDetail * secondStock;
@property (nonatomic, weak) ITUITheme* theme;

@property (weak, nonatomic) IBOutlet UILabel *symbol1;
@property (weak, nonatomic) IBOutlet UILabel *value1;
@property (weak, nonatomic) IBOutlet UILabel *symbol2;
@property (weak, nonatomic) IBOutlet UILabel *value2;

@end
