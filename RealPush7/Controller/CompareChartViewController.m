//
//  CompareChartViewController.m
//  RealPush7
//
//  Created by Waratnan Suriyasorn on 5/22/2557 BE.
//  Copyright (c) 2557 Sutean Rutjanalard. All rights reserved.
//

#import "CompareChartViewController.h"
#import "PNChart.h"
#import "PNLineChartData.h"
#import "PNLineChartDataItem.h"

@interface CompareChartViewController ()<PNChartDelegate>
{
    float yMax;
    float yMin;
}
@property (nonatomic, weak) AppDelegate* appDelegate;
@property (nonatomic, strong) PNLineChart * lineChart;

@end

@implementation CompareChartViewController
-(ITUITheme *)theme {
    if (!_theme) {
        _theme = ((AppDelegate*)[[UIApplication sharedApplication] delegate]).theme;
    }
    return _theme;
}
-(AppDelegate *)appDelegate {
    if (! _appDelegate) {
        _appDelegate = [[UIApplication sharedApplication] delegate];
    }
    return _appDelegate;
}

-(PNLineChart*)lineChart
{
    if(!_lineChart){
        _lineChart = [[PNLineChart alloc] initWithFrame:CGRectMake(5, 60, self.view.frame.size.height, 240.0)];
        _lineChart.yLabelFormat = @"%1.2f";
        _lineChart.backgroundColor = self.theme.chartBackgroundColor;
        _lineChart.showLabelTimeMode = YES;
        _lineChart.delegate = self;
        _lineChart.lineWidth = 2;
        _lineChart.yLabelNum = 3;
        _lineChart.labelFont = [UIFont systemFontOfSize:9.0];
        _lineChart.labelTextColor = self.theme.lineChartlabelColor;
        [self.view addSubview:_lineChart];
    }
    return _lineChart;
}

static int currentStockObservanceContext;
-(void)setSecondStock:(ITStockDetail *)secondStock {
    
    [_secondStock removeObserver:self forKeyPath:@"chartTimeSeries" context:&currentStockObservanceContext];
    _secondStock = secondStock;

    [secondStock addObserver:self forKeyPath:@"chartTimeSeries" options:NSKeyValueObservingOptionNew context:&currentStockObservanceContext];
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSLog(@"%d",[self.firstStock.chartTimeSeries count]);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)endEditSymbol:(id)sender {
    [self listenToNewSymbol:[sender text]];
}

-(void)viewDidAppear:(BOOL)animated
{
    [self createChart];
}

-(void)createChart
{
    [_lineChart removeFromSuperview];
    _lineChart = nil;
    
    NSMutableArray * times = [[NSMutableArray alloc]init];
    NSMutableArray * priceData = [[NSMutableArray alloc]init];

    for (NSArray * data in self.firstStock.chartTimeSeries) {
        
        [times addObject:data[0]];
        [priceData addObject:data[1]];

    }
    
    [self.lineChart setCompareMode:NO];
    [self.lineChart setShowLineWhenTouch:NO];
    [self.lineChart setPriorValue:[self.firstStock.prior floatValue]];
    //    [self.lineChart setXLabelSkip:60];
    
    [self.lineChart setXLabels:times];
    PNLineChartData *data01 = [PNLineChartData new];
    data01.color = PNWeiboColor;
    data01.itemCount = priceData.count;
    data01.getData = ^(NSUInteger index) {
        CGFloat yValue = [priceData[index] floatValue];
        return [PNLineChartDataItem dataItemWithY:yValue];
    };
    
    float max = fmaxf([self.firstStock.high floatValue], [self.firstStock.prior floatValue]);
    float min = fminf([self.firstStock.low floatValue], [self.firstStock.prior floatValue]);
    
    [self.lineChart setYValueMax:max];
    [self.lineChart setYValueMin:min];
    
    if(max*100 - min*100 <3)
        [self.lineChart setYLabelNum:2];
    self.lineChart.chartData = @[data01];
    [self.lineChart strokeChart];
    
}

-(void)createCompareChart
{
    
    self.symbol1.text = self.firstStock.symbol;
    self.symbol2.text = self.secondStock.symbol;
    self.symbol1.textColor =PNWeiboColor;
    self.symbol2.textColor =PNTwitterColor;
    
    yMax = 0;
    yMin = 0;
    [_lineChart removeFromSuperview];
    _lineChart = nil;
    
    NSMutableArray * times = [[NSMutableArray alloc]init];
    NSMutableArray * priceData = [[NSMutableArray alloc]init];
    
    NSMutableArray * times2 = [[NSMutableArray alloc]init];
    NSMutableArray * priceData2 = [[NSMutableArray alloc]init];
    
    for (NSArray * data in self.firstStock.chartTimeSeries) {
        [times addObject:data[0]];
        NSNumber * yValue = [NSNumber numberWithFloat:[self findPercenttageFromOpen:[data[1] floatValue] value:[self.firstStock.prior floatValue]]];
        [priceData addObject:yValue];
    }
    
    for (NSArray * data in self.secondStock.chartTimeSeries) {
        [times2 addObject:data[0]];
        NSNumber * yValue = [NSNumber numberWithFloat:[self findPercenttageFromOpen:[data[1] floatValue] value:[self.secondStock.prior floatValue]]];
        [priceData2 addObject:yValue];
    }
    
    [self.lineChart setCompareMode:YES];
    [self.lineChart setShowLineWhenTouch:YES];
    [self.lineChart setXLabels:([times count]>=[times2 count])?times:times2];
    
    PNLineChartData *data01 = [PNLineChartData new];
    data01.color = PNWeiboColor;
    data01.itemCount = priceData.count;
    data01.getData = ^(NSUInteger index) {
        CGFloat yValue = [priceData[index] floatValue];
        return [PNLineChartDataItem dataItemWithY:yValue];
    };
    PNLineChartData *data02 = [PNLineChartData new];
    data02.color = PNTwitterColor;
    data02.itemCount = priceData2.count;
    data02.getData = ^(NSUInteger index) {
        CGFloat yValue = [priceData2[index] floatValue];
        return [PNLineChartDataItem dataItemWithY:yValue];
    };
    
    [self.lineChart setPriorValue:0.0];
    [self.lineChart setYValueMax:yMax];
    [self.lineChart setYValueMin:yMin];
    
    if(yMax*100 - yMin*100 <3)
        [self.lineChart setYLabelNum:2];
//
    self.lineChart.chartData = @[data01,data02];
    [self.lineChart strokeChart];
    
}

-(float)findPercenttageFromOpen:(float)open value:(float)value
{
    float result = ((open - value) * 100 /open);

    yMax = fmaxf(yMax, result);
    yMin = fminf(yMin, result);
    return result;
}

#pragma mark - private method
//change symbol that this view will listen to
- (void) listenToNewSymbol:(NSString*) enteredSymbol {
    
    if (self.secondStock) {
        [self stopObserve];//try to remove previous symbol
    }
    
    self.secondStock = [self.appDelegate.stockModel.stocks objectForKey:enteredSymbol ];
    if (self.secondStock) {
        //        [self.appDelegate pullUpdateForSymbol:enteredSymbol];
        [self.appDelegate.stockModel pullStock:enteredSymbol];
        [self.appDelegate.stockModel pullChartForStock:enteredSymbol];
        
//        [self updateFromModel];
        //        [self updatePort];
    } else {
        if ([enteredSymbol length] > 0) {
            [self alert:[NSString stringWithFormat:@"Stock %@ not found", enteredSymbol]];
        }
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    
    if([keyPath isEqualToString:@"chartTimeSeries"]){
        
                if (self.secondStock == object) {
                            [self createCompareChart];
                }

    }
}

-(void) stopObserve {
    @try {
        self.secondStock = nil;
    }
    @catch (NSException *exception) {
        NSLog(@"Fail to self.currentStock removeObserver:self forKeyPath:@'hasUpdate'");
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark chart delegate

-(void)userClickedOnLineKeyPoint:(CGPoint)point lineIndex:(NSInteger)lineIndex andPointIndex:(NSInteger)pointIndex{
    NSLog(@"Click Key on line %f, %f line index is %d and point index is %d",point.x, point.y,(int)lineIndex, (int)pointIndex);

    PNLineChartData *chartData = self.lineChart.chartData[0];
    PNLineChartData *chartData2 = self.lineChart.chartData[1];

    if(chartData.itemCount > pointIndex)
    self.value1.text = [NSString stringWithFormat:@"%.2f%%",chartData.getData(pointIndex).y];
    if(chartData2.itemCount > pointIndex)
        self.value2.text = [NSString stringWithFormat:@"%.2f%%",chartData2.getData(pointIndex).y];
    
    
}

-(void)userClickedOnLinePoint:(CGPoint)point lineIndex:(NSInteger)lineIndex{
//    NSLog(@"Click on line %f, %f, line index is %d",point.x, point.y, (int)lineIndex);
}

-(IBAction)dismissViewController:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark - ui helper
-(void)alert:(NSString*) message {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:self.title
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles: nil];
    [alert show];
}
@end
