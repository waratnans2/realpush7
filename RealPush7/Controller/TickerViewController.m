//
//  TickerViewController.m
//  RealPush
//
//  Created by Sutean Rutjanalard on 7/29/13.
//  Copyright (c) 2013 Sutean Rutjanalard. All rights reserved.
//

#import "TickerViewController.h"
#import "TickerCell.h"
#import "AppDelegate.h"
#import "NSNumber+formatter.h"
#import "UILabel+blink.h"
#define IS_WIDESCREEN ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE ( [ [ [ UIDevice currentDevice ] model ] isEqualToString: @"iPhone" ] )
#define IS_IPOD   ( [ [ [ UIDevice currentDevice ] model ] isEqualToString: @"iPod touch" ] )
#define IS_IPHONE_5 ( IS_IPHONE && IS_WIDESCREEN )


@interface TickerViewController ()
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (weak, readonly) AppDelegate* appDelegate;
@property (readwrite) int currentIndex;
@property (readwrite) int previousIndex;
@property   (readonly) int MAX_TICKER_INDEX;

@property (weak, nonatomic) IBOutlet UILabel *labelForSymbol;
@property (weak, nonatomic) IBOutlet UILabel *labelForSide;
@property (weak, nonatomic) IBOutlet UILabel *labelForPrice;
@property (weak, nonatomic) IBOutlet UILabel *labelForVolume;
@property (weak, nonatomic) IBOutlet UILabel *labelForChange;
@property (weak, nonatomic) IBOutlet UILabel *labelForPercentChange;


@end


@implementation TickerViewController

-(AppDelegate *)appDelegate {
    return (AppDelegate*)[[UIApplication sharedApplication] delegate];
}
-(int)MAX_TICKER_INDEX {
    //iphone4 = 22, iphone5 = 27
    return IS_WIDESCREEN ? 27  : 22;
}
-(void)applyTheme {
    [super applyTheme];
    self.labelForChange.textColor =
    self.labelForPercentChange.textColor =
    self.labelForPrice.textColor =
    self.labelForSide.textColor =
    self.labelForSymbol.textColor =
    self.labelForVolume.textColor = self.theme.labelTextColor;

    self.labelForPercentChange.backgroundColor =
    self.labelForChange.backgroundColor =
    self.labelForPrice.backgroundColor =
    self.labelForSide.backgroundColor =
    self.labelForSymbol.backgroundColor =
    self.labelForVolume.backgroundColor = self.theme.defaultBackgroundColor;
    self.tableView.backgroundColor = self.theme.defaultBackgroundColor;

    
}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView reloadData];
    [self.appDelegate.stockModel addObserver:self
                             forKeyPath:@"lastTicker"
                                options:NSKeyValueObservingOptionNew
                                context:nil];
    self.currentIndex = 0;
    self.tableView.scrollEnabled = NO;
    
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    //Slide view title
    self.titleText = @"Ticker";

}

-(void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    @try {
        [self.appDelegate.stockModel removeObserver:self forKeyPath:@"lastTicker"];

    }
    @catch (NSException *exception) {
        NSLog(@"Can not remove observer from self.appDelegate.model.lastTicker" );
    }

}
-(void) setCell:(TickerCell*) cell withLastTicker:(ITStockDetail*) lastTicker {
    cell.userInteractionEnabled = YES;
    cell.highlightedLastTicker = YES;
    cell.sideLabel.text = lastTicker.lastTickerSide;
    cell.symbolLabel.text = lastTicker.symbol;
    cell.priceLabel.text = [lastTicker.price priceFormatStyle];
    cell.volumeLabel.text = [lastTicker.volume stringWithINNumberStyle:volumeFormatStyle];
    
    cell.changeLabel.text = [lastTicker.change stringWithINNumberStyle:priceFormatStyle];
    cell.percentChangeLabel.text = [NSString stringWithFormat:@"%@%%",[lastTicker.percentChange stringWithINNumberStyle:priceFormatStyle]];
    
    [cell.priceLabel setColorWithPrior:lastTicker.prior currentValue:lastTicker.price withTheme:self.theme blink:NO];
    cell.changeLabel.textColor =
    cell.percentChangeLabel.textColor =
    cell.symbolLabel.textColor =  cell.priceLabel.textColor;
    
    if ([lastTicker.lastTickerSide isEqualToString:@"B"]) {
        cell.sideLabel.textColor = 
        cell.volumeLabel.textColor = self.theme.cyanColor;
    } else {
        cell.sideLabel.textColor = 
        cell.volumeLabel.textColor = self.theme.magentaColor;
    }
    
    cell.volumeLabel.textAlignment =
    cell.changeLabel.textAlignment =
    cell.percentChangeLabel.textAlignment = NSTextAlignmentRight;
    cell.backgroundColor = [UIColor clearColor]; //ios7
}
#pragma mark - observer
-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if (object == self.appDelegate.stockModel) {
        ITStockModel* stocks = object;
        NSIndexPath* previousIndexPath = [NSIndexPath indexPathForRow:self.previousIndex inSection:0];
        TickerCell* previousCell = (TickerCell*)[self.tableView cellForRowAtIndexPath:previousIndexPath];
        NSString* lastPrice = [stocks.lastTicker.price stringWithINNumberStyle:priceFormatStyle];

        if ([previousCell.symbolLabel.text isEqualToString:stocks.lastTicker.symbol] &&
            [previousCell.priceLabel.text isEqualToString:lastPrice] &&
            [previousCell.sideLabel.text isEqualToString:stocks.lastTicker.lastTickerSide]
            ) {
            double lastVolume = [[NSNumber numberWithString:previousCell.volumeLabel.text numberStyle:volumeFormatStyle] doubleValue];
            double newVolume   = lastVolume+[stocks.lastTicker.volume doubleValue];
            previousCell.volumeLabel.text = [[NSNumber numberWithDouble:newVolume] stringWithINNumberStyle:volumeFormatStyle];
        }  else {
           
            NSIndexPath* indexPath = [NSIndexPath indexPathForRow:self.currentIndex inSection:0];
            
            TickerCell* cell = (TickerCell*)[self.tableView cellForRowAtIndexPath:indexPath];
            [self setCell:cell withLastTicker:stocks.lastTicker];
            previousCell.userInteractionEnabled = NO;
            previousCell.highlightedLastTicker = NO;
            previousCell.selected = NO;
            
            self.previousIndex = self.currentIndex;
            if (self.currentIndex == self.MAX_TICKER_INDEX-1) {
                self.currentIndex = 0;
            } else {
                self.currentIndex += 1;
            }
            
        }
        
        
    }
}

#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.MAX_TICKER_INDEX;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  [self createCellforRowAtIndexPath:indexPath];
}
-(TickerCell*) createCellforRowAtIndexPath:(NSIndexPath*) indexPath {
    static NSString* cellIdentifier = @"tickerCell";
    TickerCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil){
        NSLog(@"Ticker Cell is nil");
    }
    //void text label cell
    cell.symbolLabel.text = @"";
    cell.sideLabel.text = @"";
    cell.priceLabel.text = @"";
    cell.volumeLabel.text = @"";
    cell.changeLabel.text = @"";
    cell.percentChangeLabel.text = @"";
    cell.highlightedLastTicker = NO;
    cell.backgroundColor = self.theme.tableBackgroundColor;
    return cell;
}


@end
