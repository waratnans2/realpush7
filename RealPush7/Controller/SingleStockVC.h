//
//  SingleStockVC.h
//  RealPush
//
//  Created by Sutean Rutjanalard on 6/21/13.
//  Copyright (c) 2013 Sutean Rutjanalard. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideViewController.h"
@protocol SingleStockVCDelegate <NSObject>

-(void) singleStockVCWillDismissModal;

@end

@interface SingleStockVC : SlideViewController<UITableViewDataSource, UITableViewDelegate>

-(void) prepareDefaultStockName:(NSString*) stockName;
-(void) prepareDefaultStockName:(NSString*) stockName withModalDelegate:(id<SingleStockVCDelegate>) delegate;
@property (nonatomic, weak) id<SingleStockVCDelegate> delegate;

@end
