//
//  KeyboardViewController.m
//  RealPush7
//
//  Created by Sutean Rutjanalard on 27/2/2557 BE.
//  Copyright (c) 2557 Sutean Rutjanalard. All rights reserved.
//

#import "KeyboardViewController.h"
#import "PriceKeyboard.h"
#import "VolumeKeyboard.h"
#import "NSNumber+formatter.h"

@interface KeyboardViewController () <UITextFieldDelegate, KeyboardDelegate>
@property (weak, nonatomic) IBOutlet UITextField *numberTF;
@property (weak, nonatomic) IBOutlet UITextField *volumeTF;
@property (weak, nonatomic) IBOutlet UITextField *pinTF;

@property (strong, nonatomic) PriceKeyboard* priceKeyboard;
@property (strong, nonatomic) VolumeKeyboard* volumeKeyboard;

@end

@implementation KeyboardViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.numberTF.delegate = self;
    self.priceKeyboard = [[PriceKeyboard alloc] initWithDelegate:self];
    self.priceKeyboard.doneButtonTitle = @"Next";
    self.priceKeyboard.textField = self.numberTF;
    
    self.volumeTF.delegate = self;
    self.volumeKeyboard = [[VolumeKeyboard alloc] initWithDelegate:self];
    self.volumeKeyboard.doneButtonTitle = @"Done";
    self.volumeKeyboard.textField = self.volumeTF;
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
//    NSLog(@"change `%@` at:%d len:%d  with `%@`", textField.text, range.location,range.length, string);
    if (textField == self.numberTF) {
        return [NSNumber validatePriceTextField:textField charactersChangeRange:range replacementString:string];
    } else if (textField == self.volumeTF) {
        return [NSNumber validateVolumeTextField:self.volumeTF charactersChangeRange:range replacementString:string];
    }
    return NO;
}
-(void)keyboard:(id)keyboard hideFromTextField:(UITextField *)textField {
    if (keyboard == self.priceKeyboard && self.numberTF == textField) {
        [self.numberTF resignFirstResponder];
    } else if (keyboard == self.volumeKeyboard && self.volumeTF == textField) {
        [self.volumeTF resignFirstResponder];
    }

}
-(void)keyboard:(id)keyboard donePressedFromeTextField:(UITextField *)textField {
    if (keyboard == self.priceKeyboard && self.numberTF == textField) {
        [self.volumeTF becomeFirstResponder];
    } else if (keyboard == self.volumeKeyboard && self.volumeTF == textField) {
        [self.volumeTF resignFirstResponder];
    }
}
-(void) keyboard:(id)keyboard backPressedFromeTextField:(UITextField *)textField {
    if (keyboard == self.priceKeyboard && self.numberTF == textField) {
        [self.numberTF resignFirstResponder];
    } else if (keyboard == self.volumeKeyboard && self.volumeTF == textField) {
        [self.numberTF becomeFirstResponder];
    }
}


@end
