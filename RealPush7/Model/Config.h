//
//  Config.h
//  RealPush
//
//  Created by Sutean Rutjanalard on 7/2/2557 BE.
//  Copyright (c) 2557 Sutean Rutjanalard. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    BrokerKimeng, BrokerZmico
} Broker;

@interface Config : NSObject

+(NSString*) zmicoTestHost;
+(NSString*) zmicoProductionHost;
+(NSString*) kimengConfigurationURL;
+(NSString*) zmicoConfigurationURL ;
+(Broker) brokerType;
+(NSString*) marketConfiguration;
+(NSString*) zmicoPullHost;
+(NSString *)zmicoAuthKey;
@end
