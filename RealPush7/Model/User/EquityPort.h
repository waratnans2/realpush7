//
//  EquityPort.h
//  RealPush
//
//  Created by Sutean Rutjanalard on 18/9/2556 BE.
//  Copyright (c) 2556 Sutean Rutjanalard. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EquityPort : NSObject
/*
 
 <stock>E</stock>
 <type>N</type>
 <trustid>0</trustid>
 <sellable>50000</sellable>
 <onhand>50000</onhand>
 <avgprice>1.5326</avgprice>
 <mktprice>1.05</mktprice>
 <cost>76629.15</cost>
 <gainloss>-24129.15</gainloss>
 <percentpl>-31.49</percentpl>
 */
@property (nonatomic, strong) NSString* stockName;
@property (nonatomic, strong) NSString* type;
@property (nonatomic) unsigned long long sellable;
@property (nonatomic) unsigned long long onhand;
@property (nonatomic, strong) NSNumber* avgPrice;
@property (nonatomic, strong) NSNumber* marketPrice;
@property (nonatomic, strong) NSNumber* cost;
@property (nonatomic, strong) NSNumber* gainloss;
@property (nonatomic, strong) NSNumber* percentPL;
@end
