//
//  StockUser.m
//  RealPush
//
//  Created by Sutean Rutjanalard on 6/21/13.
//  Copyright (c) 2013 Sutean Rutjanalard. All rights reserved.
//

#import "StockUser.h"
#import "Config.h"
#import <AFNetworking/AFNetworking.h>
@interface StockUser()
@property (nonatomic) SEL completor;
@property (nonatomic) SEL failer;
@property (nonatomic, weak) id target;





//load ssh host configuration from kimeng 
-(void)loadSslHosConfiguration:(NSString*) sslHost ;

@end

@implementation StockUser
-(void) logout {
    return;
}
-(void)loginWithUser:(NSString *)username andPassword:(NSString *)password {
    return;
}
-(void)loadSslHosConfiguration:(NSString *)sslHost {
    return;
}
-(void)loadConfigurationDone:(void (^)(StockUser* responseObject)) success
                        fail:(void (^)(NSString* response)) fail {
//    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//    [manager GET:self.marketListHost parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSLog(@"JSON: %@", responseObject);
//        success(self);
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        NSLog(@"Error: %@", error);
//        NSString* result = [NSString stringWithFormat:@"Error: %@", error];
//        fail(result);
//    }];

    success(self);
    return;
}







-(void) updateCurrentIPAddress {
    //slow method dispatch to other thread
    dispatch_queue_t downloadQueue = dispatch_queue_create("ip address downloader", NULL);
    dispatch_async(downloadQueue, ^{
        NSString* servicePath = [NSString stringWithFormat:@"%@/%@/checkip.asp", self.sslHost,self.sslPath];
        NSLog(@"Service path = %@", servicePath);
        NSURL *serviceURL = [NSURL URLWithString:servicePath];
        NSString* ipAddress =[[NSString stringWithContentsOfURL:serviceURL encoding:NSUTF8StringEncoding error:nil] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
;
        dispatch_async(dispatch_get_main_queue(), ^{
            self.currentIPAddress = ipAddress;
            NSLog(@"self.currentIPAddress= %@", self.currentIPAddress);
        });
    });
//    dispatch_release(downloadQueue);
}
#pragma mark - PUBLIC API 
-(void) clearUserData {
    self.username = nil;
    self.investorNumber= nil;
    self.investorName= nil;
    self.investorType= nil;
    self.sbl= nil;
    self.market= nil;
    self.sessionID= nil;
    self.disclaim= nil;
    self.alerthost= nil;
    self.forgetpwd= nil;
    self.facebookLink= nil;
    self.suVersion= nil;
    self.tfexMarketSymbolDefaultLink= nil;
    self.sslHost= nil;
    self.sslPath= nil;
    self.productname= nil;
    self.streammingHost= nil;
    self.streammingPort= 0;
    self.currentIPAddress= nil;
}

-(NSString *)description {
    return [NSString stringWithFormat:
            @"username:%@\n" 
            @"sslHost :%@\n"
            @"sslPath :%@\n"
            @"productname: %@\n"
            @"streamingHost: %@\n"
            @"streammingPort: %lu\n"
            @"suVersion: %@\n"
            @"disclaim: %@\n"
            @"alerthost: %@\n"
            @"forgetpwd: %@\n"
            @"facebookLink: %@\n"
            @"currentIP: %@\n",
            self.username,
            self.sslHost,
            self.sslPath,
            self.productname,
            self.streammingHost,
            (unsigned long)self.streammingPort ,
            self.suVersion,
            self.disclaim,
            self.alerthost,
            self.forgetpwd,
            self.facebookLink,
            self.currentIPAddress
            ];
}

@end
