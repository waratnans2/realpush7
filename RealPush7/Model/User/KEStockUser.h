//
//  ITStockUser.h
//  RealPush
//
//  Created by Sutean Rutjanalard on 6/9/2556 BE.
//  Copyright (c) 2556 Sutean Rutjanalard. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "StockUser.h"
#import "EquityPort.h"


@interface KEStockUser : StockUser




//-(NSArray*) serviceGetCustomerAccounts;
//-(void) serviceChangePasswordWithOldpass:(NSString*) oldpass
//                                 newpass:(NSString*) newpass
//                         confirmPassword:(NSString*) confirmPassword;
//-(void) serviceCheckPinWithPin:(NSString*) pin;
//-(void) serviceChangePinWithOldpin:(NSString*) oldpin
//                            newpin:(NSString*) newpin
//                        confirmPin:(NSString*) confirmPin;
//
//-(void) servicePutOrderWithStock:(NSString*) stock
//                            side:(NSString*) side // 'B' or 'S'
//                          volume:(NSString*) volume
//                           price:(NSString*) price
//                            nvdr:(NSString*) trustId // 0 - normal, 1 - TTF , 2  - NVDR
//                       condition:(NSString*) condition // '' = Not used, 'F' = Fill Or Kill, 'I' = Immediately Or Cancel
//                             pin:(NSString*) pin
//                     channelType:(NSString*) channelType
//                              ip:(NSString*) ip;
//-(void) servicePutOrderTFEXWithSeries:(NSString*) series
//                                price:(NSString*) price
//                          longOrShort:(NSString*) longOrShort // L or S
//                                  qty:(NSString*) qty
//                                pbQty:(NSString*) pbQty
//                             validity:(NSString*) validity // DAY, FOK, FAK
//                          openOrClose:(NSString*) openOrClose // O, C
//                        limitOrMarket:(NSString*) limitOrMarket // L,M
//                             stopName:(NSString*) stopName
//                            stopPrice:(NSString*) stopPrice
//                        stopCondition:(NSString*) stopCondition // 1, 2, 3, 4, 5, 6
//                          productName:(NSString*) productioName
//                                  pin:(NSString*) pin
//                          channelType:(NSString*) channelType // 1 local, 2 external
//                                   ip:(NSString*) ip;
//
//-(void) serviceCancelOrderWithOrderNumber:(NSString*) orderNumber
//                                    stock:(NSString*) stock //optional
//                               stockPrice:(NSString*) stockPrice //optional
//                                     side:(NSString*) side //side
//                                orderDate:(NSString*) orderDate //YYYYMMDD
//                                      pin:(NSString*) pin
//                                       ip:(NSString*) ip;
//-(void) serviceCancelOrderTfexWithOrderNumber:(NSString*) orderNumber
//                                       series:(NSString*) series //optional
//                                    orderDate:(NSString*) orderDate //YYYYMMDD
//                                          pin:(NSString*) pin
//                                           ip:(NSString*) ip;
////qty, price, position , type
//-(void) serviceChangeOrderTfexWithOrderNumber:(NSString*) orderNumber
//                                       series:(NSString*) series //optional
//                                    orderDate:(NSString*) orderDate //YYYYMMDD
//                                       qtyREQ:(NSString*) qty //req
//                                     priceREQ:(NSString*) price //req
//                                     validity:(NSString*) validity
//                               openOrCloseREQ:(NSString*) openOrClose //req
//                             limitOrMarketREQ:(NSString*) limitOrMarket //req
//                                          pin:(NSString*) pin
//                                           ip:(NSString*) ip;
//
//-(void) serviceGetCustomerCreditWithAccount:(NSString*) account;
//-(void) serviceGetCustomerPortfolio ;
//-(void) serviceGetOrderInfoWithAccount:(NSString*) account;
//
@end