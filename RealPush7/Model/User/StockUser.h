//
//  StockUser.h
//  RealPush
//
//  Created by Sutean Rutjanalard on 6/21/13.
//  Copyright (c) 2013 Sutean Rutjanalard. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StockUser : NSObject
#pragma mark - user detail
@property (nonatomic, strong) NSString* username;
@property (nonatomic, strong) NSString* password;

@property (nonatomic, strong) NSString* investorNumber;
@property (nonatomic, strong) NSString* investorName;
@property (nonatomic, strong) NSString* investorType;
@property (nonatomic, strong) NSString* sbl;
@property (nonatomic, strong) NSString* market;
@property (nonatomic, strong) NSString* sessionID;


@property (nonatomic) BOOL isLogin;
@property (nonatomic, strong) NSString* loginStatus;
@property (nonatomic, strong) NSMutableArray* equityPorts;
@property (nonatomic, strong) NSNumber* totalCost;
@property (nonatomic, strong) NSNumber* totalMarketValue;

//ABSTRACT METHOD -- MUST IMPLEMENT
-(void) loginWithUser:(NSString*) username andPassword:(NSString*) password;
-(void) logout;






#pragma mark - configuration
@property (nonatomic, strong) NSString* disclaim;

@property (nonatomic, strong) NSString* alerthost;
@property (nonatomic, strong) NSString* forgetpwd;
@property (nonatomic, strong) NSString* facebookLink;
@property (nonatomic, strong) NSString* suVersion;
@property (nonatomic, strong) NSString* tfexMarketSymbolDefaultLink;

@property (nonatomic, strong) NSString* sslHost;
@property (nonatomic, strong) NSString* sslPath;
@property (nonatomic, strong) NSString* productname;

@property (nonatomic, strong) NSString* streammingHost;
@property (nonatomic) NSUInteger streammingPort;

@property (nonatomic, strong) NSString* currentIPAddress;



-(void) clearUserData;
-(void) updateCurrentIPAddress;
//load config from settings.plist
//"configuration_host" = <configuration host url>
-(void)loadConfigurationDone:(void (^)(StockUser* responseObject)) success
                        fail:(void (^)(NSString* response)) fail ;
@end
