//
//  ITStockUser.m
//  RealPush
//
//  Created by Sutean Rutjanalard on 6/9/2556 BE.
//  Copyright (c) 2556 Sutean Rutjanalard. All rights reserved.
//

#import "KEStockUser.h"
#import "TBXML.h"
#import <AFNetworking.h>
#import "NSData+utf8string.h"
#import "Config.h"
@interface KEStockUser ()
@property (nonatomic, readonly) NSString* fullHostPath;
@property (nonatomic, strong) AFHTTPRequestOperationManager* httpRequestOperationManager;


@end
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
@implementation KEStockUser
@synthesize equityPorts = _equityPorts;
-(NSString *)fullHostPath {
    return [self.sslHost stringByAppendingFormat:@"/%@/", self.sslPath];
}
-(AFHTTPRequestOperationManager *)httpRequestOperationManager {
    if (_httpRequestOperationManager) {
        return _httpRequestOperationManager;
    }
    _httpRequestOperationManager = [AFHTTPRequestOperationManager manager];
    _httpRequestOperationManager.securityPolicy.allowInvalidCertificates = NO;
    _httpRequestOperationManager.responseSerializer = [AFHTTPResponseSerializer serializer];
    return _httpRequestOperationManager;
}
-(NSMutableArray *)equityPorts {
    if ( ! _equityPorts) {
        _equityPorts = [NSMutableArray array];
    }
    return _equityPorts;
}
#pragma mark - xml helper
-(NSString*) getTextFromElementNamed:(NSString*) elementName withParentElement:(TBXMLElement*) element{
    //get element by name and return text
    TBXMLElement *tag = [TBXML childElementNamed:elementName parentElement:element];
    if(tag) {
        return [TBXML textForElement:tag];
    }
    else {
        NSLog(@"'%@' does not exist", elementName);
        return nil;
    }
}
-(TBXML* ) xmlWithString:(NSString*) dataString {
    NSError* error = nil;
    TBXML* tbxml = [TBXML tbxmlWithXMLString:dataString error:&error];
    if( [[TBXML elementName:tbxml.rootXMLElement] isEqualToString:@"result"] ) {
        //ok
        return tbxml;
    } else {
        NSLog(@"unknow xml = '%@'", dataString);
        return nil;
    }

}


#pragma mark - request
//-(void) sendRequestWithURLString:(NSString *)urlString
//                       paramDict:(NSDictionary*) params
//                     andSelector:(SEL)selector {
//    [self.httpRequestOperationManager POST:urlString
//                                parameters:params
//                                   success:^(AFHTTPRequestOperation *operation, id responseObject)
//     {
//         [self performSelector:@selector(selector) withObject:responseObject];
//     }
//                                   failure:^(AFHTTPRequestOperation *operation, NSError *error)
//     {
//         [self requestFailed:operation];
//     }];
//}
- (void)requestFailed:(AFHTTPRequestOperation *)request
{
    NSLog(@"Request fail.");
}
#pragma mark - login/logout
-(void) loginFailWithMessage: (NSString*) message {
    self.loginStatus = message;
    self.isLogin = NO;
}
-(void) loginWithUser:(NSString*) username
          andPassword:(NSString*) password {
    NSString *urlString = [NSString stringWithFormat:@"%@/%@/login.asp",
                           self.sslHost,self.sslPath];
    NSDictionary* postParam = @{@"user" : username,
                            @"password" : password,
                             @"product" : self.productname};
    self.username = username;
    [self.httpRequestOperationManager POST:urlString
                                parameters:postParam
                                   success:^(AFHTTPRequestOperation *request, id response){
                                       [self kimengLoginDone:response];
                                   }
                                   failure:^(AFHTTPRequestOperation* request, NSError* error) {
                                       NSLog(@"Login fail, %@", [error localizedDescription]);
                                       [self loginFailWithMessage:@"Login fail because of internet connection"];
                                   }];

}
-(void)logout {
    NSString* urlString = [NSString stringWithFormat:@"%@/%@/logout.asp", self.sslHost, self.sslPath];
    NSDictionary* params = @{@"user" : self.username};
    [self.httpRequestOperationManager POST:urlString
                                parameters:params
                                   success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                       [self kimengLoginDone:responseObject];
                                   } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                       [self requestFailed:operation];
                                   }];

}
/**
 *
 fail xml
 <?xml version='1.0' encoding='ISO-8859-1'?>
 <result>
 <code>1</code>
 <errmsg>Invalid user or password</errmsg>
 </result>
 ***
 login done
 <result>
 <code>0</code>
 <invno>71016334</invno>
 <invname>PAKORNWUT UDOMPIPATSKUL</invname>
 <type>I</type>
 <sbl>N</sbl>
 <market>E</market>
 <sid>5801308</sid>
 </result>
 */
-(void) kimengLoginDone:(NSData *)data {
    NSString* xmlString = [data utf8String];
    
    NSLog(@"data = %@", xmlString);
    TBXML* tbxml = [self xmlWithString:xmlString];
    if (tbxml) {
        if ([self getTextFromElementNamed:@"errmsg" withParentElement:tbxml.rootXMLElement]) {
            self.loginStatus = [self getTextFromElementNamed:@"errmsg" withParentElement:tbxml.rootXMLElement];
            self.isLogin = NO;
        } else {
            [self processKimengLoginData:tbxml];
            self.isLogin = YES;
            [self serviceGetCustomerPortfolio];
        }
    } else {
        [self loginFailWithMessage:@"Server response with unknow message."];
    }
}
/**
 *
 <result>
 <code>0</code>
 <invno>71016334</invno>
 <invname>PAKORNWUT UDOMPIPATSKUL</invname>
 <type>I</type>
 <sbl>N</sbl>
 <market>E</market>
 <sid>5801308</sid>
 </result>
 */
-(void) processKimengLoginData:(TBXML* ) tbxml {
    self.investorNumber = [self getTextFromElementNamed:@"invno"withParentElement:tbxml.rootXMLElement];
    self.investorName = [self getTextFromElementNamed:@"invname"withParentElement:tbxml.rootXMLElement];
    self.investorType = [self getTextFromElementNamed:@"type" withParentElement:tbxml.rootXMLElement];
    self.sbl          = [self getTextFromElementNamed:@"sbl" withParentElement:tbxml.rootXMLElement];
    self.market       = [self getTextFromElementNamed:@"market" withParentElement:tbxml.rootXMLElement];
    self.sessionID    = [self getTextFromElementNamed:@"sid" withParentElement:tbxml.rootXMLElement];
    
}

-(void)kimengLogoutDone:(NSData *)data {

    NSString* xmlString = [data utf8String];
    NSLog(@"data = %@", xmlString);
    TBXML* tbxml = [self xmlWithString:xmlString];
    if (tbxml) {
        if ([self getTextFromElementNamed:@"errmsg" withParentElement:tbxml.rootXMLElement]) {
            self.loginStatus = [self getTextFromElementNamed:@"errmsg" withParentElement:tbxml.rootXMLElement];
        } else {
            [self processKimengLogout:tbxml];
        }
    }
}
/*
 <?xml version='1.0' encoding='ISO-8859-1'?>
 <result>
 <code>0</code>
 <userid>71016334</userid>
 <msg>Logout Complete</msg>
 </result>
 */
-(void) processKimengLogout:(TBXML*) tbxml {
    if ([@"0" isEqualToString:[self getTextFromElementNamed:@"code"
                                          withParentElement:tbxml.rootXMLElement]]) {
        NSLog(@"msg = '%@'", [self getTextFromElementNamed:@"msg"
                                         withParentElement:tbxml.rootXMLElement]);
        [self clearUserData];
        self.isLogin = NO;
        
    }
}
-(void)clearUserData {
    [super clearUserData];
    self.username = nil;
    self.equityPorts = nil;
}
#pragma mark - portfolio
-(void)serviceGetCustomerPortfolio {
    NSString* urlString = [self.fullHostPath stringByAppendingString:@"get_cust_posi.asp"];
    
    NSDictionary* form = @{@"userid": self.username, @"sid": self.sessionID, @"accountno": self.investorNumber};
    [self.httpRequestOperationManager POST:urlString
                                parameters:form
                                   success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self serviceGetCustomerPortDone:responseObject];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"request logout fail");
        [self requestFailed:operation];
    }];
    
}
/**
 *
 <result>
 <res_code>0</res_code>
 <res_msg>Successfully</res_msg>
 <list>
 <port>
 <stock>BLAND</stock>
 <type>N</type>
 <trustid>0</trustid>
 <sellable>160000</sellable>
 <onhand>160000</onhand>
 <avgprice>1.5002</avgprice>
 <mktprice>1.46</mktprice>
 <cost>240027.52</cost>
 <gainloss>-6427.52</gainloss>
 <percentpl>-2.68</percentpl>
 </port>
 <port>
 <stock>E</stock>
 <type>N</type>
 <trustid>0</trustid>
 <sellable>50000</sellable>
 <onhand>50000</onhand>
 <avgprice>1.5326</avgprice>
 <mktprice>1.05</mktprice>
 <cost>76629.15</cost>
 <gainloss>-24129.15</gainloss>
 <percentpl>-31.49</percentpl>
 </port>
 <totalcost>1093564.91</totalcost>
 <totalmktval>941750</totalmktval>
 </list>
 </result>
*/

-(void) processCustomerPort:(TBXML*) tbxml {
    TBXMLElement *tag = [TBXML childElementNamed:@"list" parentElement:tbxml.rootXMLElement];
    if (tag) {
        TBXMLElement *port = [TBXML childElementNamed:@"port" parentElement:tag];
        do {
            NSString* tagName = [TBXML elementName:port];
            if ([tagName isEqualToString:@"port"]) {
                EquityPort* equityPort = [[EquityPort alloc] init];
                NSLog(@"onhand = %@",[self getTextFromElementNamed:@"stock" withParentElement:port ]);
                equityPort.stockName = [self getTextFromElementNamed:@"stock" withParentElement:port];
                equityPort.type = [self getTextFromElementNamed:@"type" withParentElement:port];
                equityPort.sellable = [[self getTextFromElementNamed:@"sellable" withParentElement:port] longLongValue];
                equityPort.onhand = [[self getTextFromElementNamed:@"onhand" withParentElement:port] longLongValue];
                equityPort.avgPrice =[NSNumber numberWithDouble: [[self getTextFromElementNamed:@"avgprice" withParentElement:port] doubleValue]];
                equityPort.marketPrice = [NSNumber numberWithDouble:[[self getTextFromElementNamed:@"mktprice" withParentElement:port] doubleValue]];
                equityPort.cost = [NSNumber numberWithDouble:[[self getTextFromElementNamed:@"cost" withParentElement:port] doubleValue]] ;
                equityPort.gainloss = [NSNumber numberWithDouble:[[self getTextFromElementNamed:@"gainloss" withParentElement:port] doubleValue]];
                equityPort.percentPL = [NSNumber numberWithDouble:[[self getTextFromElementNamed:@"percentpl" withParentElement:port] doubleValue]];
                [self.equityPorts addObject:equityPort];
            } else if ([tagName isEqualToString:@"totalcost"]) {
                self.totalCost = [NSNumber  numberWithDouble:[[TBXML textForElement:port] doubleValue]];
            } else if ([tagName isEqualToString:@"totalmktval"]) {
                self.totalMarketValue = [NSNumber numberWithDouble:[[TBXML textForElement:port] doubleValue]];
            }
            

        } while ((port = port->nextSibling));
    }
 
}
- (void) serviceGetCustomerPortDone:(NSData*) data {
    NSString* xmlString = [[[NSString alloc] initWithData:data
                                                 encoding:NSUTF8StringEncoding]
                           stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSLog(@"data = %@", xmlString);
    TBXML* tbxml = [self xmlWithString:xmlString];
    if (tbxml) {
        if ([self getTextFromElementNamed:@"errmsg" withParentElement:tbxml.rootXMLElement]) {
            NSLog(@"Error msg: %@ ",[self getTextFromElementNamed:@"errmsg" withParentElement:tbxml.rootXMLElement]);
        } else {
            [self processCustomerPort:tbxml];
        }
    }


}

-(void)loadConfigurationWithCompleteTarget:(id)target doneSelector:(SEL)doneSelector fail:(SEL)failSelector {
    NSURL* configurationURL = [NSURL URLWithString:[Config kimengConfigurationURL]];
    
    dispatch_queue_t downloadQueue = dispatch_queue_create("Load config queue", NULL);
    dispatch_async(downloadQueue, ^{
        NSString* configuration = [NSString stringWithContentsOfURL:configurationURL
                                                           encoding:NSUTF8StringEncoding error:nil];
        if (configuration == nil) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [target performSelector:failSelector withObject:@"Loading configuration file error."];
            });
            
        } else {
            NSError* error = nil;
            TBXML * tbxml = nil;
            tbxml = [TBXML tbxmlWithXMLString:configuration error:&error];
            if ([[TBXML elementName:tbxml.rootXMLElement] isEqualToString:@"server"]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    @try {
                        [self readConfiguration:tbxml.rootXMLElement];
                        [self loadSslHosConfiguration:self.sslHost target:target doneSelector:doneSelector];
                    }
                    @catch (NSException *exception) {
                        [target performSelector:failSelector withObject:exception.reason];
                    }
                });
                
            } else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [target performSelector:failSelector withObject:@"Passing configuration file error"];
                });
            }
            
        }
        
    });
}
/**
 <server>
 <appURL/>
 <sslHost>http://www.maybank-ke.co.th/getservername.asp</sslHost>
 <!--<sslHost>http://58.97.60.23/getServerTest.xml</sslHost>-->
 <host>58.97.60.23</host>
 <sslPath>KEAPI_S2</sslPath>
 <disclaim>KimEngDis.txt</disclaim>
 <streammingHost>keitrade.maybank-ke.co.th</streammingHost>
 <alertHost>http://58.97.60.26/SendNews/</alertHost>
 <streammingPort>23452</streammingPort>
 <forgotpwd>pda/ios/fpassword.asp?logo=N</forgotpwd>
 <productname>keitrade</productname>
 <facebookLink>http://www.facebook.com/MaybankKE</facebookLink>
 <suVersion>http://61.90.198.76</suVersion>
 <tfexMarketSymbolDefaultLink>http://dl.dropbox.com/u/65046631/TFEXGraphJSON.json</tfexMarketSymbolDefaultLink>
 </server>
 */
- (void) readConfiguration:(TBXMLElement *)element {
        [self getTextFromElementNamed:@"appURL" withParentElement:element];
        self.sslHost = [self getTextFromElementNamed:@"sslHost" withParentElement:element];
        self.disclaim = [self getTextFromElementNamed:@"disclaim" withParentElement:element];
        self.streammingHost = [self getTextFromElementNamed:@"streammingHost" withParentElement:element];
        self.streammingPort = [[self getTextFromElementNamed:@"streammingPort" withParentElement:element] intValue];
        self.alerthost = [self getTextFromElementNamed:@"alertHost" withParentElement:element];
        self.forgetpwd  = [self getTextFromElementNamed:@"forgotpwd" withParentElement:element];
        self.productname = [self getTextFromElementNamed:@"productname" withParentElement:element];
        self.facebookLink = [self getTextFromElementNamed:@"facebookLink" withParentElement:element];
        self.suVersion = [self getTextFromElementNamed:@"suVersion" withParentElement:element];
        self.sslPath = [self getTextFromElementNamed:@"sslPath" withParentElement:element];
        self.tfexMarketSymbolDefaultLink = [self getTextFromElementNamed:@"tfexMarketSymbolDefaultLink" withParentElement:element];
        
        
        NSLog(@"Load basic configuration done.");
}
-(void)loadSslHosConfiguration:(NSString*) sslHost target:(id)target doneSelector:(SEL)doneSelector{
    
    dispatch_queue_t downloadQueue = dispatch_queue_create("Load config queue", NULL);
    dispatch_async(downloadQueue, ^{
        NSString* configuration = [NSString stringWithContentsOfURL:[NSURL URLWithString:sslHost ]
                                                           encoding:NSUTF8StringEncoding error:nil];
        if (configuration != nil) {
            
            NSError* error = nil;
            TBXML* tbxml = [TBXML tbxmlWithXMLString:configuration error:&error];
            dispatch_async(dispatch_get_main_queue(), ^{
                if ([[TBXML elementName:tbxml.rootXMLElement] isEqualToString:@"result"]) {
                    [self readSSLConfiguration:tbxml.rootXMLElement];
                    
                    [target performSelector:doneSelector withObject:self];

                } else {
                    [NSException raise:@"Fail while passing ssl configuration" format:@"no element `result` from xml %@", tbxml];
                }
            });
        }
    });
    //    dispatch_release(downloadQueue);
}
/**
 <result>
 <servername>https://tradings.maybank-ke.co.th</servername>
 </result>
 */
- (void) readSSLConfiguration:(TBXMLElement *)element {
        self.sslHost = [self getTextFromElementNamed:@"servername" withParentElement:element];
        NSLog(@"Load sslhost configuration done");
        [self updateCurrentIPAddress];
}
@end
