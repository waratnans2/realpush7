//
//  ZMStockUser.m
//  RealPush7
//
//  Created by Sutean Rutjanalard on 26/3/2557 BE.
//  Copyright (c) 2557 Sutean Rutjanalard. All rights reserved.
//

#import "ZMStockUser.h"
#import "Config.h"

#pragma clang diagnostic ignored "-Warc-performSelector-leaks"

@implementation ZMStockUser

-(void)loadConfigurationDone:(void (^)(StockUser *))success fail:(void (^)(NSString *))fail {
    [super loadConfigurationDone:success fail:fail];
    [self demoUser];
}
-(void) demoUser {
    self.streammingHost = @"58.137.159.125";
    self.streammingPort = 4321;
    self.username = @"407943I";
    self.password = @"1234";
}
-(void) demoUserLocalhost {
    self.streammingHost = @"localhost";
    self.streammingPort = 4321;
    self.username = @"Pakit";
    self.password = @"1234";
}
@end

