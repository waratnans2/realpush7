//
//  ITPull.m
//  RealPush
//
//  Created by Sutean Rutjanalard on 12/9/2556 BE.
//  Copyright (c) 2556 Sutean Rutjanalard. All rights reserved.
//

#import "ITPull.h"
#import "AsyncSocket.h"
#import "INUtility.h"
@interface ITPull()
@property (nonatomic, strong) AsyncSocket* socket;
@property (nonatomic, strong) NSString* host;
@property (nonatomic) int port;
@property (nonatomic, weak) id<ITPullResponse> delegate;
@property (nonatomic, strong) NSString* username;
@property (nonatomic) BOOL isLogin;
@property (nonatomic) NSMutableArray*   requestMessage;



@end

@implementation ITPull
-(AsyncSocket *)socket {
    if ( ! _socket) {
         _socket = [[AsyncSocket alloc] initWithDelegate:self];
    }
    return _socket;
}
-(NSMutableArray *)requestMessage {
    if ( ! _requestMessage) {
        _requestMessage = [[NSMutableArray alloc] init];
    }
    return _requestMessage;
}
#pragma mark - Siamsquared protocal
- (id)initWithHost:(NSString*) host port:(int) port username:(NSString*) username delegate:(id<ITPullResponse>) delegate
{
    self = [super init];
    if (self) {
        NSError* error = nil;
        self.host = host;
        self.port = port;
        self.delegate = delegate;
        self.username = username;
        [self.socket connectToHost:self.host onPort:self.port error:&error];
    }
    return self;
}

-(void) requestMessage:(NSString*) requestMessage {
    if([self.socket isConnected] && self.isLogin) {
        [self.socket writeData:[requestMessage dataUsingEncoding:NSUTF8StringEncoding] withTimeout:-1 tag:0];
        [self.socket readDataWithTimeout:20 tag:0];
        NSLog(@"sent %@", requestMessage);
    } else if([self.socket isConnected] && ! self.isLogin){
        [self.requestMessage addObject:requestMessage];
        NSLog(@"save %@", requestMessage);

    } else if([self.socket isDisconnected]) {
        [self.requestMessage addObject:requestMessage];
        NSLog(@"save %@", requestMessage);
        [self.socket connectToHost:self.host onPort:self.port error:nil];
    } else {
        NSLog(@"What happen to socket= %@", self.socket);
    }
    
}
-(void) login {
    NSString* loginSecret =  [NSString stringWithFormat:@"L|%@|%@!", self.username, [INUtility createSiamPushKey]];
    NSData* login = [loginSecret dataUsingEncoding:NSUTF8StringEncoding];
    [self.socket writeData:login withTimeout:-1 tag:0];
    [self.socket readDataWithTimeout:-1 tag:0];
    
}
-(void) requestInQueueMessage {
    for (NSString* requestMessage in self.requestMessage) {
        NSData* data = [requestMessage dataUsingEncoding:NSUTF8StringEncoding];
        NSLog(@"re-request = %@", requestMessage);
        [self.socket writeData:data withTimeout:-1 tag:0];
        [self.socket readDataWithTimeout:-1 tag:0];
        NSLog(@"sent %@", requestMessage);
    }
    [self.requestMessage removeAllObjects];
}
-(void) processLoginMessage:(NSString*) loginMessage {
    NSArray* dataComponent = [loginMessage componentsSeparatedByString:@"|"];
    if ([[dataComponent objectAtIndex:2] isEqualToString:@"TRUE"]) {
        self.siamLoginToken = [dataComponent lastObject];
        self.isLogin = YES;
        [self requestInQueueMessage];
    }
}
-(void) readDataString:(NSString*) dataString {
    //NSLog(@"puller read: %@", dataString);
    if ([dataString hasPrefix:@"L|"]) {
        [self processLoginMessage:dataString];
    }
    [self.delegate pullResponseString:dataString];
    
}
/**
 * disconnect all socket inside
 */
-(void)disconnect {
    NSLog(@"command to disconnect");
    if ([self.socket isConnected]) {
        [self.socket disconnect];
    }
}

#pragma mark - AsyncSocket
- (void)onSocket:(AsyncSocket *)sock willDisconnectWithError:(NSError *)err {
    if (sock == self.socket) {
        NSLog(@"Err!!! socket will disconnect. with error %@", [err debugDescription]);
    } 
}
- (void)onSocketDidDisconnect:(AsyncSocket *)sock {
    if (sock == self.socket) {
        if ( ! [self.socket isConnected]) {
            NSLog(@"Pull disconnected reset state of login");
            self.isLogin = NO;
        }
    } 
}
- (void)onSocket:(AsyncSocket *)sock didConnectToHost:(NSString *)host port:(UInt16)port {
    NSLog(@"connected to %@:%d", host,port);
    //    NSLog(@"login as :%@", loginSecret);
    if (sock == self.socket) {
        [self login];
    }
}
- (void)onSocket:(AsyncSocket *)sock didReadData:(NSData *)data withTag:(long)tag {
    NSString* dataString = [[NSString alloc] initWithData:data
                                                 encoding:NSUTF8StringEncoding];
    if (sock == self.socket) {
        [self readDataString:dataString];
    }
    [self.socket readDataWithTimeout:-1 tag:0];
}


@end
