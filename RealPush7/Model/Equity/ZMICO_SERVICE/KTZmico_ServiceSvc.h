#import <Foundation/Foundation.h>
#import "USAdditions.h"
#import <libxml/tree.h>
#import "USGlobals.h"
#import <objc/runtime.h>
@class KTZmico_ServiceSvc_SingleSingOn;
@class KTZmico_ServiceSvc_SingleSingOnResponse;
@class KTZmico_ServiceSvc_SingleSingOnResult;
@class KTZmico_ServiceSvc_CheckZnetVersion;
@class KTZmico_ServiceSvc_CheckZnetVersionResponse;
@class KTZmico_ServiceSvc_AccountList;
@class KTZmico_ServiceSvc_AccountListResponse;
@class KTZmico_ServiceSvc_AccountListResult;
@class KTZmico_ServiceSvc_AccountInfo;
@class KTZmico_ServiceSvc_AccountInfoResponse;
@class KTZmico_ServiceSvc_AccountInfoResult;
@class KTZmico_ServiceSvc_Login_Service;
@class KTZmico_ServiceSvc_Login_ServiceResponse;
@class KTZmico_ServiceSvc_Login_ServiceResult;
@class KTZmico_ServiceSvc_OSXLogin_Service;
@class KTZmico_ServiceSvc_OSXLogin_ServiceResponse;
@class KTZmico_ServiceSvc_OSXLogin_ServiceResult;
@class KTZmico_ServiceSvc_ResetPin_Service;
@class KTZmico_ServiceSvc_ResetPin_ServiceResponse;
@class KTZmico_ServiceSvc_ResetPin_ServiceResult;
@class KTZmico_ServiceSvc_OrderInfo;
@class KTZmico_ServiceSvc_OrderInfoResponse;
@class KTZmico_ServiceSvc_OrderInfoResult;
@class KTZmico_ServiceSvc_Portfolio;
@class KTZmico_ServiceSvc_PortfolioResponse;
@class KTZmico_ServiceSvc_PortfolioResult;
@class KTZmico_ServiceSvc_PutOrderPublic;
@class KTZmico_ServiceSvc_PutOrderPublicResponse;
@class KTZmico_ServiceSvc_PutOrderPublicResult;
@class KTZmico_ServiceSvc_CancelOrder;
@class KTZmico_ServiceSvc_CancelOrderResponse;
@class KTZmico_ServiceSvc_CancelOrderResult;
@class KTZmico_ServiceSvc_GETFAV;
@class KTZmico_ServiceSvc_GETFAVResponse;
@class KTZmico_ServiceSvc_GETFAVResult;
@class KTZmico_ServiceSvc_SAVEFAV;
@class KTZmico_ServiceSvc_SAVEFAVResponse;
@class KTZmico_ServiceSvc_SAVEFAVResult;
@class KTZmico_ServiceSvc_Post_ATS;
@class KTZmico_ServiceSvc_Post_ATSResponse;
@class KTZmico_ServiceSvc_Post_ATSResult;
@class KTZmico_ServiceSvc_Post_Settlement;
@class KTZmico_ServiceSvc_Post_SettlementResponse;
@class KTZmico_ServiceSvc_Post_SettlementResult;
@class KTZmico_ServiceSvc_Post_Deposit;
@class KTZmico_ServiceSvc_Post_DepositResponse;
@class KTZmico_ServiceSvc_Post_DepositResult;
@class KTZmico_ServiceSvc_Post_Withdraw;
@class KTZmico_ServiceSvc_Post_WithdrawResponse;
@class KTZmico_ServiceSvc_Post_WithdrawResult;
@class KTZmico_ServiceSvc_Post_Transferstock;
@class KTZmico_ServiceSvc_Post_TransferstockResponse;
@class KTZmico_ServiceSvc_Post_TransferstockResult;
@class KTZmico_ServiceSvc_TodayTradeSum;
@class KTZmico_ServiceSvc_TodayTradeSumResponse;
@class KTZmico_ServiceSvc_TodayTradeSumResult;
@interface KTZmico_ServiceSvc_SingleSingOn : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * autkey;
	NSString * ssid;
	NSString * login;
	NSString * page;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (KTZmico_ServiceSvc_SingleSingOn *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * autkey;
@property (nonatomic, retain) NSString * ssid;
@property (nonatomic, retain) NSString * login;
@property (nonatomic, retain) NSString * page;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface KTZmico_ServiceSvc_SingleSingOnResult : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (KTZmico_ServiceSvc_SingleSingOnResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface KTZmico_ServiceSvc_SingleSingOnResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	KTZmico_ServiceSvc_SingleSingOnResult * SingleSingOnResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (KTZmico_ServiceSvc_SingleSingOnResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) KTZmico_ServiceSvc_SingleSingOnResult * SingleSingOnResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface KTZmico_ServiceSvc_CheckZnetVersion : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * device_version;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (KTZmico_ServiceSvc_CheckZnetVersion *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * device_version;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface KTZmico_ServiceSvc_CheckZnetVersionResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * CheckZnetVersionResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (KTZmico_ServiceSvc_CheckZnetVersionResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * CheckZnetVersionResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface KTZmico_ServiceSvc_AccountList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * customer_map_key;
	NSString * autkey;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (KTZmico_ServiceSvc_AccountList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * customer_map_key;
@property (nonatomic, retain) NSString * autkey;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface KTZmico_ServiceSvc_AccountListResult : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (KTZmico_ServiceSvc_AccountListResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface KTZmico_ServiceSvc_AccountListResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	KTZmico_ServiceSvc_AccountListResult * AccountListResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (KTZmico_ServiceSvc_AccountListResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) KTZmico_ServiceSvc_AccountListResult * AccountListResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface KTZmico_ServiceSvc_AccountInfo : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * username;
	NSString * password;
	NSString * ssid;
	NSString * autkey;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (KTZmico_ServiceSvc_AccountInfo *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * username;
@property (nonatomic, retain) NSString * password;
@property (nonatomic, retain) NSString * ssid;
@property (nonatomic, retain) NSString * autkey;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface KTZmico_ServiceSvc_AccountInfoResult : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (KTZmico_ServiceSvc_AccountInfoResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface KTZmico_ServiceSvc_AccountInfoResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	KTZmico_ServiceSvc_AccountInfoResult * AccountInfoResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (KTZmico_ServiceSvc_AccountInfoResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) KTZmico_ServiceSvc_AccountInfoResult * AccountInfoResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface KTZmico_ServiceSvc_Login_Service : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * username;
	NSString * password;
	NSString * autkey;
	NSString * version;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (KTZmico_ServiceSvc_Login_Service *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * username;
@property (nonatomic, retain) NSString * password;
@property (nonatomic, retain) NSString * autkey;
@property (nonatomic, retain) NSString * version;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface KTZmico_ServiceSvc_Login_ServiceResult : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (KTZmico_ServiceSvc_Login_ServiceResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface KTZmico_ServiceSvc_Login_ServiceResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	KTZmico_ServiceSvc_Login_ServiceResult * Login_ServiceResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (KTZmico_ServiceSvc_Login_ServiceResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) KTZmico_ServiceSvc_Login_ServiceResult * Login_ServiceResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface KTZmico_ServiceSvc_OSXLogin_Service : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * username;
	NSString * password;
	NSString * autkey;
	NSString * version;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (KTZmico_ServiceSvc_OSXLogin_Service *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * username;
@property (nonatomic, retain) NSString * password;
@property (nonatomic, retain) NSString * autkey;
@property (nonatomic, retain) NSString * version;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface KTZmico_ServiceSvc_OSXLogin_ServiceResult : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (KTZmico_ServiceSvc_OSXLogin_ServiceResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface KTZmico_ServiceSvc_OSXLogin_ServiceResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	KTZmico_ServiceSvc_OSXLogin_ServiceResult * OSXLogin_ServiceResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (KTZmico_ServiceSvc_OSXLogin_ServiceResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) KTZmico_ServiceSvc_OSXLogin_ServiceResult * OSXLogin_ServiceResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface KTZmico_ServiceSvc_ResetPin_Service : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * username;
	NSString * password;
	NSString * pinid;
	NSString * idcard;
	NSString * customer_map_key;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (KTZmico_ServiceSvc_ResetPin_Service *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * username;
@property (nonatomic, retain) NSString * password;
@property (nonatomic, retain) NSString * pinid;
@property (nonatomic, retain) NSString * idcard;
@property (nonatomic, retain) NSString * customer_map_key;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface KTZmico_ServiceSvc_ResetPin_ServiceResult : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (KTZmico_ServiceSvc_ResetPin_ServiceResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface KTZmico_ServiceSvc_ResetPin_ServiceResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	KTZmico_ServiceSvc_ResetPin_ServiceResult * ResetPin_ServiceResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (KTZmico_ServiceSvc_ResetPin_ServiceResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) KTZmico_ServiceSvc_ResetPin_ServiceResult * ResetPin_ServiceResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface KTZmico_ServiceSvc_OrderInfo : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * username;
	NSString * password;
	NSString * ssid;
	NSString * autkey;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (KTZmico_ServiceSvc_OrderInfo *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * username;
@property (nonatomic, retain) NSString * password;
@property (nonatomic, retain) NSString * ssid;
@property (nonatomic, retain) NSString * autkey;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface KTZmico_ServiceSvc_OrderInfoResult : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (KTZmico_ServiceSvc_OrderInfoResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface KTZmico_ServiceSvc_OrderInfoResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	KTZmico_ServiceSvc_OrderInfoResult * OrderInfoResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (KTZmico_ServiceSvc_OrderInfoResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) KTZmico_ServiceSvc_OrderInfoResult * OrderInfoResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface KTZmico_ServiceSvc_Portfolio : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * username;
	NSString * password;
	NSString * ssid;
	NSString * autkey;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (KTZmico_ServiceSvc_Portfolio *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * username;
@property (nonatomic, retain) NSString * password;
@property (nonatomic, retain) NSString * ssid;
@property (nonatomic, retain) NSString * autkey;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface KTZmico_ServiceSvc_PortfolioResult : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (KTZmico_ServiceSvc_PortfolioResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface KTZmico_ServiceSvc_PortfolioResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	KTZmico_ServiceSvc_PortfolioResult * PortfolioResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (KTZmico_ServiceSvc_PortfolioResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) KTZmico_ServiceSvc_PortfolioResult * PortfolioResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface KTZmico_ServiceSvc_PutOrderPublic : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * ipaddress;
	NSString * username;
	NSString * password;
	NSString * cust_key;
	NSString * pin;
	NSString * stock;
	NSString * side;
	NSNumber * qty;
	NSString * price;
	NSString * nvdr;
	NSString * Validity;
	NSDecimalNumber * public_vol;
	NSString * ssid;
	NSString * autkey;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (KTZmico_ServiceSvc_PutOrderPublic *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * ipaddress;
@property (nonatomic, retain) NSString * username;
@property (nonatomic, retain) NSString * password;
@property (nonatomic, retain) NSString * cust_key;
@property (nonatomic, retain) NSString * pin;
@property (nonatomic, retain) NSString * stock;
@property (nonatomic, retain) NSString * side;
@property (nonatomic, retain) NSNumber * qty;
@property (nonatomic, retain) NSString * price;
@property (nonatomic, retain) NSString * nvdr;
@property (nonatomic, retain) NSString * Validity;
@property (nonatomic, retain) NSDecimalNumber * public_vol;
@property (nonatomic, retain) NSString * ssid;
@property (nonatomic, retain) NSString * autkey;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface KTZmico_ServiceSvc_PutOrderPublicResult : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (KTZmico_ServiceSvc_PutOrderPublicResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface KTZmico_ServiceSvc_PutOrderPublicResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	KTZmico_ServiceSvc_PutOrderPublicResult * PutOrderPublicResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (KTZmico_ServiceSvc_PutOrderPublicResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) KTZmico_ServiceSvc_PutOrderPublicResult * PutOrderPublicResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface KTZmico_ServiceSvc_CancelOrder : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * username;
	NSString * password;
	NSString * custkey;
	NSString * pin;
	NSString * ordno;
	NSString * senddate;
	NSString * ssid;
	NSString * autkey;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (KTZmico_ServiceSvc_CancelOrder *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * username;
@property (nonatomic, retain) NSString * password;
@property (nonatomic, retain) NSString * custkey;
@property (nonatomic, retain) NSString * pin;
@property (nonatomic, retain) NSString * ordno;
@property (nonatomic, retain) NSString * senddate;
@property (nonatomic, retain) NSString * ssid;
@property (nonatomic, retain) NSString * autkey;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface KTZmico_ServiceSvc_CancelOrderResult : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (KTZmico_ServiceSvc_CancelOrderResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface KTZmico_ServiceSvc_CancelOrderResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	KTZmico_ServiceSvc_CancelOrderResult * CancelOrderResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (KTZmico_ServiceSvc_CancelOrderResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) KTZmico_ServiceSvc_CancelOrderResult * CancelOrderResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface KTZmico_ServiceSvc_GETFAV : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * username;
	NSString * autkey;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (KTZmico_ServiceSvc_GETFAV *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * username;
@property (nonatomic, retain) NSString * autkey;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface KTZmico_ServiceSvc_GETFAVResult : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (KTZmico_ServiceSvc_GETFAVResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface KTZmico_ServiceSvc_GETFAVResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	KTZmico_ServiceSvc_GETFAVResult * GETFAVResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (KTZmico_ServiceSvc_GETFAVResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) KTZmico_ServiceSvc_GETFAVResult * GETFAVResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface KTZmico_ServiceSvc_SAVEFAV : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * username;
	NSString * fav;
	NSString * autkey;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (KTZmico_ServiceSvc_SAVEFAV *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * username;
@property (nonatomic, retain) NSString * fav;
@property (nonatomic, retain) NSString * autkey;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface KTZmico_ServiceSvc_SAVEFAVResult : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (KTZmico_ServiceSvc_SAVEFAVResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface KTZmico_ServiceSvc_SAVEFAVResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	KTZmico_ServiceSvc_SAVEFAVResult * SAVEFAVResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (KTZmico_ServiceSvc_SAVEFAVResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) KTZmico_ServiceSvc_SAVEFAVResult * SAVEFAVResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface KTZmico_ServiceSvc_Post_ATS : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * account_no;
	NSString * account_name;
	NSString * Tel;
	NSString * valid_date;
	NSString * bank;
	NSString * amount;
	NSString * amountword;
	NSString * share_tran;
	NSString * share_amount;
	NSString * physic_tran;
	NSString * physic_amount;
	NSString * penalty;
	NSString * penalty_amount;
	NSString * cash_sec;
	NSString * cash_sec_amount;
	NSString * cash_tfex;
	NSString * cash_tfex_amount;
	NSString * prepayment;
	NSString * prepayment_amount;
	NSString * other;
	NSString * other_amount;
	NSString * summary_pay_amount;
	NSString * autkey;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (KTZmico_ServiceSvc_Post_ATS *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * account_no;
@property (nonatomic, retain) NSString * account_name;
@property (nonatomic, retain) NSString * Tel;
@property (nonatomic, retain) NSString * valid_date;
@property (nonatomic, retain) NSString * bank;
@property (nonatomic, retain) NSString * amount;
@property (nonatomic, retain) NSString * amountword;
@property (nonatomic, retain) NSString * share_tran;
@property (nonatomic, retain) NSString * share_amount;
@property (nonatomic, retain) NSString * physic_tran;
@property (nonatomic, retain) NSString * physic_amount;
@property (nonatomic, retain) NSString * penalty;
@property (nonatomic, retain) NSString * penalty_amount;
@property (nonatomic, retain) NSString * cash_sec;
@property (nonatomic, retain) NSString * cash_sec_amount;
@property (nonatomic, retain) NSString * cash_tfex;
@property (nonatomic, retain) NSString * cash_tfex_amount;
@property (nonatomic, retain) NSString * prepayment;
@property (nonatomic, retain) NSString * prepayment_amount;
@property (nonatomic, retain) NSString * other;
@property (nonatomic, retain) NSString * other_amount;
@property (nonatomic, retain) NSString * summary_pay_amount;
@property (nonatomic, retain) NSString * autkey;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface KTZmico_ServiceSvc_Post_ATSResult : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (KTZmico_ServiceSvc_Post_ATSResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface KTZmico_ServiceSvc_Post_ATSResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	KTZmico_ServiceSvc_Post_ATSResult * Post_ATSResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (KTZmico_ServiceSvc_Post_ATSResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) KTZmico_ServiceSvc_Post_ATSResult * Post_ATSResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface KTZmico_ServiceSvc_Post_Settlement : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * account_no;
	NSString * account_name;
	NSString * Tel;
	NSString * Trade_date;
	NSString * Settle_date;
	NSString * bank;
	NSString * amount;
	NSString * amountword;
	NSString * share_tran;
	NSString * share_amount;
	NSString * physic_tran;
	NSString * physic_amount;
	NSString * penalty;
	NSString * penalty_amount;
	NSString * cash_sec;
	NSString * cash_sec_amount;
	NSString * cash_tfex;
	NSString * cash_tfex_amount;
	NSString * prepayment;
	NSString * prepayment_amount;
	NSString * other;
	NSString * other_amount;
	NSString * summary_pay_amount;
	NSString * autkey;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (KTZmico_ServiceSvc_Post_Settlement *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * account_no;
@property (nonatomic, retain) NSString * account_name;
@property (nonatomic, retain) NSString * Tel;
@property (nonatomic, retain) NSString * Trade_date;
@property (nonatomic, retain) NSString * Settle_date;
@property (nonatomic, retain) NSString * bank;
@property (nonatomic, retain) NSString * amount;
@property (nonatomic, retain) NSString * amountword;
@property (nonatomic, retain) NSString * share_tran;
@property (nonatomic, retain) NSString * share_amount;
@property (nonatomic, retain) NSString * physic_tran;
@property (nonatomic, retain) NSString * physic_amount;
@property (nonatomic, retain) NSString * penalty;
@property (nonatomic, retain) NSString * penalty_amount;
@property (nonatomic, retain) NSString * cash_sec;
@property (nonatomic, retain) NSString * cash_sec_amount;
@property (nonatomic, retain) NSString * cash_tfex;
@property (nonatomic, retain) NSString * cash_tfex_amount;
@property (nonatomic, retain) NSString * prepayment;
@property (nonatomic, retain) NSString * prepayment_amount;
@property (nonatomic, retain) NSString * other;
@property (nonatomic, retain) NSString * other_amount;
@property (nonatomic, retain) NSString * summary_pay_amount;
@property (nonatomic, retain) NSString * autkey;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface KTZmico_ServiceSvc_Post_SettlementResult : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (KTZmico_ServiceSvc_Post_SettlementResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface KTZmico_ServiceSvc_Post_SettlementResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	KTZmico_ServiceSvc_Post_SettlementResult * Post_SettlementResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (KTZmico_ServiceSvc_Post_SettlementResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) KTZmico_ServiceSvc_Post_SettlementResult * Post_SettlementResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface KTZmico_ServiceSvc_Post_Deposit : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * account_no;
	NSString * account_name;
	NSString * Tel;
	NSString * Email;
	NSString * deposit_date;
	NSString * deposit_time;
	NSString * amount;
	NSString * amountword;
	NSString * currency;
	NSString * depositby;
	NSString * bankname;
	NSString * bankbranch;
	NSString * bankacno;
	NSString * ipaddress;
	NSString * typebank;
	NSString * bankaddress;
	NSString * country;
	NSString * depositto;
	NSString * autkey;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (KTZmico_ServiceSvc_Post_Deposit *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * account_no;
@property (nonatomic, retain) NSString * account_name;
@property (nonatomic, retain) NSString * Tel;
@property (nonatomic, retain) NSString * Email;
@property (nonatomic, retain) NSString * deposit_date;
@property (nonatomic, retain) NSString * deposit_time;
@property (nonatomic, retain) NSString * amount;
@property (nonatomic, retain) NSString * amountword;
@property (nonatomic, retain) NSString * currency;
@property (nonatomic, retain) NSString * depositby;
@property (nonatomic, retain) NSString * bankname;
@property (nonatomic, retain) NSString * bankbranch;
@property (nonatomic, retain) NSString * bankacno;
@property (nonatomic, retain) NSString * ipaddress;
@property (nonatomic, retain) NSString * typebank;
@property (nonatomic, retain) NSString * bankaddress;
@property (nonatomic, retain) NSString * country;
@property (nonatomic, retain) NSString * depositto;
@property (nonatomic, retain) NSString * autkey;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface KTZmico_ServiceSvc_Post_DepositResult : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (KTZmico_ServiceSvc_Post_DepositResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface KTZmico_ServiceSvc_Post_DepositResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	KTZmico_ServiceSvc_Post_DepositResult * Post_DepositResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (KTZmico_ServiceSvc_Post_DepositResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) KTZmico_ServiceSvc_Post_DepositResult * Post_DepositResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface KTZmico_ServiceSvc_Post_Withdraw : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * account_no;
	NSString * account_name;
	NSString * Tel;
	NSString * Email;
	NSString * amount;
	NSString * amountword;
	NSString * wtype;
	NSString * wtype_desc;
	NSString * receive_method;
	NSString * receive_type;
	NSString * bankname;
	NSString * branchname;
	NSString * bank_account_name;
	NSString * bank_account_no;
	NSString * bank_address;
	NSString * country;
	NSString * currency;
	NSString * ipaddress;
	NSString * autkey;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (KTZmico_ServiceSvc_Post_Withdraw *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * account_no;
@property (nonatomic, retain) NSString * account_name;
@property (nonatomic, retain) NSString * Tel;
@property (nonatomic, retain) NSString * Email;
@property (nonatomic, retain) NSString * amount;
@property (nonatomic, retain) NSString * amountword;
@property (nonatomic, retain) NSString * wtype;
@property (nonatomic, retain) NSString * wtype_desc;
@property (nonatomic, retain) NSString * receive_method;
@property (nonatomic, retain) NSString * receive_type;
@property (nonatomic, retain) NSString * bankname;
@property (nonatomic, retain) NSString * branchname;
@property (nonatomic, retain) NSString * bank_account_name;
@property (nonatomic, retain) NSString * bank_account_no;
@property (nonatomic, retain) NSString * bank_address;
@property (nonatomic, retain) NSString * country;
@property (nonatomic, retain) NSString * currency;
@property (nonatomic, retain) NSString * ipaddress;
@property (nonatomic, retain) NSString * autkey;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface KTZmico_ServiceSvc_Post_WithdrawResult : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (KTZmico_ServiceSvc_Post_WithdrawResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface KTZmico_ServiceSvc_Post_WithdrawResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	KTZmico_ServiceSvc_Post_WithdrawResult * Post_WithdrawResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (KTZmico_ServiceSvc_Post_WithdrawResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) KTZmico_ServiceSvc_Post_WithdrawResult * Post_WithdrawResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface KTZmico_ServiceSvc_Post_Transferstock : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * login;
	NSString * contact;
	NSString * email;
	NSString * frm_acc;
	NSString * to_acc;
	NSString * transfer_text;
	NSString * autkey;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (KTZmico_ServiceSvc_Post_Transferstock *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * login;
@property (nonatomic, retain) NSString * contact;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * frm_acc;
@property (nonatomic, retain) NSString * to_acc;
@property (nonatomic, retain) NSString * transfer_text;
@property (nonatomic, retain) NSString * autkey;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface KTZmico_ServiceSvc_Post_TransferstockResult : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (KTZmico_ServiceSvc_Post_TransferstockResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface KTZmico_ServiceSvc_Post_TransferstockResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	KTZmico_ServiceSvc_Post_TransferstockResult * Post_TransferstockResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (KTZmico_ServiceSvc_Post_TransferstockResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) KTZmico_ServiceSvc_Post_TransferstockResult * Post_TransferstockResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface KTZmico_ServiceSvc_TodayTradeSum : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * username;
	NSString * autkey;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (KTZmico_ServiceSvc_TodayTradeSum *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * username;
@property (nonatomic, retain) NSString * autkey;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface KTZmico_ServiceSvc_TodayTradeSumResult : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (KTZmico_ServiceSvc_TodayTradeSumResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface KTZmico_ServiceSvc_TodayTradeSumResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	KTZmico_ServiceSvc_TodayTradeSumResult * TodayTradeSumResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (KTZmico_ServiceSvc_TodayTradeSumResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) KTZmico_ServiceSvc_TodayTradeSumResult * TodayTradeSumResult;
/* attributes */
- (NSDictionary *)attributes;
@end
/* Cookies handling provided by http://en.wikibooks.org/wiki/Programming:WebObjects/Web_Services/Web_Service_Provider */
#import <libxml/parser.h>
#import "xsd.h"
#import "KTZmico_ServiceSvc.h"
@class KTZmico_ServiceSoapBinding;
@class KTZmico_ServiceSoap12Binding;
@interface KTZmico_ServiceSvc : NSObject {
	
}
+ (KTZmico_ServiceSoapBinding *)KTZmico_ServiceSoapBinding;
+ (KTZmico_ServiceSoap12Binding *)KTZmico_ServiceSoap12Binding;
@end
@class KTZmico_ServiceSoapBindingResponse;
@class KTZmico_ServiceSoapBindingOperation;
@protocol KTZmico_ServiceSoapBindingResponseDelegate <NSObject>
- (void) operation:(KTZmico_ServiceSoapBindingOperation *)operation completedWithResponse:(KTZmico_ServiceSoapBindingResponse *)response;
@end
#define kServerAnchorCertificates   @"kServerAnchorCertificates"
#define kServerAnchorsOnly          @"kServerAnchorsOnly"
#define kClientIdentity             @"kClientIdentity"
#define kClientCertificates         @"kClientCertificates"
#define kClientUsername             @"kClientUsername"
#define kClientPassword             @"kClientPassword"
#define kNSURLCredentialPersistence @"kNSURLCredentialPersistence"
#define kValidateResult             @"kValidateResult"
@interface KTZmico_ServiceSoapBinding : NSObject <KTZmico_ServiceSoapBindingResponseDelegate> {
	NSURL *address;
	NSTimeInterval timeout;
	NSMutableArray *cookies;
	NSMutableDictionary *customHeaders;
	BOOL logXMLInOut;
	BOOL ignoreEmptyResponse;
	BOOL synchronousOperationComplete;
	id<SSLCredentialsManaging> sslManager;
	SOAPSigner *soapSigner;
}
@property (nonatomic, copy) NSURL *address;
@property (nonatomic) BOOL logXMLInOut;
@property (nonatomic) BOOL ignoreEmptyResponse;
@property (nonatomic) NSTimeInterval timeout;
@property (nonatomic, retain) NSMutableArray *cookies;
@property (nonatomic, retain) NSMutableDictionary *customHeaders;
@property (nonatomic, retain) id<SSLCredentialsManaging> sslManager;
@property (nonatomic, retain) SOAPSigner *soapSigner;
+ (NSTimeInterval) defaultTimeout;
- (id)initWithAddress:(NSString *)anAddress;
- (void)sendHTTPCallUsingBody:(NSString *)body soapAction:(NSString *)soapAction forOperation:(KTZmico_ServiceSoapBindingOperation *)operation;
- (void)addCookie:(NSHTTPCookie *)toAdd;
- (NSString *)MIMEType;
- (KTZmico_ServiceSoapBindingResponse *)SingleSingOnUsingParameters:(KTZmico_ServiceSvc_SingleSingOn *)aParameters ;
- (void)SingleSingOnAsyncUsingParameters:(KTZmico_ServiceSvc_SingleSingOn *)aParameters  delegate:(id<KTZmico_ServiceSoapBindingResponseDelegate>)responseDelegate;
- (KTZmico_ServiceSoapBindingResponse *)CheckZnetVersionUsingParameters:(KTZmico_ServiceSvc_CheckZnetVersion *)aParameters ;
- (void)CheckZnetVersionAsyncUsingParameters:(KTZmico_ServiceSvc_CheckZnetVersion *)aParameters  delegate:(id<KTZmico_ServiceSoapBindingResponseDelegate>)responseDelegate;
- (KTZmico_ServiceSoapBindingResponse *)AccountListUsingParameters:(KTZmico_ServiceSvc_AccountList *)aParameters ;
- (void)AccountListAsyncUsingParameters:(KTZmico_ServiceSvc_AccountList *)aParameters  delegate:(id<KTZmico_ServiceSoapBindingResponseDelegate>)responseDelegate;
- (KTZmico_ServiceSoapBindingResponse *)AccountInfoUsingParameters:(KTZmico_ServiceSvc_AccountInfo *)aParameters ;
- (void)AccountInfoAsyncUsingParameters:(KTZmico_ServiceSvc_AccountInfo *)aParameters  delegate:(id<KTZmico_ServiceSoapBindingResponseDelegate>)responseDelegate;
- (KTZmico_ServiceSoapBindingResponse *)Login_ServiceUsingParameters:(KTZmico_ServiceSvc_Login_Service *)aParameters ;
- (void)Login_ServiceAsyncUsingParameters:(KTZmico_ServiceSvc_Login_Service *)aParameters  delegate:(id<KTZmico_ServiceSoapBindingResponseDelegate>)responseDelegate;
- (KTZmico_ServiceSoapBindingResponse *)OSXLogin_ServiceUsingParameters:(KTZmico_ServiceSvc_OSXLogin_Service *)aParameters ;
- (void)OSXLogin_ServiceAsyncUsingParameters:(KTZmico_ServiceSvc_OSXLogin_Service *)aParameters  delegate:(id<KTZmico_ServiceSoapBindingResponseDelegate>)responseDelegate;
- (KTZmico_ServiceSoapBindingResponse *)ResetPin_ServiceUsingParameters:(KTZmico_ServiceSvc_ResetPin_Service *)aParameters ;
- (void)ResetPin_ServiceAsyncUsingParameters:(KTZmico_ServiceSvc_ResetPin_Service *)aParameters  delegate:(id<KTZmico_ServiceSoapBindingResponseDelegate>)responseDelegate;
- (KTZmico_ServiceSoapBindingResponse *)OrderInfoUsingParameters:(KTZmico_ServiceSvc_OrderInfo *)aParameters ;
- (void)OrderInfoAsyncUsingParameters:(KTZmico_ServiceSvc_OrderInfo *)aParameters  delegate:(id<KTZmico_ServiceSoapBindingResponseDelegate>)responseDelegate;
- (KTZmico_ServiceSoapBindingResponse *)PortfolioUsingParameters:(KTZmico_ServiceSvc_Portfolio *)aParameters ;
- (void)PortfolioAsyncUsingParameters:(KTZmico_ServiceSvc_Portfolio *)aParameters  delegate:(id<KTZmico_ServiceSoapBindingResponseDelegate>)responseDelegate;
- (KTZmico_ServiceSoapBindingResponse *)PutOrderPublicUsingParameters:(KTZmico_ServiceSvc_PutOrderPublic *)aParameters ;
- (void)PutOrderPublicAsyncUsingParameters:(KTZmico_ServiceSvc_PutOrderPublic *)aParameters  delegate:(id<KTZmico_ServiceSoapBindingResponseDelegate>)responseDelegate;
- (KTZmico_ServiceSoapBindingResponse *)CancelOrderUsingParameters:(KTZmico_ServiceSvc_CancelOrder *)aParameters ;
- (void)CancelOrderAsyncUsingParameters:(KTZmico_ServiceSvc_CancelOrder *)aParameters  delegate:(id<KTZmico_ServiceSoapBindingResponseDelegate>)responseDelegate;
- (KTZmico_ServiceSoapBindingResponse *)GETFAVUsingParameters:(KTZmico_ServiceSvc_GETFAV *)aParameters ;
- (void)GETFAVAsyncUsingParameters:(KTZmico_ServiceSvc_GETFAV *)aParameters  delegate:(id<KTZmico_ServiceSoapBindingResponseDelegate>)responseDelegate;
- (KTZmico_ServiceSoapBindingResponse *)SAVEFAVUsingParameters:(KTZmico_ServiceSvc_SAVEFAV *)aParameters ;
- (void)SAVEFAVAsyncUsingParameters:(KTZmico_ServiceSvc_SAVEFAV *)aParameters  delegate:(id<KTZmico_ServiceSoapBindingResponseDelegate>)responseDelegate;
- (KTZmico_ServiceSoapBindingResponse *)Post_ATSUsingParameters:(KTZmico_ServiceSvc_Post_ATS *)aParameters ;
- (void)Post_ATSAsyncUsingParameters:(KTZmico_ServiceSvc_Post_ATS *)aParameters  delegate:(id<KTZmico_ServiceSoapBindingResponseDelegate>)responseDelegate;
- (KTZmico_ServiceSoapBindingResponse *)Post_SettlementUsingParameters:(KTZmico_ServiceSvc_Post_Settlement *)aParameters ;
- (void)Post_SettlementAsyncUsingParameters:(KTZmico_ServiceSvc_Post_Settlement *)aParameters  delegate:(id<KTZmico_ServiceSoapBindingResponseDelegate>)responseDelegate;
- (KTZmico_ServiceSoapBindingResponse *)Post_DepositUsingParameters:(KTZmico_ServiceSvc_Post_Deposit *)aParameters ;
- (void)Post_DepositAsyncUsingParameters:(KTZmico_ServiceSvc_Post_Deposit *)aParameters  delegate:(id<KTZmico_ServiceSoapBindingResponseDelegate>)responseDelegate;
- (KTZmico_ServiceSoapBindingResponse *)Post_WithdrawUsingParameters:(KTZmico_ServiceSvc_Post_Withdraw *)aParameters ;
- (void)Post_WithdrawAsyncUsingParameters:(KTZmico_ServiceSvc_Post_Withdraw *)aParameters  delegate:(id<KTZmico_ServiceSoapBindingResponseDelegate>)responseDelegate;
- (KTZmico_ServiceSoapBindingResponse *)Post_TransferstockUsingParameters:(KTZmico_ServiceSvc_Post_Transferstock *)aParameters ;
- (void)Post_TransferstockAsyncUsingParameters:(KTZmico_ServiceSvc_Post_Transferstock *)aParameters  delegate:(id<KTZmico_ServiceSoapBindingResponseDelegate>)responseDelegate;
- (KTZmico_ServiceSoapBindingResponse *)TodayTradeSumUsingParameters:(KTZmico_ServiceSvc_TodayTradeSum *)aParameters ;
- (void)TodayTradeSumAsyncUsingParameters:(KTZmico_ServiceSvc_TodayTradeSum *)aParameters  delegate:(id<KTZmico_ServiceSoapBindingResponseDelegate>)responseDelegate;
@end
@interface KTZmico_ServiceSoapBindingOperation : NSOperation {
	KTZmico_ServiceSoapBinding *binding;
	KTZmico_ServiceSoapBindingResponse *response;
	NSMutableData *responseData;
	NSURLConnection *urlConnection;
}
@property (nonatomic, retain) KTZmico_ServiceSoapBinding *binding;
@property (nonatomic, readonly) KTZmico_ServiceSoapBindingResponse *response;
@property (nonatomic, assign) id<KTZmico_ServiceSoapBindingResponseDelegate> delegate;
@property (nonatomic, retain) NSMutableData *responseData;
@property (nonatomic, retain) NSURLConnection *urlConnection;
- (id)initWithBinding:(KTZmico_ServiceSoapBinding *)aBinding delegate:(id<KTZmico_ServiceSoapBindingResponseDelegate>)aDelegate;
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error;
@end
@interface KTZmico_ServiceSoapBinding_SingleSingOn : KTZmico_ServiceSoapBindingOperation {
	KTZmico_ServiceSvc_SingleSingOn * parameters;
}
@property (nonatomic, retain) KTZmico_ServiceSvc_SingleSingOn * parameters;
- (id)initWithBinding:(KTZmico_ServiceSoapBinding *)aBinding delegate:(id<KTZmico_ServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(KTZmico_ServiceSvc_SingleSingOn *)aParameters
;
@end
@interface KTZmico_ServiceSoapBinding_CheckZnetVersion : KTZmico_ServiceSoapBindingOperation {
	KTZmico_ServiceSvc_CheckZnetVersion * parameters;
}
@property (nonatomic, retain) KTZmico_ServiceSvc_CheckZnetVersion * parameters;
- (id)initWithBinding:(KTZmico_ServiceSoapBinding *)aBinding delegate:(id<KTZmico_ServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(KTZmico_ServiceSvc_CheckZnetVersion *)aParameters
;
@end
@interface KTZmico_ServiceSoapBinding_AccountList : KTZmico_ServiceSoapBindingOperation {
	KTZmico_ServiceSvc_AccountList * parameters;
}
@property (nonatomic, retain) KTZmico_ServiceSvc_AccountList * parameters;
- (id)initWithBinding:(KTZmico_ServiceSoapBinding *)aBinding delegate:(id<KTZmico_ServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(KTZmico_ServiceSvc_AccountList *)aParameters
;
@end
@interface KTZmico_ServiceSoapBinding_AccountInfo : KTZmico_ServiceSoapBindingOperation {
	KTZmico_ServiceSvc_AccountInfo * parameters;
}
@property (nonatomic, retain) KTZmico_ServiceSvc_AccountInfo * parameters;
- (id)initWithBinding:(KTZmico_ServiceSoapBinding *)aBinding delegate:(id<KTZmico_ServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(KTZmico_ServiceSvc_AccountInfo *)aParameters
;
@end
@interface KTZmico_ServiceSoapBinding_Login_Service : KTZmico_ServiceSoapBindingOperation {
	KTZmico_ServiceSvc_Login_Service * parameters;
}
@property (nonatomic, retain) KTZmico_ServiceSvc_Login_Service * parameters;
- (id)initWithBinding:(KTZmico_ServiceSoapBinding *)aBinding delegate:(id<KTZmico_ServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(KTZmico_ServiceSvc_Login_Service *)aParameters
;
@end
@interface KTZmico_ServiceSoapBinding_OSXLogin_Service : KTZmico_ServiceSoapBindingOperation {
	KTZmico_ServiceSvc_OSXLogin_Service * parameters;
}
@property (nonatomic, retain) KTZmico_ServiceSvc_OSXLogin_Service * parameters;
- (id)initWithBinding:(KTZmico_ServiceSoapBinding *)aBinding delegate:(id<KTZmico_ServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(KTZmico_ServiceSvc_OSXLogin_Service *)aParameters
;
@end
@interface KTZmico_ServiceSoapBinding_ResetPin_Service : KTZmico_ServiceSoapBindingOperation {
	KTZmico_ServiceSvc_ResetPin_Service * parameters;
}
@property (nonatomic, retain) KTZmico_ServiceSvc_ResetPin_Service * parameters;
- (id)initWithBinding:(KTZmico_ServiceSoapBinding *)aBinding delegate:(id<KTZmico_ServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(KTZmico_ServiceSvc_ResetPin_Service *)aParameters
;
@end
@interface KTZmico_ServiceSoapBinding_OrderInfo : KTZmico_ServiceSoapBindingOperation {
	KTZmico_ServiceSvc_OrderInfo * parameters;
}
@property (nonatomic, retain) KTZmico_ServiceSvc_OrderInfo * parameters;
- (id)initWithBinding:(KTZmico_ServiceSoapBinding *)aBinding delegate:(id<KTZmico_ServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(KTZmico_ServiceSvc_OrderInfo *)aParameters
;
@end
@interface KTZmico_ServiceSoapBinding_Portfolio : KTZmico_ServiceSoapBindingOperation {
	KTZmico_ServiceSvc_Portfolio * parameters;
}
@property (nonatomic, retain) KTZmico_ServiceSvc_Portfolio * parameters;
- (id)initWithBinding:(KTZmico_ServiceSoapBinding *)aBinding delegate:(id<KTZmico_ServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(KTZmico_ServiceSvc_Portfolio *)aParameters
;
@end
@interface KTZmico_ServiceSoapBinding_PutOrderPublic : KTZmico_ServiceSoapBindingOperation {
	KTZmico_ServiceSvc_PutOrderPublic * parameters;
}
@property (nonatomic, retain) KTZmico_ServiceSvc_PutOrderPublic * parameters;
- (id)initWithBinding:(KTZmico_ServiceSoapBinding *)aBinding delegate:(id<KTZmico_ServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(KTZmico_ServiceSvc_PutOrderPublic *)aParameters
;
@end
@interface KTZmico_ServiceSoapBinding_CancelOrder : KTZmico_ServiceSoapBindingOperation {
	KTZmico_ServiceSvc_CancelOrder * parameters;
}
@property (nonatomic, retain) KTZmico_ServiceSvc_CancelOrder * parameters;
- (id)initWithBinding:(KTZmico_ServiceSoapBinding *)aBinding delegate:(id<KTZmico_ServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(KTZmico_ServiceSvc_CancelOrder *)aParameters
;
@end
@interface KTZmico_ServiceSoapBinding_GETFAV : KTZmico_ServiceSoapBindingOperation {
	KTZmico_ServiceSvc_GETFAV * parameters;
}
@property (nonatomic, retain) KTZmico_ServiceSvc_GETFAV * parameters;
- (id)initWithBinding:(KTZmico_ServiceSoapBinding *)aBinding delegate:(id<KTZmico_ServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(KTZmico_ServiceSvc_GETFAV *)aParameters
;
@end
@interface KTZmico_ServiceSoapBinding_SAVEFAV : KTZmico_ServiceSoapBindingOperation {
	KTZmico_ServiceSvc_SAVEFAV * parameters;
}
@property (nonatomic, retain) KTZmico_ServiceSvc_SAVEFAV * parameters;
- (id)initWithBinding:(KTZmico_ServiceSoapBinding *)aBinding delegate:(id<KTZmico_ServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(KTZmico_ServiceSvc_SAVEFAV *)aParameters
;
@end
@interface KTZmico_ServiceSoapBinding_Post_ATS : KTZmico_ServiceSoapBindingOperation {
	KTZmico_ServiceSvc_Post_ATS * parameters;
}
@property (nonatomic, retain) KTZmico_ServiceSvc_Post_ATS * parameters;
- (id)initWithBinding:(KTZmico_ServiceSoapBinding *)aBinding delegate:(id<KTZmico_ServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(KTZmico_ServiceSvc_Post_ATS *)aParameters
;
@end
@interface KTZmico_ServiceSoapBinding_Post_Settlement : KTZmico_ServiceSoapBindingOperation {
	KTZmico_ServiceSvc_Post_Settlement * parameters;
}
@property (nonatomic, retain) KTZmico_ServiceSvc_Post_Settlement * parameters;
- (id)initWithBinding:(KTZmico_ServiceSoapBinding *)aBinding delegate:(id<KTZmico_ServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(KTZmico_ServiceSvc_Post_Settlement *)aParameters
;
@end
@interface KTZmico_ServiceSoapBinding_Post_Deposit : KTZmico_ServiceSoapBindingOperation {
	KTZmico_ServiceSvc_Post_Deposit * parameters;
}
@property (nonatomic, retain) KTZmico_ServiceSvc_Post_Deposit * parameters;
- (id)initWithBinding:(KTZmico_ServiceSoapBinding *)aBinding delegate:(id<KTZmico_ServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(KTZmico_ServiceSvc_Post_Deposit *)aParameters
;
@end
@interface KTZmico_ServiceSoapBinding_Post_Withdraw : KTZmico_ServiceSoapBindingOperation {
	KTZmico_ServiceSvc_Post_Withdraw * parameters;
}
@property (nonatomic, retain) KTZmico_ServiceSvc_Post_Withdraw * parameters;
- (id)initWithBinding:(KTZmico_ServiceSoapBinding *)aBinding delegate:(id<KTZmico_ServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(KTZmico_ServiceSvc_Post_Withdraw *)aParameters
;
@end
@interface KTZmico_ServiceSoapBinding_Post_Transferstock : KTZmico_ServiceSoapBindingOperation {
	KTZmico_ServiceSvc_Post_Transferstock * parameters;
}
@property (nonatomic, retain) KTZmico_ServiceSvc_Post_Transferstock * parameters;
- (id)initWithBinding:(KTZmico_ServiceSoapBinding *)aBinding delegate:(id<KTZmico_ServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(KTZmico_ServiceSvc_Post_Transferstock *)aParameters
;
@end
@interface KTZmico_ServiceSoapBinding_TodayTradeSum : KTZmico_ServiceSoapBindingOperation {
	KTZmico_ServiceSvc_TodayTradeSum * parameters;
}
@property (nonatomic, retain) KTZmico_ServiceSvc_TodayTradeSum * parameters;
- (id)initWithBinding:(KTZmico_ServiceSoapBinding *)aBinding delegate:(id<KTZmico_ServiceSoapBindingResponseDelegate>)aDelegate
	parameters:(KTZmico_ServiceSvc_TodayTradeSum *)aParameters
;
@end
@interface KTZmico_ServiceSoapBinding_envelope : NSObject {
}
+ (KTZmico_ServiceSoapBinding_envelope *)sharedInstance;
- (NSString *)serializedFormUsingHeaderElements:(NSDictionary *)headerElements bodyElements:(NSDictionary *)bodyElements bodyKeys:(NSArray *)bodyKeys;
@end
@interface KTZmico_ServiceSoapBindingResponse : NSObject {
	NSArray *headers;
	NSArray *bodyParts;
	NSError *error;
}
@property (nonatomic, retain) NSArray *headers;
@property (nonatomic, retain) NSArray *bodyParts;
@property (nonatomic, retain) NSError *error;
@end
@class KTZmico_ServiceSoap12BindingResponse;
@class KTZmico_ServiceSoap12BindingOperation;
@protocol KTZmico_ServiceSoap12BindingResponseDelegate <NSObject>
- (void) operation:(KTZmico_ServiceSoap12BindingOperation *)operation completedWithResponse:(KTZmico_ServiceSoap12BindingResponse *)response;
@end
#define kServerAnchorCertificates   @"kServerAnchorCertificates"
#define kServerAnchorsOnly          @"kServerAnchorsOnly"
#define kClientIdentity             @"kClientIdentity"
#define kClientCertificates         @"kClientCertificates"
#define kClientUsername             @"kClientUsername"
#define kClientPassword             @"kClientPassword"
#define kNSURLCredentialPersistence @"kNSURLCredentialPersistence"
#define kValidateResult             @"kValidateResult"
@interface KTZmico_ServiceSoap12Binding : NSObject <KTZmico_ServiceSoap12BindingResponseDelegate> {
	NSURL *address;
	NSTimeInterval timeout;
	NSMutableArray *cookies;
	NSMutableDictionary *customHeaders;
	BOOL logXMLInOut;
	BOOL ignoreEmptyResponse;
	BOOL synchronousOperationComplete;
	id<SSLCredentialsManaging> sslManager;
	SOAPSigner *soapSigner;
}
@property (nonatomic, copy) NSURL *address;
@property (nonatomic) BOOL logXMLInOut;
@property (nonatomic) BOOL ignoreEmptyResponse;
@property (nonatomic) NSTimeInterval timeout;
@property (nonatomic, retain) NSMutableArray *cookies;
@property (nonatomic, retain) NSMutableDictionary *customHeaders;
@property (nonatomic, retain) id<SSLCredentialsManaging> sslManager;
@property (nonatomic, retain) SOAPSigner *soapSigner;
+ (NSTimeInterval) defaultTimeout;
- (id)initWithAddress:(NSString *)anAddress;
- (void)sendHTTPCallUsingBody:(NSString *)body soapAction:(NSString *)soapAction forOperation:(KTZmico_ServiceSoap12BindingOperation *)operation;
- (void)addCookie:(NSHTTPCookie *)toAdd;
- (NSString *)MIMEType;
- (KTZmico_ServiceSoap12BindingResponse *)SingleSingOnUsingParameters:(KTZmico_ServiceSvc_SingleSingOn *)aParameters ;
- (void)SingleSingOnAsyncUsingParameters:(KTZmico_ServiceSvc_SingleSingOn *)aParameters  delegate:(id<KTZmico_ServiceSoap12BindingResponseDelegate>)responseDelegate;
- (KTZmico_ServiceSoap12BindingResponse *)CheckZnetVersionUsingParameters:(KTZmico_ServiceSvc_CheckZnetVersion *)aParameters ;
- (void)CheckZnetVersionAsyncUsingParameters:(KTZmico_ServiceSvc_CheckZnetVersion *)aParameters  delegate:(id<KTZmico_ServiceSoap12BindingResponseDelegate>)responseDelegate;
- (KTZmico_ServiceSoap12BindingResponse *)AccountListUsingParameters:(KTZmico_ServiceSvc_AccountList *)aParameters ;
- (void)AccountListAsyncUsingParameters:(KTZmico_ServiceSvc_AccountList *)aParameters  delegate:(id<KTZmico_ServiceSoap12BindingResponseDelegate>)responseDelegate;
- (KTZmico_ServiceSoap12BindingResponse *)AccountInfoUsingParameters:(KTZmico_ServiceSvc_AccountInfo *)aParameters ;
- (void)AccountInfoAsyncUsingParameters:(KTZmico_ServiceSvc_AccountInfo *)aParameters  delegate:(id<KTZmico_ServiceSoap12BindingResponseDelegate>)responseDelegate;
- (KTZmico_ServiceSoap12BindingResponse *)Login_ServiceUsingParameters:(KTZmico_ServiceSvc_Login_Service *)aParameters ;
- (void)Login_ServiceAsyncUsingParameters:(KTZmico_ServiceSvc_Login_Service *)aParameters  delegate:(id<KTZmico_ServiceSoap12BindingResponseDelegate>)responseDelegate;
- (KTZmico_ServiceSoap12BindingResponse *)OSXLogin_ServiceUsingParameters:(KTZmico_ServiceSvc_OSXLogin_Service *)aParameters ;
- (void)OSXLogin_ServiceAsyncUsingParameters:(KTZmico_ServiceSvc_OSXLogin_Service *)aParameters  delegate:(id<KTZmico_ServiceSoap12BindingResponseDelegate>)responseDelegate;
- (KTZmico_ServiceSoap12BindingResponse *)ResetPin_ServiceUsingParameters:(KTZmico_ServiceSvc_ResetPin_Service *)aParameters ;
- (void)ResetPin_ServiceAsyncUsingParameters:(KTZmico_ServiceSvc_ResetPin_Service *)aParameters  delegate:(id<KTZmico_ServiceSoap12BindingResponseDelegate>)responseDelegate;
- (KTZmico_ServiceSoap12BindingResponse *)OrderInfoUsingParameters:(KTZmico_ServiceSvc_OrderInfo *)aParameters ;
- (void)OrderInfoAsyncUsingParameters:(KTZmico_ServiceSvc_OrderInfo *)aParameters  delegate:(id<KTZmico_ServiceSoap12BindingResponseDelegate>)responseDelegate;
- (KTZmico_ServiceSoap12BindingResponse *)PortfolioUsingParameters:(KTZmico_ServiceSvc_Portfolio *)aParameters ;
- (void)PortfolioAsyncUsingParameters:(KTZmico_ServiceSvc_Portfolio *)aParameters  delegate:(id<KTZmico_ServiceSoap12BindingResponseDelegate>)responseDelegate;
- (KTZmico_ServiceSoap12BindingResponse *)PutOrderPublicUsingParameters:(KTZmico_ServiceSvc_PutOrderPublic *)aParameters ;
- (void)PutOrderPublicAsyncUsingParameters:(KTZmico_ServiceSvc_PutOrderPublic *)aParameters  delegate:(id<KTZmico_ServiceSoap12BindingResponseDelegate>)responseDelegate;
- (KTZmico_ServiceSoap12BindingResponse *)CancelOrderUsingParameters:(KTZmico_ServiceSvc_CancelOrder *)aParameters ;
- (void)CancelOrderAsyncUsingParameters:(KTZmico_ServiceSvc_CancelOrder *)aParameters  delegate:(id<KTZmico_ServiceSoap12BindingResponseDelegate>)responseDelegate;
- (KTZmico_ServiceSoap12BindingResponse *)GETFAVUsingParameters:(KTZmico_ServiceSvc_GETFAV *)aParameters ;
- (void)GETFAVAsyncUsingParameters:(KTZmico_ServiceSvc_GETFAV *)aParameters  delegate:(id<KTZmico_ServiceSoap12BindingResponseDelegate>)responseDelegate;
- (KTZmico_ServiceSoap12BindingResponse *)SAVEFAVUsingParameters:(KTZmico_ServiceSvc_SAVEFAV *)aParameters ;
- (void)SAVEFAVAsyncUsingParameters:(KTZmico_ServiceSvc_SAVEFAV *)aParameters  delegate:(id<KTZmico_ServiceSoap12BindingResponseDelegate>)responseDelegate;
- (KTZmico_ServiceSoap12BindingResponse *)Post_ATSUsingParameters:(KTZmico_ServiceSvc_Post_ATS *)aParameters ;
- (void)Post_ATSAsyncUsingParameters:(KTZmico_ServiceSvc_Post_ATS *)aParameters  delegate:(id<KTZmico_ServiceSoap12BindingResponseDelegate>)responseDelegate;
- (KTZmico_ServiceSoap12BindingResponse *)Post_SettlementUsingParameters:(KTZmico_ServiceSvc_Post_Settlement *)aParameters ;
- (void)Post_SettlementAsyncUsingParameters:(KTZmico_ServiceSvc_Post_Settlement *)aParameters  delegate:(id<KTZmico_ServiceSoap12BindingResponseDelegate>)responseDelegate;
- (KTZmico_ServiceSoap12BindingResponse *)Post_DepositUsingParameters:(KTZmico_ServiceSvc_Post_Deposit *)aParameters ;
- (void)Post_DepositAsyncUsingParameters:(KTZmico_ServiceSvc_Post_Deposit *)aParameters  delegate:(id<KTZmico_ServiceSoap12BindingResponseDelegate>)responseDelegate;
- (KTZmico_ServiceSoap12BindingResponse *)Post_WithdrawUsingParameters:(KTZmico_ServiceSvc_Post_Withdraw *)aParameters ;
- (void)Post_WithdrawAsyncUsingParameters:(KTZmico_ServiceSvc_Post_Withdraw *)aParameters  delegate:(id<KTZmico_ServiceSoap12BindingResponseDelegate>)responseDelegate;
- (KTZmico_ServiceSoap12BindingResponse *)Post_TransferstockUsingParameters:(KTZmico_ServiceSvc_Post_Transferstock *)aParameters ;
- (void)Post_TransferstockAsyncUsingParameters:(KTZmico_ServiceSvc_Post_Transferstock *)aParameters  delegate:(id<KTZmico_ServiceSoap12BindingResponseDelegate>)responseDelegate;
- (KTZmico_ServiceSoap12BindingResponse *)TodayTradeSumUsingParameters:(KTZmico_ServiceSvc_TodayTradeSum *)aParameters ;
- (void)TodayTradeSumAsyncUsingParameters:(KTZmico_ServiceSvc_TodayTradeSum *)aParameters  delegate:(id<KTZmico_ServiceSoap12BindingResponseDelegate>)responseDelegate;
@end
@interface KTZmico_ServiceSoap12BindingOperation : NSOperation {
	KTZmico_ServiceSoap12Binding *binding;
	KTZmico_ServiceSoap12BindingResponse *response;
	NSMutableData *responseData;
	NSURLConnection *urlConnection;
}
@property (nonatomic, retain) KTZmico_ServiceSoap12Binding *binding;
@property (nonatomic, readonly) KTZmico_ServiceSoap12BindingResponse *response;
@property (nonatomic, assign) id<KTZmico_ServiceSoap12BindingResponseDelegate> delegate;
@property (nonatomic, retain) NSMutableData *responseData;
@property (nonatomic, retain) NSURLConnection *urlConnection;
- (id)initWithBinding:(KTZmico_ServiceSoap12Binding *)aBinding delegate:(id<KTZmico_ServiceSoap12BindingResponseDelegate>)aDelegate;
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error;
@end
@interface KTZmico_ServiceSoap12Binding_SingleSingOn : KTZmico_ServiceSoap12BindingOperation {
	KTZmico_ServiceSvc_SingleSingOn * parameters;
}
@property (nonatomic, retain) KTZmico_ServiceSvc_SingleSingOn * parameters;
- (id)initWithBinding:(KTZmico_ServiceSoap12Binding *)aBinding delegate:(id<KTZmico_ServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(KTZmico_ServiceSvc_SingleSingOn *)aParameters
;
@end
@interface KTZmico_ServiceSoap12Binding_CheckZnetVersion : KTZmico_ServiceSoap12BindingOperation {
	KTZmico_ServiceSvc_CheckZnetVersion * parameters;
}
@property (nonatomic, retain) KTZmico_ServiceSvc_CheckZnetVersion * parameters;
- (id)initWithBinding:(KTZmico_ServiceSoap12Binding *)aBinding delegate:(id<KTZmico_ServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(KTZmico_ServiceSvc_CheckZnetVersion *)aParameters
;
@end
@interface KTZmico_ServiceSoap12Binding_AccountList : KTZmico_ServiceSoap12BindingOperation {
	KTZmico_ServiceSvc_AccountList * parameters;
}
@property (nonatomic, retain) KTZmico_ServiceSvc_AccountList * parameters;
- (id)initWithBinding:(KTZmico_ServiceSoap12Binding *)aBinding delegate:(id<KTZmico_ServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(KTZmico_ServiceSvc_AccountList *)aParameters
;
@end
@interface KTZmico_ServiceSoap12Binding_AccountInfo : KTZmico_ServiceSoap12BindingOperation {
	KTZmico_ServiceSvc_AccountInfo * parameters;
}
@property (nonatomic, retain) KTZmico_ServiceSvc_AccountInfo * parameters;
- (id)initWithBinding:(KTZmico_ServiceSoap12Binding *)aBinding delegate:(id<KTZmico_ServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(KTZmico_ServiceSvc_AccountInfo *)aParameters
;
@end
@interface KTZmico_ServiceSoap12Binding_Login_Service : KTZmico_ServiceSoap12BindingOperation {
	KTZmico_ServiceSvc_Login_Service * parameters;
}
@property (nonatomic, retain) KTZmico_ServiceSvc_Login_Service * parameters;
- (id)initWithBinding:(KTZmico_ServiceSoap12Binding *)aBinding delegate:(id<KTZmico_ServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(KTZmico_ServiceSvc_Login_Service *)aParameters
;
@end
@interface KTZmico_ServiceSoap12Binding_OSXLogin_Service : KTZmico_ServiceSoap12BindingOperation {
	KTZmico_ServiceSvc_OSXLogin_Service * parameters;
}
@property (nonatomic, retain) KTZmico_ServiceSvc_OSXLogin_Service * parameters;
- (id)initWithBinding:(KTZmico_ServiceSoap12Binding *)aBinding delegate:(id<KTZmico_ServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(KTZmico_ServiceSvc_OSXLogin_Service *)aParameters
;
@end
@interface KTZmico_ServiceSoap12Binding_ResetPin_Service : KTZmico_ServiceSoap12BindingOperation {
	KTZmico_ServiceSvc_ResetPin_Service * parameters;
}
@property (nonatomic, retain) KTZmico_ServiceSvc_ResetPin_Service * parameters;
- (id)initWithBinding:(KTZmico_ServiceSoap12Binding *)aBinding delegate:(id<KTZmico_ServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(KTZmico_ServiceSvc_ResetPin_Service *)aParameters
;
@end
@interface KTZmico_ServiceSoap12Binding_OrderInfo : KTZmico_ServiceSoap12BindingOperation {
	KTZmico_ServiceSvc_OrderInfo * parameters;
}
@property (nonatomic, retain) KTZmico_ServiceSvc_OrderInfo * parameters;
- (id)initWithBinding:(KTZmico_ServiceSoap12Binding *)aBinding delegate:(id<KTZmico_ServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(KTZmico_ServiceSvc_OrderInfo *)aParameters
;
@end
@interface KTZmico_ServiceSoap12Binding_Portfolio : KTZmico_ServiceSoap12BindingOperation {
	KTZmico_ServiceSvc_Portfolio * parameters;
}
@property (nonatomic, retain) KTZmico_ServiceSvc_Portfolio * parameters;
- (id)initWithBinding:(KTZmico_ServiceSoap12Binding *)aBinding delegate:(id<KTZmico_ServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(KTZmico_ServiceSvc_Portfolio *)aParameters
;
@end
@interface KTZmico_ServiceSoap12Binding_PutOrderPublic : KTZmico_ServiceSoap12BindingOperation {
	KTZmico_ServiceSvc_PutOrderPublic * parameters;
}
@property (nonatomic, retain) KTZmico_ServiceSvc_PutOrderPublic * parameters;
- (id)initWithBinding:(KTZmico_ServiceSoap12Binding *)aBinding delegate:(id<KTZmico_ServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(KTZmico_ServiceSvc_PutOrderPublic *)aParameters
;
@end
@interface KTZmico_ServiceSoap12Binding_CancelOrder : KTZmico_ServiceSoap12BindingOperation {
	KTZmico_ServiceSvc_CancelOrder * parameters;
}
@property (nonatomic, retain) KTZmico_ServiceSvc_CancelOrder * parameters;
- (id)initWithBinding:(KTZmico_ServiceSoap12Binding *)aBinding delegate:(id<KTZmico_ServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(KTZmico_ServiceSvc_CancelOrder *)aParameters
;
@end
@interface KTZmico_ServiceSoap12Binding_GETFAV : KTZmico_ServiceSoap12BindingOperation {
	KTZmico_ServiceSvc_GETFAV * parameters;
}
@property (nonatomic, retain) KTZmico_ServiceSvc_GETFAV * parameters;
- (id)initWithBinding:(KTZmico_ServiceSoap12Binding *)aBinding delegate:(id<KTZmico_ServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(KTZmico_ServiceSvc_GETFAV *)aParameters
;
@end
@interface KTZmico_ServiceSoap12Binding_SAVEFAV : KTZmico_ServiceSoap12BindingOperation {
	KTZmico_ServiceSvc_SAVEFAV * parameters;
}
@property (nonatomic, retain) KTZmico_ServiceSvc_SAVEFAV * parameters;
- (id)initWithBinding:(KTZmico_ServiceSoap12Binding *)aBinding delegate:(id<KTZmico_ServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(KTZmico_ServiceSvc_SAVEFAV *)aParameters
;
@end
@interface KTZmico_ServiceSoap12Binding_Post_ATS : KTZmico_ServiceSoap12BindingOperation {
	KTZmico_ServiceSvc_Post_ATS * parameters;
}
@property (nonatomic, retain) KTZmico_ServiceSvc_Post_ATS * parameters;
- (id)initWithBinding:(KTZmico_ServiceSoap12Binding *)aBinding delegate:(id<KTZmico_ServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(KTZmico_ServiceSvc_Post_ATS *)aParameters
;
@end
@interface KTZmico_ServiceSoap12Binding_Post_Settlement : KTZmico_ServiceSoap12BindingOperation {
	KTZmico_ServiceSvc_Post_Settlement * parameters;
}
@property (nonatomic, retain) KTZmico_ServiceSvc_Post_Settlement * parameters;
- (id)initWithBinding:(KTZmico_ServiceSoap12Binding *)aBinding delegate:(id<KTZmico_ServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(KTZmico_ServiceSvc_Post_Settlement *)aParameters
;
@end
@interface KTZmico_ServiceSoap12Binding_Post_Deposit : KTZmico_ServiceSoap12BindingOperation {
	KTZmico_ServiceSvc_Post_Deposit * parameters;
}
@property (nonatomic, retain) KTZmico_ServiceSvc_Post_Deposit * parameters;
- (id)initWithBinding:(KTZmico_ServiceSoap12Binding *)aBinding delegate:(id<KTZmico_ServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(KTZmico_ServiceSvc_Post_Deposit *)aParameters
;
@end
@interface KTZmico_ServiceSoap12Binding_Post_Withdraw : KTZmico_ServiceSoap12BindingOperation {
	KTZmico_ServiceSvc_Post_Withdraw * parameters;
}
@property (nonatomic, retain) KTZmico_ServiceSvc_Post_Withdraw * parameters;
- (id)initWithBinding:(KTZmico_ServiceSoap12Binding *)aBinding delegate:(id<KTZmico_ServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(KTZmico_ServiceSvc_Post_Withdraw *)aParameters
;
@end
@interface KTZmico_ServiceSoap12Binding_Post_Transferstock : KTZmico_ServiceSoap12BindingOperation {
	KTZmico_ServiceSvc_Post_Transferstock * parameters;
}
@property (nonatomic, retain) KTZmico_ServiceSvc_Post_Transferstock * parameters;
- (id)initWithBinding:(KTZmico_ServiceSoap12Binding *)aBinding delegate:(id<KTZmico_ServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(KTZmico_ServiceSvc_Post_Transferstock *)aParameters
;
@end
@interface KTZmico_ServiceSoap12Binding_TodayTradeSum : KTZmico_ServiceSoap12BindingOperation {
	KTZmico_ServiceSvc_TodayTradeSum * parameters;
}
@property (nonatomic, retain) KTZmico_ServiceSvc_TodayTradeSum * parameters;
- (id)initWithBinding:(KTZmico_ServiceSoap12Binding *)aBinding delegate:(id<KTZmico_ServiceSoap12BindingResponseDelegate>)aDelegate
	parameters:(KTZmico_ServiceSvc_TodayTradeSum *)aParameters
;
@end
@interface KTZmico_ServiceSoap12Binding_envelope : NSObject {
}
+ (KTZmico_ServiceSoap12Binding_envelope *)sharedInstance;
- (NSString *)serializedFormUsingHeaderElements:(NSDictionary *)headerElements bodyElements:(NSDictionary *)bodyElements bodyKeys:(NSArray *)bodyKeys;
@end
@interface KTZmico_ServiceSoap12BindingResponse : NSObject {
	NSArray *headers;
	NSArray *bodyParts;
	NSError *error;
}
@property (nonatomic, retain) NSArray *headers;
@property (nonatomic, retain) NSArray *bodyParts;
@property (nonatomic, retain) NSError *error;
@end
