#import <Foundation/Foundation.h>
#import "USAdditions.h"
#import <libxml/tree.h>
#import "USGlobals.h"
#import <objc/runtime.h>
@class ios_pullSvc_ticker;
@class ios_pullSvc_tickerResponse;
@class ios_pullSvc_tickerResult;
@class ios_pullSvc_Get_SectorVol;
@class ios_pullSvc_Get_SectorVolResponse;
@class ios_pullSvc_Get_SectorVolResult;
@class ios_pullSvc_load_sec;
@class ios_pullSvc_load_secResponse;
@class ios_pullSvc_load_mkinfo;
@class ios_pullSvc_load_mkinfoResponse;
@class ios_pullSvc_stock;
@class ios_pullSvc_stockResponse;
@class ios_pullSvc_stockResult;
@class ios_pullSvc_market_index;
@class ios_pullSvc_market_indexResponse;
@class ios_pullSvc_market_indexResult;
@class ios_pullSvc_chart_index;
@class ios_pullSvc_chart_indexResponse;
@class ios_pullSvc_chart_indexResult;
@class ios_pullSvc_Get_Most_Active_Vol;
@class ios_pullSvc_Get_Most_Active_VolResponse;
@class ios_pullSvc_Get_Most_Active_VolResult;
@class ios_pullSvc_Get_Most_Active_Val;
@class ios_pullSvc_Get_Most_Active_ValResponse;
@class ios_pullSvc_Get_Most_Active_ValResult;
@class ios_pullSvc_Get_Most_Swing;
@class ios_pullSvc_Get_Most_SwingResponse;
@class ios_pullSvc_Get_Most_SwingResult;
@class ios_pullSvc_Get_Most_Loser;
@class ios_pullSvc_Get_Most_LoserResponse;
@class ios_pullSvc_Get_Most_LoserResult;
@class ios_pullSvc_Get_Most_Gainer;
@class ios_pullSvc_Get_Most_GainerResponse;
@class ios_pullSvc_Get_Most_GainerResult;
@class ios_pullSvc_Get_stock_history;
@class ios_pullSvc_Get_stock_historyResponse;
@class ios_pullSvc_Get_stock_historyResult;
@class ios_pullSvc_Get_fundamental;
@class ios_pullSvc_Get_fundamentalResponse;
@class ios_pullSvc_Get_fundamentalResult;
@class ios_pullSvc_Get_StockInPlay;
@class ios_pullSvc_Get_StockInPlayResponse;
@class ios_pullSvc_Get_StockInPlayResult;
@class ios_pullSvc_Get_HighLowAvg;
@class ios_pullSvc_Get_HighLowAvgResponse;
@class ios_pullSvc_Get_HighLowAvgResult;
@class ios_pullSvc_stockbyprice;
@class ios_pullSvc_stockbypriceResponse;
@class ios_pullSvc_stockbypriceResult;
@class ios_pullSvc_getticker;
@class ios_pullSvc_gettickerResponse;
@class ios_pullSvc_gettickerResult;
@class ios_pullSvc_getDWList;
@class ios_pullSvc_getDWListResponse;
@class ios_pullSvc_getDWListResult;
@class ios_pullSvc_getsalebytime;
@class ios_pullSvc_getsalebytimeResponse;
@class ios_pullSvc_getsalebytimeResult;
@interface ios_pullSvc_ticker : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * autkey;
	NSNumber * lasttime;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (ios_pullSvc_ticker *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * autkey;
@property (nonatomic, retain) NSNumber * lasttime;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface ios_pullSvc_tickerResult : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (ios_pullSvc_tickerResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface ios_pullSvc_tickerResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	ios_pullSvc_tickerResult * tickerResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (ios_pullSvc_tickerResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) ios_pullSvc_tickerResult * tickerResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface ios_pullSvc_Get_SectorVol : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * autkey;
	NSNumber * sector_id;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (ios_pullSvc_Get_SectorVol *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * autkey;
@property (nonatomic, retain) NSNumber * sector_id;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface ios_pullSvc_Get_SectorVolResult : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (ios_pullSvc_Get_SectorVolResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface ios_pullSvc_Get_SectorVolResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	ios_pullSvc_Get_SectorVolResult * Get_SectorVolResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (ios_pullSvc_Get_SectorVolResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) ios_pullSvc_Get_SectorVolResult * Get_SectorVolResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface ios_pullSvc_load_sec : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * autkey;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (ios_pullSvc_load_sec *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * autkey;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface ios_pullSvc_load_secResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * load_secResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (ios_pullSvc_load_secResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * load_secResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface ios_pullSvc_load_mkinfo : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * autkey;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (ios_pullSvc_load_mkinfo *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * autkey;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface ios_pullSvc_load_mkinfoResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * load_mkinfoResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (ios_pullSvc_load_mkinfoResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * load_mkinfoResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface ios_pullSvc_stock : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * autkey;
	NSString * stock;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (ios_pullSvc_stock *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * autkey;
@property (nonatomic, retain) NSString * stock;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface ios_pullSvc_stockResult : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (ios_pullSvc_stockResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface ios_pullSvc_stockResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	ios_pullSvc_stockResult * stockResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (ios_pullSvc_stockResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) ios_pullSvc_stockResult * stockResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface ios_pullSvc_market_index : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * autkey;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (ios_pullSvc_market_index *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * autkey;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface ios_pullSvc_market_indexResult : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (ios_pullSvc_market_indexResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface ios_pullSvc_market_indexResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	ios_pullSvc_market_indexResult * market_indexResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (ios_pullSvc_market_indexResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) ios_pullSvc_market_indexResult * market_indexResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface ios_pullSvc_chart_index : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * autkey;
	NSNumber * market;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (ios_pullSvc_chart_index *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * autkey;
@property (nonatomic, retain) NSNumber * market;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface ios_pullSvc_chart_indexResult : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (ios_pullSvc_chart_indexResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface ios_pullSvc_chart_indexResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	ios_pullSvc_chart_indexResult * chart_indexResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (ios_pullSvc_chart_indexResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) ios_pullSvc_chart_indexResult * chart_indexResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface ios_pullSvc_Get_Most_Active_Vol : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * autkey;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (ios_pullSvc_Get_Most_Active_Vol *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * autkey;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface ios_pullSvc_Get_Most_Active_VolResult : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (ios_pullSvc_Get_Most_Active_VolResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface ios_pullSvc_Get_Most_Active_VolResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	ios_pullSvc_Get_Most_Active_VolResult * Get_Most_Active_VolResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (ios_pullSvc_Get_Most_Active_VolResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) ios_pullSvc_Get_Most_Active_VolResult * Get_Most_Active_VolResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface ios_pullSvc_Get_Most_Active_Val : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * autkey;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (ios_pullSvc_Get_Most_Active_Val *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * autkey;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface ios_pullSvc_Get_Most_Active_ValResult : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (ios_pullSvc_Get_Most_Active_ValResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface ios_pullSvc_Get_Most_Active_ValResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	ios_pullSvc_Get_Most_Active_ValResult * Get_Most_Active_ValResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (ios_pullSvc_Get_Most_Active_ValResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) ios_pullSvc_Get_Most_Active_ValResult * Get_Most_Active_ValResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface ios_pullSvc_Get_Most_Swing : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * autkey;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (ios_pullSvc_Get_Most_Swing *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * autkey;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface ios_pullSvc_Get_Most_SwingResult : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (ios_pullSvc_Get_Most_SwingResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface ios_pullSvc_Get_Most_SwingResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	ios_pullSvc_Get_Most_SwingResult * Get_Most_SwingResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (ios_pullSvc_Get_Most_SwingResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) ios_pullSvc_Get_Most_SwingResult * Get_Most_SwingResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface ios_pullSvc_Get_Most_Loser : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * autkey;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (ios_pullSvc_Get_Most_Loser *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * autkey;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface ios_pullSvc_Get_Most_LoserResult : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (ios_pullSvc_Get_Most_LoserResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface ios_pullSvc_Get_Most_LoserResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	ios_pullSvc_Get_Most_LoserResult * Get_Most_LoserResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (ios_pullSvc_Get_Most_LoserResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) ios_pullSvc_Get_Most_LoserResult * Get_Most_LoserResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface ios_pullSvc_Get_Most_Gainer : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * autkey;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (ios_pullSvc_Get_Most_Gainer *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * autkey;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface ios_pullSvc_Get_Most_GainerResult : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (ios_pullSvc_Get_Most_GainerResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface ios_pullSvc_Get_Most_GainerResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	ios_pullSvc_Get_Most_GainerResult * Get_Most_GainerResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (ios_pullSvc_Get_Most_GainerResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) ios_pullSvc_Get_Most_GainerResult * Get_Most_GainerResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface ios_pullSvc_Get_stock_history : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * autkey;
	NSString * stock;
	NSNumber * day;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (ios_pullSvc_Get_stock_history *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * autkey;
@property (nonatomic, retain) NSString * stock;
@property (nonatomic, retain) NSNumber * day;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface ios_pullSvc_Get_stock_historyResult : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (ios_pullSvc_Get_stock_historyResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface ios_pullSvc_Get_stock_historyResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	ios_pullSvc_Get_stock_historyResult * Get_stock_historyResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (ios_pullSvc_Get_stock_historyResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) ios_pullSvc_Get_stock_historyResult * Get_stock_historyResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface ios_pullSvc_Get_fundamental : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * autkey;
	NSString * stock;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (ios_pullSvc_Get_fundamental *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * autkey;
@property (nonatomic, retain) NSString * stock;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface ios_pullSvc_Get_fundamentalResult : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (ios_pullSvc_Get_fundamentalResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface ios_pullSvc_Get_fundamentalResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	ios_pullSvc_Get_fundamentalResult * Get_fundamentalResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (ios_pullSvc_Get_fundamentalResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) ios_pullSvc_Get_fundamentalResult * Get_fundamentalResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface ios_pullSvc_Get_StockInPlay : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * autkey;
	NSString * stock;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (ios_pullSvc_Get_StockInPlay *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * autkey;
@property (nonatomic, retain) NSString * stock;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface ios_pullSvc_Get_StockInPlayResult : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (ios_pullSvc_Get_StockInPlayResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface ios_pullSvc_Get_StockInPlayResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	ios_pullSvc_Get_StockInPlayResult * Get_StockInPlayResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (ios_pullSvc_Get_StockInPlayResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) ios_pullSvc_Get_StockInPlayResult * Get_StockInPlayResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface ios_pullSvc_Get_HighLowAvg : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * autkey;
	NSString * stock;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (ios_pullSvc_Get_HighLowAvg *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * autkey;
@property (nonatomic, retain) NSString * stock;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface ios_pullSvc_Get_HighLowAvgResult : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (ios_pullSvc_Get_HighLowAvgResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface ios_pullSvc_Get_HighLowAvgResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	ios_pullSvc_Get_HighLowAvgResult * Get_HighLowAvgResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (ios_pullSvc_Get_HighLowAvgResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) ios_pullSvc_Get_HighLowAvgResult * Get_HighLowAvgResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface ios_pullSvc_stockbyprice : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * autkey;
	NSString * stock;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (ios_pullSvc_stockbyprice *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * autkey;
@property (nonatomic, retain) NSString * stock;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface ios_pullSvc_stockbypriceResult : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (ios_pullSvc_stockbypriceResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface ios_pullSvc_stockbypriceResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	ios_pullSvc_stockbypriceResult * stockbypriceResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (ios_pullSvc_stockbypriceResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) ios_pullSvc_stockbypriceResult * stockbypriceResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface ios_pullSvc_getticker : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * autkey;
	NSString * key;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (ios_pullSvc_getticker *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * autkey;
@property (nonatomic, retain) NSString * key;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface ios_pullSvc_gettickerResult : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (ios_pullSvc_gettickerResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface ios_pullSvc_gettickerResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	ios_pullSvc_gettickerResult * gettickerResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (ios_pullSvc_gettickerResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) ios_pullSvc_gettickerResult * gettickerResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface ios_pullSvc_getDWList : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * autkey;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (ios_pullSvc_getDWList *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * autkey;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface ios_pullSvc_getDWListResult : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (ios_pullSvc_getDWListResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface ios_pullSvc_getDWListResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	ios_pullSvc_getDWListResult * getDWListResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (ios_pullSvc_getDWListResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) ios_pullSvc_getDWListResult * getDWListResult;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface ios_pullSvc_getsalebytime : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	NSString * autkey;
	NSString * stock;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (ios_pullSvc_getsalebytime *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) NSString * autkey;
@property (nonatomic, retain) NSString * stock;
/* attributes */
- (NSDictionary *)attributes;
@end
@interface ios_pullSvc_getsalebytimeResult : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (ios_pullSvc_getsalebytimeResult *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
/* attributes */
- (NSDictionary *)attributes;
@end
@interface ios_pullSvc_getsalebytimeResponse : NSObject <NSCoding> {
SOAPSigner *soapSigner;
/* elements */
	ios_pullSvc_getsalebytimeResult * getsalebytimeResult;
/* attributes */
}
- (NSString *)nsPrefix;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
- (void)addAttributesToNode:(xmlNodePtr)node;
- (void)addElementsToNode:(xmlNodePtr)node;
+ (ios_pullSvc_getsalebytimeResponse *)deserializeNode:(xmlNodePtr)cur;
- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;
- (void)deserializeElementsFromNode:(xmlNodePtr)cur;
@property (retain) SOAPSigner *soapSigner;
/* elements */
@property (nonatomic, retain) ios_pullSvc_getsalebytimeResult * getsalebytimeResult;
/* attributes */
- (NSDictionary *)attributes;
@end
/* Cookies handling provided by http://en.wikibooks.org/wiki/Programming:WebObjects/Web_Services/Web_Service_Provider */
#import <libxml/parser.h>
#import "xsd.h"
#import "ios_pullSvc.h"
@class ios_pullSoapBinding;
@class ios_pullSoap12Binding;
@interface ios_pullSvc : NSObject {
	
}
+ (ios_pullSoapBinding *)ios_pullSoapBinding;
+ (ios_pullSoap12Binding *)ios_pullSoap12Binding;
@end
@class ios_pullSoapBindingResponse;
@class ios_pullSoapBindingOperation;
@protocol ios_pullSoapBindingResponseDelegate <NSObject>
- (void) operation:(ios_pullSoapBindingOperation *)operation completedWithResponse:(ios_pullSoapBindingResponse *)response;
@end
#define kServerAnchorCertificates   @"kServerAnchorCertificates"
#define kServerAnchorsOnly          @"kServerAnchorsOnly"
#define kClientIdentity             @"kClientIdentity"
#define kClientCertificates         @"kClientCertificates"
#define kClientUsername             @"kClientUsername"
#define kClientPassword             @"kClientPassword"
#define kNSURLCredentialPersistence @"kNSURLCredentialPersistence"
#define kValidateResult             @"kValidateResult"
@interface ios_pullSoapBinding : NSObject <ios_pullSoapBindingResponseDelegate> {
	NSURL *address;
	NSTimeInterval timeout;
	NSMutableArray *cookies;
	NSMutableDictionary *customHeaders;
	BOOL logXMLInOut;
	BOOL ignoreEmptyResponse;
	BOOL synchronousOperationComplete;
	id<SSLCredentialsManaging> sslManager;
	SOAPSigner *soapSigner;
}
@property (nonatomic, copy) NSURL *address;
@property (nonatomic) BOOL logXMLInOut;
@property (nonatomic) BOOL ignoreEmptyResponse;
@property (nonatomic) NSTimeInterval timeout;
@property (nonatomic, retain) NSMutableArray *cookies;
@property (nonatomic, retain) NSMutableDictionary *customHeaders;
@property (nonatomic, retain) id<SSLCredentialsManaging> sslManager;
@property (nonatomic, retain) SOAPSigner *soapSigner;
+ (NSTimeInterval) defaultTimeout;
- (id)initWithAddress:(NSString *)anAddress;
- (void)sendHTTPCallUsingBody:(NSString *)body soapAction:(NSString *)soapAction forOperation:(ios_pullSoapBindingOperation *)operation;
- (void)addCookie:(NSHTTPCookie *)toAdd;
- (NSString *)MIMEType;
- (ios_pullSoapBindingResponse *)tickerUsingParameters:(ios_pullSvc_ticker *)aParameters ;
- (void)tickerAsyncUsingParameters:(ios_pullSvc_ticker *)aParameters  delegate:(id<ios_pullSoapBindingResponseDelegate>)responseDelegate;
- (ios_pullSoapBindingResponse *)Get_SectorVolUsingParameters:(ios_pullSvc_Get_SectorVol *)aParameters ;
- (void)Get_SectorVolAsyncUsingParameters:(ios_pullSvc_Get_SectorVol *)aParameters  delegate:(id<ios_pullSoapBindingResponseDelegate>)responseDelegate;
- (ios_pullSoapBindingResponse *)load_secUsingParameters:(ios_pullSvc_load_sec *)aParameters ;
- (void)load_secAsyncUsingParameters:(ios_pullSvc_load_sec *)aParameters  delegate:(id<ios_pullSoapBindingResponseDelegate>)responseDelegate;
- (ios_pullSoapBindingResponse *)load_mkinfoUsingParameters:(ios_pullSvc_load_mkinfo *)aParameters ;
- (void)load_mkinfoAsyncUsingParameters:(ios_pullSvc_load_mkinfo *)aParameters  delegate:(id<ios_pullSoapBindingResponseDelegate>)responseDelegate;
- (ios_pullSoapBindingResponse *)stockUsingParameters:(ios_pullSvc_stock *)aParameters ;
- (void)stockAsyncUsingParameters:(ios_pullSvc_stock *)aParameters  delegate:(id<ios_pullSoapBindingResponseDelegate>)responseDelegate;
- (ios_pullSoapBindingResponse *)market_indexUsingParameters:(ios_pullSvc_market_index *)aParameters ;
- (void)market_indexAsyncUsingParameters:(ios_pullSvc_market_index *)aParameters  delegate:(id<ios_pullSoapBindingResponseDelegate>)responseDelegate;
- (ios_pullSoapBindingResponse *)chart_indexUsingParameters:(ios_pullSvc_chart_index *)aParameters ;
- (void)chart_indexAsyncUsingParameters:(ios_pullSvc_chart_index *)aParameters  delegate:(id<ios_pullSoapBindingResponseDelegate>)responseDelegate;
- (ios_pullSoapBindingResponse *)Get_Most_Active_VolUsingParameters:(ios_pullSvc_Get_Most_Active_Vol *)aParameters ;
- (void)Get_Most_Active_VolAsyncUsingParameters:(ios_pullSvc_Get_Most_Active_Vol *)aParameters  delegate:(id<ios_pullSoapBindingResponseDelegate>)responseDelegate;
- (ios_pullSoapBindingResponse *)Get_Most_Active_ValUsingParameters:(ios_pullSvc_Get_Most_Active_Val *)aParameters ;
- (void)Get_Most_Active_ValAsyncUsingParameters:(ios_pullSvc_Get_Most_Active_Val *)aParameters  delegate:(id<ios_pullSoapBindingResponseDelegate>)responseDelegate;
- (ios_pullSoapBindingResponse *)Get_Most_SwingUsingParameters:(ios_pullSvc_Get_Most_Swing *)aParameters ;
- (void)Get_Most_SwingAsyncUsingParameters:(ios_pullSvc_Get_Most_Swing *)aParameters  delegate:(id<ios_pullSoapBindingResponseDelegate>)responseDelegate;
- (ios_pullSoapBindingResponse *)Get_Most_LoserUsingParameters:(ios_pullSvc_Get_Most_Loser *)aParameters ;
- (void)Get_Most_LoserAsyncUsingParameters:(ios_pullSvc_Get_Most_Loser *)aParameters  delegate:(id<ios_pullSoapBindingResponseDelegate>)responseDelegate;
- (ios_pullSoapBindingResponse *)Get_Most_GainerUsingParameters:(ios_pullSvc_Get_Most_Gainer *)aParameters ;
- (void)Get_Most_GainerAsyncUsingParameters:(ios_pullSvc_Get_Most_Gainer *)aParameters  delegate:(id<ios_pullSoapBindingResponseDelegate>)responseDelegate;
- (ios_pullSoapBindingResponse *)Get_stock_historyUsingParameters:(ios_pullSvc_Get_stock_history *)aParameters ;
- (void)Get_stock_historyAsyncUsingParameters:(ios_pullSvc_Get_stock_history *)aParameters  delegate:(id<ios_pullSoapBindingResponseDelegate>)responseDelegate;
- (ios_pullSoapBindingResponse *)Get_fundamentalUsingParameters:(ios_pullSvc_Get_fundamental *)aParameters ;
- (void)Get_fundamentalAsyncUsingParameters:(ios_pullSvc_Get_fundamental *)aParameters  delegate:(id<ios_pullSoapBindingResponseDelegate>)responseDelegate;
- (ios_pullSoapBindingResponse *)Get_StockInPlayUsingParameters:(ios_pullSvc_Get_StockInPlay *)aParameters ;
- (void)Get_StockInPlayAsyncUsingParameters:(ios_pullSvc_Get_StockInPlay *)aParameters  delegate:(id<ios_pullSoapBindingResponseDelegate>)responseDelegate;
- (ios_pullSoapBindingResponse *)Get_HighLowAvgUsingParameters:(ios_pullSvc_Get_HighLowAvg *)aParameters ;
- (void)Get_HighLowAvgAsyncUsingParameters:(ios_pullSvc_Get_HighLowAvg *)aParameters  delegate:(id<ios_pullSoapBindingResponseDelegate>)responseDelegate;
- (ios_pullSoapBindingResponse *)stockbypriceUsingParameters:(ios_pullSvc_stockbyprice *)aParameters ;
- (void)stockbypriceAsyncUsingParameters:(ios_pullSvc_stockbyprice *)aParameters  delegate:(id<ios_pullSoapBindingResponseDelegate>)responseDelegate;
- (ios_pullSoapBindingResponse *)gettickerUsingParameters:(ios_pullSvc_getticker *)aParameters ;
- (void)gettickerAsyncUsingParameters:(ios_pullSvc_getticker *)aParameters  delegate:(id<ios_pullSoapBindingResponseDelegate>)responseDelegate;
- (ios_pullSoapBindingResponse *)getDWListUsingParameters:(ios_pullSvc_getDWList *)aParameters ;
- (void)getDWListAsyncUsingParameters:(ios_pullSvc_getDWList *)aParameters  delegate:(id<ios_pullSoapBindingResponseDelegate>)responseDelegate;
- (ios_pullSoapBindingResponse *)getsalebytimeUsingParameters:(ios_pullSvc_getsalebytime *)aParameters ;
- (void)getsalebytimeAsyncUsingParameters:(ios_pullSvc_getsalebytime *)aParameters  delegate:(id<ios_pullSoapBindingResponseDelegate>)responseDelegate;
@end
@interface ios_pullSoapBindingOperation : NSOperation {
	ios_pullSoapBinding *binding;
	ios_pullSoapBindingResponse *response;
	NSMutableData *responseData;
	NSURLConnection *urlConnection;
}
@property (nonatomic, retain) ios_pullSoapBinding *binding;
@property (nonatomic, readonly) ios_pullSoapBindingResponse *response;
@property (nonatomic, assign) id<ios_pullSoapBindingResponseDelegate> delegate;
@property (nonatomic, retain) NSMutableData *responseData;
@property (nonatomic, retain) NSURLConnection *urlConnection;
- (id)initWithBinding:(ios_pullSoapBinding *)aBinding delegate:(id<ios_pullSoapBindingResponseDelegate>)aDelegate;
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error;
@end
@interface ios_pullSoapBinding_ticker : ios_pullSoapBindingOperation {
	ios_pullSvc_ticker * parameters;
}
@property (nonatomic, retain) ios_pullSvc_ticker * parameters;
- (id)initWithBinding:(ios_pullSoapBinding *)aBinding delegate:(id<ios_pullSoapBindingResponseDelegate>)aDelegate
	parameters:(ios_pullSvc_ticker *)aParameters
;
@end
@interface ios_pullSoapBinding_Get_SectorVol : ios_pullSoapBindingOperation {
	ios_pullSvc_Get_SectorVol * parameters;
}
@property (nonatomic, retain) ios_pullSvc_Get_SectorVol * parameters;
- (id)initWithBinding:(ios_pullSoapBinding *)aBinding delegate:(id<ios_pullSoapBindingResponseDelegate>)aDelegate
	parameters:(ios_pullSvc_Get_SectorVol *)aParameters
;
@end
@interface ios_pullSoapBinding_load_sec : ios_pullSoapBindingOperation {
	ios_pullSvc_load_sec * parameters;
}
@property (nonatomic, retain) ios_pullSvc_load_sec * parameters;
- (id)initWithBinding:(ios_pullSoapBinding *)aBinding delegate:(id<ios_pullSoapBindingResponseDelegate>)aDelegate
	parameters:(ios_pullSvc_load_sec *)aParameters
;
@end
@interface ios_pullSoapBinding_load_mkinfo : ios_pullSoapBindingOperation {
	ios_pullSvc_load_mkinfo * parameters;
}
@property (nonatomic, retain) ios_pullSvc_load_mkinfo * parameters;
- (id)initWithBinding:(ios_pullSoapBinding *)aBinding delegate:(id<ios_pullSoapBindingResponseDelegate>)aDelegate
	parameters:(ios_pullSvc_load_mkinfo *)aParameters
;
@end
@interface ios_pullSoapBinding_stock : ios_pullSoapBindingOperation {
	ios_pullSvc_stock * parameters;
}
@property (nonatomic, retain) ios_pullSvc_stock * parameters;
- (id)initWithBinding:(ios_pullSoapBinding *)aBinding delegate:(id<ios_pullSoapBindingResponseDelegate>)aDelegate
	parameters:(ios_pullSvc_stock *)aParameters
;
@end
@interface ios_pullSoapBinding_market_index : ios_pullSoapBindingOperation {
	ios_pullSvc_market_index * parameters;
}
@property (nonatomic, retain) ios_pullSvc_market_index * parameters;
- (id)initWithBinding:(ios_pullSoapBinding *)aBinding delegate:(id<ios_pullSoapBindingResponseDelegate>)aDelegate
	parameters:(ios_pullSvc_market_index *)aParameters
;
@end
@interface ios_pullSoapBinding_chart_index : ios_pullSoapBindingOperation {
	ios_pullSvc_chart_index * parameters;
}
@property (nonatomic, retain) ios_pullSvc_chart_index * parameters;
- (id)initWithBinding:(ios_pullSoapBinding *)aBinding delegate:(id<ios_pullSoapBindingResponseDelegate>)aDelegate
	parameters:(ios_pullSvc_chart_index *)aParameters
;
@end
@interface ios_pullSoapBinding_Get_Most_Active_Vol : ios_pullSoapBindingOperation {
	ios_pullSvc_Get_Most_Active_Vol * parameters;
}
@property (nonatomic, retain) ios_pullSvc_Get_Most_Active_Vol * parameters;
- (id)initWithBinding:(ios_pullSoapBinding *)aBinding delegate:(id<ios_pullSoapBindingResponseDelegate>)aDelegate
	parameters:(ios_pullSvc_Get_Most_Active_Vol *)aParameters
;
@end
@interface ios_pullSoapBinding_Get_Most_Active_Val : ios_pullSoapBindingOperation {
	ios_pullSvc_Get_Most_Active_Val * parameters;
}
@property (nonatomic, retain) ios_pullSvc_Get_Most_Active_Val * parameters;
- (id)initWithBinding:(ios_pullSoapBinding *)aBinding delegate:(id<ios_pullSoapBindingResponseDelegate>)aDelegate
	parameters:(ios_pullSvc_Get_Most_Active_Val *)aParameters
;
@end
@interface ios_pullSoapBinding_Get_Most_Swing : ios_pullSoapBindingOperation {
	ios_pullSvc_Get_Most_Swing * parameters;
}
@property (nonatomic, retain) ios_pullSvc_Get_Most_Swing * parameters;
- (id)initWithBinding:(ios_pullSoapBinding *)aBinding delegate:(id<ios_pullSoapBindingResponseDelegate>)aDelegate
	parameters:(ios_pullSvc_Get_Most_Swing *)aParameters
;
@end
@interface ios_pullSoapBinding_Get_Most_Loser : ios_pullSoapBindingOperation {
	ios_pullSvc_Get_Most_Loser * parameters;
}
@property (nonatomic, retain) ios_pullSvc_Get_Most_Loser * parameters;
- (id)initWithBinding:(ios_pullSoapBinding *)aBinding delegate:(id<ios_pullSoapBindingResponseDelegate>)aDelegate
	parameters:(ios_pullSvc_Get_Most_Loser *)aParameters
;
@end
@interface ios_pullSoapBinding_Get_Most_Gainer : ios_pullSoapBindingOperation {
	ios_pullSvc_Get_Most_Gainer * parameters;
}
@property (nonatomic, retain) ios_pullSvc_Get_Most_Gainer * parameters;
- (id)initWithBinding:(ios_pullSoapBinding *)aBinding delegate:(id<ios_pullSoapBindingResponseDelegate>)aDelegate
	parameters:(ios_pullSvc_Get_Most_Gainer *)aParameters
;
@end
@interface ios_pullSoapBinding_Get_stock_history : ios_pullSoapBindingOperation {
	ios_pullSvc_Get_stock_history * parameters;
}
@property (nonatomic, retain) ios_pullSvc_Get_stock_history * parameters;
- (id)initWithBinding:(ios_pullSoapBinding *)aBinding delegate:(id<ios_pullSoapBindingResponseDelegate>)aDelegate
	parameters:(ios_pullSvc_Get_stock_history *)aParameters
;
@end
@interface ios_pullSoapBinding_Get_fundamental : ios_pullSoapBindingOperation {
	ios_pullSvc_Get_fundamental * parameters;
}
@property (nonatomic, retain) ios_pullSvc_Get_fundamental * parameters;
- (id)initWithBinding:(ios_pullSoapBinding *)aBinding delegate:(id<ios_pullSoapBindingResponseDelegate>)aDelegate
	parameters:(ios_pullSvc_Get_fundamental *)aParameters
;
@end
@interface ios_pullSoapBinding_Get_StockInPlay : ios_pullSoapBindingOperation {
	ios_pullSvc_Get_StockInPlay * parameters;
}
@property (nonatomic, retain) ios_pullSvc_Get_StockInPlay * parameters;
- (id)initWithBinding:(ios_pullSoapBinding *)aBinding delegate:(id<ios_pullSoapBindingResponseDelegate>)aDelegate
	parameters:(ios_pullSvc_Get_StockInPlay *)aParameters
;
@end
@interface ios_pullSoapBinding_Get_HighLowAvg : ios_pullSoapBindingOperation {
	ios_pullSvc_Get_HighLowAvg * parameters;
}
@property (nonatomic, retain) ios_pullSvc_Get_HighLowAvg * parameters;
- (id)initWithBinding:(ios_pullSoapBinding *)aBinding delegate:(id<ios_pullSoapBindingResponseDelegate>)aDelegate
	parameters:(ios_pullSvc_Get_HighLowAvg *)aParameters
;
@end
@interface ios_pullSoapBinding_stockbyprice : ios_pullSoapBindingOperation {
	ios_pullSvc_stockbyprice * parameters;
}
@property (nonatomic, retain) ios_pullSvc_stockbyprice * parameters;
- (id)initWithBinding:(ios_pullSoapBinding *)aBinding delegate:(id<ios_pullSoapBindingResponseDelegate>)aDelegate
	parameters:(ios_pullSvc_stockbyprice *)aParameters
;
@end
@interface ios_pullSoapBinding_getticker : ios_pullSoapBindingOperation {
	ios_pullSvc_getticker * parameters;
}
@property (nonatomic, retain) ios_pullSvc_getticker * parameters;
- (id)initWithBinding:(ios_pullSoapBinding *)aBinding delegate:(id<ios_pullSoapBindingResponseDelegate>)aDelegate
	parameters:(ios_pullSvc_getticker *)aParameters
;
@end
@interface ios_pullSoapBinding_getDWList : ios_pullSoapBindingOperation {
	ios_pullSvc_getDWList * parameters;
}
@property (nonatomic, retain) ios_pullSvc_getDWList * parameters;
- (id)initWithBinding:(ios_pullSoapBinding *)aBinding delegate:(id<ios_pullSoapBindingResponseDelegate>)aDelegate
	parameters:(ios_pullSvc_getDWList *)aParameters
;
@end
@interface ios_pullSoapBinding_getsalebytime : ios_pullSoapBindingOperation {
	ios_pullSvc_getsalebytime * parameters;
}
@property (nonatomic, retain) ios_pullSvc_getsalebytime * parameters;
- (id)initWithBinding:(ios_pullSoapBinding *)aBinding delegate:(id<ios_pullSoapBindingResponseDelegate>)aDelegate
	parameters:(ios_pullSvc_getsalebytime *)aParameters
;
@end
@interface ios_pullSoapBinding_envelope : NSObject {
}
+ (ios_pullSoapBinding_envelope *)sharedInstance;
- (NSString *)serializedFormUsingHeaderElements:(NSDictionary *)headerElements bodyElements:(NSDictionary *)bodyElements bodyKeys:(NSArray *)bodyKeys;
@end
@interface ios_pullSoapBindingResponse : NSObject {
	NSArray *headers;
	NSArray *bodyParts;
	NSError *error;
}
@property (nonatomic, retain) NSArray *headers;
@property (nonatomic, retain) NSArray *bodyParts;
@property (nonatomic, retain) NSError *error;
@end
@class ios_pullSoap12BindingResponse;
@class ios_pullSoap12BindingOperation;
@protocol ios_pullSoap12BindingResponseDelegate <NSObject>
- (void) operation:(ios_pullSoap12BindingOperation *)operation completedWithResponse:(ios_pullSoap12BindingResponse *)response;
@end
#define kServerAnchorCertificates   @"kServerAnchorCertificates"
#define kServerAnchorsOnly          @"kServerAnchorsOnly"
#define kClientIdentity             @"kClientIdentity"
#define kClientCertificates         @"kClientCertificates"
#define kClientUsername             @"kClientUsername"
#define kClientPassword             @"kClientPassword"
#define kNSURLCredentialPersistence @"kNSURLCredentialPersistence"
#define kValidateResult             @"kValidateResult"
@interface ios_pullSoap12Binding : NSObject <ios_pullSoap12BindingResponseDelegate> {
	NSURL *address;
	NSTimeInterval timeout;
	NSMutableArray *cookies;
	NSMutableDictionary *customHeaders;
	BOOL logXMLInOut;
	BOOL ignoreEmptyResponse;
	BOOL synchronousOperationComplete;
	id<SSLCredentialsManaging> sslManager;
	SOAPSigner *soapSigner;
}
@property (nonatomic, copy) NSURL *address;
@property (nonatomic) BOOL logXMLInOut;
@property (nonatomic) BOOL ignoreEmptyResponse;
@property (nonatomic) NSTimeInterval timeout;
@property (nonatomic, retain) NSMutableArray *cookies;
@property (nonatomic, retain) NSMutableDictionary *customHeaders;
@property (nonatomic, retain) id<SSLCredentialsManaging> sslManager;
@property (nonatomic, retain) SOAPSigner *soapSigner;
+ (NSTimeInterval) defaultTimeout;
- (id)initWithAddress:(NSString *)anAddress;
- (void)sendHTTPCallUsingBody:(NSString *)body soapAction:(NSString *)soapAction forOperation:(ios_pullSoap12BindingOperation *)operation;
- (void)addCookie:(NSHTTPCookie *)toAdd;
- (NSString *)MIMEType;
- (ios_pullSoap12BindingResponse *)tickerUsingParameters:(ios_pullSvc_ticker *)aParameters ;
- (void)tickerAsyncUsingParameters:(ios_pullSvc_ticker *)aParameters  delegate:(id<ios_pullSoap12BindingResponseDelegate>)responseDelegate;
- (ios_pullSoap12BindingResponse *)Get_SectorVolUsingParameters:(ios_pullSvc_Get_SectorVol *)aParameters ;
- (void)Get_SectorVolAsyncUsingParameters:(ios_pullSvc_Get_SectorVol *)aParameters  delegate:(id<ios_pullSoap12BindingResponseDelegate>)responseDelegate;
- (ios_pullSoap12BindingResponse *)load_secUsingParameters:(ios_pullSvc_load_sec *)aParameters ;
- (void)load_secAsyncUsingParameters:(ios_pullSvc_load_sec *)aParameters  delegate:(id<ios_pullSoap12BindingResponseDelegate>)responseDelegate;
- (ios_pullSoap12BindingResponse *)load_mkinfoUsingParameters:(ios_pullSvc_load_mkinfo *)aParameters ;
- (void)load_mkinfoAsyncUsingParameters:(ios_pullSvc_load_mkinfo *)aParameters  delegate:(id<ios_pullSoap12BindingResponseDelegate>)responseDelegate;
- (ios_pullSoap12BindingResponse *)stockUsingParameters:(ios_pullSvc_stock *)aParameters ;
- (void)stockAsyncUsingParameters:(ios_pullSvc_stock *)aParameters  delegate:(id<ios_pullSoap12BindingResponseDelegate>)responseDelegate;
- (ios_pullSoap12BindingResponse *)market_indexUsingParameters:(ios_pullSvc_market_index *)aParameters ;
- (void)market_indexAsyncUsingParameters:(ios_pullSvc_market_index *)aParameters  delegate:(id<ios_pullSoap12BindingResponseDelegate>)responseDelegate;
- (ios_pullSoap12BindingResponse *)chart_indexUsingParameters:(ios_pullSvc_chart_index *)aParameters ;
- (void)chart_indexAsyncUsingParameters:(ios_pullSvc_chart_index *)aParameters  delegate:(id<ios_pullSoap12BindingResponseDelegate>)responseDelegate;
- (ios_pullSoap12BindingResponse *)Get_Most_Active_VolUsingParameters:(ios_pullSvc_Get_Most_Active_Vol *)aParameters ;
- (void)Get_Most_Active_VolAsyncUsingParameters:(ios_pullSvc_Get_Most_Active_Vol *)aParameters  delegate:(id<ios_pullSoap12BindingResponseDelegate>)responseDelegate;
- (ios_pullSoap12BindingResponse *)Get_Most_Active_ValUsingParameters:(ios_pullSvc_Get_Most_Active_Val *)aParameters ;
- (void)Get_Most_Active_ValAsyncUsingParameters:(ios_pullSvc_Get_Most_Active_Val *)aParameters  delegate:(id<ios_pullSoap12BindingResponseDelegate>)responseDelegate;
- (ios_pullSoap12BindingResponse *)Get_Most_SwingUsingParameters:(ios_pullSvc_Get_Most_Swing *)aParameters ;
- (void)Get_Most_SwingAsyncUsingParameters:(ios_pullSvc_Get_Most_Swing *)aParameters  delegate:(id<ios_pullSoap12BindingResponseDelegate>)responseDelegate;
- (ios_pullSoap12BindingResponse *)Get_Most_LoserUsingParameters:(ios_pullSvc_Get_Most_Loser *)aParameters ;
- (void)Get_Most_LoserAsyncUsingParameters:(ios_pullSvc_Get_Most_Loser *)aParameters  delegate:(id<ios_pullSoap12BindingResponseDelegate>)responseDelegate;
- (ios_pullSoap12BindingResponse *)Get_Most_GainerUsingParameters:(ios_pullSvc_Get_Most_Gainer *)aParameters ;
- (void)Get_Most_GainerAsyncUsingParameters:(ios_pullSvc_Get_Most_Gainer *)aParameters  delegate:(id<ios_pullSoap12BindingResponseDelegate>)responseDelegate;
- (ios_pullSoap12BindingResponse *)Get_stock_historyUsingParameters:(ios_pullSvc_Get_stock_history *)aParameters ;
- (void)Get_stock_historyAsyncUsingParameters:(ios_pullSvc_Get_stock_history *)aParameters  delegate:(id<ios_pullSoap12BindingResponseDelegate>)responseDelegate;
- (ios_pullSoap12BindingResponse *)Get_fundamentalUsingParameters:(ios_pullSvc_Get_fundamental *)aParameters ;
- (void)Get_fundamentalAsyncUsingParameters:(ios_pullSvc_Get_fundamental *)aParameters  delegate:(id<ios_pullSoap12BindingResponseDelegate>)responseDelegate;
- (ios_pullSoap12BindingResponse *)Get_StockInPlayUsingParameters:(ios_pullSvc_Get_StockInPlay *)aParameters ;
- (void)Get_StockInPlayAsyncUsingParameters:(ios_pullSvc_Get_StockInPlay *)aParameters  delegate:(id<ios_pullSoap12BindingResponseDelegate>)responseDelegate;
- (ios_pullSoap12BindingResponse *)Get_HighLowAvgUsingParameters:(ios_pullSvc_Get_HighLowAvg *)aParameters ;
- (void)Get_HighLowAvgAsyncUsingParameters:(ios_pullSvc_Get_HighLowAvg *)aParameters  delegate:(id<ios_pullSoap12BindingResponseDelegate>)responseDelegate;
- (ios_pullSoap12BindingResponse *)stockbypriceUsingParameters:(ios_pullSvc_stockbyprice *)aParameters ;
- (void)stockbypriceAsyncUsingParameters:(ios_pullSvc_stockbyprice *)aParameters  delegate:(id<ios_pullSoap12BindingResponseDelegate>)responseDelegate;
- (ios_pullSoap12BindingResponse *)gettickerUsingParameters:(ios_pullSvc_getticker *)aParameters ;
- (void)gettickerAsyncUsingParameters:(ios_pullSvc_getticker *)aParameters  delegate:(id<ios_pullSoap12BindingResponseDelegate>)responseDelegate;
- (ios_pullSoap12BindingResponse *)getDWListUsingParameters:(ios_pullSvc_getDWList *)aParameters ;
- (void)getDWListAsyncUsingParameters:(ios_pullSvc_getDWList *)aParameters  delegate:(id<ios_pullSoap12BindingResponseDelegate>)responseDelegate;
- (ios_pullSoap12BindingResponse *)getsalebytimeUsingParameters:(ios_pullSvc_getsalebytime *)aParameters ;
- (void)getsalebytimeAsyncUsingParameters:(ios_pullSvc_getsalebytime *)aParameters  delegate:(id<ios_pullSoap12BindingResponseDelegate>)responseDelegate;
@end
@interface ios_pullSoap12BindingOperation : NSOperation {
	ios_pullSoap12Binding *binding;
	ios_pullSoap12BindingResponse *response;
	NSMutableData *responseData;
	NSURLConnection *urlConnection;
}
@property (nonatomic, retain) ios_pullSoap12Binding *binding;
@property (nonatomic, readonly) ios_pullSoap12BindingResponse *response;
@property (nonatomic, assign) id<ios_pullSoap12BindingResponseDelegate> delegate;
@property (nonatomic, retain) NSMutableData *responseData;
@property (nonatomic, retain) NSURLConnection *urlConnection;
- (id)initWithBinding:(ios_pullSoap12Binding *)aBinding delegate:(id<ios_pullSoap12BindingResponseDelegate>)aDelegate;
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error;
@end
@interface ios_pullSoap12Binding_ticker : ios_pullSoap12BindingOperation {
	ios_pullSvc_ticker * parameters;
}
@property (nonatomic, retain) ios_pullSvc_ticker * parameters;
- (id)initWithBinding:(ios_pullSoap12Binding *)aBinding delegate:(id<ios_pullSoap12BindingResponseDelegate>)aDelegate
	parameters:(ios_pullSvc_ticker *)aParameters
;
@end
@interface ios_pullSoap12Binding_Get_SectorVol : ios_pullSoap12BindingOperation {
	ios_pullSvc_Get_SectorVol * parameters;
}
@property (nonatomic, retain) ios_pullSvc_Get_SectorVol * parameters;
- (id)initWithBinding:(ios_pullSoap12Binding *)aBinding delegate:(id<ios_pullSoap12BindingResponseDelegate>)aDelegate
	parameters:(ios_pullSvc_Get_SectorVol *)aParameters
;
@end
@interface ios_pullSoap12Binding_load_sec : ios_pullSoap12BindingOperation {
	ios_pullSvc_load_sec * parameters;
}
@property (nonatomic, retain) ios_pullSvc_load_sec * parameters;
- (id)initWithBinding:(ios_pullSoap12Binding *)aBinding delegate:(id<ios_pullSoap12BindingResponseDelegate>)aDelegate
	parameters:(ios_pullSvc_load_sec *)aParameters
;
@end
@interface ios_pullSoap12Binding_load_mkinfo : ios_pullSoap12BindingOperation {
	ios_pullSvc_load_mkinfo * parameters;
}
@property (nonatomic, retain) ios_pullSvc_load_mkinfo * parameters;
- (id)initWithBinding:(ios_pullSoap12Binding *)aBinding delegate:(id<ios_pullSoap12BindingResponseDelegate>)aDelegate
	parameters:(ios_pullSvc_load_mkinfo *)aParameters
;
@end
@interface ios_pullSoap12Binding_stock : ios_pullSoap12BindingOperation {
	ios_pullSvc_stock * parameters;
}
@property (nonatomic, retain) ios_pullSvc_stock * parameters;
- (id)initWithBinding:(ios_pullSoap12Binding *)aBinding delegate:(id<ios_pullSoap12BindingResponseDelegate>)aDelegate
	parameters:(ios_pullSvc_stock *)aParameters
;
@end
@interface ios_pullSoap12Binding_market_index : ios_pullSoap12BindingOperation {
	ios_pullSvc_market_index * parameters;
}
@property (nonatomic, retain) ios_pullSvc_market_index * parameters;
- (id)initWithBinding:(ios_pullSoap12Binding *)aBinding delegate:(id<ios_pullSoap12BindingResponseDelegate>)aDelegate
	parameters:(ios_pullSvc_market_index *)aParameters
;
@end
@interface ios_pullSoap12Binding_chart_index : ios_pullSoap12BindingOperation {
	ios_pullSvc_chart_index * parameters;
}
@property (nonatomic, retain) ios_pullSvc_chart_index * parameters;
- (id)initWithBinding:(ios_pullSoap12Binding *)aBinding delegate:(id<ios_pullSoap12BindingResponseDelegate>)aDelegate
	parameters:(ios_pullSvc_chart_index *)aParameters
;
@end
@interface ios_pullSoap12Binding_Get_Most_Active_Vol : ios_pullSoap12BindingOperation {
	ios_pullSvc_Get_Most_Active_Vol * parameters;
}
@property (nonatomic, retain) ios_pullSvc_Get_Most_Active_Vol * parameters;
- (id)initWithBinding:(ios_pullSoap12Binding *)aBinding delegate:(id<ios_pullSoap12BindingResponseDelegate>)aDelegate
	parameters:(ios_pullSvc_Get_Most_Active_Vol *)aParameters
;
@end
@interface ios_pullSoap12Binding_Get_Most_Active_Val : ios_pullSoap12BindingOperation {
	ios_pullSvc_Get_Most_Active_Val * parameters;
}
@property (nonatomic, retain) ios_pullSvc_Get_Most_Active_Val * parameters;
- (id)initWithBinding:(ios_pullSoap12Binding *)aBinding delegate:(id<ios_pullSoap12BindingResponseDelegate>)aDelegate
	parameters:(ios_pullSvc_Get_Most_Active_Val *)aParameters
;
@end
@interface ios_pullSoap12Binding_Get_Most_Swing : ios_pullSoap12BindingOperation {
	ios_pullSvc_Get_Most_Swing * parameters;
}
@property (nonatomic, retain) ios_pullSvc_Get_Most_Swing * parameters;
- (id)initWithBinding:(ios_pullSoap12Binding *)aBinding delegate:(id<ios_pullSoap12BindingResponseDelegate>)aDelegate
	parameters:(ios_pullSvc_Get_Most_Swing *)aParameters
;
@end
@interface ios_pullSoap12Binding_Get_Most_Loser : ios_pullSoap12BindingOperation {
	ios_pullSvc_Get_Most_Loser * parameters;
}
@property (nonatomic, retain) ios_pullSvc_Get_Most_Loser * parameters;
- (id)initWithBinding:(ios_pullSoap12Binding *)aBinding delegate:(id<ios_pullSoap12BindingResponseDelegate>)aDelegate
	parameters:(ios_pullSvc_Get_Most_Loser *)aParameters
;
@end
@interface ios_pullSoap12Binding_Get_Most_Gainer : ios_pullSoap12BindingOperation {
	ios_pullSvc_Get_Most_Gainer * parameters;
}
@property (nonatomic, retain) ios_pullSvc_Get_Most_Gainer * parameters;
- (id)initWithBinding:(ios_pullSoap12Binding *)aBinding delegate:(id<ios_pullSoap12BindingResponseDelegate>)aDelegate
	parameters:(ios_pullSvc_Get_Most_Gainer *)aParameters
;
@end
@interface ios_pullSoap12Binding_Get_stock_history : ios_pullSoap12BindingOperation {
	ios_pullSvc_Get_stock_history * parameters;
}
@property (nonatomic, retain) ios_pullSvc_Get_stock_history * parameters;
- (id)initWithBinding:(ios_pullSoap12Binding *)aBinding delegate:(id<ios_pullSoap12BindingResponseDelegate>)aDelegate
	parameters:(ios_pullSvc_Get_stock_history *)aParameters
;
@end
@interface ios_pullSoap12Binding_Get_fundamental : ios_pullSoap12BindingOperation {
	ios_pullSvc_Get_fundamental * parameters;
}
@property (nonatomic, retain) ios_pullSvc_Get_fundamental * parameters;
- (id)initWithBinding:(ios_pullSoap12Binding *)aBinding delegate:(id<ios_pullSoap12BindingResponseDelegate>)aDelegate
	parameters:(ios_pullSvc_Get_fundamental *)aParameters
;
@end
@interface ios_pullSoap12Binding_Get_StockInPlay : ios_pullSoap12BindingOperation {
	ios_pullSvc_Get_StockInPlay * parameters;
}
@property (nonatomic, retain) ios_pullSvc_Get_StockInPlay * parameters;
- (id)initWithBinding:(ios_pullSoap12Binding *)aBinding delegate:(id<ios_pullSoap12BindingResponseDelegate>)aDelegate
	parameters:(ios_pullSvc_Get_StockInPlay *)aParameters
;
@end
@interface ios_pullSoap12Binding_Get_HighLowAvg : ios_pullSoap12BindingOperation {
	ios_pullSvc_Get_HighLowAvg * parameters;
}
@property (nonatomic, retain) ios_pullSvc_Get_HighLowAvg * parameters;
- (id)initWithBinding:(ios_pullSoap12Binding *)aBinding delegate:(id<ios_pullSoap12BindingResponseDelegate>)aDelegate
	parameters:(ios_pullSvc_Get_HighLowAvg *)aParameters
;
@end
@interface ios_pullSoap12Binding_stockbyprice : ios_pullSoap12BindingOperation {
	ios_pullSvc_stockbyprice * parameters;
}
@property (nonatomic, retain) ios_pullSvc_stockbyprice * parameters;
- (id)initWithBinding:(ios_pullSoap12Binding *)aBinding delegate:(id<ios_pullSoap12BindingResponseDelegate>)aDelegate
	parameters:(ios_pullSvc_stockbyprice *)aParameters
;
@end
@interface ios_pullSoap12Binding_getticker : ios_pullSoap12BindingOperation {
	ios_pullSvc_getticker * parameters;
}
@property (nonatomic, retain) ios_pullSvc_getticker * parameters;
- (id)initWithBinding:(ios_pullSoap12Binding *)aBinding delegate:(id<ios_pullSoap12BindingResponseDelegate>)aDelegate
	parameters:(ios_pullSvc_getticker *)aParameters
;
@end
@interface ios_pullSoap12Binding_getDWList : ios_pullSoap12BindingOperation {
	ios_pullSvc_getDWList * parameters;
}
@property (nonatomic, retain) ios_pullSvc_getDWList * parameters;
- (id)initWithBinding:(ios_pullSoap12Binding *)aBinding delegate:(id<ios_pullSoap12BindingResponseDelegate>)aDelegate
	parameters:(ios_pullSvc_getDWList *)aParameters
;
@end
@interface ios_pullSoap12Binding_getsalebytime : ios_pullSoap12BindingOperation {
	ios_pullSvc_getsalebytime * parameters;
}
@property (nonatomic, retain) ios_pullSvc_getsalebytime * parameters;
- (id)initWithBinding:(ios_pullSoap12Binding *)aBinding delegate:(id<ios_pullSoap12BindingResponseDelegate>)aDelegate
	parameters:(ios_pullSvc_getsalebytime *)aParameters
;
@end
@interface ios_pullSoap12Binding_envelope : NSObject {
}
+ (ios_pullSoap12Binding_envelope *)sharedInstance;
- (NSString *)serializedFormUsingHeaderElements:(NSDictionary *)headerElements bodyElements:(NSDictionary *)bodyElements bodyKeys:(NSArray *)bodyKeys;
@end
@interface ios_pullSoap12BindingResponse : NSObject {
	NSArray *headers;
	NSArray *bodyParts;
	NSError *error;
}
@property (nonatomic, retain) NSArray *headers;
@property (nonatomic, retain) NSArray *bodyParts;
@property (nonatomic, retain) NSError *error;
@end
