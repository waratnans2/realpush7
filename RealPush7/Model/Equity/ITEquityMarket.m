//
//  ITEquityMarket.m
//  RealPush
//
//  Created by Sutean Rutjanalard on 8/15/13.
//  Copyright (c) 2013 Sutean Rutjanalard. All rights reserved.
//


#import "ITEquityMarket.h"
#import "NSNumber+formatter.h"
#import "ITStockDetail.h"

@interface ITEquityMarket()

@end

@implementation ITEquityMarket
- (instancetype)initWithName:(NSString*) name
{
    self = [super init];
    if (self) {
        self.name = name;
        self.fullname =  name;
    }
    return self;
}
-(NSMutableArray *)stocks {
    if ( ! _stocks) {
        _stocks = [NSMutableArray array];
    }
    return _stocks;
}
-(void) addStock:(ITStockDetail *)stock {
    [self.stocks addObject:stock];
}
-(void) updateWithSiamMarketEquityMessage:(NSArray *)data {
    if ([data count] < 16) {
        NSLog(@"short M :%@" , data);
        return;
    } 
    self.willUpdate    = YES;
    self.name          = [data objectAtIndex:1];
    self.status        = [data objectAtIndex:2];
    self.advance       = [data objectAtIndex:3] ;
    self.unchange      = [data objectAtIndex:4] ;
    self.decline       = [data objectAtIndex:5] ;
    self.change        = [NSNumber numberWithString: [data objectAtIndex:6]
                                        numberStyle: priceFormatStyle];
    self.percentChange = [NSNumber numberWithString: [data objectAtIndex:7]
                                        numberStyle: priceFormatStyle];
    self.indexValue    = [NSNumber numberWithString: [data objectAtIndex:8]
                                        numberStyle: priceFormatStyle];
    self.prior         = [NSNumber numberWithString: [data objectAtIndex:9]
                                        numberStyle: priceFormatStyle];
    self.high          = [NSNumber numberWithString: [data objectAtIndex:10]
                                        numberStyle: priceFormatStyle];
    self.low           = [NSNumber numberWithString: [data objectAtIndex:11]
                                        numberStyle: priceFormatStyle];
    self.totalVolume   = [NSNumber numberWithString: [data objectAtIndex:12]
                                        numberStyle: priceFormatStyle];
    self.totalValue    = [NSNumber numberWithString: [data objectAtIndex:13]
                                        numberStyle: priceFormatStyle];
    self.date          = [data objectAtIndex:14];
    self.indexTime     = [data objectAtIndex:15];
    self.hasUpdate     = YES;
    
    
    
    self.hasUpdate = YES;
    
}
+(ITEquityMarket*)marketWithName:(NSString*)name {
    ITEquityMarket* newMarket = [[ITEquityMarket alloc] initWithName:name];
    newMarket.marketType = @"Set index";//default value
    return newMarket;
}
-(NSString *)description {
    return [NSString stringWithFormat:@"name=%@, type=%@, index=%@",self.name, self.marketType, self.indexValue.stringValue];
}
@end

@interface ITIndustry()
@property (nonatomic, strong) NSMutableArray* mSectors;

@end

@implementation ITIndustry
-(NSArray *)sectors {
    return [_mSectors copy];
}
-(void)addSector:(ITSector *)sector {
    if ( ! _mSectors) {
        _mSectors = [NSMutableArray arrayWithCapacity:20];
    }
    [_mSectors addObject:sector];
}
@end


@implementation ITSector
- (instancetype)initWithName:(NSString *)name
{
    self = [super init];
    if (self) {
        self.name = name;
        self.fullname = name;
    }
    return self;
}
-(void)addStock:(ITStockDetail *)stock {
    [super addStock:stock];
}
@end


