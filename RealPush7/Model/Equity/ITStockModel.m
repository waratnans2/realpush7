//
//  INStock.m
//  TestZnet
//
//  Created by Sutean Rutjanalard on 5/3/13.
//  Copyright (c) 2013 Sutean Rutjanalard. All rights reserved.
//

#import "ITStockModel.h"
#import "JSONKit.h"

@interface ITStockModel()
@property (nonatomic, strong) NSCharacterSet* seperator;
@property (nonatomic, strong) NSCharacterSet* newline;
@property (nonatomic, strong) NSMutableString* siamDataBuffer;


//private for su version


@property (nonatomic, strong) NSMutableDictionary* tfexGroups;
@property (nonatomic, strong) NSMutableDictionary* tfexMarkets;
@property (nonatomic, strong) NSMutableDictionary* commodities;
@property (nonatomic, strong) NSMutableDictionary* instruments;
@property (nonatomic, strong) NSMutableSet* messageName;




-(NSString*) updateWithSecurityDict:(NSDictionary*) security;
-(void) updateWithSectorDict:(NSDictionary*) sector;
-(void) updateWithIndustryDict:(NSDictionary*) industry;

-(void) updateWithTfexMarketDict:(NSDictionary*) tfexMarketDict;
-(void) updateWithCommodityDict:(NSDictionary*) commodityDict;
-(void) updateWithInstrumentDict:(NSDictionary*) instrumentDict;
-(void) updateWithGroupDict:(NSDictionary*) groupDict;
//-(NSString* ) updateWithSeriesDict:(NSDictionary*) seriesDict;
-(ITIndustry* ) industryByIndustryID:(NSString*) industryID;
@end

@implementation ITStockModel
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"

#pragma mark - initializer
-(NSMutableDictionary *)stocks {
    if ( ! _stocks) {
        _stocks  = [[NSMutableDictionary alloc] initWithCapacity:1000];
    }
    return _stocks;
}
-(NSMutableString *)siamDataBuffer {
    if ( ! _siamDataBuffer) {
        _siamDataBuffer = [[NSMutableString alloc] init];
    }
    return _siamDataBuffer;
}
-(NSMutableDictionary *)markets {
    if (_markets) {
        return _markets;
    }
    _markets = [NSMutableDictionary dictionary];
    for (NSString* marketName in [self marketNameArray]) {
        _markets[marketName] = [ITEquityMarket marketWithName:marketName];
    }
    return _markets;
}
-(void) setupLocalVariable {
    
    self.markets = [NSMutableDictionary dictionary];
    for (NSString* marketName in [self marketNameArray]) {
        self.markets[marketName] = [ITEquityMarket marketWithName:marketName];
    }
    self.stocks =      [[NSMutableDictionary alloc] initWithCapacity:1000];
    self.industries =  [[NSMutableDictionary alloc] initWithCapacity:10];
    self.sectors =     [[NSMutableDictionary alloc] initWithCapacity:10];
    self.tfexGroups =  [[NSMutableDictionary alloc] initWithCapacity:10];
    self.tfexMarkets = [[NSMutableDictionary alloc] initWithCapacity:5];
    self.commodities = [[NSMutableDictionary alloc] initWithCapacity:50];
    self.instruments = [[NSMutableDictionary alloc] initWithCapacity:300];

    //temporary use
    self.messageName  = [[NSMutableSet alloc] init];
}

- (id)init
{
    self = [super init];
    if (self) {
        [self setupLocalVariable];
        self.seperator  = [NSCharacterSet characterSetWithCharactersInString:@""];
        self.newline    = [NSCharacterSet newlineCharacterSet];
    }
    return self;
}

#pragma mark - su version api
- (NSArray *)marketNameArray {
    return  @[@"SET", @"SET50", @"SET100", @"SETHD", @"MAI"];
}
-(ITEquityMarket*) marketOrIndustryOrSectorByName:(NSString*) marketName {
    ITEquityMarket* market = [self.markets objectForKey:marketName];
    if (market) {
        return market;
    }
    market = [self.industries objectForKey:marketName];
    if (market) {
        return market;
    }
    market = [self.sectors objectForKey:marketName];
    return market;
}
-(ITIndustry *)industryByIndustryID:(NSString*) industryID {
    for (ITIndustry* industry in [self.industries allValues]) {
        if ([industry.industryID isEqualToString:industryID]) {
            return industry;
        }
    }
    return nil;
}
-(ITSector*) sectorBySectorID:(NSString*) sectorID {
    for (ITSector* sector in [self.sectors allValues]) {
        if ([sector.sectorID isEqualToString:sectorID]) {
            return sector;
        }
    }
    return nil;
}

-(NSArray*) filterSymbolByName:(NSString *) searchText {
    
    NSPredicate *resultPredicate  =    [NSPredicate
                                        predicateWithFormat:@"SELF BEGINSWITH[cd] %@",
                                        searchText];
    NSMutableArray* searchResults = [[self.stocksNameArray filteredArrayUsingPredicate:resultPredicate] mutableCopy];
    
    
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"length"
                                                                    ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    [searchResults sortUsingSelector:@selector(localizedCompare:)];
    [searchResults sortUsingDescriptors:sortDescriptors];
    return [searchResults copy];
}

-(NSString* ) updateWithSecurityDict:(NSDictionary*) security{
    ITStockDetail* stock = [[ITStockDetail alloc] initWithName:[security objectForKey:@"SYMBOL"]];
    stock.fullname = [security objectForKey:@"COMPANY"];
    stock.industry = [self industryByIndustryID:[[security objectForKey:@"INDUSTRY"] stringValue]];
    stock.sector = [self sectorBySectorID:[[security objectForKey:@"SECTOR"] stringValue] ];
    [stock.industry addStock:stock];
    [stock.sector addStock:stock];
    int marketCode = [[security objectForKey:@"MARKET"] intValue];
    ITEquityMarket* market = nil;
    if(marketCode / 16 == 1 )  {
        market = [self.markets objectForKey:@"SET"];
        [market addStock:stock];
    }
    marketCode = marketCode % 16;
    if (marketCode / 8 == 1) {
        market = [self.markets objectForKey:@"SET50"];
        [market addStock:stock];
    }
    marketCode = marketCode % 8;
    if (marketCode / 4 ==1 ) {
        market = [self.markets objectForKey:@"SET100"];
        [market addStock:stock];
    }
    marketCode = marketCode %4;
    if (marketCode / 2 == 1) {
        market = [self.markets objectForKey:@"SETHD"];
        [market addStock:stock];
    }
    if (marketCode % 2 == 1) {
        market = [self.markets objectForKey:@"MAI"];
        [market addStock:stock];
    }
    stock.stockType = @"Equity";
    [self.stocks setObject:stock forKey:stock.symbol];
    
    return stock.symbol;
    
}
-(void) updateWithSectorDict:(NSDictionary*) sectorDict {
    ITSector* sector = [[ITSector alloc] init];
    sector.name  = [sectorDict objectForKey:@"SECTOR_NAME"];
    sector.marketType = @"Sector";
    sector.fullname  = [sectorDict objectForKey:@"SECTOR_FULLNAME"];
    sector.sectorID = [[sectorDict objectForKey:@"SECTOR_ID"] stringValue];
    sector.industry = [self industryByIndustryID:[sectorDict objectForKey:@"INDUSTRY_ID"]];
    [sector.industry addSector:sector];
    [self.sectors setObject:sector forKey:[sectorDict objectForKey:@"SECTOR_NAME"] ];
    
    
}
-(void) updateWithIndustryDict:(NSDictionary*) industryDict {
    ITIndustry* industry = [[ITIndustry alloc] init];
    industry.name  = [industryDict objectForKey:@"INDUSTRY_NAME"];
    industry.marketType = @"Industry";
    industry.fullname  = [industryDict objectForKey:@"INDUSTRY_FULLNAME"];
    industry.industryID = [[industryDict objectForKey:@"INDUSTRY_ID"] stringValue];
    [self.industries setObject:industry forKey: [industryDict objectForKey:@"INDUSTRY_NAME"] ];
}
/*
-(void) updateWithGroupDict:(NSDictionary*) groupDict{
    TfexGroup* group = [[TfexGroup alloc ] init];
    group.groupName = [groupDict objectForKey:@"GROUP_NAME"];
    [self.tfexGroups setObject:group forKey:[groupDict objectForKey:@"GROUP_ID"]];
    
}
-(void) updateWithTfexMarketDict:(NSDictionary*) tfexMarketDict {
    TfexMarket* market = [[TfexMarket alloc] init];
    market.marketID = [tfexMarketDict objectForKey:@"MARKET_ID"];
    market.marketName = [tfexMarketDict objectForKey:@"MARKET_NAME"];
    [self.tfexMarkets setObject:market forKey:[tfexMarketDict objectForKey:@"MARKET_CODE"]];
}
-(void) updateWithCommodityDict:(NSDictionary*) commodityDict{
    Commodity* commodity = [[Commodity alloc] init];
    commodity.commodityID = [commodityDict objectForKey:@"COMMODITY_ID"];
    commodity.commodityName = [commodityDict objectForKey:@"COMMODITY_NAME"];
    [self.commodities setObject:commodity forKey:[commodityDict objectForKey:@"COMMODITY_CODE"]];
}
-(void) updateWithInstrumentDict:(NSDictionary*) instrumentDict{
    Instrument* instrument = [[Instrument alloc] init];
    instrument.instrumentID = [instrumentDict objectForKey:@"INSTRUMENT_ID"];
    instrument.instrumentName = [instrumentDict objectForKey:@"INSTRUMENT_NAME"];
    instrument.instrumentType = [instrumentDict objectForKey:@"INSTRUMENT_TYPE"];
    instrument.tfexGroup = [self.tfexGroups objectForKey:[instrumentDict objectForKey:@"GROUP_CODE"]];
    instrument.tfexMarket = [self.tfexMarkets objectForKey:[instrumentDict objectForKey:@"MARKET_CODE"]];
    instrument.commodity = [self.commodities objectForKey:[instrumentDict objectForKey:@"COMMODITY_CODE"]];
    [self.instruments setObject:instrument forKey:[instrumentDict objectForKey:@"INSTRUMENT_ID"]];
}


//-(NSString* ) updateWithSeriesDict:(NSDictionary*) seriesDict{
//    SuVersionStock * stock = [[SuVersionStock alloc] init];
//    stock.symbol = [seriesDict objectForKey:@"SERIES"];
//    stock.stockType = @"TFEX";
//    [self.stocks setObject:stock forKey:stock.symbol];
//    return stock.symbol;
//}
  */
//static bool STORES_SERIES_IN_STOCKS_NAME = NO;
-(void)loadDataFromHttpUrl:(NSString *)url by:(id) completor complete:(SEL)selector{
    dispatch_queue_t downloadQueue = dispatch_queue_create("su downloader", NULL);
    dispatch_async(downloadQueue, ^{
        NSLog(@"start downdloading");
        NSData* content =  [NSData dataWithContentsOfURL:[NSURL URLWithString:url]] ;
        if ( ! content) {
            NSLog(@"No internet connection");
            if ([completor respondsToSelector:selector]) {
                [completor performSelector:selector withObject:nil];
            }
            
        } else {
            NSLog(@"download done");
            
            JSONDecoder* json = [[JSONDecoder alloc] initWithParseOptions:JKParseOptionNone];
            NSDictionary *resultsDictionary = [json objectWithData:content];
            dispatch_async(dispatch_get_main_queue(), ^{
                NSArray* data = [[resultsDictionary objectForKey:@"SU"] objectForKey:@"DATA"] ;
                NSMutableArray* stocksName = [NSMutableArray arrayWithCapacity:500];

                for (NSDictionary* row in data) {
//                    NSDictionary* tfexGroup = [row objectForKey:@"GROUP"];
//                    if (tfexGroup) {
//                        [self updateWithGroupDict:tfexGroup];
//                        continue;
//                    }
                    NSDictionary* industryDict = [row objectForKey:@"INDUSTRY"];
                    if (industryDict) {
                        [self updateWithIndustryDict:industryDict];
                        continue;
                    }
                    
                    NSDictionary* sectorDict = [row objectForKey:@"SECTOR"];
                    if (sectorDict) {
                        [self updateWithSectorDict:sectorDict];
                        continue;
                    }
                    
                    NSDictionary* security = [row objectForKey:@"SECURITY"];
                    if (security) {
                        [stocksName addObject:[self updateWithSecurityDict:security]];
                        continue;
                    }
//                    NSDictionary* series = [row objectForKey:@"SERIES"];
//                    if(series) {
//                        if (STORES_SERIES_IN_STOCKS_NAME) {
//                            [stocksName addObject:[self updateWithSeriesDict:series]];
//                        }
//                        continue;
//                    }
                }
                NSSortDescriptor *sortDescriptor;
                sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"symbol"
                                                             ascending:YES];
                NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
                for (NSString* key in self.markets) {
                    ITEquityMarket* market = [self.markets objectForKey:key];
                    market.stocks = [NSMutableArray arrayWithArray:[market.stocks sortedArrayUsingDescriptors:sortDescriptors]];
                }
                for (NSString* key in self.industries) {
                    ITIndustry* industry = [self.industries objectForKey:key];
                    industry.stocks = [NSMutableArray arrayWithArray:[industry.stocks sortedArrayUsingDescriptors:sortDescriptors]];
                }
                for (NSString* key in self.sectors) {
                    ITSector* sector = [self.sectors objectForKey:key];

                    sector.stocks = [NSMutableArray arrayWithArray:[sector.stocks sortedArrayUsingDescriptors:sortDescriptors]];
                }
                self.stocksNameArray = [stocksName sortedArrayUsingSelector:@selector(localizedCompare:)]    ;
                NSLog(@"call back");
                if ([completor respondsToSelector:selector]) {
                    [completor performSelector:selector withObject:self.stocks];
                }
                
                
            });
        }
        
    });
//    dispatch_release(downloadQueue);
    
    
}

#pragma mark - real time api
+(NSNumber*) changeByPrior:(NSNumber*) prior current:(NSNumber*) current {
    return [NSNumber numberWithDouble:[current doubleValue] - [prior doubleValue]];;
}
+(NSNumber*) percentChangeByChange:(NSNumber*) change prior:(NSNumber*) prior {
    return [NSNumber numberWithDouble:100 *([change doubleValue]/[prior doubleValue]) ];
}


-(MostType) mosTypeFromString:(NSString*) mostString {
    if ([mostString isEqualToString:@"MA"]) {
        return MostValue;
    } else if ([mostString isEqualToString:@"MV"]) {
        return MostVolume;
    } else if ([mostString isEqualToString:@"MS"]) {
        return MostSwing;
    } else if ([mostString isEqualToString:@"MG"]) {
        return MostGainer;
    } else {
        return MostLose;
    }
}
- (void)mostFromSiam:(NSString *)completeData {
    NSArray* component = [completeData componentsSeparatedByString:@"|"];
    //self.mostList
    //1 symbol, 2 volbid , 3 bid , 4 offer , 5 voloffer ,6 last , 7 change , 8 percent change
    int componentCount = [component count];
    NSMutableArray* mostStock = [NSMutableArray arrayWithCapacity:30];
    for (int i = 0 ; i < componentCount-9; i+=23) {
        
        NSString* stockName = [component objectAtIndex:2+i ];
        NSString* volbid = [component objectAtIndex:3+i ];
        NSString* bid = [component objectAtIndex:4+i ];
        NSString* offer = [component objectAtIndex:5+i ];
        NSString* voloffer = [component objectAtIndex:6+i ];
        NSString* last = [component objectAtIndex:7+i ];
        NSString* change = [component objectAtIndex:8+i ];
        NSString* percent = [component objectAtIndex:9+i ];
//        NSLog(@"most = %@ %@ %@ %@ %@ %@ %@ %@", stockName, volbid, bid, offer, voloffer, last, change, percent);
        NSDictionary* stockDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                   stockName , @"Stock",
                                   volbid,@"Vol_Offer",
                                   bid,@"Bid",
                                   offer,@"Offer",
                                   voloffer,@"Vol_Offer",
                                   last,@"Last",
                                   change,@"Change",
                                   percent,@"Percent_Change"
                                   , nil];
        [self updateStock:stockName siamStockDict:stockDict];
        [mostStock addObject:stockName];
        
    }
    self.mostType = [self mosTypeFromString:[component objectAtIndex:0]];
    NSLog(@"model change most list");
    self.mostList = [mostStock copy];

}
-(ITStockDetail *)stockByName:(NSString *)stockName {
    return [self.stocks objectForKey:stockName];
}
-(ITStockDetail * ) getSymbolButIfNotExistInsertThis:(NSString*) stockName {
    ITStockDetail * stock = [self.stocks objectForKey:stockName];
    if (! stock){
        stock = [[ITStockDetail alloc] initWithName:stockName];
        [self.stocks setObject:stock forKey:stockName];
    }
    return stock ;

}

-(void) tickerFromSiam:(NSString*) tickerData {
    NSArray* component = [tickerData componentsSeparatedByString:@"|"];
    if ([component count] >= 16) {
        
        NSString* stockName = [component objectAtIndex:2];
        ITStockDetail * stock = [self  getSymbolButIfNotExistInsertThis:stockName];
        [stock updateWithSiamTicker:component];
        if ([[component objectAtIndex:0] isEqualToString:@"T"]) {
            self.lastTicker = stock ;
        }
    }
}
// B|JAS|S|890|5191500|W|895|14510000|W|900|9162100|W|905|5026700|W|910|4676400|V
-(void) bidOfferFromSiam:(NSString*) bidOfferData {
    NSArray* component = [bidOfferData componentsSeparatedByString:@"|"];
    if ([component count] > 4) {
        NSString* stockName = [component objectAtIndex:1];
        [[self  getSymbolButIfNotExistInsertThis:stockName] updateWithSiamBidOffer:component];
        //[stock updateWithSiamBidOffer:component];
        //self.lastTicker = stock;
    }
}



-(void) projectedPriceFromSiam:(NSString*) projectedData {
    NSArray* component = [projectedData componentsSeparatedByString:@"|"];
    if ([component count] >4) {
        NSString* stockName = [component objectAtIndex:2];
        [[self  getSymbolButIfNotExistInsertThis:stockName] updateWithSiamProjected:component];
    }
}
-(void)updateStockAverageWithSiamData: (NSString*) stockAveragePrice {
    NSArray* component = [stockAveragePrice componentsSeparatedByString:@"|"];
    int messageLength = 3;
    if ([component count] >=  messageLength) {
        NSString* stockName = [component objectAtIndex:1];
        [[self getSymbolButIfNotExistInsertThis:stockName] updateWithSiamStockAverage:component ];
        
    }
}
-(void)updateStockDetailWithSiamData: (NSString*) stockDetailData {
    NSArray* component = [stockDetailData componentsSeparatedByString:@"|"];
    int messageD_length = 38;
    if ([component count] >=  messageD_length) {
        NSString* stockName = [component objectAtIndex:1];
        [[self getSymbolButIfNotExistInsertThis:stockName] updateWithSiamStockDetail:component ];
         
    }
}
-(void)updateMarketWithSiamData:(NSString*) marketData {
    NSLog(@"<ITStockModel> marketdata = %@", marketData);
    NSArray* component = [marketData componentsSeparatedByString:@"|"];
    int messageLength = 16;
    if ([component count] >= messageLength) {
        NSString* marketName =[component objectAtIndex:1];
        ITEquityMarket* market = [self marketOrIndustryOrSectorByName:marketName];
        if (market) {
            [market updateWithSiamMarketEquityMessage:component];
            return;
        }
    } else {
        NSLog(@"short M = '%@'", marketData);
    }

    
}
/**
 * @todo log message that exist in this program 
 */

-(void) checkMessagePrefix:(NSString*) completeData {
    if ([completeData length] == 0) {
        return;
    }
    NSString* messagePrefix = [completeData substringToIndex:1];
    if ( ! [self.messageName containsObject:messagePrefix]) {
        NSLog(@"unhandle message prefix %@, %@", messagePrefix, completeData);
        [self.messageName addObject:messagePrefix];
        
    }
}
-(void) processSiamCompleteData: (NSString*) completeData {
    if ([completeData length] <= 0) {
        return;
    }
    if ([completeData hasPrefix:@"MA"] ||
        [completeData hasPrefix:@"MV"] ||
        [completeData hasPrefix:@"MG"] ||
        [completeData hasPrefix:@"MS"] ||
        [completeData hasPrefix:@"ML"] ) {
        [self mostFromSiam:completeData];
    } else if ([completeData hasPrefix:@"T"]) { // message Ticker
        
//       NSLog(@"Ticker:%@", completeData);
        [self tickerFromSiam:completeData];
    } else if ([completeData hasPrefix:@"Q"]) {
        [self tickerFromSiam:completeData];
    }
    else if ([completeData hasPrefix:@"B"]) { // message Bid/Offer
        
        [self bidOfferFromSiam:completeData];
    } else if ([completeData hasPrefix:@"P"]) { // message projected price
        [self projectedPriceFromSiam: completeData];
    } else if ([completeData hasPrefix:@"D"]) { // message stock detail
        [self updateStockDetailWithSiamData: completeData];
    } else if ([completeData hasPrefix:@"A"]) {
        [self updateStockAverageWithSiamData: completeData];
    } else if ([completeData hasPrefix:@"M"]){ // only M not MA, MV, MS, MG, ML
        //NSLog(@"Market = %@", completeData);
        [self updateMarketWithSiamData: completeData];
    } else {
        // will display unhandle message
        [self checkMessagePrefix:completeData];
    }
}
-(void)processSiamData:(NSString*)dataString {
//    NSLog(@"dataString :%@", dataString);
    [self.siamDataBuffer appendString:dataString];

    NSArray* component = [self.siamDataBuffer  componentsSeparatedByString:@"!"];
    int count  = [component count];
    if (count == 1) { //no '!' in string
        return;
    }
    if ([dataString hasSuffix:@"!"]) { // all data are complete data
        for (NSString* data in component) {
            [self processSiamCompleteData:data];
        }
        [self.siamDataBuffer setString:@""];
    } else { // last data is not complete data
        for (int i=0; i < count-1; i++) {
            [self processSiamCompleteData:[component objectAtIndex:i]];
        }
        [self.siamDataBuffer setString:[component lastObject]];
        
    }
}



- (void)updateStock:(NSString *)stockName stockDict:(NSDictionary *)stockDict {
    ITStockDetail * stock = [self.stocks objectForKey:stockName];
    if (stock){
        [stock updateWithMostDict:stockDict];
    } else {
        stock = [[ITStockDetail alloc] initWithName:stockName];
        [self.stocks setObject:stock forKey:stockName];
        [stock updateWithMostDict:stockDict];
    }
}

- (void)updateStock:(NSString *)stockName siamStockDict:(NSDictionary *)stockDict {
    ITStockDetail * stock = [self.stocks objectForKey:stockName];
    if (stock){
        [stock updateWithSiamMostDict:stockDict];
    } else {
        stock = [[ITStockDetail alloc] initWithName:stockName];
        [self.stocks setObject:stock forKey:stockName];
        [stock updateWithSiamMostDict:stockDict];
    }
}

-(void)updateWithMostArray:(NSArray *)mostArray {
    for (NSDictionary* stockDict in mostArray) {
        NSString* stockName = [stockDict objectForKey:@"Stock"];
        [self updateStock:stockName stockDict:stockDict];
    }
}
//-(void) requestMost:(MostType) mostType {}

@end
