//
//  ZMStockModel.m
//  RealPush7
//
//  Created by Sutean Rutjanalard on 26/3/2557 BE.
//  Copyright (c) 2557 Sutean Rutjanalard. All rights reserved.
//

#import "ZMStockModel.h"
#import "AsyncSocket.h"
#import "NSString+mod96.h"
#import "NSNumber+formatter.h"
#import <AFNetworking.h>
#import <JSONKit.h>
#import "ios_pullSvc.h"
#import "Config.h"
#import <RXMLElement.h>

@interface ZMStockModel () <ios_pullSoapBindingResponseDelegate>
@property (nonatomic, strong) AsyncSocket* socket;
@property (nonatomic, strong) NSCharacterSet* seperator;
@property (nonatomic, strong) NSCharacterSet* newline;
@property (nonatomic, strong) NSString* host;
@property (nonatomic) NSUInteger port;
@property (nonatomic) BOOL login;
@property (nonatomic) ConnectionState state;
@property (nonatomic,strong) NSMutableString* messageBuffer;
@property (nonatomic, strong) NSDictionary* marketStatusForCode;


@property (nonatomic, strong) NSString* username;
@property (nonatomic, strong) NSString* password;





@end
static int LAST_EXECUTE_LENGTH = 19;
static int TOP_PRICE_LENGTH = 13;


@implementation ZMStockModel
#pragma mark - CLASS method
+(int)lastExecuteLength {
    return LAST_EXECUTE_LENGTH;
}
#pragma mark - initialize
-(NSMutableString *)messageBuffer {
    if (_messageBuffer) {
        return _messageBuffer;
    }
    _messageBuffer = [[NSMutableString alloc] init];
    return _messageBuffer;
}

-(AsyncSocket *)socket {
    if ( ! _socket ) {
        _socket = [[AsyncSocket alloc] initWithDelegate:self];
    }
    return _socket;
}
+(NSDictionary *)marketStatusForCode {
    return @{@"S": @"Start",
             @"P": @"PreOpen",
             @"O": @"Open",
             @"I": @"Intermission",
             @"X": @"PreClose",
             @"Y": @"OffHour",
             @"C": @"Close",
             @"H": @"Halt",
             @"Z": @"Circuit Breaker",
             //K & T No need to display
             @"K": @"Close2",
             @"T": @"AfterMarket"
            };
    
}
+(NSDictionary*) marketStatusCodeForStatus {
    return @{@"Start": @"S",
             @"PreOpen": @"P",
             @"Open": @"O",
             @"Intermission": @"I",
             @"PreClose": @"X",
             @"OffHour": @"Y",
             @"Close": @"C",
             @"Halt": @"H",
             @"Circuit Breaker": @"Z",
             //K & T No need to display
             @"Close2": @"K",
             @"AfterMarket": @"T"
             };
}
//disable init
-(id)init {
    return nil;
}
- (instancetype)initWithHost:(NSString*) host port:(NSInteger)port marketConfigurationURL:(NSString*) url;

{
    self = [super init];
    if (self) {
        self.seperator = [NSCharacterSet characterSetWithCharactersInString:[NSString stringWithFormat:@"%c" , (char)134]];
        self.newline = [NSCharacterSet newlineCharacterSet];
        self.host = host;
        self.port = port;
        self.state = StateInitial;
        [self loadMarketConfiguration:url];
    }
    return self;
}
-(void) loadMarketConfiguration:(NSString*) url {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSLog(@"JSON: %@", responseObject );
        [self updateMarketWithStockRadarJSON:responseObject];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
//        NSString* result = [NSString stringWithFormat:@"Error: %@", error];
    }];

}
-(void) updateMarketWithStockRadarJSON:(id) jsonObject {
    
    for (NSDictionary* marketDict in jsonObject) {
        NSString* marketName = marketDict[@"market"];
        NSArray* symbolList = marketDict[@"symbol"];
        
        for (NSString* symbol in symbolList) {
//            [self insertSymbolIfNotExist:symbol];
            if (self.markets[marketName]) {
                [self.markets[marketName] addStock: [self getSymbolButIfNotExistInsertThis:symbol]];
            } else if(self.sectors[marketName]){
                [self.sectors[marketName] addStock:[self getSymbolButIfNotExistInsertThis:symbol]];
            } else {
                self.sectors[marketName] = [[ITSector alloc] initWithName:marketName];
                [self.sectors[marketName] addStock:[self getSymbolButIfNotExistInsertThis:symbol]];
            }
        }
    }
}
-(void)startConnectWithUsername:(NSString*) username password:(NSString*) password {
    [self.socket connectToHost:self.host onPort:self.port error:nil];
    self.state = StateConnecting;
    self.username = username;
    self.password = password;
}
-(void) sendMessage:(NSString*) message {
    if ([self.socket isConnected]) {
        message = [message stringByAppendingString:@"\n"];
        [self.socket writeData:[message dataUsingEncoding:NSUTF8StringEncoding] withTimeout:-1 tag:0];
    } else {
        NSLog(@"Socket not connect can not send message");
    }
}
#pragma mark - PULL 

-(void) pullMarket {
    ios_pullSoapBinding *service = [[ios_pullSoapBinding alloc] initWithAddress:[Config zmicoPullHost]] ;
    service.logXMLInOut = NO;
    ios_pullSvc_market_index *request = [ios_pullSvc_market_index new] ;
    request.autkey = [Config zmicoAuthKey];
    [service market_indexAsyncUsingParameters:request delegate:self];
}
-(void) pullStock:(NSString*) stockName {
    ios_pullSoapBinding *service = [[ios_pullSoapBinding alloc] initWithAddress:[Config zmicoPullHost]] ;
    service.logXMLInOut = NO;
    ios_pullSvc_stock *request = [ios_pullSvc_stock new] ;
    request.autkey = [Config zmicoAuthKey];
    request.stock = stockName;
    [service stockAsyncUsingParameters:request delegate:self];

}
-(void) pullFundamental:(NSString*) stockName {
    ios_pullSoapBinding *service = [[ios_pullSoapBinding alloc] initWithAddress:[Config zmicoPullHost]] ;
    ios_pullSvc_Get_fundamental* requestFunda = [ios_pullSvc_Get_fundamental new];
    requestFunda.autkey = [Config zmicoAuthKey];
    requestFunda.stock = stockName;
    [service Get_fundamentalAsyncUsingParameters:requestFunda delegate:self];
}
-(void) pullChartForStock:(NSString*) stock {
    ios_pullSoapBinding* service = [[ios_pullSoapBinding alloc] initWithAddress:[Config zmicoPullHost]];
    ios_pullSvc_stock* request = [ios_pullSvc_stock new];
    request.autkey = [NSString stringWithFormat:@"%@#1", [Config zmicoAuthKey]];
    request.stock = stock;
    [service stockAsyncUsingParameters:request delegate:self];
}

-(void)pullMost:(MostType)mostType {
    ios_pullSoapBinding *service = [[ios_pullSoapBinding alloc] initWithAddress:[Config zmicoPullHost]];
    service.logXMLInOut = NO;
    self.mostType = mostType;
    self.mostList = nil;
    switch (mostType) {
        case MostVolume:{
            ios_pullSvc_Get_Most_Active_Vol* request = [ios_pullSvc_Get_Most_Active_Vol new];
            request.autkey = [Config zmicoAuthKey];
            [service Get_Most_Active_VolAsyncUsingParameters:request delegate:self];
            break;
        }
        case MostValue: {
            ios_pullSvc_Get_Most_Active_Val* request = [ios_pullSvc_Get_Most_Active_Val new];
            request.autkey = [Config zmicoAuthKey];
            [service Get_Most_Active_ValAsyncUsingParameters:request delegate:self];
            break;
        }
        case MostSwing: {
            ios_pullSvc_Get_Most_Swing* request = [ios_pullSvc_Get_Most_Swing new];
            request.autkey = [Config zmicoAuthKey];
            [service Get_Most_SwingAsyncUsingParameters:request delegate:self];
            break;
        }
        case MostGainer: {
            ios_pullSvc_Get_Most_Gainer* request = [ios_pullSvc_Get_Most_Gainer new];
            request.autkey = [Config zmicoAuthKey];
            [service Get_Most_GainerAsyncUsingParameters:request delegate:self];
            break;
        }
        case MostLose: {
            ios_pullSvc_Get_Most_Loser* request = [ios_pullSvc_Get_Most_Loser new];
            request.autkey = [Config zmicoAuthKey];
            [service Get_Most_LoserAsyncUsingParameters:request delegate:self];
            break;
        }
    }
}

-(void)operation:(ios_pullSoapBindingOperation *)operation completedWithResponse:(ios_pullSoapBindingResponse *)response {
    NSArray *responseBodyParts = response.bodyParts;
    
    for(id bodyPart in responseBodyParts) {
        if([bodyPart isKindOfClass:[ios_pullSvc_market_indexResponse class]]) {
            [self pullResultOfMarketIndex:operation.responseData];
        } else if ([bodyPart isKindOfClass:[ios_pullSvc_stockResponse class]]) {
            [self pullResultOfStock:operation.responseData];
        } else if ([bodyPart isKindOfClass:[ios_pullSvc_Get_Most_Active_ValResponse class]] ||
                   [bodyPart isKindOfClass:[ios_pullSvc_Get_Most_Active_VolResponse class]] ||
                   [bodyPart isKindOfClass:[ios_pullSvc_Get_Most_SwingResponse class]] ||
                   [bodyPart isKindOfClass:[ios_pullSvc_Get_Most_LoserResponse class]] ||
                   [bodyPart isKindOfClass:[ios_pullSvc_Get_Most_GainerResponse class]]
                   ) {
            [self pullResultOfMost:operation.responseData];
        }else if ([bodyPart isKindOfClass:[ios_pullSvc_Get_fundamentalResponse class]]) {
            [self pullResltOfFundamental:operation.responseData];
        }
    }
}
-(void) pullResltOfFundamental:(NSData*) responseData {
    RXMLElement* rxml = [RXMLElement elementFromXMLData:responseData];
    NSLog(@"responseData = %@", [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding]);

    [rxml iterate:@"Body.*.*.KTZMICO.Fundamental" usingBlock:^(RXMLElement* e) {
        ITStockDetail* stock = [self getSymbolButIfNotExistInsertThis:[e child:@"Stock"].text];
        stock.willUpdate = YES;
        stock.pe = [e child:@"pe"].text;
        stock.pbv = [e child:@"pbv"].text;
        stock.yield = [e child:@"yield"].text;
        stock.eps = [e child:@"eps"].text;
        stock.marketCap = [e child:@"mcap"].text;
        stock.hasUpdate = YES;
    }];
}

/*
 <Most_Swing><Stock>SCC-F</Stock><Vol_Bid>8900</Vol_Bid><Bid>41,600.00</Bid><Offer>42,000.00</Offer><Vol_Offer>1000</Vol_Offer><Last>416.00</Last><Change>-12.00</Change><Percent_Change>-2.80</Percent_Change><High>426.00</High><Low>416.00</Low><AVG>4.21</AVG><Acc_Val>343979800</Acc_Val><Acc_Vol>818000</Acc_Vol></Most_Swing></KTZMICO></Get_Most_Active_ValResult></Get_Most_Active_ValResponse></soap:Body></soap:Envelope>
 */
-(void) pullResultOfMost:(NSData*) responseData {
    RXMLElement* rxml = [RXMLElement elementFromXMLData:responseData];
    NSMutableArray* mostList = [[NSMutableArray alloc] init];
    [rxml iterate:@"Body.*.*.KTZMICO.Most_Swing" usingBlock:^(RXMLElement* e) {
//        NSLog(@"stock = %@", [e child:@"Stock"].text);
        ITStockDetail* stock = [self getSymbolButIfNotExistInsertThis:[e child:@"Stock"].text];
        stock.willUpdate = YES;
        stock.volbid1 = [NSNumber numberWithDouble:[e child:@"Vol_Bid"].textAsDouble];
        stock.voloffer1 = [NSNumber numberWithDouble:[e child:@"Vol_Offer"].textAsDouble];
        if ([[e child:@"Bid"].text hasPrefix:@"A"]) {
            stock.bid1 = [e child:@"Bid"].text;
        } else {
            stock.bid1Number    =  [NSNumber numberWithDouble:[e child:@"Bid"].textAsDouble];
            stock.bid1 = [self formateFirstBidOfferPrice:stock.bid1Number volume:stock.volbid1 marketStatusCode:self.marketStatusCode];
        }
        if ([[e child:@"Offer"].text hasPrefix:@"A"]) {
            stock.offer1 = [e child:@"Offer"].text;
        } else {
            stock.offer1Number    =  [NSNumber numberWithDouble:[e child:@"Offer"].textAsDouble];
            stock.offer1 = [self formateFirstBidOfferPrice:stock.offer1Number volume:stock.voloffer1 marketStatusCode:self.marketStatusCode];
        }
        stock.price = [NSNumber numberWithDouble:[e child:@"Last"].textAsDouble ];
        stock.change = [NSNumber numberWithDouble:[e child:@"Change"].textAsDouble];
        if ([e child:@"PercentChg"]) {
            stock.percentChange = [NSNumber numberWithDouble:[e child:@"PercentChg"].textAsDouble];
        } else if ([e child:@"Percent_Change"]) {
            stock.percentChange = [NSNumber numberWithDouble:[e child:@"Percent_Change"].textAsDouble];
        }

        stock.high = [NSNumber numberWithDouble:[e child:@"High"].textAsDouble];
        stock.low = [NSNumber numberWithDouble:[e child:@"Low"].textAsDouble];
        stock.averagePrice = [NSNumber numberWithDouble:[e child:@"AVG"].textAsDouble];
        stock.totalValue = [NSNumber numberWithDouble:[e child:@"Acc_Val"].textAsDouble];
        stock.totalVolume = [NSNumber numberWithDouble:[e child:@"Acc_Vol"].textAsDouble];
        stock.hasUpdate = YES;

        [mostList addObject:stock];
    }];
    NSLog(@"mostList = %@", mostList);
    self.mostList = [mostList copy];
}

/*
 <?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Body><market_indexResponse xmlns="http://tempuri.org/"><market_indexResult><KTZMICO xmlns=""><marketindex><Index>SET</Index><IndexValue>1421.07</IndexValue><Advances>255</Advances><Nochange>217</Nochange><Declines>330</Declines><High>1426.52</High><Low>1419.13</Low><totalVol>10959936731.3</totalVol><totalShares>2282875898</totalShares><Prior>1421.48</Prior><mkt_status>Intermission</mkt_status></marketindex><marketindex><Index>MAI</Index><IndexValue>424.28</IndexValue><Advances>42</Advances><Nochange>34</Nochange><Declines>40</Declines><High>426.52</High><Low>423.64</Low><totalVol>658260948.7</totalVol><totalShares>326558968</totalShares><Prior>424.68</Prior><mkt_status>Intermission</mkt_status></marketindex><marketindex><Index>SET50</Index><IndexValue>963.97</IndexValue><Advances>0</Advances><Nochange>0</Nochange><Declines>0</Declines><High>968.39</High><Low>961.87</Low><totalVol>6258208820</totalVol><totalShares>318254799</totalShares><Prior>964.36</Prior><mkt_status>Intermission</mkt_status></marketindex><marketindex><Index>SET100</Index><IndexValue>2117.33</IndexValue><Advances>0</Advances><Nochange>0</Nochange><Declines>0</Declines><High>2126.67</High><Low>2113.51</Low><totalVol>7576275136</totalVol><totalShares>542169564</totalShares><Prior>2118.7</Prior><mkt_status>Intermission</mkt_status></marketindex><marketindex><Index>SETHD</Index><IndexValue>1149.47</IndexValue><Advances>0</Advances><Nochange>0</Nochange><Declines>0</Declines><High>1155.1</High><Low>1147.32</Low><totalVol>2441990867</totalVol><totalShares>92269780</totalShares><Prior>1149.71</Prior><mkt_status>Intermission</mkt_status></marketindex></KTZMICO></market_indexResult></market_indexResponse></soap:Body></soap:Envelope>
 */
-(void) pullResultOfMarketIndex :(NSData*) responseData {
    RXMLElement* rxml = [RXMLElement elementFromXMLData:responseData];
    NSLog(@"RXML = %@" , rxml.tag);
    [rxml iterate:@"Body.market_indexResponse.market_indexResult.KTZMICO.marketindex" usingBlock: ^(RXMLElement *e) {
        NSLog(@"Market Index : %@", [e child:@"Index"].text);
        ITEquityMarket *market = self.markets[[e child:@"Index" ].text];
        market.willUpdate = YES;
        market.advance = [e child:@"Advances"].text ;
        market.unchange = [e child:@"Nochange"].text;
        market.decline = [e child:@"Declines"].text;
        market.indexValue = [NSNumber numberWithDouble:[e child:@"IndexValue"].textAsDouble];
        market.high = [NSNumber numberWithDouble:[e child:@"High"].textAsDouble];
        market.low = [NSNumber numberWithDouble:[e child:@"Low"].textAsDouble];
        market.totalValue = [NSNumber numberWithDouble:[e child:@"totalVol"].textAsDouble];
        market.totalVolume = [NSNumber numberWithDouble:[e child:@"totalShares"].textAsDouble];
        market.prior = [NSNumber numberWithDouble:[e child:@"Prior"].textAsDouble];
        market.change = [ITStockModel changeByPrior:market.prior current:market.indexValue];
        market.percentChange = [ITStockModel percentChangeByChange:market.change prior:market.prior];
        self.marketStatus = [e child:@"mkt_status"].text;
        self.marketStatusCode = [ZMStockModel marketStatusCodeForStatus][self.marketStatus];
        market.hasUpdate = YES;
        
    }];
    
}
/*
 G|AAV|1|3.88|3.82|3.86|0958,3.86,24400000|0959,3.86,25200000|1000,3.86,25200000|1001,3.86,25200000|1002,3.86,25200000|1003,3.88,25800000|
 symbol | freq | high | low | prior | graph...
 graph = time, price , vol
*/
-(void) pullResultUpdateStockGraph:(NSString*) graph {
    NSArray* result = [graph componentsSeparatedByString:@"|"];
    ITStockDetail* stock = [self getSymbolButIfNotExistInsertThis:result[1]];
    stock.willUpdate = YES;
    stock.high = [NSNumber numberWithDouble:[result[3] doubleValue]];
    stock.low = [NSNumber numberWithDouble:[result[4] doubleValue]];
    stock.prior = [NSNumber numberWithDouble:[result[5] doubleValue]];
    NSUInteger count = [result count];
    NSMutableArray* timeSeries = [[NSMutableArray alloc] init];
    for (int i = 6; i < count; i ++) {
        NSArray* row = [result[i] componentsSeparatedByString:@","];
        NSArray* resultRow = @[row[0] , [NSNumber numberWithDouble:[row[1] doubleValue]] , [NSNumber numberWithDouble:[row[2] doubleValue]]];
        [timeSeries addObject:resultRow];
    }
    stock.chartTimeSeries = [timeSeries copy];
    stock.hasUpdate = YES;
    NSLog(@"graph %@", stock.chartTimeSeries);
}
/*
 <?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Body><stockResponse xmlns="http://tempuri.org/"><stockResult><KTZMICO xmlns=""><GetStock><Symbol>AAV</Symbol><SycurityName>ASIA AVIATION PUBLIC COMPANY LIMITED</SycurityName><Ceiling>5.10</Ceiling><Floor>2.76</Floor><Prior>3.94</Prior><High>3.96</High><Low>3.92</Low><Avg>3.93</Avg><Open>3.92</Open><Open2>3.92</Open2><ProjectedOpen>3.92</ProjectedOpen><BuyVolume>2704000</BuyVolume><SellVolume>5172300</SellVolume><UnKnownVolume>1113600</UnKnownVolume><Bid1>3.92</Bid1><Bid1Vol>203300</Bid1Vol><Bid2>3.90</Bid2><Bid2Vol>1724100</Bid2Vol><Bid3>3.88</Bid3><Bid3Vol>1720500</Bid3Vol><Bid4>3.86</Bid4><Bid4Vol>1480700</Bid4Vol><Bid5>3.84</Bid5><Bid5Vol>757500</Bid5Vol><Offer1>3.94</Offer1><Offer1Vol>555400</Offer1Vol><Offer2>3.96</Offer2><Offer2Vol>102700</Offer2Vol><Offer3>3.98</Offer3><Offer3Vol>328900</Offer3Vol><Offer4>4.00</Offer4><Offer4Vol>1144200</Offer4Vol><Offer5>4.02</Offer5><Offer5Vol>609600</Offer5Vol><Vol1>584400</Vol1><Last1>3.92</Last1><Time1>163838</Time1><Side1>U</Side1><Vol2>10000</Vol2><Last2>3.92</Last2><Time2>162958</Time2><Side2>B</Side2><Vol3>10000</Vol3><Last3>3.92</Last3><Time3>162933</Time3><Side3>B</Side3><Vol4>100</Vol4><Last4>3.92</Last4><Time4>162855</Time4><Side4>B</Side4><Vol5>100</Vol5><Last5>3.92</Last5><Time5>162853</Time5><Side5>B</Side5><Flags xml:space="preserve">    </Flags><Acc_Val>35319374</Acc_Val><Acc_Vol>8989900</Acc_Vol><PrjPrice>3.92</PrjPrice><PrjVol /></GetStock></KTZMICO></stockResult></stockResponse></soap:Body></soap:Envelope>
 */
-(void) pullResultOfStock: (NSData*) responseData {
    // @TODO
    RXMLElement* rxml = [RXMLElement elementFromXMLData:responseData];
    NSLog(@"pullResultOfStock: %@", [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding]);
    
    
    [rxml iterate:@"Body.stockResponse.stockResult.KTZMICO.GetStock" usingBlock:^(RXMLElement *e) {
        NSLog(@"stock name = %@" , [e child:@"Symbol"].text);
        if ( ! [e child:@"Symbol"].text) { // graph
            NSString* graph = [e child:@"IndexValue"].text;
            NSLog(@"Graph %@", graph);
            //Symbol , freq per min, high , low , prior
            // time,price,vol| time,price,vol
            [self pullResultUpdateStockGraph:graph];
            return;
        }
        ITStockDetail* stock = [self getSymbolButIfNotExistInsertThis:[e child:@"Symbol"].text];
        stock.fullname = [e child:@"SycurityName"].text;
        stock.ceiling  = [NSNumber numberWithDouble:[e child:@"Ceiling"].textAsDouble];
        stock.floor    = [NSNumber numberWithDouble:[e child:@"Floor"].textAsDouble];
        stock.prior    = [NSNumber numberWithDouble:[e child:@"Prior"].textAsDouble];
        stock.high    = [NSNumber numberWithDouble:[e child:@"High"].textAsDouble];
        stock.low    = [NSNumber numberWithDouble:[e child:@"Low"].textAsDouble];
        stock.averagePrice    = [NSNumber numberWithDouble:[e child:@"Avg"].textAsDouble];
        stock.open1    = [NSNumber numberWithDouble:[e child:@"Open"].textAsDouble];
        stock.open2    = [NSNumber numberWithDouble:[e child:@"Open2"].textAsDouble];
        stock.projectOpen    = [NSNumber numberWithDouble:[e child:@"ProjectedOpen"].textAsDouble]; //duplicate
        stock.buyVolume    = [NSNumber numberWithDouble:[e child:@"BuyVolume"].textAsDouble];
        stock.sellVolume    = [NSNumber numberWithDouble:[e child:@"SellVolume"].textAsDouble];
        stock.unknowVolume    = [NSNumber numberWithDouble:[e child:@"UnKnownVolume"].textAsDouble];
        stock.volbid1    = [NSNumber numberWithDouble:[e child:@"Bid1Vol"].textAsDouble];

        if ([[e child:@"Bid1"].text hasPrefix:@"A"]) {
            stock.bid1 = [e child:@"Bid1"].text;
        } else {
            stock.bid1Number    =  [NSNumber numberWithDouble:[e child:@"Bid1"].textAsDouble];
            stock.bid1 = [self formateFirstBidOfferPrice:stock.bid1Number volume:stock.volbid1 marketStatusCode:self.marketStatusCode];
        }
        
        stock.bid2    = [NSNumber numberWithDouble:[e child:@"Bid2"].textAsDouble];
        stock.volbid2    = [NSNumber numberWithDouble:[e child:@"Bid2Vol"].textAsDouble];
        stock.bid3    = [NSNumber numberWithDouble:[e child:@"Bid3"].textAsDouble];
        stock.volbid3    = [NSNumber numberWithDouble:[e child:@"Bid3Vol"].textAsDouble];
        stock.bid4    = [NSNumber numberWithDouble:[e child:@"Bid4"].textAsDouble];
        stock.volbid4    = [NSNumber numberWithDouble:[e child:@"Bid4Vol"].textAsDouble];
        stock.bid5    = [NSNumber numberWithDouble:[e child:@"Bid5"].textAsDouble];
        stock.volbid5    = [NSNumber numberWithDouble:[e child:@"Bid5Vol"].textAsDouble];
        
        stock.voloffer1    = [NSNumber numberWithDouble:[e child:@"Offer1Vol"].textAsDouble];
        if ([[e child:@"Offer1"].text hasPrefix:@"A"]) {
            stock.offer1 = [e child:@"Offer1"].text;
        } else {
            stock.offer1Number    = [NSNumber numberWithDouble:[e child:@"Offer1"].textAsDouble];
            stock.offer1 = [self formateFirstBidOfferPrice:stock.offer1Number volume:stock.voloffer1 marketStatusCode:self.marketStatusCode];
        }
        
        
        stock.offer2    = [NSNumber numberWithDouble:[e child:@"Offer2"].textAsDouble];
        stock.offer3    = [NSNumber numberWithDouble:[e child:@"Offer3"].textAsDouble];
        stock.offer4    = [NSNumber numberWithDouble:[e child:@"Offer4"].textAsDouble];
        stock.offer5    = [NSNumber numberWithDouble:[e child:@"Offer5"].textAsDouble];
        stock.voloffer2    = [NSNumber numberWithDouble:[e child:@"Offer2Vol"].textAsDouble];
        stock.voloffer3    = [NSNumber numberWithDouble:[e child:@"Offer3Vol"].textAsDouble];
        stock.voloffer4    = [NSNumber numberWithDouble:[e child:@"Offer4Vol"].textAsDouble];
        stock.voloffer5    = [NSNumber numberWithDouble:[e child:@"Offer5Vol"].textAsDouble];
        
        stock.flags    = [e child:@"Flags"].text;
        stock.totalValue    = [NSNumber numberWithDouble:[e child:@"Acc_Val"].textAsDouble];
        stock.totalVolume    = [NSNumber numberWithDouble:[e child:@"Acc_Vol"].textAsDouble];
        stock.projectOpen    = [NSNumber numberWithDouble:[e child:@"PrjPrice"].textAsDouble]; //duplicate
        stock.projectVolume    = [NSNumber numberWithDouble:[e child:@"PrjVol"].textAsDouble];
        stock.tickersArray = [[NSMutableArray alloc] init]; //clear older ticker
        stock.price = [NSNumber numberWithDouble:[e child:@"Last5"].textAsDouble];
        [stock addNewTickerWithTime:[e child:@"Time5"].text
                               side:[e child:@"Side5"].text
                              price:stock.price
                             volume:[NSNumber numberWithDouble:[e child:@"Vol5"].textAsDouble]
                             change:[ITStockModel changeByPrior:stock.prior current:stock.price]];
        
        stock.price = [NSNumber numberWithDouble:[e child:@"Last4"].textAsDouble];
        [stock addNewTickerWithTime:[e child:@"Time4"].text
                               side:[e child:@"Side4"].text
                              price:stock.price
                             volume:[NSNumber numberWithDouble:[e child:@"Vol4"].textAsDouble]
                             change:[ITStockModel changeByPrior:stock.prior current:stock.price]];
        
        stock.price = [NSNumber numberWithDouble:[e child:@"Last3"].textAsDouble];
        [stock addNewTickerWithTime:[e child:@"Time3"].text
                               side:[e child:@"Side3"].text
                              price:stock.price
                             volume:[NSNumber numberWithDouble:[e child:@"Vol3"].textAsDouble]
                             change:[ITStockModel changeByPrior:stock.prior current:stock.price]];
        
        stock.price = [NSNumber numberWithDouble:[e child:@"Last2"].textAsDouble];
        [stock addNewTickerWithTime:[e child:@"Time2"].text
                               side:[e child:@"Side2"].text
                              price:stock.price
                             volume:[NSNumber numberWithDouble:[e child:@"Vol2"].textAsDouble]
                             change:[ITStockModel changeByPrior:stock.prior current:stock.price]];
        
        stock.price = [NSNumber numberWithDouble:[e child:@"Last1"].textAsDouble];
        [stock addNewTickerWithTime:[e child:@"Time1"].text
                               side:[e child:@"Side1"].text
                              price:stock.price
                             volume:[NSNumber numberWithDouble:[e child:@"Vol1"].textAsDouble]
                             change:[ITStockModel changeByPrior:stock.prior current:stock.price]];
        
    }];
}
#pragma mark - AsyncSocket
-(void) resetConnectionState {
    self.username = nil;
    self.password = nil;
    self.login = NO;
}
- (void)onSocket:(AsyncSocket *)sock willDisconnectWithError:(NSError *)err {
    if (sock == self.socket) {
        NSLog(@"Err!!! socket will disconnect. with error %@", [err debugDescription]);
    }
}

- (void)onSocketDidDisconnect:(AsyncSocket *)sock {
    if (sock == self.socket) {
        if ( ! [self.socket isConnected]) {
            NSLog(@"All Socket disconnected reset state of login");
            [self resetConnectionState];
        }
    }
}
- (void)onSocket:(AsyncSocket *)sock didConnectToHost:(NSString *)host port:(UInt16)port {
    NSLog(@"connected to %@:%d", host,port);
    self.state = StateConnected;
    if (sock == self.socket) {
        self.login = NO;
        [self.socket readDataWithTimeout:-1 tag:0];
    }
    
}
- (void)onSocket:(AsyncSocket *)sock didReadData:(NSData *)data withTag:(long)tag {
    if (self.socket == sock) {
        [self pushSocketReadData:data];
    }
}

-(void) processData:(NSData*) data {
    NSString* dataString =  [[NSString alloc] initWithData:data
                                                  encoding:CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingASCII)];
//    NSLog(@"dataString len = %d", [data length]);
    dataString = [self.messageBuffer stringByAppendingString:dataString];
    [self.messageBuffer setString:@""]; //clear buffer
    NSArray *dataArray = [dataString componentsSeparatedByCharactersInSet:self.newline]; //35%

    NSString* junk = nil;
    int count = 0;
    for (NSString* message in dataArray) {
        junk = [self processMessage:message];
        if (junk) {
            count += 0;
//            NSLog(@"count = %d, junk = '%@'", count, junk);
        }
    }
    if (junk) {
        [self.messageBuffer appendString:junk];
    }
    
}
-(NSString*) processMessage:(NSString *)message  {
    NSString* junk = nil;
    if ([message hasPrefix:@"TP"]) {
        NSArray* dataArray = [message componentsSeparatedByCharactersInSet:self.seperator];
        if ( ! [self processTopPrice:dataArray]) {
            junk = message;
        }
        
    } else if ([message hasPrefix:@"LE"]) {
        NSArray* dataArray = [message componentsSeparatedByCharactersInSet:self.seperator];
        if ( ! [self processLastExecute:dataArray] ) {
            junk = message;
        }
    } else if ([message hasPrefix:@"SC"]) {
        //SC,time,code
        NSArray* dataArray = [message componentsSeparatedByString:@","];
        self.marketStatusCode = dataArray[2];
        NSLog(@"sc = %@", dataArray);
    } else if([message hasPrefix:@"AP"]) {
        //NSLog(@"AP = %@", message);
        NSArray* dataArray = [message componentsSeparatedByString:@","];
        if (! [self processAvgMessage:dataArray]) {
            junk = message;
        }
    } else if ([message hasPrefix:@"PO"]) {
//        NSLog(@"PO = %@", message);
        NSArray* dataArray = [message componentsSeparatedByString:@","];
        if ( ! [self processProjected:dataArray]) {
            junk = message;
        }
    } else if ([message hasPrefix:@"IU"]) {
        NSArray* dataArray = [message componentsSeparatedByString:@","];
        if ( ! [self processIndexUpdate:dataArray]) {
            junk = message;
        }
    } else if ([message hasPrefix:@"MU"]) {
        NSArray* dataArray = [message componentsSeparatedByString:@","];
        if ( ! [self processIndexUpdate:dataArray]) {
            junk = message;
        }
    } else if ([message hasPrefix:@"SX"]) {
//        NSLog(@"SX = %@", message);
        
    } else if ([message hasPrefix:@"SS"]) { //Security Status
        //NSLog(@"");
    }
    else {
//        NSLog(@"Other = %@",message);
    }
    return junk;
}
//MU,time,set_index,total_trdes,total_shares,total_value,up_volume,no_change _volume,down_volume,advances,no_change,declines,set50_index,set100_ind ex,nBuyVol,nSellVol ,HighIndex,LowIndex,PriorIndex
-(BOOL) processMaiUpdate:(NSArray*) dataArray {
    if ([dataArray count] < 19) {
        return NO;
    }
    ITEquityMarket* mai = self.markets[@"MAI"];
    
    
    mai.willUpdate = YES;
    mai.indexTime = dataArray[1];
    NSLog(@"set_index = %@", dataArray[2]);
    NSLog(@"total trade = %@", dataArray[3] );
    NSLog(@"total shares = %@", dataArray[4]);
    NSLog(@"total value = %@", dataArray[5]);
    NSLog(@"up volume = %@", dataArray[6]);
    NSLog(@"no change volume = %@", dataArray[7]);
    NSLog(@"down volume = %@", dataArray[8]);
    NSLog(@"advance = %@", dataArray[9]);
    NSLog(@"no change = %@", dataArray[10]);
    NSLog(@"declines = %@", dataArray[11]);
    NSLog(@"s50 index= %@", dataArray[12]);
    NSLog(@"s100 index = %@", dataArray[13]);
    NSLog(@"buy vol = %@", dataArray[14]);
    NSLog(@"sell vol = %@", dataArray[15]);
    NSLog(@"high index = %@", dataArray[16]);
    NSLog(@"low index= %@", dataArray[17]);
    NSLog(@"prior index = %@", dataArray[18]);

    
    mai.hasUpdate = YES;
    return YES;
}

//IU,time,set_index,total_trades,total_shares,total_value,up_volume, no_change_volume,down_volume,advances,no_change,declines,set50_index,s et100_index,nBuyVol,nSellVol ,SectorGroup_No ,HighIndex,LowIndex,PriorIndex

-(BOOL) processIndexUpdate:(NSArray*) dataArray {
    if ([dataArray count] < 20 ) {
        return NO;
    }
    NSDictionary* setName = @{ @"90" : @"SET100",
                               @"95" : @"SET50",
                               @"99" : @"SET",
                               @"98" : @"MAI"};
    
    ITEquityMarket* set = self.markets[setName[dataArray[16]]];
    if ( ! setName[dataArray[16]]) {
        NSLog(@"IU = %@", dataArray);
    }
    set.willUpdate = YES;
//    ITEquityMarket* set100 = self.markets[@"SET100"];
//    ITEquityMarket* set50 = self.markets[@"SET50"];
    set.indexTime = dataArray[1];
//    set100.indexTime = dataArray[1];
//    set50.indexTime = dataArray[1];
    set.indexValue = [NSNumber numberWithString:dataArray[2] numberStyle:priceWithoutDivide];
    set.totalVolume = [NSNumber numberWithString:dataArray[4] numberStyle:volumeFormatStyle];
    set.totalValue = [NSNumber numberWithString:dataArray[5] numberStyle:priceFormatStyle];
    
    //data display as -2147483648.0 for this 3 fields
    set.upVolume = [NSNumber numberWithString:dataArray[6] numberStyle:volumeFormatStyle];
    set.noChangeVolume = [NSNumber numberWithString:dataArray[7] numberStyle:volumeFormatStyle];
    set.downVolume = [NSNumber numberWithString:dataArray[8] numberStyle:volumeFormatStyle];
    
    set.advance = dataArray[9];
    set.unchange = dataArray[10];
    set.decline = dataArray[11];
//    set50.indexValue = [NSNumber numberWithString:dataArray[12] numberStyle:priceWithoutDivide];
//    set100.indexValue = [NSNumber numberWithString:dataArray[13] numberStyle:priceWithoutDivide];
    set.buyVolume = [NSNumber numberWithString:dataArray[14] numberStyle:volumeFormatStyle];
    set.sellVolume = [NSNumber numberWithString:dataArray[15] numberStyle:volumeFormatStyle];
//    NSLog(@"SectorGroup_No = %@", dataArray[16] );
    set.high =[NSNumber numberWithString:dataArray[17] numberStyle:priceWithoutDivide];
    set.low = [NSNumber numberWithString:dataArray[18] numberStyle:priceWithoutDivide];
    set.prior = [NSNumber numberWithString:dataArray[19] numberStyle:priceWithoutDivide];
    
    set.hasUpdate = YES;
//    NSLog(@"%@", self.markets);

    return YES;
}

-(BOOL) processProjected:(NSArray*) dataArray {
    if ([dataArray count] < 5) {
        return NO;
    }
    ITStockDetail* stock = [self getSymbolButIfNotExistInsertThis:dataArray[1]];
    stock.projectOpen =  [NSNumber numberWithString:dataArray[3] numberStyle:priceWithoutDivide];
    stock.projectVolume = [NSNumber numberWithString:dataArray[4] numberStyle:volumeFormatStyle];
    return YES;
}
-(BOOL) processAvgMessage:(NSArray*) dataArray {
    if ([dataArray count] < 4) {
        return NO;
    }
    ITStockDetail *stock  = [self getSymbolButIfNotExistInsertThis:dataArray[1]];
    stock.averagePrice = [NSNumber numberWithString:dataArray[3] numberStyle:priceWithoutDivide];
    stock.hasUpdate = YES;
    return YES;
}
- (BOOL)processTopPrice:(NSArray *)dataArray {
    if ([dataArray count] <= TOP_PRICE_LENGTH) {
//        NSLog(@"short TP array :%@",dataArray);
        return NO;
    }
    NSString* symbol = [dataArray objectAtIndex:1];
    ITStockDetail *stock  = [self getSymbolButIfNotExistInsertThis:symbol];
    [self updateStock:stock withTopPrice:dataArray];

    return YES;
}
-(void)updateStock: (ITStockDetail* ) stock withTopPrice:(NSArray *) data {

    NSString* side = [data objectAtIndex:3];
    stock.willUpdate = YES;
    stock.symbol = [data objectAtIndex:1];
    if ([side isEqualToString:@"B"]) {
        
        stock.bid1Number = [[data objectAtIndex:4] mod96price];
        stock.bid2 = [[data objectAtIndex:6] mod96price];
        stock.bid3 = [[data objectAtIndex:8] mod96price];
        stock.bid4 = [[data objectAtIndex:10] mod96price];
        stock.bid5 = [[data objectAtIndex:12] mod96price];
        
        stock.volbid1 = [[data objectAtIndex:5] mod96];
        stock.volbid2 = [[data objectAtIndex:7] mod96];
        stock.volbid3 = [[data objectAtIndex:9] mod96];
        stock.volbid4 = [[data objectAtIndex:11] mod96];
        stock.volbid5 = [[data objectAtIndex:13] mod96];
        stock.bid1 = [self formateFirstBidOfferPrice:stock.bid1Number volume:stock.volbid1 marketStatusCode:self.marketStatusCode];

        
        
        
        
    } else {
        stock.offer1Number = [[data objectAtIndex:4] mod96price];
        stock.offer1 = [stock.offer1Number stringValue];
        stock.offer2 = [[data objectAtIndex:6] mod96price];
        stock.offer3 = [[data objectAtIndex:8] mod96price];
        stock.offer4 = [[data objectAtIndex:10] mod96price];
        stock.offer5 = [[data objectAtIndex:12] mod96price];
        
        stock.voloffer1 = [[data objectAtIndex:5] mod96];
        stock.voloffer2 = [[data objectAtIndex:7] mod96];
        stock.voloffer3 = [[data objectAtIndex:9] mod96];
        stock.voloffer4 = [[data objectAtIndex:11] mod96];
        stock.voloffer5 = [[data objectAtIndex:13] mod96];
        stock.offer1 = [self formateFirstBidOfferPrice:stock.offer1Number volume:stock.voloffer1 marketStatusCode:self.marketStatusCode];
        
        
    }
    stock.hasUpdate = YES;
    
}

-(void) insertSymbolIfNotExist:(NSString*) stockName {
    ITStockDetail * stock = [self.stocks objectForKey:stockName];
    if (! stock){
        stock = [[ITStockDetail alloc] initWithName:stockName];
        [self.stocks setObject:stock forKey:stockName];
        //        NSLog(@"Create %@",stockName );
    }
}

-(ITStockDetail * ) getSymbolButIfNotExistInsertThis:(NSString*) stockName {
    ITStockDetail * stock = [self.stocks objectForKey:stockName];
    if (! stock){
        stock = [[ITStockDetail alloc] initWithName:stockName];
        [self.stocks setObject:stock forKey:stockName];
//        NSLog(@"Create %@",stockName );
    }
    return stock ;
}
-(NSArray *)stocksNameArray {
    return [[self.stocks allKeys] sortedArrayUsingSelector:@selector(localizedCompare:)];
}
- (BOOL)processLastExecute:(NSArray *)dataArray {
    if ([dataArray count] <= [ZMStockModel lastExecuteLength]) {
//        NSLog(@"short LE array :%@",dataArray);
        return NO;
    }
    NSString* symbol = dataArray[11];
    
    ITStockDetail * stock = [self  getSymbolButIfNotExistInsertThis:symbol];
    [self updateStock:stock withLastExecute:dataArray];
    self.lastTicker = stock;
    return YES;
    //    NSLog(@"%@,price:%@", symbol, bidOffer.price);
}
-(void) updateStock:(ITStockDetail*) stock withLastExecute:(NSArray *)data {
    stock.willUpdate = YES;
    stock.lastTickerTimeStamp = data[1];
    //secno = data[2]
    stock.price          = [data [3] mod96price];
    stock.accVolume      = [data [4] mod96];
    stock.accValue       = [data [5] mod96];
    stock.volume         = [data [6] mod96];
    stock.value          = [data [7] mod96];
    stock.high           = [data [8] mod96price];
    stock.low            = [data [9] mod96price];
    //boardlot  = [data[10] mod96price];
    stock.symbol         = data[11];
    
    stock.lastTickerSide = data[13];
    stock.buyVolume      = [data [14] mod96];
    stock.sellVolume     = [data [15] mod96];
    stock.unknowVolume   = [data [16] mod96];
    stock.prior          = [data [17] mod96price];
    stock.stockType      = data[18];
    stock.market_id      = data[19];
    
    stock.change         = [ITStockModel changeByPrior:stock.prior current:stock.price];
    //update percentChange after get Prior and Change
    stock.percentChange  = [ITStockModel percentChangeByChange:stock.change prior:stock.prior];
    
    [stock addNewTickerWithTime:stock.lastTickerTimeStamp side:stock.lastTickerSide price:stock.price volume:stock.volume change:stock.change];
    stock.hasUpdate = YES;
    
}

-(NSString*) formateFirstBidOfferPrice:(NSNumber*) dblPrice volume:(NSNumber*) dblVol marketStatusCode:(NSString*) sMarketStatus {
    NSString* strFormat;
    
    if([dblPrice doubleValue] == 0)
    {
        if([dblVol doubleValue] > 0)
        {
            if([sMarketStatus isEqualToString:@"P"] || [sMarketStatus isEqualToString:@"F"] ){ //pre open
                strFormat  = @"ATO";
            }
            else if([sMarketStatus isEqualToString:@"A"] || [sMarketStatus isEqualToString:@"X"] || [sMarketStatus isEqualToString:@"Y"]) //call market
            {
                strFormat = @"ATC";
            }
            else
            {
                //strFormat.Format("%.2f",dblPrice);
                strFormat = [dblPrice priceFormatStyle];
            }
        }
        else
        {
            //strFormat.Format("%.2f",dblPrice); //Suspend
            strFormat = [dblPrice priceFormatStyle];
        }
    }
    else
    {
        //strFormat.Format("%.2f",dblPrice);
        strFormat = [dblPrice priceFormatStyle];
    }
    return strFormat;
}

-(void) checkLogin:(NSString*) dataString {
    if ([dataString hasPrefix: @"300 Welcome to Stocknet"]) {
        [self sendMessage:[NSString stringWithFormat:@"user %@", self.username]];
        self.state = StateSendUser;
    } else if ([ dataString hasPrefix: @"200 OK:user" ] && self.state == StateSendUser) {
        [self sendMessage:[NSString stringWithFormat:@"pass %@", self.password]];
        self.state = StateSendPassword;
    } else if ([dataString hasPrefix: @"200 OK:pass" ] && self.state == StateSendPassword) {
        self.state = StateLogin;
        self.login = YES;
    }
}
-(void) pushSocketReadData:(NSData*) data {

    if (data) {
        
        if (self.login) {
            [self processData:data];
        } else {
            NSString* dataString = [[NSString alloc] initWithData:data
                                                         encoding:CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingASCII)];
            dataString = [dataString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

            [self checkLogin:dataString];
        }

    }
    [self.socket readDataWithTimeout:-1 tag:0];
}

@end
