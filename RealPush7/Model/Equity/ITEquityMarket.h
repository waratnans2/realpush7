//
//  ITEquityMarket.h
//  RealPush
//
//  Created by Sutean Rutjanalard on 8/15/13.
//  Copyright (c) 2013 Sutean Rutjanalard. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ITStockDetail.h"
@class ITStockDetail;
@interface ITEquityMarket : NSObject
@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* fullname;
@property (nonatomic, strong) NSMutableArray* stocks;
@property (nonatomic, strong) NSString* marketType; //Set index, Sector, Industry

+(ITEquityMarket*)marketWithName:(NSString*)name ;
-(void) addStock:(ITStockDetail *) stock;
- (id)initWithName:(NSString*) name;


/**
 * @todo must observe this 2 property that will set before update and after updated
 */
@property (nonatomic, readwrite) BOOL willUpdate;
@property (nonatomic, readwrite) BOOL hasUpdate;


//@property (nonatomic, strong) NSString* name; //1 provide by super
@property (nonatomic, strong) NSString* status; //2
@property (nonatomic, strong) NSString* advance; //3 go up stock
@property (nonatomic, strong) NSString* unchange; //4 unchange
@property (nonatomic, strong) NSString* decline; //5 go down stock
@property (nonatomic, strong) NSNumber* change; // 6
@property (nonatomic, strong) NSNumber* percentChange; // 7
@property (nonatomic, strong) NSNumber* indexValue; // 8
@property (nonatomic, strong) NSNumber* prior; // 9
@property (nonatomic, strong) NSNumber* high; // 10
@property (nonatomic, strong) NSNumber* low; // 11
@property (nonatomic, strong) NSNumber* totalVolume; // 12
@property (nonatomic, strong) NSNumber* totalValue; // 13
@property (nonatomic, strong) NSString* date; // 14
@property (nonatomic, strong) NSString* indexTime; // 15
@property (nonatomic, strong) NSNumber* noChangeVolume;
@property (nonatomic, strong) NSNumber* upVolume;
@property (nonatomic, strong) NSNumber* downVolume;
@property (nonatomic, strong) NSNumber* buyVolume;
@property (nonatomic, strong) NSNumber* sellVolume;



-(void)updateWithSiamMarketEquityMessage:(NSArray* ) completeData;

@end

@class ITSector;

@interface ITIndustry : ITEquityMarket

@property (nonatomic, strong) NSString* industryID;
-(void)addSector:(ITSector*) sector;
@property (weak, readonly) NSArray* sectors;
@end

@interface ITSector : ITEquityMarket
@property (nonatomic, weak) ITIndustry* industry;
@property (nonatomic, strong) NSString* sectorID;

@end