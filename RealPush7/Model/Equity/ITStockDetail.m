//
//  INBidOffer.m
//  inPlay
//
//  Created by Sutean Rutjanalard on 4/30/13.
//  Copyright (c) 2013 Sutean Rutjanalard. All rights reserved.
//

#import "ITStockDetail.h"
#import "NSString+mod96.h"
#import "NSNumber+formatter.h"
#import "ITStockModel.h"
@interface ITStockDetail ()
@property (nonatomic, readwrite) double percentBid1;
@property (nonatomic, readwrite) double percentBid2;
@property (nonatomic, readwrite) double percentBid3;
@property (nonatomic, readwrite) double percentBid4;
@property (nonatomic, readwrite) double percentBid5;
@property (nonatomic, readwrite) double percentOffer1;
@property (nonatomic, readwrite) double percentOffer2;
@property (nonatomic, readwrite) double percentOffer3;
@property (nonatomic, readwrite) double percentOffer4;
@property (nonatomic, readwrite) double percentOffer5;




-(void) oldCopy;


@end

@implementation ITStockDetail

#define NORMAL  [UIColor whiteColor]
#define EQUAL  [UIColor yellowColor]
#define LESS  [UIColor redColor]
#define MORE  [UIColor greenColor]

-(void)setWillUpdate:(BOOL)willUpdate {
    _willUpdate = willUpdate;
    [self oldCopy];
}
-(NSMutableArray *)tickersArray {
    if ( ! _tickersArray) {
        _tickersArray= [[NSMutableArray alloc] init];
    }
    return _tickersArray;
}
- (id)initWithName:(NSString* ) stockName
{
    self = [super init];
    if (self) {
        NSNumber* defaultNumber = [NSNumber numberWithInt:0];
        self.price = defaultNumber;
        self.bid1 = @"0";
        self.bid2 = self.bid3 = self.bid4 = self.bid5 = defaultNumber;
        self.offer1 = @"0";
        self.offer2 = self.offer3 = self.offer4 = self.offer5 = defaultNumber;
        self.voloffer1 = self.voloffer2 = self.voloffer3 = self.voloffer4 = self.voloffer5 = defaultNumber;
        self.volbid1 = self.volbid2 = self.volbid3 = self.volbid4 = self.volbid5 = defaultNumber;
        self.prior = self.change = self.percentChange = defaultNumber;
        self.symbol = stockName;
    }
    return self;
}
-(void) updatePercentBidOfferVolume {
    double bid1 = self.volbid1.doubleValue;
    double bid2 = self.volbid2.doubleValue ;
    double bid3 = self.volbid3.doubleValue ;
    double bid4 = self.volbid4.doubleValue ;
    double bid5 = self.volbid5.doubleValue ;
    double offer1 = self.voloffer1.doubleValue ;
    double offer2 = self.voloffer2.doubleValue ;
    double offer3 = self.voloffer3.doubleValue ;
    double offer4 = self.voloffer4.doubleValue ;
    double offer5 = self.voloffer5.doubleValue ;
    double sum = bid1 + bid2 + bid3 + bid4 + bid5 + offer1 + offer2 + offer3 + offer4 + offer5;
    if (sum != 0) {
        self.percentBid1 = bid1/sum;
        self.percentBid2 = bid2/sum;
        self.percentBid3 = bid3/sum;
        self.percentBid4 = bid4/sum;
        self.percentBid5 = bid5/sum;
        self.percentOffer1 = offer1 /sum;
        self.percentOffer2 = offer2 /sum;
        self.percentOffer3 = offer3 /sum;
        self.percentOffer4 = offer4 /sum;
        self.percentOffer5 = offer5 /sum;
    } else {
        self.percentBid1 =
        self.percentBid2 =
        self.percentBid3 =
        self.percentBid4 =
        self.percentBid5 =
        self.percentOffer1 =
        self.percentOffer2 =
        self.percentOffer3 =
        self.percentOffer4 =
        self.percentOffer5 = 0;
    }
   
}
/**
 LE,
 144233, time
 2027, secno
 "\"r", execprice
 "au#,", accvol
 "!u6&\\", accval
 "*H", vol
 "<T", val
 "\"v", High
 "\"p", Low
 "!", Broad
 NMG, symbol
 "$", chang
 S, side
 "Ch=$", buy volume
 "9Pd$", sell volume
 "$\\\"$", u volume
 "\"n", prior
 S, stock type
 A  market id
 */
-(void)oldCopy {
    self.volbid1Old = self.volbid1;
    self.volbid2Old = self.volbid2;
    self.volbid3Old = self.volbid3;
    self.volbid4Old = self.volbid4;
    self.volbid5Old = self.volbid5;
    
    self.voloffer1Old  = self.voloffer1;
    self.voloffer2Old  = self.voloffer2;
    self.voloffer3Old  = self.voloffer3;
    self.voloffer4Old  = self.voloffer4;
    self.voloffer5Old  = self.voloffer5;
    
    self.priceOld  = self.price;
}
-(void)setBid1WithString:(NSString *)bid1 {
    if ([bid1 hasPrefix:@"A"]) {
        self.bid1 = bid1;
        self.bid1Number = [NSNumber numberWithDouble:0];
    } else {
        self.bid1Number = [NSNumber numberWithString:bid1 numberStyle:priceWithoutDivide];
        self.bid1 = [self.bid1Number stringWithINNumberStyle:priceFormatStyle];
    }
    
}
-(void)setOffer1WithString:(NSString *)offer1 {
    if ([offer1 hasPrefix:@"A"]) {
        self.offer1 = offer1;
        self.offer1Number = [NSNumber numberWithDouble:0];
    } else {
        self.offer1Number = [NSNumber numberWithString:offer1 numberStyle:priceWithoutDivide];
        self.offer1 = [self.offer1Number stringWithINNumberStyle:priceFormatStyle];
    }
    
}

/// @param NSString 1000
/// @return NSNumber 10.00
-(NSNumber*) priceFromString:(NSString*) price {
    return [NSNumber numberWithString:price numberStyle:priceWithoutDivide];
}
/// @param NSString 10000
/// @return NSNumber 10000
-(NSNumber*) volumeFromString:(NSString*) volume {
    return [NSNumber numberWithString:volume numberStyle:volumeFormatStyle];
}
-(void) updateWithSiamStockAverage:(NSArray*) data  {
    self.averagePrice = [self priceFromString:[data objectAtIndex:2]];
}

-(void) updateWithSiamStockDetail:(NSArray*) data  {
    /// array must pre-check from Stock model
//    self.bid1 = [NSNumber numberWithString:[data objectAtIndex:14] numberStyle:priceWithoutDivide];
    self.willUpdate = YES;

    [self setBid1WithString:[data objectAtIndex:14]];
    self.bid2 = [self priceFromString:[data objectAtIndex:18]];
    self.bid3 = [self priceFromString:[data objectAtIndex:22]];
    self.bid4 = [self priceFromString:[data objectAtIndex:26]];
    self.bid5 = [self priceFromString:[data objectAtIndex:30]];
    self.volbid1 = [self volumeFromString:[data objectAtIndex:15]];
    self.volbid2 = [self volumeFromString:[data objectAtIndex:19]];
    self.volbid3 = [self volumeFromString:[data objectAtIndex:23]];
    self.volbid4 = [self volumeFromString:[data objectAtIndex:27]];
    self.volbid5 = [self volumeFromString:[data objectAtIndex:31]];
    
    [self setOffer1WithString:[data objectAtIndex:16]];
    self.offer2 = [self priceFromString:[data objectAtIndex:20]];
    self.offer3 = [self priceFromString:[data objectAtIndex:24]];
    self.offer4 = [self priceFromString:[data objectAtIndex:28]];
    self.offer5 = [self priceFromString:[data objectAtIndex:32]];
    self.voloffer1 = [self volumeFromString:[data objectAtIndex:17]];
    self.voloffer2 = [self volumeFromString:[data objectAtIndex:21]];
    self.voloffer3 = [self volumeFromString:[data objectAtIndex:25]];
    self.voloffer4 = [self volumeFromString:[data objectAtIndex:29]];
    self.voloffer5 = [self volumeFromString:[data objectAtIndex:33]];
    
    
    
    
    self.open1 = [self priceFromString:[data objectAtIndex:6]];
    self.open2 = [self priceFromString:[data objectAtIndex:7]];
    self.ceiling = [self priceFromString:[data objectAtIndex:8]];
    self.floor = [self priceFromString:[data objectAtIndex:9]];
    self.buyVolume = [self volumeFromString:[data objectAtIndex:10]];
    self.buyValue = [self priceFromString:[data objectAtIndex:11]];
    self.sellVolume = [self volumeFromString:[data objectAtIndex:12]];
    self.sellValue  = [self priceFromString:[data objectAtIndex:13]];
    self.totalVolume = [self priceFromString:[data objectAtIndex:34]];
    self.totalValue = [self priceFromString:[data objectAtIndex:35]];
//    self.unknowVolume
    self.flags = [data objectAtIndex:37];
    
     
    self.hasUpdate = YES;
    
}
-(void) updateWithSiamProjected:(NSArray*) data  {
    if ([data count] > 4) {
        self.projectOpen = [NSNumber numberWithString:[data objectAtIndex:4] numberStyle:priceWithoutDivide];
        self.projectVolume = [NSNumber numberWithString:[data objectAtIndex:3] numberStyle:volumeFormatStyle];
    }
}
-(void) updateWithSiamBidOffer:(NSArray*) data {
    if ([data count] > 17) {
        self.willUpdate =YES;
        if ([[data objectAtIndex:2] isEqualToString:@"B"]) {
            self.bid2 = [NSNumber numberWithString:[data objectAtIndex:6] numberStyle:priceWithoutDivide];
            self.bid3 = [NSNumber numberWithString:[data objectAtIndex:9] numberStyle:priceWithoutDivide];
            self.bid4 = [NSNumber numberWithString:[data objectAtIndex:12] numberStyle:priceWithoutDivide];
            self.bid5 = [NSNumber numberWithString:[data objectAtIndex:15] numberStyle:priceWithoutDivide];
            
            self.volbid1 = [NSNumber numberWithDouble:[[data objectAtIndex:4] doubleValue]];
            self.volbid2 = [NSNumber numberWithDouble:[[data objectAtIndex:7] doubleValue]];
            self.volbid3 = [NSNumber numberWithDouble:[[data objectAtIndex:10] doubleValue]];
            self.volbid4 = [NSNumber numberWithDouble:[[data objectAtIndex:13] doubleValue]];
            self.volbid5 = [NSNumber numberWithDouble:[[data objectAtIndex:16] doubleValue]];
            [self setBid1WithString:[data objectAtIndex:3]];
        } else {
            self.offer2 = [NSNumber numberWithString:[data objectAtIndex:6] numberStyle:priceWithoutDivide];
            self.offer3 = [NSNumber numberWithString:[data objectAtIndex:9] numberStyle:priceWithoutDivide];
            self.offer4 = [NSNumber numberWithString:[data objectAtIndex:12] numberStyle:priceWithoutDivide];
            self.offer5 = [NSNumber numberWithString:[data objectAtIndex:15] numberStyle:priceWithoutDivide];

            self.voloffer1 = [NSNumber numberWithDouble:[[data objectAtIndex:4] doubleValue]];
            self.voloffer2 = [NSNumber numberWithDouble:[[data objectAtIndex:7] doubleValue]];
            self.voloffer3 = [NSNumber numberWithDouble:[[data objectAtIndex:10] doubleValue]];
            self.voloffer4 = [NSNumber numberWithDouble:[[data objectAtIndex:13] doubleValue]];
            self.voloffer5 = [NSNumber numberWithDouble:[[data objectAtIndex:16] doubleValue]];
            [self setOffer1WithString:[data objectAtIndex:3]];
        }
        self.hasUpdate = YES;
    }
}
+(NSUInteger) maxTickersArraySize {
    return 10;
}
- (void)addNewTickerWithTime:(NSString *)time side:(NSString *)side price:(NSNumber *)price volume:(NSNumber *)volume change:(NSNumber *)change {
    if ([time length] != 6) {
        NSLog(@"%@ time:`%@` side:`%@`, price:`%@`, volume:`%@`, change:`%@`", self.symbol, time, side, price, volume, change);
        return;
    }
    self.willUpdate = YES;
    self.lastTickerSide = side;
    self.lastTickerTimeStamp = time;
    self.price = price;
    self.volume = volume;
    self.change = change;
    self.percentChange = [ITStockModel percentChangeByChange:self.change prior:self.prior];
    self.hasUpdate = YES;
    NSArray* tickerArray = @[time, side, price, volume, change];
    if ([self.tickersArray count] > [ITStockDetail maxTickersArraySize] -1) {
        [self.tickersArray removeObjectAtIndex:0];
        [self.tickersArray addObject:tickerArray];
    } else {
        [self.tickersArray addObject:tickerArray];
    }
    self.lastTickerArrived = YES;
}
-(void) updateWithSiamTicker:(NSArray*) data {
    unsigned long count = [data count];
    if (count >= 16) {
        self.willUpdate = YES;
        NSString* time = [data objectAtIndex:1];
        self.lastTickerTimeStamp = time;
        self.symbol = [data objectAtIndex:2];
        self.price = [NSNumber numberWithString:[data objectAtIndex:4] numberStyle:priceWithoutDivide];
        self.volume = [NSNumber numberWithDouble:[[data objectAtIndex:3] doubleValue]];
        self.prior = [NSNumber numberWithString:[data objectAtIndex:5] numberStyle:priceWithoutDivide];
        self.lastTickerSide = [data objectAtIndex:6];
        self.change = [NSNumber numberWithString:[data objectAtIndex:7] numberStyle:priceWithoutDivide];
        //self.value = [NSNumber numberWithString:[data objectAtIndex:9] numberStyle:priceFormatStyle];
        self.high = [NSNumber numberWithString:[data objectAtIndex:11] numberStyle:priceWithoutDivide];
        self.low = [NSNumber numberWithString:[data objectAtIndex:12] numberStyle:priceWithoutDivide];
        //NSLog(@"%@ share = %@", self.symbol, [data objectAtIndex:8]);
        self.percentChange = [NSNumber numberWithDouble:100 *([self.change doubleValue]/[self.prior doubleValue]) ];
       
        self.totalVolume = [NSNumber numberWithDouble:[[data objectAtIndex:13] doubleValue]];
        
        self.totalValue = [NSNumber numberWithDouble:[[data objectAtIndex:14] doubleValue] ];
        
        if (count > 16) {
            self.averagePrice = [NSNumber numberWithString:[data objectAtIndex:16] numberStyle:priceWithoutDivide];
        }
       

        
        self.share =   [NSNumber numberWithDouble:[[data objectAtIndex:8] doubleValue]];
        self.amount  = [NSNumber numberWithDouble:[[data objectAtIndex:9] doubleValue]];
        
        self.hasUpdate = YES;
        
    } else {
        NSLog(@"short Ticker :%@", data);
    }
}
-(void) updateWithSiamMostDict:(NSDictionary *)data {
    self.offer1 = [data objectForKey:@"Offer"];
    self.voloffer1 = [NSNumber numberWithString:[data objectForKey:@"Vol_Offer"] numberStyle:volumeFormatStyle];
    self.bid1 = [data objectForKey:@"Bid"];
    self.volbid1 = [NSNumber numberWithString:[data objectForKey:@"Vol_Offer"] numberStyle:volumeFormatStyle];
    self.price  = [NSNumber numberWithString:[data objectForKey:@"Last"] numberStyle:priceFormatStyle];
    self.change = [NSNumber numberWithString:[data objectForKey:@"Change"] numberStyle:priceFormatStyle];
    if ([data objectForKey:@"PercentChg"]) {
        self.percentChange = [NSNumber numberWithString:[data objectForKey:@"PercentChg"] numberStyle:priceFormatStyle];
    } else if ([data objectForKey:@"Percent_Change"]) {
        self.percentChange = [NSNumber numberWithString:[data objectForKey:@"Percent_Change"] numberStyle:priceFormatStyle];
    }
    
    self.symbol = [data objectForKey:@"Stock"];
}
#pragma mark - ZMICO feed
-(NSString*) atxOrPriceByPrice:(NSString*) priceStr volume:(NSNumber*) volumeStr {
    NSNumber* price = [priceStr  mod96price];
    if ([self.volbid1 doubleValue] > 0 && [price doubleValue] == 0) {
        return @"ATX";
    } else {
        return [price stringWithINNumberStyle:priceFormatStyle];
        
    }
}

-(void) updateWithMostDict:(NSDictionary *)data {
    self.offer1 = [data objectForKey:@"Offer"];
    self.voloffer1 = [NSNumber numberWithString:[data objectForKey:@"Vol_Offer"] numberStyle:volumeFormatStyle];
    self.bid1 = [data objectForKey:@"Bid"];
    self.volbid1 = [NSNumber numberWithString:[data objectForKey:@"Vol_Offer"] numberStyle:volumeFormatStyle];
    self.price  = [NSNumber numberWithString:[data objectForKey:@"Last"] numberStyle:priceFormatStyle];
    self.change = [NSNumber numberWithString:[data objectForKey:@"Change"] numberStyle:priceFormatStyle];
    if ([data objectForKey:@"PercentChg"]) {
        self.percentChange = [NSNumber numberWithString:[data objectForKey:@"PercentChg"] numberStyle:priceFormatStyle];
    } else if ([data objectForKey:@"Percent_Change"]) {
        self.percentChange = [NSNumber numberWithString:[data objectForKey:@"Percent_Change"] numberStyle:priceFormatStyle];
    }
    
    self.symbol = [data objectForKey:@"Stock"];
}


-(NSString *)description {
    NSMutableString *resultString = [NSMutableString stringWithFormat:@"symbol: %@ price: %@ prior: %@", self.symbol, self.price, self.prior];
    [resultString appendString:@"bid/offer"];
    [resultString appendFormat:@"%@ %@ %@ %@", self.volbid1 , self.bid1, self.offer1, self.voloffer1];
    
    return resultString;
}

@end
