//
//  ZMStockModel.h
//  RealPush7
//
//  Created by Sutean Rutjanalard on 26/3/2557 BE.
//  Copyright (c) 2557 Sutean Rutjanalard. All rights reserved.
//

#import "ITStockModel.h"

@interface ZMStockModel : ITStockModel
//- (instancetype) initWithHost:(NSString*) string port:(NSInteger)port;
- (instancetype) initWithHost:(NSString*) host
                         port:(NSInteger)port
       marketConfigurationURL:(NSString*) url;

- (void) startConnectWithUsername:(NSString*) username password:(NSString*) password ;

+(int) lastExecuteLength ;
-(void) pullMarket ;
-(void) pullStock:(NSString*) stockName ;
@end
