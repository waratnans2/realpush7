//
//  INBidOffer.h
//  inPlay
//
//  Created by Sutean Rutjanalard on 4/30/13.
//  Copyright (c) 2013 Sutean Rutjanalard. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ITEquityMarket.h"
@class ITIndustry;
@class ITSector;
@class ITEquityMarket;
@interface ITStockDetail : NSObject
//suversion

@property (nonatomic, strong) NSString* symbol;
@property (nonatomic, strong) NSString* fullname;
@property (nonatomic, weak) ITIndustry* industry;
@property (nonatomic, weak) ITSector* sector;
@property (nonatomic, weak) ITEquityMarket* market;
@property (nonatomic, strong) NSString* stockType;

//@property (nonatomic, weak) Instrument* instrument;
@property (nonatomic, strong) NSString* lastday;

//realtime
@property (nonatomic, strong) NSString* bid1;
@property (nonatomic, strong) NSNumber* bid1Number;

@property (nonatomic, strong) NSNumber* bid2;
@property (nonatomic, strong) NSNumber* bid3;
@property (nonatomic, strong) NSNumber* bid4;
@property (nonatomic, strong) NSNumber* bid5;

@property (nonatomic, strong) NSString* offer1;
@property (nonatomic, strong) NSNumber* offer1Number;

@property (nonatomic, strong) NSNumber* offer2;
@property (nonatomic, strong) NSNumber* offer3;
@property (nonatomic, strong) NSNumber* offer4;
@property (nonatomic, strong) NSNumber* offer5;

@property (nonatomic, strong) NSNumber* voloffer1;
@property (nonatomic, strong) NSNumber* voloffer2;
@property (nonatomic, strong) NSNumber* voloffer3;
@property (nonatomic, strong) NSNumber* voloffer4;
@property (nonatomic, strong) NSNumber* voloffer5;
@property (nonatomic, strong) NSNumber* volbid1;
@property (nonatomic, strong) NSNumber* volbid2;
@property (nonatomic, strong) NSNumber* volbid3;
@property (nonatomic, strong) NSNumber* volbid4;
@property (nonatomic, strong) NSNumber* volbid5;
//call updatePercentBidOfferVolume before use all of this properties
-(double) percentBid1;
-(double) percentBid2;
-(double) percentBid3;
-(double) percentBid4;
-(double) percentBid5;
-(double) percentOffer1;
-(double) percentOffer2;
-(double) percentOffer3;
-(double) percentOffer4;
-(double) percentOffer5;
-(void) updatePercentBidOfferVolume;



@property (nonatomic, strong) NSNumber* voloffer1Old;
@property (nonatomic, strong) NSNumber* voloffer2Old;
@property (nonatomic, strong) NSNumber* voloffer3Old;
@property (nonatomic, strong) NSNumber* voloffer4Old;
@property (nonatomic, strong) NSNumber* voloffer5Old;

@property (nonatomic, strong) NSNumber* volbid1Old;
@property (nonatomic, strong) NSNumber* volbid2Old;
@property (nonatomic, strong) NSNumber* volbid3Old;
@property (nonatomic, strong) NSNumber* volbid4Old;
@property (nonatomic, strong) NSNumber* volbid5Old;

@property (nonatomic, strong) NSNumber* price;
@property (nonatomic, strong) NSNumber* priceOld;

@property (nonatomic, strong) NSNumber* averagePrice;

@property (nonatomic, strong) NSString* lastTickerSide;

@property (nonatomic, strong) NSNumber* projectOpen;
@property (nonatomic, strong) NSNumber* projectVolume;


@property (nonatomic, strong) NSNumber* change;
@property (nonatomic, strong) NSNumber* volume;
@property (nonatomic, strong) NSNumber* value;
@property (nonatomic, strong) NSNumber* accVolume;
@property (nonatomic, strong) NSNumber* accValue;

@property (nonatomic, strong) NSNumber* high;
@property (nonatomic, strong) NSNumber* low;
@property (nonatomic, strong) NSNumber* prior;

@property (nonatomic, strong) NSNumber* buyVolume;
@property (nonatomic, strong) NSNumber* buyValue;
@property (nonatomic, strong) NSNumber* sellVolume;
@property (nonatomic, strong) NSNumber* sellValue;
@property (nonatomic, strong) NSNumber* unknowVolume;

@property (nonatomic, strong) NSNumber* open1;
@property (nonatomic, strong) NSNumber* open2;
@property (nonatomic, strong) NSNumber* ceiling;
@property (nonatomic, strong) NSNumber* floor;


@property (nonatomic, strong) NSNumber* percentChange;
@property (nonatomic, strong) NSString* lastTickerTimeStamp;
@property (nonatomic, strong) NSNumber* share;
@property (nonatomic, strong) NSNumber* amount;
@property (nonatomic, strong) NSNumber* totalValue;
@property (nonatomic, strong) NSNumber* totalVolume;
@property (nonatomic, strong) NSString* flags;
@property (nonatomic, strong) NSString* market_id;
@property (nonatomic, strong) NSString* pe;
@property (nonatomic, strong) NSString* pbv;
@property (nonatomic, strong) NSString* yield;
@property (nonatomic, strong) NSString* eps;
@property (nonatomic, strong) NSString* marketCap;

//[ [time, side, price, volume,change],[time, side, price, volume,change],...  ]
@property (nonatomic, strong) NSMutableArray* tickersArray;
-(void) addNewTickerWithTime:(NSString*) time
                        side:(NSString*) side
                       price:(NSNumber*) price
                      volume:(NSNumber*) volume
                      change:(NSNumber*) change;
+(NSUInteger) maxTickersArraySize;
@property (nonatomic,readwrite) BOOL lastTickerArrived;









@property (nonatomic, readwrite) BOOL hasUpdate; // after update
@property (nonatomic, readwrite) BOOL willUpdate; //before update
//@[ @[@"time", @"price", @"vol"] , @[...] , ... ]
@property (nonatomic, strong) NSArray* chartTimeSeries;


-(void) updateWithSiamTicker:(NSArray*) data;
-(void) updateWithMostDict:(NSDictionary*) data;
-(void) updateWithSiamMostDict:(NSDictionary *)data;
-(void) updateWithSiamBidOffer:(NSArray*) data ;
-(void) updateWithSiamProjected:(NSArray*) data ;
-(void) updateWithSiamStockDetail:(NSArray*) data  ;
-(void) updateWithSiamStockAverage:(NSArray*) data  ;


-(id) initWithName:(NSString* ) stockName;

@end
