//
//  INStock.h
//  TestZnet
//
//  Created by Sutean Rutjanalard on 5/3/13.
//  Copyright (c) 2013 Sutean Rutjanalard. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AsyncSocket.h"
#import "ITStockDetail.h"
#import "ITEquityMarket.h"


@interface ITStockModel : NSObject
typedef enum  {
    MostValue =0,
    MostVolume =1,
    MostSwing=2,
    MostGainer=3,
    MostLose=4
}MostType;
typedef enum {
   StateInitial, StateConnecting,StateConnected, StateSendUser, StateSendPassword , StateLogin, StateLogingOut, StateLogout
} ConnectionState;

#pragma mark - su version api
//-(void)loadDataFromHttpUrl:(NSString *)url by:(id) completor complete:(SEL)selector;
-(NSArray*) filterSymbolByName:(NSString *) searchText ;
//-(NSArray*) filterSectorByName:(NSString *) searchText ;
//-(NSArray*) filterIndustryByName:(NSString *) searchText ;



#pragma mark - realtime api
//-(void) processData:(NSString*) dataString ;
//-(void) updateWithMostArray:(NSArray*) mostArray;
//-(void)processSiamData:(NSString*)dataString ;

-(ITStockDetail*) stockByName:(NSString*) stockName;
//Dictionary to INBidOffer key is SYMBOL
@property (nonatomic, strong) NSMutableDictionary* stocks;
@property (nonatomic, strong) NSArray* mostList;
@property (readwrite) MostType mostType;

@property (nonatomic, weak) ITStockDetail * lastTicker;

@property (nonatomic, strong) NSArray* stocksNameArray;
@property (weak, nonatomic, readonly) NSArray* marketNameArray;

/// key is name
@property (nonatomic, strong) NSMutableDictionary* markets;
/// key is industry name
@property (nonatomic, strong) NSMutableDictionary* industries;
/// key is name
@property (nonatomic, strong) NSMutableDictionary* sectors;
@property (nonatomic, strong) NSString* marketStatusCode;

@property (nonatomic, strong) NSString* marketStatus;



-(ITEquityMarket*) marketOrIndustryOrSectorByName:(NSString*) marketName;
+(NSNumber*) changeByPrior:(NSNumber*) prior current:(NSNumber*) current;
+(NSNumber*) percentChangeByChange:(NSNumber*) change prior:(NSNumber*) prior ;

#pragma mark - zmico
- (instancetype) initWithHost:(NSString*) string port:(NSInteger)port;
- (void) startConnectWithUsername:(NSString*) username password:(NSString*) password ;
-(void) pullMarket ;
-(void) pullStock:(NSString*) stockName ;
-(void) pullMost:(MostType) mostType;
-(void) pullChartForStock:(NSString*) stock ;


@end
