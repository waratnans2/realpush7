//
//  ITPull.h
//  RealPush
//
//  Created by Sutean Rutjanalard on 12/9/2556 BE.
//  Copyright (c) 2556 Sutean Rutjanalard. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol ITPullResponse <NSObject>

-(void) pullResponseString:(NSString*) string;

@end

@interface ITPull : NSObject


/**
 * Use to append to the end of the message request.
 * If not login, yet. It will be nil.
 */
@property (nonatomic, strong) NSString* siamLoginToken;

/**
 * @param delegate must be ITPullRequest delegate
 */
- (id)initWithHost:(NSString*) host port:(int) port username:(NSString*) username delegate:(id<ITPullResponse>) delegate;

/**
 * Request message to server.
 * If server is not connected, It will try to connect by itself.
 */
-(void) requestMessage:(NSString*) requestMessage;


/**
 * disconnect all socket inside
 */
-(void)disconnect;
@end
