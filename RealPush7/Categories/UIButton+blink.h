//
//  UIButton+blink.h
//  RealPush
//
//  Created by Sutean Rutjanalard on 22/9/2556 BE.
//  Copyright (c) 2556 Sutean Rutjanalard. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ITUITheme.h"

@interface UIButton (blink)
- (void) setTextAndBlink:(NSString *)aString ;
- (void) setTextWithKeepingAttributes:(NSString *)text;

- (void) setColorWithPrior:(NSNumber*) prior currentValue:(NSNumber*) currentValue ; 
//set price and blink
-(void) setColorWithPrior:(NSNumber*) prior currentValue:(NSNumber*) currentValue withTheme:(ITUITheme*) theme blink:(BOOL) blink;


@end
