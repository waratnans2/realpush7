//
//  NSData+utf8String.m
//  RealPush7
//
//  Created by Sutean Rutjanalard on 25/2/2557 BE.
//  Copyright (c) 2557 Sutean Rutjanalard. All rights reserved.
//

#import "NSData+utf8String.h"

@implementation NSData (utf8String)
-(NSString *)utf8String {
    return [[[NSString alloc] initWithData:self encoding:NSUTF8StringEncoding]
     stringByTrimmingCharactersInSet:
     [NSCharacterSet whitespaceAndNewlineCharacterSet]];
}
@end
