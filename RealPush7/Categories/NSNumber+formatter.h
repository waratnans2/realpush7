//
//  NSNumber+formatter.h
//  TestZmico
//
//  Created by Sutean Rutjanalard on 5/3/13.
//  Copyright (c) 2013 Sutean Rutjanalard. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNumber (formatter)
enum INNumberFormatterStyle {	// number format styles
	priceFormatStyle = 0,
	volumeFormatStyle = 1,
    changeFormatStyle = 2,
    largeVolumeFormatStyle = 3,
    priceWithoutDivide = 99
};
//fast formatter
-(NSString*) priceFormatStyle;
-(NSString*) changeFormatStyle;
-(NSString*) volumeFormatStyle;
-(NSString*) largeVolumeFormatStyle;
- (NSString *)stringWithINNumberStyle:(enum INNumberFormatterStyle)style;

+ (NSNumber *)numberWithString:(NSString *)string
                   numberStyle:(enum INNumberFormatterStyle)style;
//slow formatter
-(NSString*) portAvgFormat;

+(BOOL) validateVolumeTextField:(UITextField*) textField
          charactersChangeRange:(NSRange) range
              replacementString:(NSString*) string ;
+(BOOL) validatePriceTextField:(UITextField*) textField
         charactersChangeRange:(NSRange) range
             replacementString:(NSString*) string ;

@end
