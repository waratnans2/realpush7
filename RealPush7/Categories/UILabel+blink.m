//
//  UILabel+blink.m
//  RealPush
//
//  Created by Sutean Rutjanalard on 20/9/2556 BE.
//  Copyright (c) 2556 Sutean Rutjanalard. All rights reserved.
//

#import "UILabel+blink.h"
#import "NSNumber+formatter.h"


@implementation UILabel (blink)
-(void) blinkBack {
    self.backgroundColor = [UIColor clearColor];
    //    [self setDrawsBackground:YES];
    
}
- (void) setVolumeText:(NSNumber*) volume oldVolume:(NSNumber*) oldVolume theme:(ITUITheme*) theme blink:(BOOL)blink {
    if (volume.doubleValue > oldVolume.doubleValue) {
        //green blink
        [self setTextWithKeepingAttributes: volume.volumeFormatStyle];
        if (blink) { self.backgroundColor = theme.greenColor; }
    } else if (volume.doubleValue < oldVolume.doubleValue) {
        //red blink
        [self setTextWithKeepingAttributes:volume.volumeFormatStyle];
        if (blink) { self.backgroundColor = theme.redColor; }
    } else { // old == new
        self.text  = volume.volumeFormatStyle;
    }
    if (blink) { [self performSelector:@selector(blinkBack) withObject:nil afterDelay:0.3]; }
}
-(void)setTextAndBlink:(NSString *)aString theme:(ITUITheme *)theme{
    if (aString == nil) {
        return;
    }
    if ([self.text isEqualToString:aString]) {
        return;
    }
    if ([self.text length] ==0) { // first set ignore change color
        [self setText:aString];
        return;
    }
    [self setTextWithKeepingAttributes:aString];
    
    self.backgroundColor = theme.cyanColor;
    
    [self performSelector:@selector(blinkBack) withObject:nil afterDelay:0.3];
}

-(void)setTextAndBlink:(NSString *)aString {
    if (aString == nil) {
        return;
    }
    if ([self.text isEqualToString:aString]) {
        return;
    }
    if ([self.text length] ==0) { // first set ignore change color
        [self setText:aString];
        return;
    }
    [self setTextWithKeepingAttributes:aString];
    
    self.backgroundColor = [UIColor magentaColor];
        
    [self performSelector:@selector(blinkBack) withObject:nil afterDelay:0.3];
}

- (void)setTextWithKeepingAttributes:(NSString *)text{
//    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
//        self.text = text;
//    } else {
    NSDictionary *attributes = nil;
    if ([self.attributedText length] > 0) {
        @try {
            attributes = [(NSAttributedString *)self.attributedText attributesAtIndex:0 effectiveRange:NULL];
        }
        @catch (NSException *exception) {
            NSLog(@"can not get attribute on textlable %@", text);
        }
    }
    
    if ([text length] == 0) {
        self.attributedText = [[NSAttributedString alloc] initWithString:@" " attributes:attributes];
        
    } else {
        self.attributedText = [[NSAttributedString alloc] initWithString:text attributes:attributes];
    }
    //    }
    
}
-(void) setColorWithPrior:(NSNumber*) prior currentValue:(NSNumber*) currentValue withTheme:(ITUITheme*) theme blink:(BOOL)blink {
    if ( ! currentValue) {
        return;
    }
    double base = [prior doubleValue];
    double current = [currentValue doubleValue];
    if (base == 0) {
        self.textColor = theme.whiteColor;
    } else if (base == current) {
        self.textColor = theme.yellowColor;
    } else if (base > current) {
        self.textColor = theme.redColor;
    } else {
        self.textColor = theme.greenColor;
    }
    if (blink) {
        if ( ! [self.text isEqualToString:currentValue.priceFormatStyle]) {
            self.backgroundColor = theme.cyanColor;
            [self performSelector:@selector(blinkBack) withObject:nil afterDelay:0.3];
        }
    }
    [self setTextWithKeepingAttributes:currentValue.priceFormatStyle];

}
-(void) setColorWithPrior:(NSNumber*) prior currentValue:(NSNumber*) currentValue {
    double base = [prior doubleValue];
    double current = [currentValue doubleValue];
    if (base == 0) {
        self.textColor = [UIColor whiteColor];
    } else if (base == current) {
        self.textColor = [UIColor yellowColor];
    } else if (base > current) {
        self.textColor = [UIColor redColor];
    } else {
        self.textColor = [UIColor greenColor];
    }
}

@end
