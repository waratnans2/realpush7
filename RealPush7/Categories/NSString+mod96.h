//
//  NSString+mod96.h
//  TestZmico
//
//  Created by Sutean Rutjanalard on 5/2/13.
//  Copyright (c) 2013 Sutean Rutjanalard. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (mod96)
-(NSNumber*) mod96;
-(NSNumber*) mod96price;
@end
