//
//  NSString+mod96.m
//  TestZmico
//
//  Created by Sutean Rutjanalard on 5/2/13.
//  Copyright (c) 2013 Sutean Rutjanalard. All rights reserved.
//

#import "NSString+mod96.h"


@implementation NSString (mod96)
-(double)privateMod96 {
    double value = 0;
    NSUInteger lengthBuffer = [self length];
    for (int i=0; i< lengthBuffer; i++) {
        int asciiCode = [self characterAtIndex:i]-32;
        value += asciiCode * pow(96, lengthBuffer-(i+1));
    }
    return  value;

}


- (NSNumber*)mod96 {
    
    return [NSNumber numberWithDouble:[self privateMod96]];
    
}
- (NSNumber*)mod96price {
    
    
    return [NSNumber numberWithDouble:[self privateMod96] / 100];
    
}

@end
