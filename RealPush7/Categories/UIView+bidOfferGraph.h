//
//  UIView+bargraph.h
//  RealPush
//
//  Created by Sutean Rutjanalard on 8/10/2556 BE.
//  Copyright (c) 2556 Sutean Rutjanalard. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ITUITheme.h"
@interface UIView (bidOfferGraph)

-(void) setBidPercent:(double) percent theme:(ITUITheme*) theme;
-(void) setOfferPercent:(double) percent theme:(ITUITheme*) theme;


@end
