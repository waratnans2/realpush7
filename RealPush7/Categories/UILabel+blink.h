//
//  UILabel+blink.h
//  RealPush
//
//  Created by Sutean Rutjanalard on 20/9/2556 BE.
//  Copyright (c) 2556 Sutean Rutjanalard. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ITUITheme.h"

@interface UILabel (blink)
- (void) setTextAndBlink:(NSString *)aString ;
- (void) setTextAndBlink:(NSString *)aString theme:(ITUITheme*) theme;
- (void) setTextWithKeepingAttributes:(NSString *)text;
- (void) setColorWithPrior:(NSNumber*) prior currentValue:(NSNumber*) currentValue ;

//theme
-(void) setColorWithPrior:(NSNumber*) prior currentValue:(NSNumber*) currentValue withTheme:(ITUITheme*) theme blink:(BOOL) blink;
- (void) setVolumeText:(NSNumber*) volume oldVolume:(NSNumber*) oldVolume theme:(ITUITheme*) theme blink:(BOOL) blink;

@end
