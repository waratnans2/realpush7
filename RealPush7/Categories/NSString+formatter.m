//
//  NSString+formatter.m
//  RealPush7
//
//  Created by Sutean Rutjanalard on 8/4/2557 BE.
//  Copyright (c) 2557 Sutean Rutjanalard. All rights reserved.
//

#import "NSString+formatter.h"

@implementation NSString (formatter)
-(NSString *)timeFormatStyle {
    return [NSString stringWithFormat:@"%@:%@:%@", [self substringToIndex:2] , [self substringWithRange:NSMakeRange(2, 2)] , [self substringFromIndex:4]];
}
@end
