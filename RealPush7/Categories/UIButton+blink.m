//
//  UIButton+blink.m
//  RealPush
//
//  Created by Sutean Rutjanalard on 22/9/2556 BE.
//  Copyright (c) 2556 Sutean Rutjanalard. All rights reserved.
//

#import "UIButton+blink.h"
#import "NSNumber+formatter.h"
@implementation UIButton (blink)
-(void) blinkBack {
    self.backgroundColor = [UIColor clearColor];
    //    [self setDrawsBackground:YES];
    
}

-(void) blinkBackWithTheme:(ITUITheme*) theme {
    self.backgroundColor = theme.defaultButtonColor;
    //    [self setDrawsBackground:YES];
    
}

-(void)setTextAndBlink:(NSString *)aString {
    if (aString == nil) {
        return;
    }
    if ([self.titleLabel.text isEqualToString:aString]) {
        return;
    }
    if ([self.titleLabel.text length] ==0) { // first set ignore change color
        [self setTitle:aString forState:UIControlStateNormal];
        return;
    }
    [self setTextWithKeepingAttributes:aString];
    
    self.backgroundColor = [UIColor magentaColor];
    
    [self performSelector:@selector(blinkBack) withObject:nil afterDelay:0.3];
}

- (void)setTextWithKeepingAttributes:(NSString *)text{
    [self setTitle:text forState:UIControlStateNormal];
    
}

-(void) setColorWithPrior:(NSNumber*) prior currentValue:(NSNumber*) currentValue {
    double base = [prior doubleValue];
    double current = [currentValue doubleValue];
    if (base == 0) {
        [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    } else if (base == current) {
        [self setTitleColor:[UIColor yellowColor] forState:UIControlStateNormal];
    } else if (base > current) {
        [self setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    } else {
        [self setTitleColor:[UIColor greenColor] forState:UIControlStateNormal];
    }
}


-(void) setColorWithPrior:(NSNumber*) prior currentValue:(NSNumber*) currentValue withTheme:(ITUITheme*) theme blink:(BOOL)blink{
    
    
    if (blink) {
        if ( ! [self.titleLabel.text isEqualToString:currentValue.priceFormatStyle]) {
            self.backgroundColor = theme. magentaColor;
            [self performSelector:@selector(blinkBackWithTheme:) withObject:theme afterDelay:0.3];
        }
    }
    [self setTitle:currentValue.priceFormatStyle forState:UIControlStateNormal];
    
    double base = [prior doubleValue];
    double current = [currentValue doubleValue];
    if (base == 0) {
        [self setTitleColor:theme.whiteColor forState:UIControlStateNormal];
    } else if (base == current) {
        [self setTitleColor:theme.yellowColor forState:UIControlStateNormal];
    } else if (base > current) {
        [self setTitleColor:theme.redColor forState:UIControlStateNormal];
    } else {
        [self setTitleColor:theme.greenColor forState:UIControlStateNormal];
    }
    
}

@end
