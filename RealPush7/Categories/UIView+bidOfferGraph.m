//
//  UIView+bargraph.m
//  RealPush
//
//  Created by Sutean Rutjanalard on 8/10/2556 BE.
//  Copyright (c) 2556 Sutean Rutjanalard. All rights reserved.
//

#import "UIView+bidOfferGraph.h"

@implementation UIView (bidOfferGraph)
static double SCALE = 1.3;
-(void)setBidPercent:(double)percent theme:(ITUITheme *)theme{
    
    CGRect frame = self.frame;
    frame.size.width = (theme.graphWidth*SCALE)*percent;
    frame.origin.x   = (theme.offerStartX-(SCALE*theme.graphWidth - theme.graphWidth)) + ((theme.graphWidth*SCALE) - frame.size.width);
    self.frame = frame;
}
-(void)setOfferPercent:(double)percent theme:(ITUITheme *)theme{
    
    CGRect frame = self.frame;
    frame.size.width = (theme.graphWidth*SCALE)*percent;
    self.frame = frame;
}
@end
