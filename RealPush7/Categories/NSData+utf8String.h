//
//  NSData+utf8String.h
//  RealPush7
//
//  Created by Sutean Rutjanalard on 25/2/2557 BE.
//  Copyright (c) 2557 Sutean Rutjanalard. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (utf8String)
-(NSString*) utf8String;

@end
