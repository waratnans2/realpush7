//
//  NSString+formatter.h
//  RealPush7
//
//  Created by Sutean Rutjanalard on 8/4/2557 BE.
//  Copyright (c) 2557 Sutean Rutjanalard. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (formatter)
-(NSString*) timeFormatStyle;
@end
