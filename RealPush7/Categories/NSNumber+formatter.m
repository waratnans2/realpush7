//
//  NSNumber+formatter.m
//  TestZmico
//
//  Created by Sutean Rutjanalard on 5/3/13.
//  Copyright (c) 2013 Sutean Rutjanalard. All rights reserved.
//

#import "NSNumber+formatter.h"

@implementation NSNumber (formatter)
NSNumberFormatter *sharedNumberFormatter = nil;
NSNumberFormatter *sharedNumberFormatterPriceStyle = nil;
NSNumberFormatter *sharedNumberFormatterVolumeStyle = nil;
NSNumberFormatter *sharedNumberFormatterChangeStyle = nil;

static NSString *kSharedNumberFormatterPriceStyleLock = @"kSharedNumberFormatterPriceStyleLock";
static NSString *kSharedNumberFormatterVolumeStyleLock = @"kSharedNumberFormatterVolumeStyleLock";
static NSString *kSharedNumberFormatterChangeStyleLock = @"kSharedNumberFormatterVolumeStyleLock";
+(BOOL) validatePriceTextField:(UITextField*) textField charactersChangeRange:(NSRange) range replacementString:(NSString*) string {
    if ([string length] ==0) {//delete
        return YES;
    }
    NSSet* validString =[[NSSet alloc] initWithArray:@[@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9"]];
    NSRange rangeOfDot =[textField.text rangeOfString:@"."];
    if ( rangeOfDot.location== NSNotFound) { //before dot
        if ([string isEqualToString:@"."] || [validString containsObject:string]) {
            
            return YES;
        }
    } else if (  range.location < rangeOfDot.location) { // change before dot 123|4.00
        if ([validString containsObject:string]) {
            return YES;
        }
    } else { // after dot
        NSString* subString = [textField.text substringFromIndex:[textField.text rangeOfString:@"."].location];
        if ([subString length] == 3) {//.12
            return NO;
        } else if ([validString containsObject:string]) {
            return YES;
        }
    }
    return NO;
}
+(BOOL) validateVolumeTextField:(UITextField*) textField charactersChangeRange:(NSRange) range replacementString:(NSString*) string {
    if ([string length] ==0) {//delete
        return YES;
    }
    NSSet* validString =[[NSSet alloc] initWithArray:@[@"00",@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9"]];
    if([validString containsObject:string]) {
            return YES;
    }
    return NO;
}

-(NSString*) portAvgFormat {
    NSNumberFormatter* formatter  = [[NSNumberFormatter alloc] init];
    formatter.numberStyle = NSNumberFormatterDecimalStyle;
    formatter.maximumFractionDigits = 4;
    return [formatter stringFromNumber:self];
}
-(NSString *)priceFormatStyle {
    return [self stringWithINNumberStyle:priceFormatStyle];
}
-(NSString*) volumeFormatStyle {
    return [self stringWithINNumberStyle:volumeFormatStyle];
}
-(NSString *)changeFormatStyle {
    return [self stringWithINNumberStyle:changeFormatStyle];
}
-(NSString*) largeVolumeFormatStyle {
    return [self stringWithINNumberStyle:largeVolumeFormatStyle];
}
//-(NSNumberFormatter*) createPriceFormat {
//    NSNumberFormatter* formatter  = [[NSNumberFormatter alloc] init];
//    [formatter setMinimumFractionDigits:2];
//    return formatter;
//}

NSNumberFormatter* createPriceFormat() {
    NSNumberFormatter* formatter  = [[NSNumberFormatter alloc] init];
    [formatter setPositiveFormat:@"#,##0.00"];
    return formatter;
}
NSNumberFormatter* createChangeNumberFormatter() {
    NSNumberFormatter* formatter = [[NSNumberFormatter alloc] init];
    formatter.numberStyle = NSNumberFormatterDecimalStyle;
    formatter.maximumFractionDigits = 2;
    formatter.minimumFractionDigits = 2;
    formatter.positivePrefix = formatter.plusSign;
    formatter.negativePrefix = formatter.minusSign;
    return formatter;
}

- (NSString *)stringWithINNumberStyle:(enum INNumberFormatterStyle)style
{
    if ( ! self) {
        return @"0";
    }
    NSNumberFormatter *formatter = nil;
    switch (style) {
        case priceFormatStyle:
            if (sharedNumberFormatterPriceStyle) {
                formatter = sharedNumberFormatterPriceStyle;
                break;
            }
            @synchronized(kSharedNumberFormatterPriceStyleLock) {
                if (sharedNumberFormatterPriceStyle == nil) {
                    sharedNumberFormatterPriceStyle = createPriceFormat();
                }
            }
            formatter = sharedNumberFormatterPriceStyle;
            break;
        case changeFormatStyle:
            if (self.doubleValue == 0) {
                return @"0.00";
            }
            if (sharedNumberFormatterChangeStyle) {
                formatter = sharedNumberFormatterChangeStyle;
                break;
            }
            @synchronized(kSharedNumberFormatterChangeStyleLock) {
                if (sharedNumberFormatterChangeStyle == nil) {
                    sharedNumberFormatterChangeStyle =  createChangeNumberFormatter();
                    
                }
            }
            formatter = sharedNumberFormatterChangeStyle;
            break;
        case volumeFormatStyle:
            if (sharedNumberFormatterVolumeStyle) {
                formatter = sharedNumberFormatterVolumeStyle;
                break;
            }
            @synchronized(kSharedNumberFormatterVolumeStyleLock) {
                if (sharedNumberFormatterVolumeStyle == nil) {
                    sharedNumberFormatterVolumeStyle = [[NSNumberFormatter alloc] init];
                    sharedNumberFormatterVolumeStyle.numberStyle = NSNumberFormatterDecimalStyle;
                }
            }
            formatter = sharedNumberFormatterVolumeStyle;
            break;
        case priceWithoutDivide:// special case that need divide before formating
            if (sharedNumberFormatterPriceStyle) {
                formatter = sharedNumberFormatterPriceStyle;
                return [formatter stringFromNumber:[NSNumber numberWithDouble:[self doubleValue]/100] ];
            }
            @synchronized(kSharedNumberFormatterPriceStyleLock) {
                if (sharedNumberFormatterPriceStyle == nil) {
                    sharedNumberFormatterPriceStyle = createPriceFormat();
                }
            }
            formatter = sharedNumberFormatterPriceStyle;
            
            return [formatter stringFromNumber:[NSNumber numberWithDouble:[self doubleValue]/100] ];
        case largeVolumeFormatStyle:
            if (sharedNumberFormatterPriceStyle) {
                formatter = sharedNumberFormatterPriceStyle;
                
            } else {
                @synchronized(kSharedNumberFormatterPriceStyleLock) {
                    if (sharedNumberFormatterPriceStyle == nil) {
                        sharedNumberFormatterPriceStyle = createPriceFormat();
                    }
                }
                formatter = sharedNumberFormatterPriceStyle;
            }
            double number = [self doubleValue];
            if (number >= 100000 ) {
                double mktcapM = number/1000000;
                NSString *tempWithFormat = [formatter stringFromNumber:[NSNumber numberWithDouble:mktcapM]];
                return [NSString stringWithFormat:@"%@ M",tempWithFormat];
    
            }
            return  [formatter stringFromNumber:self];
            break;
        default:
            break;
    }
    return [formatter stringFromNumber:self];
}
+ (NSNumber *)numberWithString:(NSString *)string
                   numberStyle:(enum INNumberFormatterStyle)style;
{
    NSNumberFormatter *formatter = nil;
    switch (style) {
        case priceFormatStyle:
            if (sharedNumberFormatterPriceStyle) {
                formatter = sharedNumberFormatterPriceStyle;
                break;
            }
            @synchronized(kSharedNumberFormatterPriceStyleLock) {
                if (sharedNumberFormatterPriceStyle == nil) {
                    sharedNumberFormatterPriceStyle = createPriceFormat();
                }
            }
            formatter = sharedNumberFormatterPriceStyle;
            break;
        case volumeFormatStyle:
            if (sharedNumberFormatterVolumeStyle) {
                formatter = sharedNumberFormatterVolumeStyle;
                break;
            }
            @synchronized(kSharedNumberFormatterVolumeStyleLock) {
                if (sharedNumberFormatterVolumeStyle == nil) {
                    sharedNumberFormatterVolumeStyle = [[NSNumberFormatter alloc] init];
                    sharedNumberFormatterVolumeStyle.numberStyle = NSNumberFormatterDecimalStyle;
                }
            }
            formatter = sharedNumberFormatterVolumeStyle;
            break;
        case priceWithoutDivide:// special case that need divide before formating
            if (sharedNumberFormatterPriceStyle) {
                formatter = sharedNumberFormatterPriceStyle;
                double number = [string doubleValue] /100;
                //                double number = [[formatter numberFromString:string] doubleValue] /100;
                return [NSNumber numberWithDouble:number];
            }
            @synchronized(kSharedNumberFormatterPriceStyleLock) {
                if (sharedNumberFormatterPriceStyle == nil) {
                    sharedNumberFormatterPriceStyle = createPriceFormat();
                }
            }
            formatter = sharedNumberFormatterPriceStyle;
            double number = [[formatter numberFromString:string] doubleValue] /100;
            return [NSNumber numberWithDouble:number];
            
            break;
            
        default:
            break;
    }
    return [formatter numberFromString:string];
}

@end
